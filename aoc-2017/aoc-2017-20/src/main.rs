use aoc::{Aoc, Solution};
use hashbrown::{HashMap, HashSet};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Particle {
    p: (isize, isize, isize),
    v: (isize, isize, isize),
    a: (isize, isize, isize),
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 20;
    const TITLE: &'static str = "Particle Swarm";

    type Data = Vec<Particle>;

    fn part1(particles: Self::Data) -> usize {
        particles
            .into_iter()
            .map(
                |Particle {
                     v: (vx, vy, vz),
                     a: (ax, ay, az),
                     ..
                 }| {
                    (
                        vx.abs() + vy.abs() + vz.abs(),
                        ax.abs() + ay.abs() + az.abs(),
                    )
                },
            )
            .enumerate()
            .min_by(|(_, (v1, a1)), (_, (v2, a2))| if a1 == a2 { v1.cmp(v2) } else { a1.cmp(a2) })
            .unwrap()
            .0
    }

    fn part2(particles: Self::Data) -> usize {
        let mut ps = HashSet::new();
        for p in particles {
            if !ps.insert(p.clone()) {
                ps.remove(&p);
            }
        }
        let mut nc_ticks = 0;
        loop {
            let len = ps.len();
            let mut psn = HashSet::new();
            let mut map: HashMap<(isize, isize, isize), usize> = HashMap::new();
            for mut p in ps {
                p.v.0 += p.a.0;
                p.v.1 += p.a.1;
                p.v.2 += p.a.2;
                p.p.0 += p.v.0;
                p.p.1 += p.v.1;
                p.p.2 += p.v.2;
                *map.entry(p.p).or_default() += 1;
                psn.insert(p);
            }
            map.into_iter()
                .filter(|&(_, v)| v > 1usize)
                .flat_map(|(pos, _)| psn.iter().filter(move |p| p.p == pos))
                .cloned()
                .collect::<Vec<Particle>>()
                .into_iter()
                .for_each(|p| {
                    psn.remove(&p);
                });
            nc_ticks = if len == psn.len() { nc_ticks + 1 } else { 0 };
            ps = psn;
            if nc_ticks > 100 {
                break;
            }
        }
        ps.len()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::particle_list(s)
    }
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{char, newline, space0},
        isize,
        multi::separated_list1,
        sequence::{delimited, preceded, tuple},
        IResult,
    };

    use crate::Particle;

    pub fn particle_list(s: &str) -> IResult<&str, Vec<Particle>> {
        separated_list1(newline, particle)(s)
    }

    fn particle(s: &str) -> IResult<&str, Particle> {
        let (s, (p, v, a)) = tuple((
            preceded(tag("p="), nums),
            preceded(tag(", v="), nums),
            preceded(tag(", a="), nums),
        ))(s)?;
        Ok((s, Particle { p, v, a }))
    }

    fn nums(s: &str) -> IResult<&str, (isize, isize, isize)> {
        tuple((
            preceded(tuple((char('<'), space0)), isize),
            preceded(tuple((char(','), space0)), isize),
            delimited(tuple((char(','), space0)), isize, char('>')),
        ))(s)
    }
}
