use adjacent::adjacent;
use aoc::{Aoc, Solution};
use hashbrown::HashSet;
use knot_hash::knot_hash;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 14;
    const TITLE: &'static str = "Disk Defragmentation";

    type Data = Vec<Vec<bool>>;

    fn part1(disk: Self::Data) -> usize {
        disk.into_iter().flatten().filter(|&x| x).count()
    }

    fn part2(mut disk: Self::Data) -> usize {
        let mut groups = 0;
        loop {
            let g = group(&disk);
            if g.is_empty() {
                return groups;
            }
            groups += 1;
            g.into_iter().for_each(|(x, y)| disk[y][x] = false);
        }
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::alpha1, combinator::map};

        map(alpha1, |key| {
            (0..128)
                .map(|i| format!("{}-{}", key, i))
                .map(|s| knot_hash(&s))
                .map(|h| {
                    (0..128)
                        .scan(h, |h, _| {
                            let lsb = *h & 1 != 0;
                            *h >>= 1;
                            Some(lsb)
                        })
                        .collect()
                })
                .collect()
        })(s)
    }
}

fn group(disk: &[Vec<bool>]) -> HashSet<(usize, usize)> {
    for x in 0..128 {
        for y in 0..128 {
            if disk[y][x] {
                return group_(x, y, disk, HashSet::new());
            }
        }
    }
    HashSet::new()
}

fn group_(
    x: usize,
    y: usize,
    disk: &[Vec<bool>],
    mut group: HashSet<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    group.insert((x, y));
    adjacent(x, y, 127, 127)
        .filter(|&(x, y)| disk[y][x])
        .fold(group, |mut group, (x, y)| {
            if !group.contains(&(x, y)) {
                group = group_(x, y, disk, group);
            }
            group
        })
}
