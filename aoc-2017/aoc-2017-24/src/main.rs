use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 24;
    const TITLE: &'static str = "Electromagnetic Moat";

    type Data = Vec<(usize, usize)>;

    fn part1(components: Self::Data) -> usize {
        strongest(0, components)
    }

    fn part2(components: Self::Data) -> usize {
        longest(0, components).1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.lines()
                .map(|l| {
                    let vs = l
                        .split('/')
                        .map(str::parse)
                        .map(Result::unwrap)
                        .collect::<Vec<usize>>();
                    (vs[0], vs[1])
                })
                .collect(),
        ))
    }
}

fn strongest(port: usize, components: Vec<(usize, usize)>) -> usize {
    components
        .iter()
        .filter(|(a, b)| *a == port || *b == port)
        .map(|(a, b)| {
            let p = if *a == port { *b } else { *a };
            let c = components
                .iter()
                .filter(|c| **c != (*a, *b))
                .copied()
                .collect::<Vec<_>>();
            *a + *b + strongest(p, c)
        })
        .max()
        .unwrap_or_default()
}

fn longest(port: usize, components: Vec<(usize, usize)>) -> (usize, usize) {
    components
        .iter()
        .filter(|(a, b)| *a == port || *b == port)
        .map(|(a, b)| {
            let p = if *a == port { *b } else { *a };
            let c = components
                .iter()
                .filter(|c| **c != (*a, *b))
                .copied()
                .collect::<Vec<_>>();
            let (l, s) = longest(p, c);
            (l + 1, s + *a + *b)
        })
        .max_by(
            |(l1, s1), (l2, s2)| {
                if l1 == l2 {
                    s1.cmp(s2)
                } else {
                    l1.cmp(l2)
                }
            },
        )
        .unwrap_or_default()
}
