use aoc::{Aoc, Solution};
use std::str::FromStr;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Copy)]
enum Dir {
    U,
    D,
    L,
    R,
}

#[derive(Debug, Clone)]
pub struct Path {
    grid: Vec<Vec<char>>,
    pos: (usize, usize),
    dir: Dir,
}

impl Solution<String, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 19;
    const TITLE: &'static str = "A Series of Tubes";

    type Data = Path;

    fn part1(path: Self::Data) -> String {
        path.filter(|&c| !matches!(c, '|' | '-' | '+')).collect()
    }

    fn part2(path: Self::Data) -> usize {
        path.count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}

impl FromStr for Path {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let grid: Vec<Vec<char>> = s.lines().map(|l| l.chars().collect()).collect();
        let x = grid[0]
            .iter()
            .enumerate()
            .find(|(_, c)| **c != ' ')
            .ok_or("could not find starting point")?
            .0;
        Ok(Path {
            grid,
            dir: Dir::D,
            pos: (x, 0),
        })
    }
}

impl Iterator for Path {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        let c = self.grid[self.pos.1][self.pos.0];
        if c == ' ' {
            return None;
        }
        if c == '+' {
            self.dir = match self.dir {
                Dir::D | Dir::U if self.grid[self.pos.1][self.pos.0 + 1] == ' ' => Dir::L,
                Dir::D | Dir::U => Dir::R,
                Dir::L | Dir::R if self.grid[self.pos.1 + 1][self.pos.0] == ' ' => Dir::U,
                Dir::L | Dir::R => Dir::D,
            }
        }
        match self.dir {
            Dir::U => self.pos.1 -= 1,
            Dir::D => self.pos.1 += 1,
            Dir::L => self.pos.0 -= 1,
            Dir::R => self.pos.0 += 1,
        }
        Some(c)
    }
}
