use aoc::{Aoc, Solution};
use assembly::Computer;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Coprocessor Conflagration";

    type Data = Computer;

    fn part1(mut cp: Self::Data) -> usize {
        cp.debug()
    }

    fn part2(_: Self::Data) -> usize {
        (105700..=122700)
            .step_by(17)
            .filter(|&b| (2..(b as f64).sqrt().floor() as i32).any(|x| b % x == 0))
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}

// # assembler:
//
// set b 57
// set c b
// jnz a 2
// jnz 1 5
// mul b 100
// sub b -100000
// set c b
// sub c -17000
// set f 1
// set d 2
// set e 2
// set g d
// mul g e
// sub g b
// jnz g 2
// set f 0
// sub e -1
// set g e
// sub g b
// jnz g -8
// sub d -1
// set g d
// sub g b
// jnz g -13
// jnz f 2
// sub h -1
// set g b
// sub g c
// jnz g 2
// jnz 1 3
// sub b -17
// jnz 1 -23
//
// # translated to rust:
//
// fn program() -> isize {
//     let mut a = 1;
//     let mut b = 0;
//     let mut c = 0;
//     let mut d = 0;
//     let mut e = 0;
//     let mut f = 0;
//     let mut g = 0;
//     let mut h = 0;
//     b = 57;
//     c = b;
//     b *= 100;
//     b += 100000;
//     c = b;
//     c += 17000;
//     loop {
//         f = 1;
//         d = 2;
//         loop {
//             e = 2;
//             loop {
//                 g = d;
//                 g *= e;
//                 g -= b;
//                 if g == 0 {
//                     f = 0;
//                 }
//                 e += 1;
//                 g = e;
//                 g -= b;
//                 if g == 0 {
//                     break;
//                 }
//             }
//             d += 1;
//             g = d;
//             g -= b;
//             if g == 0 {
//                 break;
//             }
//         }
//         if f == 0 {
//             h += 1;
//         }
//         g = b;
//         g -= c;
//         if g == 0 {
//             break;
//         }
//         b += 17;
//     }
//     h
// }
//
// # simplified:
//
// fn program() -> usize {
//     (105700..=122700).step_by(17).filter(|&b| (2..b).any(|x| b % x == 0)).count()
// }
//
// turns out it's just counting nonprime numbers
