use aoc::{Aoc, Solution};
use apply::Also as _;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 4;
    const TITLE: &'static str = "High-Entropy Passphrases";

    type Data = Vec<Vec<String>>;

    fn part1(input: Self::Data) -> usize {
        input.into_iter().filter(|ws| no_duplicates(ws)).count()
    }

    fn part2(input: Self::Data) -> usize {
        input.into_iter().filter(|ws| no_anagrams(ws)).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{alpha1, newline, space1},
            combinator::map,
            multi::separated_list1,
        };

        let word = map(alpha1, String::from);
        let phrase = separated_list1(space1, word);
        separated_list1(newline, phrase)(s)
    }
}

fn no_duplicates(ws: &[String]) -> bool {
    ws.iter().unique().count() == ws.len()
}

fn no_anagrams(ws: &[String]) -> bool {
    ws.iter()
        .map(|w| w.chars().collect::<Vec<_>>().also(|w| w.sort_unstable()))
        .unique()
        .count()
        == ws.len()
}
