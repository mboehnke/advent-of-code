use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 17;
    const TITLE: &'static str = "Spinlock";

    type Data = usize;

    fn part1(steps: Self::Data) -> usize {
        let (buf, i) =
            insertions(steps, 2017).fold((vec![0], 0), |(mut buf, _), (index, element)| {
                buf.insert(index, element);
                (buf, index)
            });
        buf[(i + 1) % buf.len()]
    }

    fn part2(steps: Self::Data) -> usize {
        insertions(steps, 50_000_000)
            .fold((0, 0), |(xpos, xval), (pos, val)| match pos {
                p if p <= xpos => (xpos + 1, xval),
                p if p == xpos + 1 => (xpos, val),
                _ => (xpos, xval),
            })
            .1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize(s)
    }
}

fn insertions(steps: usize, n: usize) -> impl Iterator<Item = (usize, usize)> {
    (1..=n).scan(0, move |pos, num| {
        *pos = (*pos + steps) % num + 1;
        Some((*pos, num))
    })
}
