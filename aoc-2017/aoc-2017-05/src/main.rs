use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 5;
    const TITLE: &'static str = "A Maze of Twisty Trampolines, All Alike";

    type Data = Vec<isize>;

    fn part1(offsets: Self::Data) -> usize {
        jumps(offsets, false)
    }

    fn part2(offsets: Self::Data) -> usize {
        jumps(offsets, true)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::isize_list(s)
    }
}

fn jumps(mut offsets: Vec<isize>, p2: bool) -> usize {
    let (mut pos, mut count) = (0, 0);
    while pos >= 0 && pos < offsets.len() as isize {
        let j = offsets[pos as usize];
        offsets[pos as usize] = j + if p2 && j >= 3 { -1 } else { 1 };
        pos += j;
        count += 1;
    }
    count
}
