use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Copy)]
pub struct Generator(usize, usize);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Dueling Generators";

    type Data = (usize, usize);

    fn part1((a, b): Self::Data) -> usize {
        let ga = Generator(a, 16807);
        let gb = Generator(b, 48271);
        judge(ga.zip(gb), 40_000_000)
    }

    fn part2((a, b): Self::Data) -> usize {
        let ga = Generator(a, 16807).filter(|&x| x % 4 == 0);
        let gb = Generator(b, 48271).filter(|&x| x % 8 == 0);
        judge(ga.zip(gb), 5_000_000)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::newline, preceded_usize, sequence::separated_pair};

        separated_pair(preceded_usize, newline, preceded_usize)(s)
    }
}

fn judge<T: Iterator<Item = (usize, usize)>>(i: T, n: usize) -> usize {
    i.take(n).filter(|&(a, b)| a & 0xFFFF == b & 0xFFFF).count()
}

impl Iterator for Generator {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        self.0 = (self.0 * self.1) % 2147483647;
        Some(self.0)
    }
}
