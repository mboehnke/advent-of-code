use aoc::{Aoc, Solution};
use apply::Apply as _;
use itertools::Itertools as _;
use std::{iter::once, usize};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Grid {
    r2: Vec<Vec<bool>>,
    r3: Vec<Vec<bool>>,
    grid: Vec<Vec<bool>>,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 21;
    const TITLE: &'static str = "Fractal Art";

    type Data = (Grid, usize);

    fn part1((mut grid, iterations): Self::Data) -> usize {
        grid.nth(iterations - 1).unwrap()
    }

    fn part2((mut grid, _): Self::Data) -> usize {
        grid.nth(17).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut i = s.splitn(2, '\n');
        let n = i.next().unwrap().parse().unwrap();
        let s = i.next().unwrap();
        let mut r2: Vec<Vec<bool>> = vec![Vec::new(); 16];
        let mut r3: Vec<Vec<bool>> = vec![Vec::new(); 512];
        for line in s.lines() {
            let squares = line
                .split(" => ")
                .map(|s| s.chars().filter(|&c| c != '/').map(|c| c == '#').collect())
                .collect::<Vec<Vec<bool>>>();
            for i in indeces(&squares[0]) {
                if squares[1].len() == 9 {
                    r2[i] = squares[1].clone();
                } else {
                    r3[i] = squares[1].clone();
                }
            }
        }
        let grid = vec![
            vec![false, true, false],
            vec![false, false, true],
            vec![true, true, true],
        ];
        Ok(("", (Grid { r2, r3, grid }, n)))
    }
}

fn indeces(x: &[bool]) -> Vec<usize> {
    (0..3)
        .scan(x.to_vec(), |x, _| {
            *x = rotate(x);
            Some(x.to_vec())
        })
        .chain(once(x.to_vec()))
        .flat_map(|x| once(flip(&x)).chain(once(x)))
        .map(|v| v.into_iter().fold(0, |acc, d| (acc << 1) | d as usize))
        .unique()
        .collect()
}

fn rotate(x: &[bool]) -> Vec<bool> {
    if x.len() == 4 {
        [2, 0, 3, 1].iter()
    } else {
        [6, 3, 0, 7, 4, 1, 8, 5, 2].iter()
    }
    .map(|&i| x[i])
    .collect()
}

fn flip(x: &[bool]) -> Vec<bool> {
    if x.len() == 4 {
        [2, 3, 0, 1].iter()
    } else {
        [6, 7, 8, 3, 4, 5, 0, 1, 2].iter()
    }
    .map(|&i| x[i])
    .collect()
}

impl Iterator for Grid {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let s = if self.grid.len() % 2 == 0 { 2 } else { 3 };
        let len = self.grid.len() / s * (s + 1);
        let mut grid_n = vec![vec![false; len]; len];
        (0..)
            .step_by(s)
            .take_while(|&x| x < self.grid.len())
            .collect_vec()
            .apply(|v| {
                v.clone()
                    .into_iter()
                    .flat_map(move |x| v.clone().into_iter().map(move |y| (x, y)))
            })
            .flat_map(|(x, y)| {
                let (xn, yn) = (x / s * (s + 1), y / s * (s + 1));
                self.grid
                    .iter()
                    .skip(y)
                    .take(s)
                    .flat_map(|l| l.iter().skip(x).take(s))
                    .fold(0, |acc, &d| (acc << 1) | d as usize)
                    .apply(|i| if s == 2 { &self.r2[i] } else { &self.r3[i] })
                    .chunks(s + 1)
                    .enumerate()
                    .flat_map(|(y, l)| l.iter().enumerate().map(move |(x, b)| (x, y, b)))
                    .filter(|&(_, _, &b)| b)
                    .map(move |(xo, yo, _)| (xo + xn, yo + yn))
            })
            .for_each(|(x, y)| grid_n[y][x] = true);
        self.grid = grid_n;
        Some(self.grid.iter().flatten().filter(|&&x| x).count())
    }
}
