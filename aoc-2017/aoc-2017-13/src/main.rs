use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Scanner(usize, usize);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Packet Scanners";

    type Data = Vec<Scanner>;

    fn part1(scanners: Self::Data) -> usize {
        (0..=*scanners.iter().map(|Scanner(l, _)| l).max().unwrap())
            .filter_map(|t| scanners.iter().find(|s| s.0 == t).map(|s| (s, t)))
            .filter(|(s, t)| s.is_on_top(*t))
            .map(|(Scanner(l, d), _)| l * d)
            .sum()
    }

    fn part2(scanners: Self::Data) -> usize {
        let max = *scanners.iter().map(|Scanner(l, _)| l).max().unwrap();
        (0..)
            .find(|&t| {
                !(0..=max).any(|t1| {
                    scanners
                        .iter()
                        .find(|s| s.0 == t1)
                        .map(|s| s.is_on_top(t1 + t))
                        .unwrap_or(false)
                })
            })
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag, character::complete::newline, combinator::map,
            multi::separated_list1, sequence::separated_pair, usize,
        };

        let scanner = map(separated_pair(usize, tag(": "), usize), |(a, b)| {
            Scanner(a, b)
        });
        separated_list1(newline, scanner)(s)
    }
}

impl Scanner {
    fn is_on_top(&self, t: usize) -> bool {
        t % (self.1 * 2 - 2) == 0
    }
}
