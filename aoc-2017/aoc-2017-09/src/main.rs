use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

pub struct NoCancelled<'a>(std::str::Chars<'a>);
pub struct NoGarbage<'a>(NoCancelled<'a>, usize);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 9;
    const TITLE: &'static str = "Stream Processing";

    type Data = String;

    fn part1(input: Self::Data) -> usize {
        input
            .chars()
            .apply(NoCancelled)
            .apply(|i| NoGarbage(i, 0))
            .score()
    }

    fn part2(input: Self::Data) -> usize {
        input
            .chars()
            .apply(NoCancelled)
            .apply(|i| NoGarbage(i, 0))
            .garbage()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.trim().to_string()))
    }
}

impl<'a> Iterator for NoCancelled<'a> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        let mut n = self.0.next();
        while n == Some('!') {
            n = self.0.nth(1);
        }
        n
    }
}

impl<'a> Iterator for NoGarbage<'a> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        let n = self.0.next();
        if n == Some('<') {
            for c in &mut self.0 {
                if c == '>' {
                    return self.0.next();
                }
                self.1 += 1
            }
        }
        n
    }
}

impl<'a> NoGarbage<'a> {
    fn garbage(mut self) -> usize {
        for _ in &mut self {}
        self.1
    }

    fn score(self) -> usize {
        self.scan(0, |l, c| {
            match c {
                '{' => *l += 1,
                '}' => *l -= 1,
                _ => {}
            };
            Some(if c == '{' { *l } else { 0 })
        })
        .sum()
    }
}
