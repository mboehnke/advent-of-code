use aoc::{Aoc, Solution};
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Move {
    Spin(usize),
    Exchange(usize, usize),
    Partner(u8, u8),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Dance {
    p: Vec<u8>,
    i: usize,
    l: usize,
    m: Vec<Move>,
}

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Permutation Promenade";

    type Data = (String, Vec<Move>);

    fn part1((p, moves): Self::Data) -> String {
        Dance::from(&p, moves).next().unwrap().to_string()
    }

    fn part2((p, moves): Self::Data) -> String {
        let cycle = {
            let mut s = HashSet::new();
            Dance::from(&p, moves.clone())
                .enumerate()
                .find(|(_, d)| !s.insert(d.to_vec()))
                .unwrap()
                .0
        };
        let i = (1_000_000_000 % cycle) - 1;
        Dance::from(&p, moves).nth(i).unwrap().to_string()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{alpha1, anychar, char, newline},
            combinator::map,
            multi::separated_list1,
            sequence::{preceded, separated_pair},
            usize,
        };

        let programs = map(alpha1, String::from);
        let move_spin = preceded(char('s'), map(usize, Move::Spin));
        let move_exchange = preceded(
            char('x'),
            map(separated_pair(usize, char('/'), usize), |(a, b)| {
                Move::Exchange(a, b)
            }),
        );
        let move_partner = preceded(
            char('p'),
            map(separated_pair(anychar, char('/'), anychar), |(a, b)| {
                Move::Partner(a as u8, b as u8)
            }),
        );
        let move_p = alt((move_spin, move_exchange, move_partner));
        let move_list = separated_list1(char(','), move_p);
        separated_pair(programs, newline, move_list)(s)
    }
}

impl Dance {
    fn from(s: &str, m: Vec<Move>) -> Self {
        let p = s.chars().map(|c| c as u8).collect::<Vec<u8>>();
        let l = p.len();
        Self { p, i: 0, l, m }
    }

    fn to_vec(&self) -> Vec<u8> {
        let (b, e) = self.p.split_at(self.i);
        e.iter().chain(b.iter()).copied().collect()
    }
}

impl From<&str> for Move {
    fn from(s: &str) -> Self {
        let (m, d) = s.split_at(1);
        match m {
            "s" => Move::Spin(d.parse().unwrap()),
            "x" => {
                let mut i = d.split('/').map(str::parse).map(Result::unwrap);
                Move::Exchange(i.next().unwrap(), i.next().unwrap())
            }
            "p" => {
                let mut i = d.split('/').map(|s| s.chars().next().unwrap() as u8);
                Move::Partner(i.next().unwrap(), i.next().unwrap())
            }
            _ => panic!(),
        }
    }
}

impl ToString for Dance {
    fn to_string(&self) -> String {
        let (b, e) = self.p.split_at(self.i);
        e.iter().chain(b.iter()).map(|b| *b as char).collect()
    }
}

impl Iterator for Dance {
    type Item = Dance;

    fn next(&mut self) -> Option<Self::Item> {
        for m in &self.m {
            match *m {
                Move::Spin(a) => self.i = (self.l - a + self.i) % self.l,
                Move::Exchange(a, b) => self.p.swap((a + self.i) % self.l, (b + self.i) % self.l),
                Move::Partner(a, b) => {
                    let ia = self.p.iter().enumerate().find(|(_, x)| **x == a).unwrap().0;
                    let ib = self.p.iter().enumerate().find(|(_, x)| **x == b).unwrap().0;
                    self.p.swap(ia, ib)
                }
            }
        }
        Some(self.clone())
    }
}
