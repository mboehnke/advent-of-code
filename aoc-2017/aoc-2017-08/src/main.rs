use aoc::{Aoc, Solution};
use std::collections::HashMap;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Condition {
    L,
    G,
    LE,
    GE,
    NE,
    E,
}

#[derive(Debug, Clone)]
pub struct Instruction {
    register: String,
    val: isize,
    condition: Condition,
    c_register: String,
    c_val: isize,
}

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 8;
    const TITLE: &'static str = "I Heard You Like Registers";

    type Data = Vec<Instruction>;

    fn part1(instructions: Self::Data) -> isize {
        *instructions
            .into_iter()
            .fold(
                HashMap::new(),
                |mut registers: HashMap<String, isize>, i| {
                    let cr = registers.get(&i.c_register).copied().unwrap_or_default();
                    if i.condition.eval(cr, i.c_val) {
                        *registers.entry(i.register).or_default() += i.val;
                    };
                    registers
                },
            )
            .values()
            .max()
            .unwrap()
    }

    fn part2(instructions: Self::Data) -> isize {
        instructions
            .into_iter()
            .scan(
                HashMap::new(),
                |registers: &mut HashMap<String, isize>, i| {
                    let cr = registers.get(&i.c_register).copied().unwrap_or_default();
                    if i.condition.eval(cr, i.c_val) {
                        *registers.entry(i.register.clone()).or_default() += i.val;
                    };
                    registers.get(&i.register).copied().or(Some(0))
                },
            )
            .max()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::instruction_list(s)
    }
}

impl Condition {
    fn eval(&self, a: isize, b: isize) -> bool {
        match self {
            Condition::E => a == b,
            Condition::NE => a != b,
            Condition::L => a < b,
            Condition::G => a > b,
            Condition::LE => a <= b,
            Condition::GE => a >= b,
        }
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, char, i64, newline, space1},
        combinator::{map, value},
        multi::separated_list0,
        sequence::preceded,
        IResult,
    };

    use crate::{Condition, Instruction};

    pub fn instruction_list(s: &str) -> IResult<&str, Vec<Instruction>> {
        separated_list0(newline, instruction)(s)
    }

    fn instruction(s: &str) -> IResult<&str, Instruction> {
        let (s, register) = map(alpha1, String::from)(s)?;
        let (s, mul) = preceded(space1, mul)(s)?;
        let (s, val) = preceded(space1, i64)(s)?;
        let (s, c_register) = map(preceded(tag(" if "), alpha1), String::from)(s)?;
        let (s, condition) = preceded(space1, condition)(s)?;
        let (s, c_val) = preceded(space1, i64)(s)?;
        let instruction = Instruction {
            register,
            val: (val * mul) as isize,
            condition,
            c_register,
            c_val: c_val as isize,
        };
        Ok((s, instruction))
    }

    fn mul(s: &str) -> IResult<&str, i64> {
        let inc = value(1, tag("inc"));
        let dec = value(-1, tag("dec"));
        alt((inc, dec))(s)
    }

    fn condition(s: &str) -> IResult<&str, Condition> {
        let e = value(Condition::E, tag("=="));
        let ne = value(Condition::NE, tag("!="));
        let l = value(Condition::L, char('<'));
        let g = value(Condition::G, char('>'));
        let le = value(Condition::LE, tag("<="));
        let ge = value(Condition::GE, tag(">="));
        alt((e, ne, le, ge, l, g))(s)
    }
}
