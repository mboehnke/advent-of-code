use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashSet;
use std::str::FromStr;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct TuringMachine {
    tape: HashSet<isize>,
    cursor: isize,
    state: usize,
    states: Vec<State>,
    steps: usize,
}

#[derive(Debug, Clone)]
pub struct State {
    val: (bool, bool),
    dir: (isize, isize),
    next: (usize, usize),
}

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 25;
    const TITLE: &'static str = "The Halting Problem";

    type Data = TuringMachine;

    fn part1(mut tm: Self::Data) -> usize {
        for _ in 0..tm.steps {
            let s = &tm.states[tm.state];
            if tm.tape.contains(&tm.cursor) {
                if !s.val.1 {
                    tm.tape.remove(&tm.cursor);
                }
                tm.cursor += s.dir.1;
                tm.state = s.next.1;
            } else {
                if s.val.0 {
                    tm.tape.insert(tm.cursor);
                }
                tm.cursor += s.dir.0;
                tm.state = s.next.0;
            };
        }
        tm.tape.len()
    }

    fn part2(_: Self::Data) -> String {
        String::new()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut i = s.split("\n\n");
        let mut j = i.next().unwrap().lines();
        let state = {
            let sn = j.next().unwrap();
            let len = sn.chars().count();
            let c = sn.chars().nth(len - 2).unwrap() as u8 - b'A';
            c as usize
        };
        let steps = j
            .next()
            .unwrap()
            .chars()
            .filter(|c| c.is_ascii_digit())
            .fold(0, |acc, c| acc * 10 + (c as u8 - b'0') as usize);
        let states = i.map(str::parse).map(Result::unwrap).collect();
        Ok((
            "",
            TuringMachine {
                tape: HashSet::new(),
                cursor: 0,
                state,
                states,
                steps,
            },
        ))
    }
}

impl FromStr for State {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let state = || {
            let mut i = s.lines().skip(2);
            let v1 = i.next()?.chars().find(|c| c.is_ascii_digit())? == '1';
            let d1 = if i.next()?.contains("right") { 1 } else { -1 };
            let n1 = {
                let sn = i.next()?;
                let len = sn.chars().count();
                let c = sn.chars().nth(len - 2)? as u8 - b'A';
                c as usize
            };
            let v2 = i.nth(1)?.chars().find(|c| c.is_ascii_digit())? == '1';
            let d2 = if i.next()?.contains("right") { 1 } else { -1 };
            let n2 = {
                let sn = i.next()?;
                let len = sn.chars().count();
                let c = sn.chars().nth(len - 2)? as u8 - b'A';
                c as usize
            };
            Some(State {
                val: (v1, v2),
                dir: (d1, d2),
                next: (n1, n2),
            })
        };
        state().ok_or("parse error")?.apply(Ok)
    }
}
