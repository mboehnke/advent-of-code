use aoc::{Aoc, Solution};
use hashbrown::{HashMap, HashSet};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Infection {
    nodes: HashSet<(isize, isize)>,
    pos: (isize, isize),
    dir: Dir,
}

#[derive(Debug, Clone)]
pub struct EvolvedInfection {
    nodes: HashMap<(isize, isize), State>,
    pos: (isize, isize),
    dir: Dir,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum State {
    Clean,
    Weakened,
    Infected,
    Flagged,
}

#[derive(Debug, Clone, Copy)]
pub enum Dir {
    U,
    D,
    L,
    R,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Sporifica Virus";

    type Data = (Infection, usize);

    fn part1((i, b): Self::Data) -> usize {
        i.take(b).filter(|&x| x).count()
    }

    fn part2((i, _): Self::Data) -> usize {
        const B: usize = 10000000;
        i.evolve().take(B).filter(|&x| x).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut i = s.splitn(2, '\n');
        let n = i.next().unwrap().parse().unwrap();
        let s = i.next().unwrap();
        let height = s.lines().count();
        let width = s.lines().next().unwrap().chars().count();
        let pos = ((width / 2) as isize, (height / 2) as isize);
        let nodes = s
            .lines()
            .enumerate()
            .flat_map(|(y, l)| l.chars().enumerate().map(move |(x, c)| (x, y, c)))
            .filter(|&(_, _, c)| c == '#')
            .map(|(x, y, _)| (x as isize, y as isize))
            .collect();
        Ok((
            "",
            (
                Infection {
                    nodes,
                    pos,
                    dir: Dir::U,
                },
                n,
            ),
        ))
    }
}

impl Iterator for Infection {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        let infected = self.nodes.insert(self.pos);
        if infected {
            self.dir = self.dir.turn_left();
        } else {
            self.dir = self.dir.turn_right();
            self.nodes.remove(&self.pos);
        }
        self.pos = self.dir.move_forward(self.pos);
        Some(infected)
    }
}

impl Iterator for EvolvedInfection {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        let node = self.nodes.get(&self.pos).copied().unwrap_or(State::Clean);
        self.dir = match node {
            State::Clean => self.dir.turn_left(),
            State::Weakened => self.dir,
            State::Infected => self.dir.turn_right(),
            State::Flagged => self.dir.turn_around(),
        };
        let node_n = match node {
            State::Clean => State::Weakened,
            State::Weakened => State::Infected,
            State::Infected => State::Flagged,
            State::Flagged => State::Clean,
        };
        self.nodes.insert(self.pos, node_n);
        self.pos = self.dir.move_forward(self.pos);
        Some(node_n == State::Infected)
    }
}

impl Dir {
    fn turn_left(&self) -> Self {
        match self {
            Dir::U => Dir::L,
            Dir::D => Dir::R,
            Dir::L => Dir::D,
            Dir::R => Dir::U,
        }
    }
    fn turn_right(&self) -> Self {
        match self {
            Dir::U => Dir::R,
            Dir::D => Dir::L,
            Dir::L => Dir::U,
            Dir::R => Dir::D,
        }
    }
    fn turn_around(&self) -> Self {
        match self {
            Dir::U => Dir::D,
            Dir::D => Dir::U,
            Dir::L => Dir::R,
            Dir::R => Dir::L,
        }
    }
    fn move_forward(&self, (x, y): (isize, isize)) -> (isize, isize) {
        match self {
            Dir::U => (x, y - 1),
            Dir::D => (x, y + 1),
            Dir::L => (x - 1, y),
            Dir::R => (x + 1, y),
        }
    }
}

impl Infection {
    fn evolve(self) -> EvolvedInfection {
        let nodes = self
            .nodes
            .into_iter()
            .map(|p| (p, State::Infected))
            .collect();
        EvolvedInfection {
            nodes,
            pos: self.pos,
            dir: self.dir,
        }
    }
}
