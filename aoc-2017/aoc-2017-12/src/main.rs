use aoc::{Aoc, Solution};
use apply::Also;
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Program {
    id: usize,
    ids: Vec<usize>,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 12;
    const TITLE: &'static str = "Digital Plumber";

    type Data = Vec<Program>;

    fn part1(programs: Self::Data) -> usize {
        group(0, &programs).len()
    }

    fn part2(mut programs: Self::Data) -> usize {
        let mut num = 0;
        while let Some(p) = programs.get(0) {
            num += 1;
            let grp = group(p.id, &programs);
            programs.retain(|p| !grp.contains(&p.id));
        }
        num
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag, character::complete::newline, combinator::map,
            multi::separated_list1, sequence::separated_pair, usize,
        };

        let id_list = separated_list1(tag(", "), usize);
        let program = map(separated_pair(usize, tag(" <-> "), id_list), |(id, ids)| {
            Program { id, ids }
        });
        separated_list1(newline, program)(s)
    }
}

fn group(id: usize, programs: &[Program]) -> HashSet<usize> {
    group_(id, programs, HashSet::new())
}

fn group_(id: usize, programs: &[Program], ids: HashSet<usize>) -> HashSet<usize> {
    programs
        .iter()
        .find(|p| p.id == id)
        .unwrap()
        .ids
        .iter()
        .fold(
            ids.also(|ids| {
                ids.insert(id);
            }),
            |mut ids, &pid| {
                if !ids.contains(&pid) {
                    ids = group_(pid, programs, ids);
                }
                ids
            },
        )
}
