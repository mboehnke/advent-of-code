use std::collections::HashMap;

use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Program {
    name: String,
    weight: usize,
    children: Vec<String>,
}

impl Solution<String, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 7;
    const TITLE: &'static str = "Recursive Circus";

    type Data = Vec<Program>;

    fn part1(programs: Self::Data) -> String {
        root(&programs)
    }

    fn part2(programs: Self::Data) -> usize {
        let ws = weights(&programs);
        let ps = programs
            .clone()
            .into_iter()
            .map(|p| {
                p.children
                    .into_iter()
                    .map(|c| (c.clone(), *ws.get(&c).unwrap()))
                    .collect_vec()
            })
            .find(|cs| cs.iter().map(|(_, w)| w).unique().collect_vec().len() > 1)
            .unwrap();
        let bw = ps
            .iter()
            .fold(HashMap::new(), |mut map: HashMap<usize, usize>, &(_, w)| {
                *map.entry(w).or_default() += 1;
                map
            })
            .into_iter()
            .find(|&(_, c)| c > 1)
            .unwrap()
            .0;
        let (up, uw) = ps.into_iter().find(|&(_, w)| w != bw).unwrap();
        programs.iter().find(|p| p.name == up).unwrap().weight + bw - uw
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::program_list(s)
    }
}

fn weights(programs: &[Program]) -> HashMap<String, usize> {
    weights_(&root(programs), programs, HashMap::new())
}

fn weights_(
    program: &str,
    programs: &[Program],
    mut ws: HashMap<String, usize>,
) -> HashMap<String, usize> {
    if ws.contains_key(program) {
        return ws;
    }
    let p = programs.iter().find(|p| p.name == program).unwrap();
    let mut w = p.weight;
    for c in &p.children {
        ws = weights_(c, programs, ws);
        w += ws.get(c).unwrap();
    }
    ws.insert(program.to_string(), w);
    ws
}

fn root(programs: &[Program]) -> String {
    let children = programs
        .iter()
        .flat_map(|p| &p.children)
        .unique()
        .cloned()
        .collect_vec();
    programs
        .iter()
        .map(|p| &p.name)
        .find(|&n| !children.contains(n))
        .unwrap()
        .to_string()
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{alpha1, char, newline},
        combinator::{map, opt},
        multi::separated_list0,
        sequence::{delimited, preceded},
        usize, IResult,
    };

    use crate::Program;

    pub fn program_list(s: &str) -> IResult<&str, Vec<Program>> {
        separated_list0(newline, program)(s)
    }

    fn program(s: &str) -> IResult<&str, Program> {
        let (s, name) = map(alpha1, String::from)(s)?;
        let (s, weight) = delimited(tag(" ("), usize, char(')'))(s)?;
        let (s, children) = map(opt(children), Option::unwrap_or_default)(s)?;
        let program = Program {
            name,
            weight,
            children,
        };
        Ok((s, program))
    }

    fn children(s: &str) -> IResult<&str, Vec<String>> {
        let child = map(alpha1, String::from);
        let child_list = separated_list0(tag(", "), child);
        preceded(tag(" -> "), child_list)(s)
    }
}
