use aoc::{Aoc, Solution};
use apply::Apply as _;
use knot_hash::{hash, knot_hash};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Knot Hash";

    type Data = (usize, String);

    fn part1((s, input): Self::Data) -> usize {
        input
            .split(',')
            .map(str::parse)
            .map(Result::unwrap)
            .apply(|l| hash(l, s))
            .into_iter()
            .take(2)
            .product()
    }

    fn part2((_, input): Self::Data) -> String {
        format!("{:032x}", knot_hash(&input))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::take_till, character::complete::newline, combinator::map,
            sequence::separated_pair, usize,
        };

        let numbers = map(take_till(|c| c == '\n'), String::from);
        separated_pair(usize, newline, numbers)(s)
    }
}
