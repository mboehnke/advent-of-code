use aoc::{Aoc, Solution};
use assembly::{Computer, State};

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 18;
    const TITLE: &'static str = "Duet";

    type Data = Computer;

    fn part1(mut tablet: Self::Data) -> isize {
        let mut s = tablet.run();
        let mut output = 0;
        loop {
            match s {
                State::AwaitingInput(x) if x == 0 => s = tablet.run_with_input(&[x]),
                State::Output(x) => {
                    output = x;
                    s = tablet.run()
                }
                _ => break,
            }
        }
        output
    }

    fn part2(tablet: Self::Data) -> usize {
        let mut tablets = vec![tablet.clone(), tablet];
        tablets[1].set_p(1);
        let mut states = [tablets[0].run(), tablets[1].run()];
        let mut nums = [0, 0];
        let mut inputs = [vec![], vec![]];
        loop {
            inputs.iter_mut().for_each(Vec::clear);
            for i in 0..=1 {
                let j = (i + 1) % 2;
                while let State::Output(x) = states[i] {
                    nums[i] += i;
                    inputs[j].push(x);
                    states[i] = tablets[i].run();
                }
            }
            if inputs.iter().all(Vec::is_empty) {
                break;
            }
            (0..=1).for_each(|i| states[i] = tablets[i].run_with_input(&inputs[i]));
        }
        nums[1]
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}
