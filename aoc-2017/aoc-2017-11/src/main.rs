use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Default, Copy)]
pub struct Position(isize, isize);

#[derive(Debug, Clone)]
pub enum Direction {
    N,
    NE,
    NW,
    S,
    SE,
    SW,
}

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Hex Ed";

    type Data = Vec<Direction>;

    fn part1(path: Self::Data) -> isize {
        path.into_iter()
            .fold(Position::default(), move_d)
            .distance()
    }

    fn part2(path: Self::Data) -> isize {
        path.into_iter()
            .scan(Position::default(), |pos: &mut Position, d| {
                *pos = move_d(*pos, d);
                Some(*pos)
            })
            .map(Position::distance)
            .max()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt, bytes::complete::tag, character::complete::char, combinator::value,
            multi::separated_list1,
        };

        let dir_n = value(Direction::N, char('n'));
        let dir_s = value(Direction::S, char('s'));
        let dir_ne = value(Direction::NE, tag("ne"));
        let dir_se = value(Direction::SE, tag("se"));
        let dir_nw = value(Direction::NW, tag("nw"));
        let dir_sw = value(Direction::SW, tag("sw"));
        let dir = alt((dir_ne, dir_se, dir_nw, dir_sw, dir_n, dir_s));
        separated_list1(char(','), dir)(s)
    }
}

fn move_d(Position(x, y): Position, d: Direction) -> Position {
    match d {
        Direction::N => Position(x, y - 1),
        Direction::NE => Position(x + 1, y - 1),
        Direction::NW => Position(x - 1, y),
        Direction::S => Position(x, y + 1),
        Direction::SE => Position(x + 1, y),
        Direction::SW => Position(x - 1, y + 1),
    }
}

impl Position {
    fn distance(self) -> isize {
        (self.0.abs() + self.1.abs() + (self.0 + self.1).abs()) / 2
    }
}

impl From<&str> for Direction {
    fn from(s: &str) -> Self {
        match s {
            "n" => Direction::N,
            "ne" => Direction::NE,
            "nw" => Direction::NW,
            "s" => Direction::S,
            "se" => Direction::SE,
            "sw" => Direction::SW,
            _ => panic!(),
        }
    }
}
