use aoc::{Aoc, Solution};
use std::collections::HashMap;
use std::iter::repeat;

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone)]
enum Dir {
    L,
    R,
    U,
    D,
}

impl Solution<isize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Spiral Memory";

    type Data = usize;

    fn part1(input: Self::Data) -> isize {
        coords()
            .nth(input - 1)
            .map(|(x, y)| x.abs() + y.abs())
            .unwrap()
    }

    fn part2(input: Self::Data) -> usize {
        coords()
            .scan(HashMap::new(), |cells, (x, y)| {
                let v = adjacent_coords(x, y)
                    .filter_map(|(x, y)| cells.get(&(x, y)))
                    .sum::<usize>()
                    .max(1);
                cells.insert((x, y), v);
                Some(v)
            })
            .find(|&v| v > input)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize(s)
    }
}

fn coords() -> impl Iterator<Item = (isize, isize)> {
    (1..)
        .flat_map(|i| {
            let (d1, d2) = if i % 2 == 0 {
                (Dir::L, Dir::D)
            } else {
                (Dir::R, Dir::U)
            };
            repeat(d1).take(i).chain(repeat(d2).take(i))
        })
        .scan((0isize, 0isize), |(x, y), dir| {
            let pos = (*x, *y);
            match dir {
                Dir::L => *x -= 1,
                Dir::R => *x += 1,
                Dir::U => *y -= 1,
                Dir::D => *y += 1,
            }
            Some(pos)
        })
}

fn adjacent_coords(x: isize, y: isize) -> impl Iterator<Item = (isize, isize)> {
    [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ]
    .iter()
    .map(move |&(dx, dy)| (x + dx, y + dy))
}
