use aoc::{Aoc, Solution};
use apply::Also as _;
use std::collections::hash_map::DefaultHasher;
use std::collections::HashSet;
use std::hash::{Hash, Hasher as _};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Memory Reallocation";

    type Data = Vec<usize>;

    fn part1(blocks: Self::Data) -> usize {
        cycles(blocks).0
    }

    fn part2(blocks: Self::Data) -> usize {
        let blocks = cycles(blocks).1;
        cycles(blocks).0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{char, tab},
            multi::separated_list1,
            usize,
        };

        separated_list1(alt((tab, char(' '))), usize)(s)
    }
}

fn cycles(mut blocks: Vec<usize>) -> (usize, Vec<usize>) {
    let len = blocks.len();
    let mut previous_states = HashSet::new();
    let mut cycles = 0;
    while previous_states.insert(hash(&blocks)) {
        let m = *blocks.iter().max().unwrap();
        let i = blocks.iter().enumerate().find(|&(_, n)| m == *n).unwrap().0;
        blocks[i] = 0;
        (i + 1..=i + m).for_each(|i| blocks[i % len] += 1);
        cycles += 1;
    }
    (cycles, blocks)
}

fn hash<T: Hash>(d: T) -> u64 {
    DefaultHasher::new().also(|mut h| d.hash(&mut h)).finish()
}
