use aoc::{Aoc, Solution};
use itertools::{iproduct, Itertools as _};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Corruption Checksum";

    type Data = Vec<Vec<usize>>;

    fn part1(input: Self::Data) -> usize {
        input.iter().map(spread).sum()
    }

    fn part2(input: Self::Data) -> usize {
        input.iter().map(divide).sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{newline, tab},
            multi::separated_list1,
            usize,
        };
        separated_list1(newline, separated_list1(tab, usize))(s)
    }
}

fn spread<T: AsRef<[usize]>>(row: T) -> usize {
    row.as_ref()
        .iter()
        .minmax()
        .into_option()
        .map(|(x, y)| y - x)
        .unwrap()
}

fn divide<T: AsRef<[usize]>>(row: T) -> usize {
    let row = row.as_ref();
    iproduct!(0..row.len(), 0..row.len())
        .find(|&(i, j)| i != j && row[i] % row[j] == 0)
        .map(|(i, j)| row[i] / row[j])
        .unwrap()
}
