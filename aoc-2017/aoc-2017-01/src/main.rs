use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2017;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Inverse Captcha";

    type Data = Vec<usize>;

    fn part1(captcha: Self::Data) -> usize {
        solve(&captcha, 1)
    }

    fn part2(captcha: Self::Data) -> usize {
        solve(&captcha, captcha.len() / 2)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{bytes::complete::take, combinator::map_parser, multi::many0, usize};

        many0(map_parser(take(1usize), usize))(s)
    }
}

fn solve(captcha: &[usize], distance: usize) -> usize {
    let len = captcha.len();
    (0..len)
        .filter(|&i| captcha[i] == captcha[(i + distance) % len])
        .map(|i| captcha[i])
        .sum()
}
