use std::{ops::RangeFrom, str::FromStr};

use nom::{
    bytes::complete::{take, take_till},
    character::complete::{anychar, i32, i64, i8, newline, u32, u64, u8},
    combinator::{map, map_parser, map_res},
    error::{ErrorKind, FromExternalError, ParseError},
    multi::{many1, many_till, separated_list0, separated_list1},
    sequence::preceded,
};

pub use nom::*;

pub fn usize(s: &str) -> IResult<&str, usize> {
    map_res(u64, usize::try_from)(s)
}

pub fn isize(s: &str) -> IResult<&str, isize> {
    map_res(i64, isize::try_from)(s)
}

pub fn separated_triple<I, O1, O2, O3, O4, O5, E: ParseError<I>, P1, P2, P3, P4, P5>(
    mut first: P1,
    mut sep1: P2,
    mut second: P3,
    mut sep2: P4,
    mut third: P5,
) -> impl FnMut(I) -> IResult<I, (O1, O3, O5), E>
where
    P1: Parser<I, O1, E>,
    P2: Parser<I, O2, E>,
    P2: Parser<I, O2, E>,
    P3: Parser<I, O3, E>,
    P4: Parser<I, O4, E>,
    P5: Parser<I, O5, E>,
{
    move |input: I| {
        let (input, o1) = first.parse(input)?;
        let (input, _) = sep1.parse(input)?;
        let (input, o2) = second.parse(input)?;
        let (input, _) = sep2.parse(input)?;
        let (input, o3) = third.parse(input)?;
        Ok((input, (o1, o2, o3)))
    }
}

pub fn parse<'a, T, E>(s: &'a str) -> IResult<&'a str, T, E>
where
    T: FromStr,
    E: FromExternalError<&'a str, <T as FromStr>::Err>,
{
    match s.parse() {
        Ok(o2) => Ok(("", o2)),
        Err(e) => Err(Err::Error(E::from_external_error(s, ErrorKind::MapRes, e))),
    }
}

pub fn string_list(s: &str) -> IResult<&str, Vec<String>> {
    let (s, mut lines) = separated_list0(newline, map(take_till(|c| c == '\n'), String::from))(s)?;
    while lines.last().map(String::is_empty).unwrap_or(false) {
        lines.pop();
    }
    Ok((s, lines))
}

pub fn usize_list(s: &str) -> IResult<&str, Vec<usize>> {
    separated_list0(newline, usize)(s)
}

pub fn isize_list(s: &str) -> IResult<&str, Vec<isize>> {
    separated_list0(newline, isize)(s)
}

pub fn i32_list(s: &str) -> IResult<&str, Vec<i32>> {
    separated_list0(newline, i32)(s)
}

pub fn u32_list(s: &str) -> IResult<&str, Vec<u32>> {
    separated_list0(newline, u32)(s)
}

pub fn u64_list(s: &str) -> IResult<&str, Vec<u64>> {
    separated_list0(newline, u64)(s)
}

pub fn preceded_usize(s: &str) -> IResult<&str, usize> {
    preceded(non_digit0, usize)(s)
}

pub fn non_digit0(s: &str) -> IResult<&str, &str> {
    take_till(|c: char| c.is_ascii_digit())(s)
}

pub fn u8_matrix(s: &str) -> IResult<&str, Vec<Vec<u8>>> {
    let digit = map_parser(take(1usize), u8);
    let row = many1(digit);
    separated_list1(newline, row)(s)
}

pub fn i8_matrix(s: &str) -> IResult<&str, Vec<Vec<i8>>> {
    let digit = map_parser(take(1usize), i8);
    let row = many1(digit);
    separated_list1(newline, row)(s)
}

pub fn usize_matrix(s: &str) -> IResult<&str, Vec<Vec<usize>>> {
    let digit = map_parser(take(1usize), usize);
    let row = many1(digit);
    separated_list1(newline, row)(s)
}

pub fn find<I, O, E, P>(p: P) -> impl FnMut(I) -> IResult<I, O, E>
where
    P: Parser<I, O, E>,
    E: ParseError<I>,
    I: InputIter + InputLength + Slice<RangeFrom<usize>> + Clone,
    <I as InputIter>::Item: AsChar,
{
    map(many_till(anychar, p), |(_, n)| n)
}
