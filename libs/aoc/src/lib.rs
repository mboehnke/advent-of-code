pub mod nom;

pub use aoc_derive::Aoc;

use std::{
    fmt::Display,
    iter::repeat,
    time::{Duration, Instant},
};

use ::nom::IResult;
use apply::Apply as _;

macro_rules! bench {
    ($e:expr) => {{
        let start = Instant::now();
        let result = $e;
        let elapsed = Instant::now().duration_since(start);
        (result, elapsed)
    }};
}

#[derive(Debug)]
pub struct Report {
    year: String,
    day: String,
    title: String,
    parse_time: Duration,
    part1_time: Duration,
    part2_time: Duration,
    part1_solution: Result<String, String>,
    part2_solution: Result<String, String>,
}

pub trait Solution<T1: Display, T2: Display> {
    const YEAR: u32;
    const DAY: u32;
    const TITLE: &'static str;
    type Data;
    fn part1(_: Self::Data) -> T1;
    fn part2(_: Self::Data) -> T2;
    fn parser(s: &str) -> IResult<&str, Self::Data>;

    fn report(input: &str, sol1: &str, sol2: &str) -> Report
    where
        Self::Data: Clone,
    {
        let (data, parse_time) = bench!(Self::parser(input).unwrap().1);

        let (s, part1_time) = bench!(Self::part1(data.clone()));
        let s = s.to_string();
        let part1_solution = if s == sol1 { Ok(s) } else { Err(s) };

        let (s, part2_time) = bench!(Self::part2(data));
        let s = s.to_string();
        let part2_solution = if s == sol2 { Ok(s) } else { Err(s) };

        Report {
            parse_time,
            part1_time,
            part2_time,
            part1_solution,
            part2_solution,
            year: Self::YEAR.to_string(),
            day: Self::DAY.to_string(),
            title: Self::TITLE.to_string(),
        }
    }
}

impl Report {
    pub fn print(&self) {
        let censored = std::env::args().any(|a| a == "-c");
        if std::env::args().any(|a| a == "-s") {
            self.print_s(censored)
        } else {
            self.print_p(censored)
        }
    }

    fn print_s(&self, censored: bool) {
        const BORDER: &str = "\x1B[0;34m";
        const TITLE1: &str = "\x1B[0;34;1m";
        const TITLE2: &str = "\x1B[0;34;1m";
        const T_WIDTH: usize = 37;
        const TEXT: &str = "\x1B[36m";
        const NONE: &str = "\x1B[0m";
        const SUM: &str = "\x1B[1m";

        let (title1, title2) = title(&self.title, T_WIDTH);

        let box1 = vec![
            format!(
                "{b}┌ {t}Advent of Code {} Day {:02}           {n}",
                self.year,
                self.day,
                t = TITLE1,
                n = NONE,
                b = BORDER
            ),
            format!("{b}│ {t}{}{n}", title1, t = TITLE2, n = NONE, b = BORDER),
            format!("{b}└ {t}{}{n}", title2, t = TITLE2, n = NONE, b = BORDER),
        ];

        let box2 = vec![
            format!(
                " {t}parsing                        {}          {b}┐{n}",
                time(self.parse_time),
                t = TEXT,
                n = NONE,
                b = BORDER
            ),
            format!(
                " {t}part 1 {}   {}          {b}│{n}",
                solution(self.part1_solution.as_ref(), censored),
                time(self.part1_time),
                t = TEXT,
                n = NONE,
                b = BORDER
            ),
            format!(
                " {t}part 2 {}   {}  {s}{} {b}┘{n}",
                solution(self.part2_solution.as_ref(), censored),
                time(self.part2_time),
                time(self.parse_time + self.part1_time + self.part2_time),
                t = TEXT,
                n = NONE,
                s = SUM,
                b = BORDER
            ),
        ];

        box1.into_iter()
            .zip(box2)
            .for_each(|(s1, s2)| println!("{}{}", s1, s2));
    }

    fn print_p(&self, censored: bool) {
        const BORDER: &str = "\x1B[0;34m";
        const TITLE1: &str = "\x1B[0;34;1m";
        const TITLE2: &str = "\x1B[0;34;1m";
        const T_WIDTH: usize = 37;
        const TEXT: &str = "\x1B[36m";
        const NONE: &str = "\x1B[0m";
        const SUM: &str = "\x1B[1m";

        let (title1, title2) = title(&self.title, T_WIDTH);

        let box1 = vec![
            format!("{}┌──────────────────────────────────────┬{}", BORDER, NONE),
            format!(
                "{b}│ {t}Advent of Code {} Day {:02}           {b}│{n}",
                self.year,
                self.day,
                b = BORDER,
                t = TITLE1,
                n = NONE
            ),
            format!("{}│                                      │{}", BORDER, NONE),
            format!(
                "{b}│ {t}{}{b}│{n}",
                title1,
                b = BORDER,
                t = TITLE2,
                n = NONE
            ),
            format!(
                "{b}│ {t}{}{b}│{n}",
                title2,
                b = BORDER,
                t = TITLE2,
                n = NONE
            ),
            format!("{}└──────────────────────────────────────┴{}", BORDER, NONE),
        ];
        let box2 = vec![
            format!(
                "{}────────────────────────────────────────┐{}",
                BORDER, NONE
            ),
            format!(
                " {t}parsing                        {} {b}│{n}",
                time(self.parse_time),
                b = BORDER,
                t = TEXT,
                n = NONE
            ),
            format!(
                " {t}part 1 {}   {} {b}│{n}",
                solution(self.part1_solution.as_ref(), censored),
                time(self.part1_time),
                b = BORDER,
                t = TEXT,
                n = NONE
            ),
            format!(
                " {t}part 2 {}   {} {b}│{n}",
                solution(self.part2_solution.as_ref(), censored),
                time(self.part2_time),
                b = BORDER,
                t = TEXT,
                n = NONE
            ),
            format!(
                "                                {s}{} {b}│{n}",
                time(self.parse_time + self.part1_time + self.part2_time),
                b = BORDER,
                n = NONE,
                s = SUM
            ),
            format!(
                "{}────────────────────────────────────────┘{}",
                BORDER, NONE
            ),
        ];

        box1.into_iter()
            .zip(box2)
            .for_each(|(s1, s2)| println!("{}{}", s1, s2));
    }
}

fn left_pad(s: &str, l: usize) -> String {
    repeat(' ')
        .take(l.saturating_sub(s.len()))
        .chain(s.chars())
        .collect()
}

fn time(t: std::time::Duration) -> String {
    let time = if t < std::time::Duration::from_millis(1) {
        format!("\x1B[32m{:.1?}\x1B[0m", t)
    } else if t < std::time::Duration::from_secs(1) {
        format!("\x1B[33m{:.1?}\x1B[0m", t)
    } else {
        format!("\x1B[31m{:.1?} \x1B[0m", t)
    };
    let p = if time.contains('µ') { 17 } else { 16 };
    left_pad(&time, p)
}

fn title(title: &str, width: usize) -> (String, String) {
    let mut rows = title_split_on_whitespace(title, width);
    if rows.len() > 2 {
        rows = title_split_simple(title, width);
    };
    while rows.len() < 2 {
        rows.push(String::new());
    }
    for row in &mut rows {
        *row = (*row).chars().chain(repeat(' ')).take(width).collect();
    }
    (rows.remove(0), rows.remove(0))
}

fn title_split_on_whitespace(title: &str, width: usize) -> Vec<String> {
    title
        .split_whitespace()
        .fold(Vec::new(), |mut rows: Vec<String>, word| {
            match rows.last_mut() {
                Some(row) if row.chars().count() + word.chars().count() < width => {
                    row.push(' ');
                    row.push_str(word);
                }
                _ => rows.push(word.to_string()),
            }
            rows
        })
}

fn title_split_simple(title: &str, width: usize) -> Vec<String> {
    let row1 = title.chars().take(width).collect();
    let row2 = title.chars().skip(width).collect();
    vec![row1, row2]
}

fn solution(s: Result<&String, &String>, censored: bool) -> String {
    if censored {
        match s {
            Ok(v) => format!("\x1B[0;36m{}\x1B[0m", "*".repeat(v.chars().count())),
            Err(v) => format!("\x1B[1;31m{}\x1B[0m", "*".repeat(v.chars().count())),
        }
    } else {
        match s {
            Ok(v) => format!("\x1B[0;36m{}\x1B[0m", v),
            Err(v) => format!("\x1B[1;31m{}\x1B[0m", v),
        }
    }
    .apply(|s| left_pad(&s, 32))
}
