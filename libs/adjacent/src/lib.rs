/// iterator over adjacent coordinates (without diagonals)
pub fn adjacent(x: usize, y: usize, mx: usize, my: usize) -> impl Iterator<Item = (usize, usize)> {
    (y > 0)
        .then_some((x, y - 1))
        .into_iter()
        .chain((x > 0).then_some((x - 1, y)))
        .chain((x < mx).then_some((x + 1, y)))
        .chain((y < my).then_some((x, y + 1)))
}

/// iterator over adjacent coordinates (with diagonals)
pub fn adjacent_d(
    x: usize,
    y: usize,
    mx: usize,
    my: usize,
) -> impl Iterator<Item = (usize, usize)> {
    adjacent(x, y, mx, my)
        .chain((x > 0 && y > 0).then_some((x - 1, y - 1)))
        .chain((x < mx && y > 0).then_some((x + 1, y - 1)))
        .chain((x > 0 && y < my).then_some((x - 1, y + 1)))
        .chain((x < mx && y < my).then_some((x + 1, y + 1)))
}
