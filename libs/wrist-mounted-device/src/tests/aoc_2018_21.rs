use crate::*;
use apply::Apply as _;
use std::collections::HashSet;

const SOL_1: &str = include_str!("../../data/201821sol1");
const SOL_2: &str = include_str!("../../data/201821sol2");
const CODE: &str = include_str!("../../data/201821input");

#[test]
fn aoc_2018_19_1_solution() {
    let mut vals = CODE.parse::<Computer>().unwrap().apply(InvalidValues);
    let res = vals.next().unwrap().to_string();
    assert_eq!(res, SOL_1);
}

#[test]
fn aoc_2018_19_2_solution() {
    let mut vals = CODE.parse::<Computer>().unwrap().apply(InvalidValues);
    let mut set = HashSet::new();
    let mut last = 0;
    loop {
        let val = vals.next().unwrap();
        if set.insert(val) {
            last = val
        } else {
            break;
        }
    }
    let res = last.to_string();
    assert_eq!(res, SOL_2);
}

#[derive(Clone)]
pub struct InvalidValues(Computer);

impl Iterator for InvalidValues {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        while self.0.registers[self.0.ip] != 28 {
            self.0.step();
        }
        let val = if let Instruction::EQRR(r, _, _) = self.0.instructions[28] {
            Some(self.0.registers[r])
        } else {
            None
        };
        self.0.step();
        val
    }
}
