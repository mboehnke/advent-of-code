use crate::*;

const SOL_1: &str = include_str!("../../data/201819sol1");
const SOL_2: &str = include_str!("../../data/201819sol2");
const CODE: &str = include_str!("../../data/201819input");

#[test]
fn aoc_2018_19_1_solution() {
    let mut device = CODE.parse::<Computer>().unwrap();
    device.run();
    let res = device.registers[0].to_string();
    assert_eq!(res, SOL_1);
}

#[test]
fn aoc_2018_19_2_solution() {
    let mut device = CODE.parse::<Computer>().unwrap();
    *device.instructions.last_mut().unwrap() = Instruction::SETI(999, 0, 3);
    device.registers[0] = 1;
    device.run();
    let res = (1..=device.registers[1])
        .filter(|n| device.registers[1] % n == 0)
        .sum::<usize>()
        .to_string();
    assert_eq!(res, SOL_2);
}
