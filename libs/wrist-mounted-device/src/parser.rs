use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{newline, space1, u64},
    combinator::{map_res, opt},
    multi::separated_list0,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};

use crate::Instruction;

pub fn ip_instructions(s: &str) -> IResult<&str, (Option<usize>, Vec<Instruction>)> {
    separated_pair(opt(ip), opt(newline), separated_list0(newline, instruction))(s)
}

fn ip(s: &str) -> IResult<&str, usize> {
    preceded(tuple((tag("#ip"), space1)), usize)(s)
}

fn instruction(s: &str) -> IResult<&str, Instruction> {
    use Instruction::*;
    alt((
        ins("addi", ADDI),
        ins("addr", ADDR),
        ins("muli", MULI),
        ins("mulr", MULR),
        ins("bani", BANI),
        ins("banr", BANR),
        ins("bori", BORI),
        ins("borr", BORR),
        ins("seti", SETI),
        ins("setr", SETR),
        ins("gtir", GTIR),
        ins("gtri", GTRI),
        ins("gtrr", GTRR),
        ins("eqir", EQIR),
        ins("eqri", EQRI),
        ins("eqrr", EQRR),
    ))(s)
}

fn ins(
    ins: &'static str,
    constructor: impl Fn(usize, usize, usize) -> Instruction,
) -> impl FnMut(&str) -> IResult<&str, Instruction> {
    move |s: &str| {
        let (s, a) = preceded(tuple((tag(ins), space1)), usize)(s)?;
        let (s, b) = preceded(space1, usize)(s)?;
        let (s, c) = preceded(space1, usize)(s)?;
        Ok((s, constructor(a, b, c)))
    }
}

fn usize(s: &str) -> IResult<&str, usize> {
    map_res(u64, usize::try_from)(s)
}
