#[cfg(test)]
mod tests;

mod parser;

use std::str::FromStr;

#[derive(Debug, Clone)]
pub struct Computer {
    pub registers: Vec<usize>,
    pub instructions: Vec<Instruction>,
    pub ip: usize,
    pub icounter: u64,
}

#[derive(Debug, Clone)]
pub enum Instruction {
    ADDR(usize, usize, usize),
    ADDI(usize, usize, usize),
    MULR(usize, usize, usize),
    MULI(usize, usize, usize),
    BANR(usize, usize, usize),
    BANI(usize, usize, usize),
    BORR(usize, usize, usize),
    BORI(usize, usize, usize),
    SETR(usize, usize, usize),
    SETI(usize, usize, usize),
    GTIR(usize, usize, usize),
    GTRI(usize, usize, usize),
    GTRR(usize, usize, usize),
    EQIR(usize, usize, usize),
    EQRI(usize, usize, usize),
    EQRR(usize, usize, usize),
}

impl Computer {
    pub fn run(&mut self) {
        while self.registers[self.ip] < self.instructions.len() {
            self.step();
        }
    }

    pub fn step(&mut self) {
        self.icounter += 1;
        match self.instructions[self.registers[self.ip]] {
            Instruction::ADDR(a, b, c) => self.registers[c] = self.registers[a] + self.registers[b],
            Instruction::ADDI(a, b, c) => self.registers[c] = self.registers[a] + b,
            Instruction::MULR(a, b, c) => self.registers[c] = self.registers[a] * self.registers[b],
            Instruction::MULI(a, b, c) => self.registers[c] = self.registers[a] * b,
            Instruction::BORR(a, b, c) => self.registers[c] = self.registers[a] | self.registers[b],
            Instruction::BORI(a, b, c) => self.registers[c] = self.registers[a] | b,
            Instruction::BANR(a, b, c) => self.registers[c] = self.registers[a] & self.registers[b],
            Instruction::BANI(a, b, c) => self.registers[c] = self.registers[a] & b,
            Instruction::SETR(a, _, c) => self.registers[c] = self.registers[a],
            Instruction::SETI(a, _, c) => self.registers[c] = a,
            Instruction::GTIR(a, b, c) => self.registers[c] = (a > self.registers[b]) as usize,
            Instruction::GTRI(a, b, c) => self.registers[c] = (self.registers[a] > b) as usize,
            Instruction::GTRR(a, b, c) => {
                self.registers[c] = (self.registers[a] > self.registers[b]) as usize
            }
            Instruction::EQIR(a, b, c) => self.registers[c] = (a == self.registers[b]) as usize,
            Instruction::EQRI(a, b, c) => self.registers[c] = (self.registers[a] == b) as usize,
            Instruction::EQRR(a, b, c) => {
                self.registers[c] = (self.registers[a] == self.registers[b]) as usize
            }
        }
        self.registers[self.ip] += 1;
    }
}

impl FromStr for Computer {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (ip, instructions) = parser::ip_instructions(s).map_err(|e| e.to_string())?.1;
        let ip = ip.unwrap_or(5);
        Ok(Computer {
            instructions,
            registers: vec![0; 6],
            ip,
            icounter: 0,
        })
    }
}
