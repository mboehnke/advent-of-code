#[cfg(test)]
mod tests;

macro_rules! font {
    ($name:ident, $width:expr, $height:expr, $num_chars:expr, $data:expr) => {
        const fn $name() -> [(u8, [[bool; $width]; $height]); $num_chars] {
            let mut i = 0;
            let mut glyphs = [(0, [[false; $width]; $height]); $num_chars];
            while i < $num_chars {
                glyphs[i].0 = $data[i];
                i += 1;
            }
            i += 1;
            let mut g = 0;
            while g < $num_chars {
                let mut row = 0;
                while row < $height {
                    let mut col = 0;
                    while col < $width {
                        glyphs[g].1[row][col] = $data[i] == b'#';
                        col += 1;
                        i += 1
                    }
                    row += 1;
                    i += 1;
                }
                g += 1;
            }
            glyphs
        }
    };
}

macro_rules! format_s {
    ($name:ident, $height:expr, $font:expr) => {
        pub fn $name<T: AsRef<str>>(s: T) -> Option<Vec<String>> {
            let mut o = vec![String::new(); $height];
            for c in s.as_ref().chars() {
                $font()
                    .iter()
                    .find(|&&(x, _)| x as char == c)?
                    .1
                    .iter()
                    .enumerate()
                    .for_each(|(row, bytes)| {
                        bytes
                            .iter()
                            .map(|&b| if b { '█' } else { ' ' })
                            .for_each(|c| o[row].push(c))
                    });
            }
            Some(o)
        }
    };
}
macro_rules! parse {
    ($name:ident, $width:expr, $height:expr, $font:expr) => {
        pub fn $name<T, I>(s: I) -> Option<String>
        where
            T: AsRef<str>,
            I: IntoIterator<Item = T>,
        {
            let mut result = String::new();
            let lines = s
                .into_iter()
                .map(|line| {
                    line.as_ref()
                        .chars()
                        .map(|c| c != '.' && c != ' ')
                        .collect::<Vec<_>>()
                        .chunks($width)
                        .map(|x| x.to_vec())
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>();
            let chars = (0..lines[0].len())
                .map(|i| {
                    (0..$height)
                        .map(|r| lines[r][i].clone())
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>();
            for c in chars {
                let ch = $font()
                    .iter()
                    .find(|(_, v)| {
                        v.iter()
                            .flat_map(|r| r.iter())
                            .zip(c.iter().flat_map(|l| l.into_iter()))
                            .all(|(a, b)| *a == *b)
                    })?
                    .0 as char;
                result.push(ch);
            }
            Some(result)
        }
    };
}

font!(font5x6, 5, 6, 18, include_bytes!("../fonts/5x6"));
font!(font8x10, 8, 10, 15, include_bytes!("../fonts/8x10"));

format_s!(format_5x6, 6, font5x6);
format_s!(format_8x10, 10, font8x10);

parse!(parse_5x6, 5, 6, font5x6);
parse!(parse_8x10, 8, 10, font8x10);
