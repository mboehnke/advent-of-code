use crate::*;

const F5X6C: &str = include_str!("../data/f5x6c");
const F5X6G: &str = include_str!("../data/f5x6g");

const F8X10C: &str = include_str!("../data/f8x10c");
const F8X10G: &str = include_str!("../data/f8x10g");

const Y2018D10C: &str = include_str!("../data/201810c");
const Y2018D10G: &str = include_str!("../data/201810g");

const Y2016D08C: &str = include_str!("../data/201608c");
const Y2016D08G: &str = include_str!("../data/201608g");

const Y2019D08C: &str = include_str!("../data/201908c");
const Y2019D08G: &str = include_str!("../data/201908g");

const Y2019D11C: &str = include_str!("../data/201911c");
const Y2019D11G: &str = include_str!("../data/201911g");

macro_rules! tests {
    ($($name_f:ident, $name_p:ident: $function_f:ident, $function_p:ident, $chars:expr, $glyphs:expr,)*) => {
    $(
        #[test]
        fn $name_f() {
            $function_f($chars)
                .unwrap()
                .iter()
                .zip($glyphs.lines())
                .for_each(|(r, e)| {
                    assert_eq!(r, e);
                })
        }

        #[test]
        fn $name_p() {
            let glyphs=$glyphs.lines().collect::<Vec<&str>>();
            assert_eq!($function_p(glyphs).unwrap(), $chars);
        }
            )*
    }
}

tests! {
    test_format_5x6_abc, test_parse_5x6_abc: format_5x6, parse_5x6, F5X6C, F5X6G,
    test_format_8x10_abc, test_parse_8x10_abc: format_8x10, parse_8x10, F8X10C, F8X10G,
    test_format_5x6_2016_08, test_parse_5x6_2016_08: format_5x6, parse_5x6, Y2016D08C, Y2016D08G,
    test_format_5x6_2019_11, test_parse_5x6_2019_11: format_5x6, parse_5x6, Y2019D11C, Y2019D11G,
    test_format_5x6_2019_08, test_parse_5x6_2019_08: format_5x6, parse_5x6, Y2019D08C, Y2019D08G,
    test_format_8x10_2018_10, test_parse_8x10_2018_10: format_8x10, parse_8x10, Y2018D10C, Y2018D10G,
}
