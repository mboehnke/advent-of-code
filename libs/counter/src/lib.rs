use hashbrown::HashMap;

#[derive(Clone, Debug)]
pub struct Counter<I, C>(HashMap<I, C>);

macro_rules! counter {
    ($type:ty) => {
        impl<I: Eq + std::hash::Hash> FromIterator<I> for Counter<I, $type> {
            fn from_iter<T: IntoIterator<Item = I>>(iter: T) -> Self {
                let c = iter
                    .into_iter()
                    .fold(HashMap::<I, $type>::new(), |mut counter, item| {
                        *counter.entry(item).or_default() += 1;
                        counter
                    });
                Counter(c)
            }
        }
    };
}

counter!(u8);
counter!(u16);
counter!(u32);
counter!(u64);
counter!(usize);
counter!(i8);
counter!(i16);
counter!(i32);
counter!(i64);
counter!(isize);

impl<I, C> std::ops::Deref for Counter<I, C> {
    type Target = HashMap<I, C>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<I, C> std::ops::DerefMut for Counter<I, C> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
