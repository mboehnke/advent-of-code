use apply::Apply as _;
use hashbrown::{HashMap, HashSet};
use std::{hash::Hash, rc::Rc};

#[allow(clippy::type_complexity)]
pub struct GameOfLifeIterator<T> {
    active: HashSet<T>,
    adjacent: Rc<dyn Fn(&T) -> Vec<T>>,
    next_t: Rc<dyn Fn(bool, usize) -> bool>,
    tiles_and_adj_count: Rc<dyn Fn(&HashSet<T>, Rc<dyn Fn(&T) -> Vec<T>>) -> HashMap<T, usize>>,
}

pub trait GameOfLife {
    type Pos;

    fn adjacent(_: &Self::Pos) -> Vec<Self::Pos>;

    fn next_t(tile: bool, adjacent: usize) -> bool;

    fn from_iter<I>(active: I) -> GameOfLifeIterator<Self::Pos>
    where
        I: IntoIterator<Item = Self::Pos>,
        Self::Pos: std::hash::Hash + Eq,
        Self: 'static,
    {
        let active = active.into_iter().collect::<HashSet<_>>();
        let adjacent = Rc::new(Self::adjacent);
        let next_t = Rc::new(Self::next_t);
        let tiles_and_adj_count = Rc::new(Self::tiles_and_adj_count);
        GameOfLifeIterator {
            active,
            adjacent,
            next_t,
            tiles_and_adj_count,
        }
    }

    #[allow(clippy::type_complexity)]
    fn tiles_and_adj_count(
        tiles: &HashSet<Self::Pos>,
        adjacent: Rc<dyn Fn(&Self::Pos) -> Vec<Self::Pos>>,
    ) -> HashMap<Self::Pos, usize>
    where
        Self::Pos: std::hash::Hash + Eq,
    {
        tiles
            .iter()
            .flat_map(|a| adjacent(a))
            .fold(HashMap::new(), |mut map, pos| {
                *map.entry(pos).or_insert(0) += 1;
                map
            })
    }
}

impl<T> Iterator for GameOfLifeIterator<T>
where
    T: Clone + Eq + Hash,
{
    type Item = HashSet<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let current = self.active.clone().apply(Some);
        self.active = (self.tiles_and_adj_count)(&self.active, self.adjacent.clone())
            .into_iter()
            .map(|(p, a)| (self.active.contains(&p), p, a))
            .flat_map(|(n, p, a)| if (self.next_t)(n, a) { Some(p) } else { None })
            .collect();
        current
    }
}
