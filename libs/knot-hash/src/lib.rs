use apply::Apply as _;

pub fn hash<T: IntoIterator<Item = usize>>(lengths: T, size: usize) -> Vec<usize> {
    lengths
        .into_iter()
        .enumerate()
        .scan(0, |pos, (skip, len)| {
            let p = *pos;
            *pos = (*pos + len + skip) % size;
            Some((p, len))
        })
        .fold((0..size).collect(), |mut nums, (pos, len)| {
            (0..len / 2)
                .map(|d| ((pos + d) % size, (pos + len - 1 - d) % size))
                .for_each(|(a, b)| nums.swap(a, b));
            nums
        })
}

pub fn knot_hash(s: &str) -> u128 {
    s.chars()
        .map(|c| c as usize)
        .chain(vec![17, 31, 73, 47, 23])
        .cycle()
        .take((s.len() + 5) * 64)
        .apply(|l| hash(l, 256))
        .chunks(16)
        .map(|c| c.iter().fold(0, |acc, &val| acc ^ val))
        .fold(0, |acc, x| (acc << 8) + x as u128)
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_1() {
        assert_eq!(
            format!("{:032x}", crate::knot_hash("")),
            "a2582a3a0e66e6e86e3812dcb672a272"
        )
    }
    #[test]
    fn test_2() {
        assert_eq!(
            format!("{:032x}", crate::knot_hash("AoC 2017")),
            "33efeb34ea91902bb2f59c9920caa6cd"
        )
    }
    #[test]
    fn test_3() {
        assert_eq!(
            format!("{:032x}", crate::knot_hash("1,2,3")),
            "3efbe78a8d82f29979031a4aa0b16a9d"
        )
    }
    #[test]
    fn test_4() {
        assert_eq!(
            format!("{:032x}", crate::knot_hash("1,2,4")),
            "63960835bcdc130f0b66d7ff4f6a5a8e"
        )
    }
    #[test]
    fn test_5() {
        assert_eq!(
            format!(
                "{:032x}",
                crate::knot_hash("183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88")
            ),
            "90adb097dd55dea8305c900372258ac6"
        )
    }
}
