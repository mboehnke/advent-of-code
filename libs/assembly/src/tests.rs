use apply::Apply;

const Y201523EX1INPUT: &str = include_str!("../data/201523ex1input");
const Y201523EX1SOL1: &str = include_str!("../data/201523ex1sol1");
const Y201523INPUT: &str = include_str!("../data/201523input");
const Y201523SOL1: &str = include_str!("../data/201523sol1");
const Y201523SOL2: &str = include_str!("../data/201523sol2");

const Y201612EX1INPUT: &str = include_str!("../data/201612ex1input");
const Y201612EX1SOL1: &str = include_str!("../data/201612ex1sol1");
const Y201612INPUT: &str = include_str!("../data/201612input");
const Y201612SOL1: &str = include_str!("../data/201612sol1");
const Y201612SOL2: &str = include_str!("../data/201612sol2");

const Y201623EX1INPUT: &str = include_str!("../data/201623ex1input");
const Y201623EX1SOL1: &str = include_str!("../data/201623ex1sol1");
const Y201623INPUT1: &str = include_str!("../data/201623input1");
const Y201623INPUT2: &str = include_str!("../data/201623input2");
const Y201623SOL1: &str = include_str!("../data/201623sol1");
const Y201623SOL2: &str = include_str!("../data/201623sol2");

const Y201625INPUT: &str = include_str!("../data/201625input");
const Y201625SOL1: &str = include_str!("../data/201625sol1");

const Y201718EX1INPUT: &str = include_str!("../data/201718ex1input");
const Y201718EX1SOL1: &str = include_str!("../data/201718ex1sol1");
const Y201718EX2INPUT: &str = include_str!("../data/201718ex2input");
const Y201718EX2SOL2: &str = include_str!("../data/201718ex2sol2");
const Y201718INPUT: &str = include_str!("../data/201718input");
const Y201718SOL1: &str = include_str!("../data/201718sol1");
const Y201718SOL2: &str = include_str!("../data/201718sol2");

const Y201723INPUT: &str = include_str!("../data/201723input");
const Y201723SOL1: &str = include_str!("../data/201723sol1");

use crate::*;
use std::iter::repeat;

macro_rules! tests {
    ($($name:ident: $function:ident, $code:expr, $expected:expr,)*) => {
    $(
        #[test]
        fn $name() {
            assert_eq!($function($code).to_string(), $expected)
        }
    )*
    };
}

tests! {
    aoc_2015_23_1_example: y2015d23p1, Y201523EX1INPUT, Y201523EX1SOL1,
    aoc_2015_23_1_solution: y2015d23p1, Y201523INPUT, Y201523SOL1,
    aoc_2015_23_2_solution: y2015d23p2, Y201523INPUT, Y201523SOL2,

    aoc_2016_12_1_example: y2016d12p1, Y201612EX1INPUT, Y201612EX1SOL1,
    aoc_2016_12_1_solution: y2016d12p1, Y201612INPUT, Y201612SOL1,
    aoc_2016_12_2_solution: y2016d12p2, Y201612INPUT, Y201612SOL2,

    aoc_2016_23_1_example: y2016d23p1, Y201623EX1INPUT, Y201623EX1SOL1,
    aoc_2016_23_1_solution: y2016d23p1, Y201623INPUT1, Y201623SOL1,
    aoc_2016_23_2_solution: y2016d23p2, Y201623INPUT2, Y201623SOL2,

    aoc_2016_25_1_solution: y2016d25p1, Y201625INPUT, Y201625SOL1,

    aoc_2017_18_1_example: y2017d18p1, Y201718EX1INPUT, Y201718EX1SOL1,
    aoc_2017_18_1_solution: y2017d18p1, Y201718INPUT, Y201718SOL1,
    aoc_2017_18_2_example: y2017d18p2, Y201718EX2INPUT, Y201718EX2SOL2,
    aoc_2017_18_2_solution: y2017d18p2, Y201718INPUT, Y201718SOL2,

    aoc_2017_23_1_solution: y2017d23p1, Y201723INPUT, Y201723SOL1,
}

fn y2016d12p1(s: &str) -> isize {
    let mut computer = str::parse::<Computer>(s).unwrap();
    computer.run();
    computer.get_a()
}

fn y2016d12p2(s: &str) -> isize {
    let mut computer = str::parse::<Computer>(s).unwrap();
    computer.set_c(1);
    computer.run();
    computer.get_a()
}

fn y2016d23p1(s: &str) -> isize {
    let mut i = s.splitn(2, '\n');
    let v = i.next().unwrap().parse().unwrap();
    let mut computer = i.next().unwrap().apply(str::parse::<Computer>).unwrap();
    computer.set_a(v);
    computer.run();
    computer.get_a()
}

fn y2016d23p2(s: &str) -> isize {
    let mut computer = str::parse::<Computer>(s).unwrap();
    computer.set_a(12);
    computer.run();
    computer.get_a()
}

fn y2016d25p1(s: &str) -> isize {
    struct Clock(Computer);
    impl Clock {
        fn from_p(computer: &Computer, num: isize) -> Self {
            let mut computer = computer.clone();
            computer.set_a(num);
            Clock(computer)
        }
    }
    impl Iterator for Clock {
        type Item = isize;
        fn next(&mut self) -> Option<Self::Item> {
            match self.0.run() {
                State::Output(x) => Some(x),
                _ => None,
            }
        }
    }

    let computer = str::parse::<Computer>(s).unwrap();
    let e = repeat(vec![0, 1]).flatten().take(1000).collect::<Vec<_>>();
    (0..)
        .find(|&num| {
            Clock::from_p(&computer, num)
                .zip(e.iter())
                .all(|(a, b)| a == *b)
        })
        .unwrap()
}

fn y2017d23p1(s: &str) -> usize {
    str::parse::<Computer>(s).unwrap().debug()
}

fn y2017d18p1(s: &str) -> isize {
    let mut tablet = str::parse::<Computer>(s).unwrap();
    let mut s = tablet.run();
    let mut output = 0;
    loop {
        match s {
            State::AwaitingInput(x) if x == 0 => s = tablet.run_with_input(&[x]),
            State::Output(x) => {
                output = x;
                s = tablet.run()
            }
            _ => break,
        }
    }
    output
}

fn y2017d18p2(s: &str) -> usize {
    let tablet = str::parse::<Computer>(s).unwrap();
    let mut tablets = vec![tablet.clone(), tablet];

    tablets[1].set_p(1);
    let mut states = [tablets[0].run(), tablets[1].run()];
    let mut nums = [0, 0];
    let mut inputs = [vec![], vec![]];
    loop {
        inputs.iter_mut().for_each(Vec::clear);
        for i in 0..=1 {
            let j = (i + 1) % 2;
            while let State::Output(x) = states[i] {
                nums[i] += i;
                inputs[j].push(x);
                states[i] = tablets[i].run();
            }
        }
        if inputs.iter().all(Vec::is_empty) {
            break;
        }
        (0..=1).for_each(|i| states[i] = tablets[i].run_with_input(&inputs[i]));
    }
    nums[1]
}

fn y2015d23p1(s: &str) -> isize {
    let mut computer = str::parse::<Computer>(s).unwrap();
    computer.run();
    computer.get_b()
}

fn y2015d23p2(s: &str) -> isize {
    let mut computer = str::parse::<Computer>(s).unwrap();
    computer.set_a(1);
    computer.run();
    computer.get_b()
}
