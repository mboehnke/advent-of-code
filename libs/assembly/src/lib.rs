#[cfg(test)]
mod tests;

mod parser;

use std::collections::VecDeque;
use std::str::FromStr;

#[derive(Debug, Clone)]
pub struct Computer {
    registers: Vec<isize>,
    program: Vec<Instruction>,
    pp: usize,
    inputs: VecDeque<isize>,
    multiplications: usize,
}

#[derive(Debug, Clone)]
pub enum State {
    AwaitingInput(isize),
    Output(isize),
    Finished,
}

#[derive(Debug, Clone)]
pub enum Instruction {
    CPY(Value, usize),
    INC(usize),
    DEC(usize),
    SET(usize, Value),
    ADD(usize, Value),
    SUB(usize, Value),
    MUL(usize, Value),
    MOD(usize, Value),
    JGZ(Value, Value),
    JNZ(Value, Value),
    EQL(usize, Value),
    SND(Value),
    RCV(usize),
    OUT(Value),
    TGL(Value),
    CPY_(Value, Value),
    INC_(Value),
    DEC_(Value),
    HLF(usize),
    TPL(usize),
    JMP(Value),
    JIE(usize, Value),
    JIO(usize, Value),
    DIV(usize, Value),
}

#[derive(Debug, Clone, Copy)]
pub enum Value {
    Num(isize),
    Register(usize),
}

impl FromStr for Computer {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let program = parser::instruction_list(s).map_err(|e| e.to_string())?.1;
        Ok(Computer {
            registers: vec![0; 14],
            program,
            pp: 0,
            multiplications: 0,
            inputs: VecDeque::new(),
        })
    }
}

impl Computer {
    pub fn get_a(&self) -> isize {
        self.registers[0]
    }
    pub fn get_b(&self) -> isize {
        self.registers[1]
    }
    pub fn get_c(&self) -> isize {
        self.registers[2]
    }
    pub fn get_z(&self) -> isize {
        self.registers[13]
    }
    pub fn set_a(&mut self, v: isize) {
        self.registers[0] = v
    }
    pub fn set_c(&mut self, v: isize) {
        self.registers[2] = v
    }
    pub fn set_p(&mut self, v: isize) {
        self.registers[9] = v
    }

    pub fn debug(&mut self) -> usize {
        self.run();
        self.multiplications
    }

    pub fn run_with_input(&mut self, input: &[isize]) -> State {
        self.inputs.extend(input);
        self.run()
    }

    pub fn run(&mut self) -> State {
        while self.pp < self.program.len() {
            let ins = &self.program[self.pp];
            self.pp += 1;
            match *ins {
                Instruction::SET(x, y) => self.registers[x] = y.value(&self.registers),
                Instruction::ADD(x, y) => self.registers[x] += y.value(&self.registers),
                Instruction::SUB(x, y) => self.registers[x] -= y.value(&self.registers),
                Instruction::MUL(x, y) => {
                    self.multiplications += 1;
                    self.registers[x] *= y.value(&self.registers)
                }
                Instruction::MOD(x, y) => self.registers[x] %= y.value(&self.registers),
                Instruction::HLF(x) => self.registers[x] /= 2,
                Instruction::TPL(x) => self.registers[x] *= 3,
                Instruction::JMP(x) => {
                    self.pp = (self.pp as isize - 1 + x.value(&self.registers)) as usize
                }

                Instruction::JGZ(x, y) if x.value(&self.registers) > 0 => {
                    self.pp = (self.pp as isize - 1 + y.value(&self.registers)) as usize
                }
                Instruction::JGZ(_, _) => {}
                Instruction::JNZ(x, y) if x.value(&self.registers) != 0 => {
                    self.pp = (self.pp as isize - 1 + y.value(&self.registers)) as usize
                }
                Instruction::JNZ(_, _) => {}
                Instruction::JIE(x, y) if self.registers[x] % 2 == 0 => {
                    self.pp = (self.pp as isize - 1 + y.value(&self.registers)) as usize
                }
                Instruction::JIE(_, _) => {}
                Instruction::JIO(x, y) if self.registers[x] == 1 => {
                    self.pp = (self.pp as isize - 1 + y.value(&self.registers)) as usize
                }
                Instruction::JIO(_, _) => {}
                Instruction::SND(x) => return State::Output(x.value(&self.registers)),
                Instruction::OUT(x) => return State::Output(x.value(&self.registers)),
                Instruction::RCV(x) => {
                    if let Some(y) = self.inputs.pop_front() {
                        self.registers[x] = y
                    } else {
                        self.pp -= 1;
                        return State::AwaitingInput(self.registers[x]);
                    }
                }
                Instruction::CPY(x, y) => self.registers[y] = x.value(&self.registers),
                Instruction::INC(x) => self.registers[x] += 1,
                Instruction::DEC(x) => self.registers[x] -= 1,
                Instruction::TGL(x) => {
                    let p = self.pp as isize - 1 + x.value(&self.registers);
                    if p >= 0 && p < self.program.len() as isize {
                        let n = match &self.program[p as usize] {
                            &Instruction::INC(x) => Instruction::DEC(x),
                            &Instruction::DEC(x) => Instruction::INC(x),
                            &Instruction::TGL(Value::Register(x)) => Instruction::INC(x),
                            &Instruction::TGL(x) => Instruction::INC_(x),
                            &Instruction::INC_(x) => Instruction::DEC_(x),
                            &Instruction::DEC_(x) => Instruction::INC_(x),
                            &Instruction::CPY(x, y) => Instruction::JNZ(x, Value::Register(y)),
                            &Instruction::JNZ(x, Value::Register(y)) => Instruction::CPY(x, y),
                            &Instruction::JNZ(x, y) => Instruction::CPY_(x, y),
                            &Instruction::CPY_(x, y) => Instruction::JNZ(x, y),
                            ins => ins.clone(),
                        };
                        self.program[p as usize] = n;
                    }
                }
                // invalid instructions (only created by TGL)
                Instruction::CPY_(_, _) => {}
                Instruction::INC_(_) => {}
                Instruction::DEC_(_) => {}
                Instruction::EQL(a, b) => {
                    self.registers[a] = isize::from(self.registers[a] == b.value(&self.registers))
                }
                Instruction::DIV(x, y) => self.registers[x] /= y.value(&self.registers),
            }
        }
        State::Finished
    }
}

impl Value {
    fn value(&self, registers: &[isize]) -> isize {
        match self {
            Value::Num(n) => *n,
            Value::Register(r) => registers[*r],
        }
    }
}
