use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, i64, newline, space1},
    combinator::{map, map_res, opt, value},
    multi::separated_list0,
    sequence::{preceded, tuple},
    IResult,
};

use crate::{Instruction, Value};

pub fn instruction_list(s: &str) -> IResult<&str, Vec<Instruction>> {
    separated_list0(newline, instruction)(s)
}

fn instruction(s: &str) -> IResult<&str, Instruction> {
    alt((
        alt((
            ins_reg("inc", Instruction::INC),
            ins_reg("dec", Instruction::DEC),
            ins_reg("rcv", Instruction::RCV),
            ins_reg("inp", Instruction::RCV),
            ins_reg("hlf", Instruction::HLF),
            ins_reg("tpl", Instruction::TPL),
            ins_value("snd", Instruction::SND),
            ins_value("out", Instruction::OUT),
            ins_value("tgl", Instruction::TGL),
            ins_value("jmp", Instruction::JMP),
        )),
        ins_reg_value("set", Instruction::SET),
        ins_reg_value("add", Instruction::ADD),
        ins_reg_value("sub", Instruction::SUB),
        ins_reg_value("mul", Instruction::MUL),
        ins_reg_value("mod", Instruction::MOD),
        ins_reg_value("jie", Instruction::JIE),
        ins_reg_value("jio", Instruction::JIO),
        ins_value_reg("cpy", Instruction::CPY),
        ins_value_value("jgz", Instruction::JGZ),
        ins_value_value("jnz", Instruction::JNZ),
        ins_reg_value("eql", Instruction::EQL),
        ins_reg_value("div", Instruction::DIV),
    ))(s)
}

fn ins_reg(
    ins: &'static str,
    constructor: impl Fn(usize) -> Instruction,
) -> impl FnMut(&str) -> IResult<&str, Instruction> {
    move |s: &str| {
        let (s, a) = preceded(tuple((tag(ins), space1)), register_id)(s)?;
        Ok((s, constructor(a)))
    }
}

fn ins_value(
    ins: &'static str,
    constructor: impl Fn(Value) -> Instruction,
) -> impl FnMut(&str) -> IResult<&str, Instruction> {
    move |s: &str| {
        let (s, a) = preceded(tuple((tag(ins), space1)), value_p)(s)?;
        Ok((s, constructor(a)))
    }
}

fn ins_reg_value(
    ins: &'static str,
    constructor: impl Fn(usize, Value) -> Instruction,
) -> impl FnMut(&str) -> IResult<&str, Instruction> {
    move |s: &str| {
        let (s, a) = preceded(tuple((tag(ins), space1)), register_id)(s)?;
        let (s, b) = preceded(tuple((opt(char(',')), space1)), value_p)(s)?;
        Ok((s, constructor(a, b)))
    }
}

fn ins_value_reg(
    ins: &'static str,
    constructor: impl Fn(Value, usize) -> Instruction,
) -> impl FnMut(&str) -> IResult<&str, Instruction> {
    move |s: &str| {
        let (s, a) = preceded(tuple((tag(ins), space1)), value_p)(s)?;
        let (s, b) = preceded(tuple((opt(char(',')), space1)), register_id)(s)?;
        Ok((s, constructor(a, b)))
    }
}

fn ins_value_value(
    ins: &'static str,
    constructor: impl Fn(Value, Value) -> Instruction,
) -> impl FnMut(&str) -> IResult<&str, Instruction> {
    move |s: &str| {
        let (s, a) = preceded(tuple((tag(ins), space1)), value_p)(s)?;
        let (s, b) = preceded(tuple((opt(char(',')), space1)), value_p)(s)?;
        Ok((s, constructor(a, b)))
    }
}

fn value_p(s: &str) -> IResult<&str, Value> {
    let number = map(isize, Value::Num);
    let register = map(register_id, Value::Register);
    alt((number, register))(s)
}

fn register_id(s: &str) -> IResult<&str, usize> {
    alt((
        value(0, char('a')),
        value(1, char('b')),
        value(2, char('c')),
        value(3, char('d')),
        value(4, char('e')),
        value(5, char('f')),
        value(6, char('g')),
        value(7, char('h')),
        value(8, char('i')),
        value(9, char('p')),
        value(10, char('w')),
        value(11, char('x')),
        value(12, char('y')),
        value(13, char('z')),
    ))(s)
}

fn isize(s: &str) -> IResult<&str, isize> {
    map_res(i64, isize::try_from)(s)
}
