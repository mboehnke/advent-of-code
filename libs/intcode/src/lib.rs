#[cfg(test)]
mod tests;

use std::collections::VecDeque;
use std::convert::TryInto as _;
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Clone, Debug)]
pub struct Computer {
    memory: Vec<isize>,
    input: VecDeque<isize>,
    ip: usize,
    rb: isize,
    output: Vec<isize>,
    state: State,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum State {
    AwaitingInput,
    Halted,
    Paused,
}

macro_rules! param {
    ($ic:ident, $mode:expr, $offset:expr) => {
        match $mode {
            0 => $ic.memory[$ic.ip + $offset].try_into().unwrap(),
            1 => $ic.ip + $offset,
            _ => ($ic.memory[$ic.ip + $offset] + $ic.rb).try_into().unwrap(),
        }
    };
}

macro_rules! params {
    ($ic:ident, $ins:expr, 1) => {
        param!($ic, ($ins / 100) % 10, 1)
    };
    ($ic:ident, $ins:expr, 2) => {
        (
            param!($ic, ($ins / 100) % 10, 1),
            param!($ic, ($ins / 1000) % 10, 2),
        )
    };
    ($ic:ident, $ins:expr, 3) => {
        (
            param!($ic, ($ins / 100) % 10, 1),
            param!($ic, ($ins / 1000) % 10, 2),
            param!($ic, $ins / 10000, 3),
        )
    };
}

impl Computer {
    pub fn mem_size(mut self, size: usize) -> Self {
        self.memory.resize(size, 0);
        self
    }

    /// # Panics
    /// memory address must be valid
    pub fn peek(&self, addr: usize) -> isize {
        self.memory[addr]
    }

    /// # Panics!
    /// memory address must be valid
    pub fn poke(mut self, addr: usize, data: isize) -> Self {
        self.memory[addr] = data;
        self
    }

    pub fn input<T>(mut self, input: T) -> Self
    where
        T: IntoIterator<Item = isize>,
    {
        for i in input {
            self.input.push_back(i)
        }
        self
    }

    pub fn input_str(self, input: &str) -> Self {
        let input = input
            .chars()
            .map(|c| c as u32 as isize)
            .collect::<Vec<isize>>();
        self.input(input)
    }

    pub fn output(&mut self) -> Vec<isize> {
        let mut o = Vec::new();
        std::mem::swap(&mut o, &mut self.output);
        o
    }

    /// # Panics
    /// outputs must be valid ASCII
    pub fn output_str(&mut self) -> String {
        self.output().into_iter().map(|i| i as u8 as char).collect()
    }

    pub fn is_halted(&self) -> bool {
        self.state == State::Halted
    }
    pub fn is_awaiting_input(&self) -> bool {
        self.state == State::AwaitingInput
    }

    /// # Panics
    /// program must be valid and memory size must be sufficient
    pub fn run(mut self) -> Self {
        loop {
            let instruction = self.memory[self.ip];

            match instruction % 100 {
                // add
                1 => {
                    let (a, b, c) = params!(self, instruction, 3);
                    self.memory[c] = self.memory[a] + self.memory[b];
                    self.ip += 4;
                }
                // mul
                2 => {
                    let (a, b, c) = params!(self, instruction, 3);
                    self.memory[c] = self.memory[a] * self.memory[b];
                    self.ip += 4;
                }
                // input
                3 => {
                    let a = params!(self, instruction, 1);
                    if let Some(val) = self.input.pop_front() {
                        self.memory[a] = val;
                        self.ip += 2
                    } else {
                        self.state = State::AwaitingInput;
                        return self;
                    }
                }
                // output
                4 => {
                    let a = params!(self, instruction, 1);
                    self.output.push(self.memory[a]);
                    self.ip += 2
                }
                // jt
                5 => {
                    let (a, b) = params!(self, instruction, 2);
                    if self.memory[a] != 0 {
                        self.ip = self.memory[b] as usize;
                    } else {
                        self.ip += 3
                    }
                }
                // jf
                6 => {
                    let (a, b) = params!(self, instruction, 2);
                    if self.memory[a] == 0 {
                        self.ip = self.memory[b] as usize;
                    } else {
                        self.ip += 3
                    }
                }
                // lt
                7 => {
                    let (a, b, c) = params!(self, instruction, 3);
                    self.memory[c] = (self.memory[a] < self.memory[b]).into();
                    self.ip += 4;
                }
                // eq
                8 => {
                    let (a, b, c) = params!(self, instruction, 3);
                    self.memory[c] = (self.memory[a] == self.memory[b]).into();
                    self.ip += 4;
                }
                // lrb
                9 => {
                    let a = params!(self, instruction, 1);
                    self.rb += self.memory[a];
                    self.ip += 2;
                }
                // hcf
                _ => {
                    self.state = State::Halted;
                    return self;
                }
            }
        }
    }
}

impl FromStr for Computer {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Computer {
            memory: s
                .trim()
                .split(',')
                .map(str::parse)
                .collect::<Result<_, _>>()?,
            input: VecDeque::new(),
            output: Vec::new(),
            ip: 0,
            rb: 0,
            state: State::Paused,
        })
    }
}
