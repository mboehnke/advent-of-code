use crate::*;

use itertools::iproduct;

const SOL_1: &str = include_str!("../../data/201902sol1");
const SOL_2: &str = include_str!("../../data/201902sol2");
const CODE: &str = include_str!("../../data/201902input");

#[test]
fn aoc_2019_02_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap();
    let res = computer.poke(1, 12).poke(2, 2).run().peek(0).to_string();
    assert_eq!(res, SOL_1);
}

#[test]
fn aoc_2019_02_2_solution() {
    let computer = CODE.parse::<Computer>().unwrap();
    let res = iproduct!(0..=99, 0..=99)
        .find(|&(a, b)| computer.clone().poke(1, a).poke(2, b).run().peek(0) == 19690720)
        .map(|(a, b)| a * 100 + b)
        .unwrap()
        .to_string();
    assert_eq!(res, SOL_2);
}
