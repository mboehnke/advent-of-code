use crate::*;

use itertools::Itertools as _;
use std::mem::swap;

const SOL_1: &str = include_str!("../../data/201923sol1");
const SOL_2: &str = include_str!("../../data/201923sol2");
const CODE: &str = include_str!("../../data/201923input");

#[test]
fn aoc_2019_23_1_solution() {
    let mut computer = CODE.parse::<Computer>().unwrap().mem_size(4096);
    let mut computer2 = computer.clone();
    let mut computers = (0..50)
        .map(|i| computer.clone().input(Some(i)))
        .collect::<Vec<Computer>>();

    for i in (0..50).cycle() {
        swap(&mut computers[i], &mut computer);

        computer = computer.input(Some(-1)).run();
        let outs = computer.output();
        for (&a, &x, &y) in outs.iter().tuples() {
            match a {
                255 => {
                    assert_eq!(y.to_string(), SOL_1);
                    return;
                }
                a => {
                    swap(&mut computers[a as usize], &mut computer2);
                    computer2 = computer2.input(vec![x, y]);
                    swap(&mut computers[a as usize], &mut computer2);
                }
            }
        }
        swap(&mut computers[i], &mut computer);
    }
    panic!()
}

#[test]
fn aoc_2019_23_2_solution() {
    let mut computer = CODE.parse::<Computer>().unwrap().mem_size(4096);
    let mut computer2 = computer.clone();
    let mut computers = (0..50)
        .map(|i| computer.clone().input(Some(i)))
        .collect::<Vec<_>>();

    let mut prev = None;
    let mut nat = (0, 0);
    let mut idle = 0;
    for i in (0..50).cycle() {
        swap(&mut computers[i], &mut computer);

        computer = computer.input(Some(-1)).run();
        let outs = computer.output();
        if outs.len() > 2 {
            for (&a, &x, &y) in outs.iter().tuples() {
                match a {
                    255 => nat = (x, y),
                    _ => {
                        swap(&mut computers[a as usize], &mut computer2);
                        computer2 = computer2.input(vec![x, y]);
                        swap(&mut computers[a as usize], &mut computer2);
                        idle = 0
                    }
                }
            }
        } else {
            idle += 1;
        }
        swap(&mut computers[i], &mut computer);
        if idle == 50 {
            if prev == Some(nat) {
                assert_eq!(nat.1.to_string(), SOL_2);
                return;
            }
            prev = Some(nat);
            swap(&mut computers[0], &mut computer);
            computer = computer.input(vec![nat.0, nat.1]);
            swap(&mut computers[0], &mut computer);
            idle = 0;
        }
    }
    panic!()
}
