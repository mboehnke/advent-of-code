use crate::*;

const SOL_1: &str = include_str!("../../data/201921sol1");
const SOL_2: &str = include_str!("../../data/201921sol2");
const CODE: &str = include_str!("../../data/201921input");

#[test]
fn aoc_2019_21_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(3072);
    let res = run(computer, "NOT C T\nAND D T\nNOT A J\nOR T J\nWALK\n");
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_21_2_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(3072);
    let res = run(
        computer,
        "OR A J\nAND B J\nAND C J\nNOT J J\nAND D J\nOR E T\nOR H T\nAND T J\nRUN\n",
    );
    assert_eq!(res.to_string(), SOL_2);
}

fn run(computer: Computer, code: &str) -> isize {
    *computer.input_str(code).run().output().last().unwrap()
}
