use crate::*;

use std::collections::HashMap;
use std::num::ParseIntError;
use std::str::FromStr;

const SOL_1: &str = include_str!("../../data/201911sol1");
const SOL_2: &str = include_str!("../../data/201911sol2");
const CODE: &str = include_str!("../../data/201911input");

#[test]
fn aoc_2019_11_1_solution() {
    let robot = CODE.parse::<Robot>().unwrap();
    let res = robot.run().painted_panels();
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_11_2_solution() {
    let mut robot = CODE.parse::<Robot>().unwrap();
    robot.paint_panel(1);
    dbg!(robot.clone().run().registration());
    robot
        .run()
        .registration()
        .into_iter()
        .zip(SOL_2.lines())
        .for_each(|(a, b)| assert_eq!(a, *b));
}

#[derive(Debug, Clone)]
pub struct Robot {
    x_pos: isize,
    y_pos: isize,
    direction: char,
    panels: HashMap<(isize, isize), bool>,
    computer: Computer,
}

impl Robot {
    fn run(mut self) -> Robot {
        while !self.computer.is_halted() {
            self.computer = self.computer.run();
            let o = self.computer.output();
            if o.len() > 1 {
                self.paint_panel(o[0]);
                self.turn_and_move(o[1]);
            }
            if self.computer.is_awaiting_input() {
                let color = self.current_panel() as isize;
                self.computer = self.computer.input(vec![color])
            }
        }
        self
    }

    fn turn_and_move(&mut self, dir: isize) {
        self.direction = match self.direction {
            'U' => ['L', 'R'][(dir > 0) as usize],
            'R' => ['U', 'D'][(dir > 0) as usize],
            'D' => ['R', 'L'][(dir > 0) as usize],
            _ => ['D', 'U'][(dir > 0) as usize],
        };
        match self.direction {
            'U' => self.y_pos -= 1,
            'D' => self.y_pos += 1,
            'L' => self.x_pos -= 1,
            _ => self.x_pos += 1,
        }
    }

    fn current_panel(&self) -> bool {
        self.panels.get(&(self.x_pos, self.y_pos)) == Some(&true)
    }

    fn paint_panel(&mut self, color: isize) {
        self.panels.insert((self.x_pos, self.y_pos), color == 1);
    }

    fn registration(&self) -> Vec<String> {
        let min_x = *self.panels.keys().map(|(x, _)| x).min().unwrap() + 1;
        let max_x = *self.panels.keys().map(|(x, _)| x).max().unwrap() - 2;
        let min_y = *self.panels.keys().map(|(_, y)| y).min().unwrap();
        let max_y = *self.panels.keys().map(|(_, y)| y).max().unwrap();
        (min_y..=max_y)
            .map(|y| {
                (min_x..=max_x)
                    .map(move |x| match self.panels.get(&(x, y)) {
                        Some(true) => '█',
                        _ => ' ',
                    })
                    .collect()
            })
            .collect()
    }

    fn painted_panels(&self) -> usize {
        self.panels.len()
    }
}

impl FromStr for Robot {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Robot {
            x_pos: 0,
            y_pos: 0,
            direction: 'U',
            panels: HashMap::new(),
            computer: s.parse::<Computer>()?.mem_size(2048),
        })
    }
}
