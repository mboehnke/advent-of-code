use crate::*;

use itertools::iproduct;

const SOL_1: &str = include_str!("../../data/201919sol1");
const SOL_2: &str = include_str!("../../data/201919sol2");
const CODE: &str = include_str!("../../data/201919input");

#[test]
fn aoc_2019_19_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(512);
    let res = iproduct!((0..50), (0..50))
        .filter(|&(x, y)| check(x, y, &computer))
        .count();
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_19_2_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(512);
    let mut b = vec![];
    let res = (0..)
        .find_map(|i| {
            b.push(boundary(i, &b, &computer));
            ship_fits(&b)
        })
        .map(|(x, y)| x * 10000 + y)
        .unwrap();
    assert_eq!(res.to_string(), SOL_2);
}

fn check(x: usize, y: usize, computer: &Computer) -> bool {
    computer
        .clone()
        .input(vec![x as isize, y as isize])
        .run()
        .output()
        .last()
        != Some(&0)
}

fn boundary(y: usize, boundaries: &[(usize, usize)], computer: &Computer) -> (usize, usize) {
    if y < 99 {
        return (0, 0);
    }
    let (start, mut end) = (boundaries[y - 1].0, boundaries[y - 1].1 + 1);
    let (mut left, mut right) = (0, 0);
    for x in start..(start + 200) {
        if check(x, y, computer) {
            left = x;
            break;
        }
    }
    if y == 99 {
        end = left;
    }
    for x in end..(end + 200) {
        if !check(x, y, computer) {
            right = x - 1;
            break;
        }
    }
    (left, right)
}

fn ship_fits(bounds: &[(usize, usize)]) -> Option<(usize, usize)> {
    if bounds.len() < 100 {
        return None;
    }
    let b = bounds.len() - 1;
    let l = bounds[b].0;
    let (t, r) = (b - 99, l + 99);
    if l < bounds[b].0 || r > bounds[t].1 {
        None
    } else {
        Some((l, t))
    }
}
