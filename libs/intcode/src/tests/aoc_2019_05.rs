use crate::*;

const SOL_1: &str = include_str!("../../data/201905sol1");
const SOL_2: &str = include_str!("../../data/201905sol2");
const CODE: &str = include_str!("../../data/201905input");

#[test]
fn aoc_2019_05_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap();
    let res = *computer.input(Some(1)).run().output().last().unwrap();
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_05_2_solution() {
    let computer = CODE.parse::<Computer>().unwrap();
    let res = *computer.input(Some(5)).run().output().last().unwrap();
    assert_eq!(res.to_string(), SOL_2);
}
