use crate::*;

const SOL_1: &str = include_str!("../../data/201909sol1");
const SOL_2: &str = include_str!("../../data/201909sol2");
const CODE: &str = include_str!("../../data/201909input");

#[test]
fn aoc_2019_09_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(2048);
    let res = *computer.input(Some(1)).run().output().last().unwrap();
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_09_2_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(2048);
    let res = *computer.input(Some(2)).run().output().last().unwrap();
    assert_eq!(res.to_string(), SOL_2);
}
