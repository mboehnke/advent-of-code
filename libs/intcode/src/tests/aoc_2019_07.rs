use crate::*;

use itertools::Itertools as _;
use std::collections::VecDeque;

const SOL_1: &str = include_str!("../../data/201907sol1");
const SOL_2: &str = include_str!("../../data/201907sol2");
const CODE: &str = include_str!("../../data/201907input");

#[test]
fn aoc_2019_07_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(512);
    let res = (0..5)
        .permutations(5)
        .map(|settings| {
            settings
                .into_iter()
                .map(|s| computer.clone().input(Some(s)).run())
                .fold(0, |i, t| *t.input(Some(i)).run().output().last().unwrap())
        })
        .max()
        .unwrap();
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_07_2_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(512);
    let res = (5..10)
        .permutations(5)
        .map(|settings| {
            let mut amps = settings
                .iter()
                .map(|s| computer.clone().input(Some(*s)))
                .collect::<VecDeque<Computer>>();
            let mut s = 0;
            while let Some(mut amp) = amps.pop_front() {
                amp = amp.input(Some(s)).run();
                if let Some(o) = amp.output().last() {
                    s = *o;
                    amps.push_back(amp);
                }
            }
            s
        })
        .max()
        .unwrap();
    assert_eq!(res.to_string(), SOL_2);
}
