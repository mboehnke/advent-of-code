use crate::*;

use itertools::Itertools as _;

const SOL_1: &str = include_str!("../../data/201913sol1");
const SOL_2: &str = include_str!("../../data/201913sol2");
const CODE: &str = include_str!("../../data/201913input");

#[test]
fn aoc_2019_13_1_solution() {
    let computer = CODE.parse::<Computer>().unwrap().mem_size(3072);
    let res = computer
        .run()
        .output()
        .into_iter()
        .skip(2)
        .step_by(3)
        .filter(|&z| z == 2)
        .count();
    assert_eq!(res.to_string(), SOL_1);
}

#[test]
fn aoc_2019_13_2_solution() {
    let mut computer = CODE.parse::<Computer>().unwrap().mem_size(3072);
    computer = computer.poke(0, 2).run();
    let (mut paddle, mut ball, mut score) = (0, 0, 0);
    loop {
        for (&x, _, &z) in computer.output().iter().tuples() {
            match z {
                3 => paddle = x,
                4 => ball = x,
                s if x == -1 => score = s,
                _ => {}
            }
        }
        if computer.is_halted() {
            assert_eq!(score.to_string(), SOL_2);
            return;
        }
        if computer.is_awaiting_input() {
            computer = computer.input(vec![ball - paddle]).run()
        }
    }
}
