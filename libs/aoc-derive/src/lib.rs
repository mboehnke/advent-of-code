extern crate proc_macro;

use std::path::PathBuf;

use apply::Apply as _;
use proc_macro::TokenStream;
use quote::{format_ident, quote};

#[proc_macro_derive(Aoc)]
pub fn aoc_derive(input: TokenStream) -> TokenStream {
    let ast: syn::DeriveInput = syn::parse(input).unwrap();

    let name = &ast.ident;

    let data = gen_data();
    let main = gen_main(name);
    let tests = gen_tests(name);

    let gen = quote! {
        #data
        #main
        #tests
    };
    gen.into()
}

fn gen_main(name: &syn::Ident) -> proc_macro2::TokenStream {
    quote! {
        pub fn main() -> Result<(), Box<dyn std::error::Error>> {
            use aoc_data::*;

            use aoc::Solution as _;

            #name::report(INPUT,SOL1,SOL2).print();

            Ok(())
        }
    }
}

fn gen_data() -> proc_macro2::TokenStream {
    let files = get_files().unwrap_or_default().into_iter().map(|file| {
        let ident = format_ident!("{}", file.to_ascii_uppercase());
        let path = format!("../data/{}", file);
        quote! {
            pub const #ident: &str = include_str!(#path);
        }
    });

    quote! {
        pub mod aoc_data {
            #(#files)*
        }
    }
}

fn gen_tests(name: &syn::Ident) -> proc_macro2::TokenStream {
    let example_tests = get_files()
        .unwrap_or_default()
        .into_iter()
        .flat_map(|f| {
            f.strip_prefix("ex")
                .and_then(|f| f.split_once("sol"))
                .map(|(a, b)| (a.to_string(), b.to_string()))
        })
        .map(|(ex, sol)| {
            let test_name = format_ident!("example{}_part{}", &ex, &sol);
            let part = format_ident!("part{}", sol);
            let input = format_ident!("EX{}INPUT", ex);
            let sol = format_ident!("EX{}SOL{}", ex, sol);
            quote! {
                #[test]
                fn #test_name() {
                    let data=#name::parser(#input).unwrap().1;
                    assert_eq!(#sol, #name::#part(data).to_string());
                }
            }
        });

    quote! {
        mod tests {
            use crate::{#name, aoc_data::*};

            use aoc::Solution as _;

            #[test]
            fn part1() {
                let data=#name::parser(INPUT).unwrap().1;
                assert_eq!(SOL1, #name::part1(data).to_string());
            }

            #[test]
            fn part2() {
                let data=#name::parser(INPUT).unwrap().1;
                assert_eq!(SOL2, #name::part2(data).to_string());
            }

            #(#example_tests)*
        }
    }
}

fn get_files() -> Result<Vec<String>, Box<dyn std::error::Error>> {
    std::env::var("CARGO_MANIFEST_DIR")?
        .apply(PathBuf::from)
        .join("data")
        .apply(std::fs::read_dir)?
        .flat_map(Result::ok)
        .map(|d| d.path())
        .filter(|p| p.is_file())
        .flat_map(|p| p.file_name().and_then(|s| s.to_str()).map(String::from))
        .collect::<Vec<_>>()
        .apply(Ok)
}
