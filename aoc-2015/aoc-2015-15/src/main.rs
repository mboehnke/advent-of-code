use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone, Debug)]
pub struct Ingredient {
    capacity: i64,
    durability: i64,
    flavor: i64,
    texture: i64,
    calories: i64,
}

impl Solution<i64, i64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Science for Hungry People";

    type Data = Vec<Ingredient>;

    fn part1(ingredients: Self::Data) -> i64 {
        max_score(&ingredients, None)
    }

    fn part2(ingredients: Self::Data) -> i64 {
        max_score(&ingredients, Some(500))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::ingredient_list(s)
    }
}

fn max_score(ingredients: &[Ingredient], calories: Option<i64>) -> i64 {
    let mut max = 0;
    for amount1 in 0..=100 {
        for amount2 in 0..=100 - amount1 {
            for amount3 in 0..=100 - amount1 - amount2 {
                let amounts = [amount1, amount2, amount3, 100 - amount1 - amount2 - amount3];

                if let Some(calories) = calories {
                    if (0..4)
                        .map(|i| amounts[i] * ingredients[i].calories)
                        .sum::<i64>()
                        != calories
                    {
                        continue;
                    }
                }

                let capacity = (0..4).map(|i| amounts[i] * ingredients[i].capacity).sum();
                let durability = (0..4).map(|i| amounts[i] * ingredients[i].durability).sum();
                let flavor = (0..4).map(|i| amounts[i] * ingredients[i].flavor).sum();
                let texture = (0..4).map(|i| amounts[i] * ingredients[i].texture).sum();
                let score = [capacity, durability, flavor, texture]
                    .iter()
                    .map(|&x| if x < 0 { 0 } else { x })
                    .product::<i64>();
                if score > max {
                    max = score
                }
            }
        }
    }
    max
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{alpha1, i64, newline},
        multi::separated_list0,
        sequence::preceded,
        IResult,
    };

    use crate::Ingredient;

    pub fn ingredient_list(s: &str) -> IResult<&str, Vec<Ingredient>> {
        separated_list0(newline, ingredient)(s)
    }

    fn ingredient(s: &str) -> IResult<&str, Ingredient> {
        let (s, _name) = alpha1(s)?;
        let (s, capacity) = preceded(tag(": capacity "), i64)(s)?;
        let (s, durability) = preceded(tag(", durability "), i64)(s)?;
        let (s, flavor) = preceded(tag(", flavor "), i64)(s)?;
        let (s, texture) = preceded(tag(", texture "), i64)(s)?;
        let (s, calories) = preceded(tag(", calories "), i64)(s)?;
        let ingredient = Ingredient {
            capacity,
            durability,
            flavor,
            texture,
            calories,
        };
        Ok((s, ingredient))
    }
}
