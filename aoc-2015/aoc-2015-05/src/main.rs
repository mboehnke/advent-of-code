#![allow(clippy::ptr_arg)]

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 5;
    const TITLE: &'static str = "Doesn't He Have Intern-Elves For This?";

    type Data = Vec<String>;

    fn part1(input: Self::Data) -> usize {
        input
            .into_iter()
            .filter(three_vowels)
            .filter(double_letter)
            .filter(excludes_patterns)
            .count()
    }

    fn part2(input: Self::Data) -> usize {
        input.into_iter().filter(is_nice).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{alpha1, newline},
            combinator::map,
            multi::separated_list0,
        };

        separated_list0(newline, map(alpha1, String::from))(s)
    }
}

fn three_vowels(s: &String) -> bool {
    s.bytes().filter(|c| b"aeiou".contains(c)).count() >= 3
}

fn double_letter(s: &String) -> bool {
    s.as_bytes().windows(2).any(|w| w[0] == w[1])
}

fn excludes_patterns(s: &String) -> bool {
    s.as_bytes()
        .windows(2)
        .all(|w| w != b"ab" && w != b"cd" && w != b"pq" && w != b"xy")
}

fn is_nice(s: &String) -> bool {
    let s = s.as_bytes();
    (0..s.len() - 3).any(|i| (i + 2..s.len() - 1).any(|j| s[i..i + 2] == s[j..j + 2]))
        && (0..s.len() - 2).any(|i| s[i] == s[i + 2])
}
