use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<i64, i64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 11;
    const TITLE: &'static str = "JSAbacusFramework.io";

    type Data = String;

    fn part1(input: Self::Data) -> i64 {
        calc_sum(&input)
    }

    fn part2(mut json: Self::Data) -> i64 {
        while json.contains('}') {
            let cb = json.chars().enumerate().find(|(_, c)| *c == '}').unwrap().0;
            let ob = (0..cb)
                .rev()
                .find(|i| json.chars().nth(*i).unwrap() == '{')
                .unwrap();
            let object = json.chars().skip(ob).take(cb - ob + 1).collect::<String>();
            let r = if object.contains(r#":"red""#) {
                "".to_string()
            } else {
                calc_sum(&object).to_string()
            };
            json = json.replace(&object, &r);
        }

        json.as_str().apply(calc_sum)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::not_line_ending, combinator::map};

        map(not_line_ending, String::from)(s)
    }
}

fn calc_sum(s: &str) -> i64 {
    s.chars()
        .map(|c| if c == '-' || c.is_numeric() { c } else { ' ' })
        .collect::<String>()
        .split_whitespace()
        .map(|s| s.parse::<i64>().unwrap())
        .sum()
}
