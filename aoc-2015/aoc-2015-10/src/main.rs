use std::fmt::Write;

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Elves Look, Elves Say";

    type Data = String;

    fn part1(x: Self::Data) -> usize {
        look_and_say(x, 40)
    }

    fn part2(x: Self::Data) -> usize {
        look_and_say(x, 50)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::digit1, combinator::map};

        map(digit1, String::from)(s)
    }
}

fn look_and_say(mut x: String, reps: usize) -> usize {
    for _ in 0..reps {
        let mut chars = x.chars();
        let mut output = vec![(chars.next().unwrap(), 1)];
        for ch in chars {
            let last = output.last_mut().unwrap();
            if last.0 == ch {
                last.1 += 1;
            } else {
                output.push((ch, 1))
            }
        }
        x = output.iter().fold(String::new(), |mut res, x| {
            write!(res, "{}{}", x.1, x.0).unwrap();
            res
        })
    }
    x.chars().count()
}
