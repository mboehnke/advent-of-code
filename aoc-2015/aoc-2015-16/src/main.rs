use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone, Debug)]
pub struct Sue {
    number: u16,
    children: Option<u16>,
    cats: Option<u16>,
    samoyeds: Option<u16>,
    pomeranians: Option<u16>,
    akitas: Option<u16>,
    vizslas: Option<u16>,
    goldfish: Option<u16>,
    trees: Option<u16>,
    cars: Option<u16>,
    perfumes: Option<u16>,
}

impl Solution<u16, u16> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Aunt Sue";

    type Data = Vec<Sue>;

    fn part1(sues: Self::Data) -> u16 {
        sues.into_iter()
            .find(|sue| {
                sue.children.unwrap_or(3) == 3
                    && sue.cats.unwrap_or(7) == 7
                    && sue.samoyeds.unwrap_or(2) == 2
                    && sue.pomeranians.unwrap_or(3) == 3
                    && sue.akitas.unwrap_or(0) == 0
                    && sue.vizslas.unwrap_or(0) == 0
                    && sue.goldfish.unwrap_or(5) == 5
                    && sue.trees.unwrap_or(3) == 3
                    && sue.cars.unwrap_or(2) == 2
                    && sue.perfumes.unwrap_or(1) == 1
            })
            .unwrap()
            .number
    }

    fn part2(sues: Self::Data) -> u16 {
        sues.into_iter()
            .find(|sue| {
                sue.children.unwrap_or(3) == 3
                    && sue.cats.unwrap_or(u16::MAX) > 7
                    && sue.samoyeds.unwrap_or(2) == 2
                    && sue.pomeranians.unwrap_or(0) < 3
                    && sue.akitas.unwrap_or(0) == 0
                    && sue.vizslas.unwrap_or(0) == 0
                    && sue.goldfish.unwrap_or(0) < 5
                    && sue.trees.unwrap_or(u16::MAX) > 3
                    && sue.cars.unwrap_or(2) == 2
                    && sue.perfumes.unwrap_or(1) == 1
            })
            .unwrap()
            .number
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::sue_list(s)
    }
}

mod parser {
    use std::collections::HashMap;

    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{alpha1, newline, u16},
        multi::separated_list0,
        sequence::{delimited, separated_pair},
        IResult,
    };

    use crate::Sue;

    pub fn sue_list(s: &str) -> IResult<&str, Vec<Sue>> {
        separated_list0(newline, sue)(s)
    }

    fn sue(s: &str) -> IResult<&str, Sue> {
        let (s, number) = delimited(tag("Sue "), u16, tag(": "))(s)?;
        let (s, things) = separated_list0(tag(", "), thing)(s)?;
        let things = things.into_iter().collect::<HashMap<_, _>>();
        let sue = Sue {
            number,
            children: things.get("children").copied(),
            cats: things.get("cats").copied(),
            samoyeds: things.get("samoyeds").copied(),
            pomeranians: things.get("pomeranians").copied(),
            akitas: things.get("akitas").copied(),
            vizslas: things.get("vizslas").copied(),
            goldfish: things.get("goldfish").copied(),
            trees: things.get("trees").copied(),
            cars: things.get("cars").copied(),
            perfumes: things.get("perfumes").copied(),
        };
        Ok((s, sue))
    }

    fn thing(s: &str) -> IResult<&str, (&str, u16)> {
        separated_pair(alpha1, tag(": "), u16)(s)
    }
}
