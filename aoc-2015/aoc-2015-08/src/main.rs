use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Matchsticks";

    type Data = Vec<String>;

    fn part1(input: Self::Data) -> usize {
        input.into_iter().map(overhead).sum()
    }

    fn part2(input: Self::Data) -> usize {
        input
            .into_iter()
            .map(|s| s.chars().filter(|&c| c == '\\' || c == '\"').count() + 2)
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::string_list(s)
    }
}

fn overhead(s: String) -> usize {
    let mut chars = s.chars().peekable();
    let mut res = 2;
    while let Some(c) = chars.next() {
        if c == '\\' {
            if let Some(n) = chars.peek().map(|&c| match c {
                '\\' | '\"' => 1,
                'x' => 3,
                _ => 0,
            }) {
                res += n;
                chars.nth(n - 1);
            }
        }
    }
    res
}
