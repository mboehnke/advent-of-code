use aoc::{Aoc, Solution};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Wire {
    Val(String),
    And(String, String),
    Or(String, String),
    Not(String),
    LShift(String, u32),
    RShift(String, u32),
}

#[derive(Debug, Clone)]
pub struct Wires(HashMap<String, Wire>);

impl Solution<u64, u64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 7;
    const TITLE: &'static str = "Some Assembly Required";

    type Data = Wires;

    fn part1(mut input: Self::Data) -> u64 {
        input.eval("a")
    }

    fn part2(mut input: Self::Data) -> u64 {
        let sig = input.clone().eval("a").to_string();
        input.0.insert("b".into(), Wire::Val(sig));
        input.eval("a")
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::wires(s)
    }
}

impl Wires {
    fn eval(&mut self, w: &str) -> u64 {
        if let Ok(num) = w.parse() {
            return num;
        }
        let out = match self.0[w].clone() {
            Wire::Val(val) => self.eval(&val),
            Wire::And(x, y) => self.eval(&x) & self.eval(&y),
            Wire::Or(x, y) => self.eval(&x) | self.eval(&y),
            Wire::Not(x) => !self.eval(&x),
            Wire::LShift(x, n) => self.eval(&x) << n,
            Wire::RShift(x, n) => self.eval(&x) >> n,
        };
        self.0.insert(w.to_string(), Wire::Val(out.to_string()));
        out
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alphanumeric1, newline, u32},
        multi::separated_list0,
        sequence::{preceded, separated_pair},
        IResult,
    };
    use hashbrown::HashMap;

    use super::{Wire, Wires};

    pub fn wires(s: &str) -> IResult<&str, Wires> {
        let (s, list) = separated_list0(newline, id_wire)(s)?;
        Ok((s, Wires(HashMap::from_iter(list))))
    }

    fn id_wire(s: &str) -> IResult<&str, (String, Wire)> {
        let (s, (w, id)) = separated_pair(wire, tag(" -> "), alphanumeric1)(s)?;
        Ok((s, (id.to_string(), w)))
    }

    fn wire(s: &str) -> IResult<&str, Wire> {
        alt((and, or, not, lshift, rshift, val))(s)
    }

    fn not(s: &str) -> IResult<&str, Wire> {
        let (s, sig) = preceded(tag("NOT "), alphanumeric1)(s)?;
        Ok((s, Wire::Not(sig.to_string())))
    }

    fn and(s: &str) -> IResult<&str, Wire> {
        let (s, (a, b)) = separated_pair(alphanumeric1, tag(" AND "), alphanumeric1)(s)?;
        Ok((s, Wire::And(a.to_string(), b.to_string())))
    }

    fn or(s: &str) -> IResult<&str, Wire> {
        let (s, (a, b)) = separated_pair(alphanumeric1, tag(" OR "), alphanumeric1)(s)?;
        Ok((s, Wire::Or(a.to_string(), b.to_string())))
    }

    fn lshift(s: &str) -> IResult<&str, Wire> {
        let (s, (a, b)) = separated_pair(alphanumeric1, tag(" LSHIFT "), u32)(s)?;
        Ok((s, Wire::LShift(a.to_string(), b)))
    }

    fn rshift(s: &str) -> IResult<&str, Wire> {
        let (s, (a, b)) = separated_pair(alphanumeric1, tag(" RSHIFT "), u32)(s)?;
        Ok((s, Wire::RShift(a.to_string(), b)))
    }

    fn val(s: &str) -> IResult<&str, Wire> {
        let (s, v) = alphanumeric1(s)?;
        Ok((s, Wire::Val(v.to_string())))
    }
}
