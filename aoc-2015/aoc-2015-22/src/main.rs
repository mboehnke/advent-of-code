use aoc::{Aoc, Solution};
use std::collections::HashMap;

#[derive(Aoc)]
pub struct Sol;

#[derive(Eq, PartialEq, Hash, Clone, Debug)]
pub enum Effect {
    Shield,
    Poison,
    Recharge,
}

#[derive(Debug, Clone)]
pub struct State {
    hard_mode: bool,
    max_mana: i32,
    player_hp: i32,
    player_armor: i32,
    player_mana: i32,
    player_mana_spent: i32,
    boss_hp: i32,
    boss_damage: i32,
    effects: HashMap<Effect, i32>,
}

impl Solution<i32, i32> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Wizard Simulator 20XX";

    type Data = (i32, i32);

    fn part1(boss: Self::Data) -> i32 {
        min_mana(boss, false)
    }

    fn part2(boss: Self::Data) -> i32 {
        min_mana(boss, true)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::take_till,
            character::complete::i32,
            sequence::{preceded, tuple},
            IResult,
        };

        fn num(s: &str) -> IResult<&str, i32> {
            preceded(take_till(|c: char| c.is_ascii_digit()), i32)(s)
        }

        tuple((num, num))(s)
    }
}

fn min_mana((boss_hp, boss_damage): (i32, i32), hard_mode: bool) -> i32 {
    (1000..)
        .step_by(300)
        .map(|max_mana| State {
            hard_mode,
            max_mana,
            boss_hp,
            boss_damage,
            player_hp: 50,
            player_mana: 500,
            player_mana_spent: 0,
            player_armor: 0,
            effects: HashMap::new(),
        })
        .flat_map(|s| {
            s.player_turn()
                .iter()
                .filter(|&&(_, w)| w)
                .map(|&(m, _)| m)
                .min()
        })
        .next()
        .unwrap()
}

impl State {
    fn apply_effects(mut self) -> Self {
        // apply effects
        if self.effects.contains_key(&Effect::Poison) {
            self.boss_hp -= 3
        }
        self.player_armor = if self.effects.contains_key(&Effect::Shield) {
            7
        } else {
            0
        };
        if self.effects.contains_key(&Effect::Recharge) {
            self.player_mana += 101
        }

        let effects = self
            .effects
            .iter()
            .filter_map(|(e, d)| {
                if *d > 1 {
                    Some((e.clone(), *d - 1))
                } else {
                    None
                }
            })
            .collect::<HashMap<_, _>>();
        self.effects = effects;
        self
    }

    fn boss_turn(self) -> Vec<(i32, bool)> {
        if self.player_mana_spent >= self.max_mana {
            return vec![(self.player_mana_spent, false)];
        }

        let mut s = self.apply_effects();

        if s.boss_hp <= 0 {
            return vec![(s.player_mana_spent, true)];
        }

        s.player_hp -= (s.boss_damage - s.player_armor).max(1);
        if s.player_hp <= 0 {
            vec![(s.player_mana_spent, false)]
        } else {
            s.player_turn()
        }
    }

    fn player_turn(mut self) -> Vec<(i32, bool)> {
        if self.hard_mode {
            self.player_hp -= 1;
        }
        if self.player_hp <= 0 {
            return vec![(self.player_mana_spent, false)];
        }

        let s = self.apply_effects();

        // player attacks
        // magic missile
        if s.player_mana < 53 {
            return vec![(s.player_mana_spent, false)];
        }

        //magic missile
        let mut fights = {
            let mut s = s.clone();
            s.player_mana -= 53;
            s.player_mana_spent += 53;
            s.boss_hp -= 4;
            s.boss_turn()
        };

        // drain
        if s.player_mana >= 73 {
            fights.extend_from_slice(&{
                let mut s = s.clone();
                s.player_mana -= 73;
                s.player_mana_spent += 73;
                s.boss_hp -= 2;
                s.player_hp += 2;
                s.boss_turn()
            })
        }

        // shield
        if s.player_mana >= 113 {
            fights.extend_from_slice(&{
                let mut s = s.clone();
                s.player_mana -= 113;
                s.player_mana_spent += 113;
                s.effects.insert(Effect::Shield, 6);
                s.boss_turn()
            })
        }

        // poison
        if s.player_mana >= 173 {
            fights.extend_from_slice(&{
                let mut s = s.clone();
                s.player_mana -= 173;
                s.player_mana_spent += 173;
                s.effects.insert(Effect::Poison, 6);
                s.boss_turn()
            })
        }

        // recharge
        if s.player_mana >= 229 {
            fights.extend_from_slice(&{
                let mut s = s;
                s.player_mana -= 229;
                s.player_mana_spent += 229;
                s.effects.insert(Effect::Recharge, 5);
                s.boss_turn()
            })
        }

        fights
    }
}
