use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Cmd {
    On,
    Off,
    Toggle,
}

pub type Command = (Cmd, usize, usize, usize, usize);

impl Solution<usize, u64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Probably a Fire Hazard";

    type Data = Vec<Command>;

    fn part1(input: Self::Data) -> usize {
        let mut lights = [[false; 1000]; 1000];

        for (cmd, x1, y1, x2, y2) in input {
            for col in &mut lights[x1..=x2] {
                for cell in &mut col[y1..=y2] {
                    match cmd {
                        Cmd::On => *cell = true,
                        Cmd::Off => *cell = false,
                        Cmd::Toggle => *cell = !*cell,
                    }
                }
            }
        }

        lights.iter().flatten().filter(|&&a| a).count()
    }

    fn part2(input: Self::Data) -> u64 {
        let mut lights = [[0u8; 1000]; 1000];

        for (cmd, x1, y1, x2, y2) in input {
            for cols in &mut lights[x1..=x2] {
                for cell in &mut cols[y1..=y2] {
                    *cell = match cmd {
                        Cmd::On => *cell + 1,
                        Cmd::Off => (*cell).saturating_sub(1),
                        Cmd::Toggle => *cell + 2,
                    };
                }
            }
        }

        lights.iter().flatten().copied().map(u64::from).sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::command_list(s)
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline},
        combinator::value,
        multi::separated_list0,
        sequence::separated_pair,
        usize, IResult,
    };

    use super::{Cmd, Command};

    pub fn command_list(s: &str) -> IResult<&str, Vec<Command>> {
        separated_list0(newline, command)(s)
    }

    fn command(s: &str) -> IResult<&str, Command> {
        let (s, cmd) = cmd(s)?;
        let (s, ((x1, y1), (x2, y2))) = separated_pair(coords, tag(" through "), coords)(s)?;
        Ok((s, (cmd, x1, y1, x2, y2)))
    }

    fn coords(s: &str) -> IResult<&str, (usize, usize)> {
        separated_pair(usize, char(','), usize)(s)
    }

    fn cmd(s: &str) -> IResult<&str, Cmd> {
        let on = value(Cmd::On, tag("turn on "));
        let off = value(Cmd::Off, tag("turn off "));
        let toggle = value(Cmd::Toggle, tag("toggle "));
        alt((on, off, toggle))(s)
    }
}
