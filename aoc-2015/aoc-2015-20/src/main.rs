use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 20;
    const TITLE: &'static str = "Infinite Elves and Infinite Houses";

    type Data = usize;

    fn part1(num: Self::Data) -> usize {
        house_number(num, 10, None).unwrap()
    }

    fn part2(num: Self::Data) -> usize {
        house_number(num, 11, Some(50)).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize(s)
    }
}

fn house_number(
    num_presents: usize,
    presents_per_house: usize,
    max_houses: Option<usize>,
) -> Option<usize> {
    (1..num_presents / presents_per_house)
        .scan(vec![0; num_presents / presents_per_house], |houses, i| {
            (i..num_presents / presents_per_house)
                .step_by(i)
                .take(max_houses.unwrap_or(usize::MAX))
                .for_each(|j| houses[j] += i * presents_per_house);
            Some((i, houses[i]))
        })
        .find(|&(_, p)| p >= num_presents)
        .map(|(n, _)| n)
}
