use aoc::{Aoc, Solution};
use permutator::Permutation as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Map {
    locations: Vec<String>,
    distances: Vec<(usize, usize, u64)>,
}

impl Solution<u64, u64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 9;
    const TITLE: &'static str = "All in a Single Night";

    type Data = Map;

    fn part1(map: Self::Data) -> u64 {
        map.route_lengths().into_iter().min().unwrap()
    }

    fn part2(map: Self::Data) -> u64 {
        map.route_lengths().into_iter().max().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::map_p(s)
    }
}

impl Map {
    fn route_lengths(self) -> Vec<u64> {
        (0..self.locations.len())
            .collect::<Vec<_>>()
            .permutation()
            .map(|route| {
                route
                    .windows(2)
                    .map(|l| self.distance(l[0], l[1]))
                    .sum::<u64>()
            })
            .collect()
    }

    fn distance(&self, loc_a: usize, loc_b: usize) -> u64 {
        *self
            .distances
            .iter()
            .filter(|(a, b, _)| loc_a == *a && loc_b == *b || loc_a == *b && loc_b == *a)
            .map(|(_, _, d)| d)
            .next()
            .unwrap()
    }
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{alphanumeric1, newline, u64},
        combinator::map,
        multi::separated_list0,
        sequence::{terminated, tuple},
        IResult,
    };
    use itertools::Itertools;

    use crate::Map;

    pub fn map_p(s: &str) -> IResult<&str, Map> {
        let (s, routes) = separated_list0(newline, route)(s)?;

        let locations = routes
            .iter()
            .flat_map(|(a, b, _)| vec![a, b])
            .unique()
            .map(String::from)
            .collect::<Vec<_>>();

        let l_index = |name| {
            locations
                .iter()
                .enumerate()
                .find(|&(_, l)| *l == name)
                .unwrap()
                .0
        };

        let distances = routes
            .into_iter()
            .map(|(a, b, dist)| (l_index(a), l_index(b), dist))
            .collect::<Vec<(usize, usize, u64)>>();

        let map = Map {
            locations,
            distances,
        };

        Ok((s, map))
    }

    fn route(s: &str) -> IResult<&str, (String, String, u64)> {
        tuple((
            terminated(map(alphanumeric1, String::from), tag(" to ")),
            terminated(map(alphanumeric1, String::from), tag(" = ")),
            u64,
        ))(s)
    }
}
