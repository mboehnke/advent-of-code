use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 25;
    const TITLE: &'static str = "Let It Snow";

    type Data = (usize, usize);

    fn part1((row, col): Self::Data) -> usize {
        let index = (((row + col - 1).pow(2) + row + col - 1) / 2) - ((row + col - 1) - col);
        (1..index).fold(20151125usize, |acc, _| (acc * 252533) % 33554393)
    }

    fn part2(_: Self::Data) -> String {
        String::new()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::anychar, multi::many_till, usize};

        let (s, (_, n1)) = many_till(anychar, usize)(s)?;
        let (s, (_, n2)) = many_till(anychar, usize)(s)?;
        Ok((s, (n1, n2)))
    }
}
