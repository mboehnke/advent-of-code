use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone, Debug)]
pub struct Reindeer {
    speed: u64,
    fly_time: u64,
    rest_time: u64,
}

impl Solution<u64, u64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 14;
    const TITLE: &'static str = "Reindeer Olympics";

    type Data = Vec<Reindeer>;

    fn part1(reindeer: Self::Data) -> u64 {
        reindeer
            .into_iter()
            .map(|r| r.distance_at_time(2503))
            .max()
            .unwrap()
    }

    fn part2(reindeer: Self::Data) -> u64 {
        let mut reindeer = reindeer.into_iter().map(|r| (r, 0)).collect_vec();

        for seconds in 1..2504 {
            let max_dist = reindeer
                .iter_mut()
                .map(|r| r.0.distance_at_time(seconds))
                .max()
                .unwrap();

            for r in reindeer.iter_mut() {
                if r.0.distance_at_time(seconds) == max_dist {
                    r.1 += 1
                }
            }
        }

        reindeer.iter().map(|r| r.1).max().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::reindeer_list(s)
    }
}

impl Reindeer {
    fn distance_at_time(&self, seconds: u64) -> u64 {
        let cycle_len = self.fly_time + self.rest_time;
        let full_cycles = seconds / cycle_len;
        let remaining_time = seconds - full_cycles * cycle_len;
        let fly_time = if remaining_time < self.fly_time {
            remaining_time
        } else {
            self.fly_time
        } + full_cycles * self.fly_time;

        fly_time * self.speed
    }
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{alpha1, newline, u64},
        multi::separated_list0,
        sequence::terminated,
        IResult,
    };

    use crate::Reindeer;

    pub fn reindeer_list(s: &str) -> IResult<&str, Vec<Reindeer>> {
        separated_list0(newline, reindeer)(s)
    }

    fn reindeer(s: &str) -> IResult<&str, Reindeer> {
        let (s, _name) = terminated(alpha1, tag(" can fly "))(s)?;
        let (s, speed) = terminated(u64, tag(" km/s for "))(s)?;
        let (s, fly_time) = terminated(u64, tag(" seconds, but then must rest for "))(s)?;
        let (s, rest_time) = terminated(u64, tag(" seconds."))(s)?;
        let reindeer = Reindeer {
            speed,
            fly_time,
            rest_time,
        };
        Ok((s, reindeer))
    }
}
