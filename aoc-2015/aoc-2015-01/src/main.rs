use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<i64, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Not Quite Lisp";

    type Data = Vec<i64>;

    fn part1(input: Self::Data) -> i64 {
        input.into_iter().sum()
    }

    fn part2(input: Self::Data) -> usize {
        input
            .into_iter()
            .scan(0, |sum, x| {
                *sum += x;
                Some(*sum)
            })
            .take_while(|&x| x != -1)
            .count()
            + 1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{branch::alt, character::complete::char, combinator::value, multi::many0};

        let obr = value(1, char('('));
        let cbr = value(-1, char(')'));
        many0(alt((obr, cbr)))(s)
    }
}
