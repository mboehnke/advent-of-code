use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Password(Vec<u8>);

#[derive(Debug, Clone)]
pub struct ValidPassword(Password);

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Corporate Policy";

    type Data = ValidPassword;

    fn part1(mut password: Self::Data) -> String {
        password.next().unwrap()
    }

    fn part2(mut password: Self::Data) -> String {
        password.nth(1).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::alpha1, combinator::map};

        map(alpha1, |s: &str| {
            s.as_bytes().to_vec().apply(Password).apply(ValidPassword)
        })(s)
    }
}

impl Iterator for Password {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        *self.0.last_mut().unwrap() += 1;

        for i in (1..self.0.len()).rev() {
            let curr = self.0.get_mut(i).unwrap();
            if *curr > b'z' {
                *curr = b'a';
                *self.0.get_mut(i - 1).unwrap() += 1;
            }
        }

        let first = self.0.first_mut().unwrap();
        if *first > b'z' {
            *first = b'a';
            self.0.insert(0, b'a');
        }

        Some(self.0.clone())
    }
}

impl Iterator for ValidPassword {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.0
            .clone()
            .filter(|p| p.windows(3).any(|w| w[0] + 1 == w[1] && w[0] + 2 == w[2]))
            .filter(|p| p.iter().all(|&c| c != b'i' && c != b'o' && c != b'l'))
            .find(|p| {
                let pairs = (0..p.len() - 1)
                    .filter(|i| p[*i] == p[i + 1])
                    .collect::<Vec<_>>();
                pairs.first().copied().unwrap_or_default() + 1
                    < pairs.last().copied().unwrap_or_default()
            })
            .map(|pw| {
                self.0 = Password(pw.clone());
                String::from_utf8(pw).unwrap()
            })
    }
}
