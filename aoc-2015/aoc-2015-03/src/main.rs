use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Perfectly Spherical Houses in a Vacuum";

    type Data = String;

    fn part1(input: Self::Data) -> usize {
        input
            .chars()
            .scan((0, 0), |(x, y), c| {
                match c {
                    '^' => *y -= 1,
                    '<' => *x -= 1,
                    '>' => *x += 1,
                    _ => *y += 1,
                };
                Some((*x, *y))
            })
            .chain(Some((0, 0)))
            .unique()
            .count()
    }

    fn part2(input: Self::Data) -> usize {
        let mut pos = [0, 0, 0, 0];
        input
            .chars()
            .zip([0, 2].iter().cycle())
            .map(|(c, &i)| {
                match c {
                    '^' => pos[1 + i] -= 1,
                    '<' => pos[i] -= 1,
                    '>' => pos[i] += 1,
                    _ => pos[1 + i] += 1,
                };
                (pos[i], pos[1 + i])
            })
            .chain(Some((0, 0)))
            .unique()
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::not_line_ending, combinator::map};
        map(not_line_ending, String::from)(s)
    }
}
