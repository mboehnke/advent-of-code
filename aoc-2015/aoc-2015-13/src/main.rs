use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<i64, i64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Knights of the Dinner Table";

    type Data = Vec<(String, String, i64)>;

    fn part1(guests: Self::Data) -> i64 {
        max_happiness(guests)
    }

    fn part2(mut guests: Self::Data) -> i64 {
        for name in guests.clone().iter().map(|g| g.0.to_string()).unique() {
            guests.push(("Me".to_string(), name.to_string(), 0));
            guests.push((name.to_string(), "Me".to_string(), 0));
        }
        max_happiness(guests)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::guest_list(s)
    }
}

fn max_happiness(guests: Vec<(String, String, i64)>) -> i64 {
    let happiness = |a: &str, b: &str, c: &str| -> i64 {
        guests
            .iter()
            .filter(|(n, o, _)| n == a && (o == b || o == c))
            .map(|(_, _, h)| h)
            .sum()
    };

    guests
        .iter()
        .map(|(n, _, _)| n)
        .unique()
        .permutations(guests.iter().map(|(n, _, _)| n).unique().count())
        .map(|list| {
            let last = list.len() - 1;
            (1..last)
                .map(|i| happiness(list[i], list[i - 1], list[i + 1]))
                .sum::<i64>()
                + happiness(list[0], list[last], list[1])
                + happiness(list[last], list[0], list[last - 1])
        })
        .max()
        .unwrap()
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, char, i64, newline},
        combinator::value,
        multi::separated_list0,
        sequence::terminated,
        IResult,
    };

    pub fn guest_list(s: &str) -> IResult<&str, Vec<(String, String, i64)>> {
        separated_list0(newline, guest)(s)
    }

    fn guest(s: &str) -> IResult<&str, (String, String, i64)> {
        let gain = value(1, tag("gain "));
        let lose = value(-1, tag("lose "));

        let (s, a) = terminated(alpha1, tag(" would "))(s)?;
        let (s, mul) = alt((gain, lose))(s)?;
        let (s, val) = terminated(i64, tag(" happiness units by sitting next to "))(s)?;
        let (s, b) = terminated(alpha1, char('.'))(s)?;

        Ok((s, (a.to_string(), b.to_string(), val * mul)))
    }
}
