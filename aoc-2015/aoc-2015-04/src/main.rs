use aoc::{Aoc, Solution};
use md5::Digest;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 4;
    const TITLE: &'static str = "The Ideal Stocking Stuffer";

    type Data = String;

    fn part1(key: Self::Data) -> usize {
        (0..)
            .find(|&n| {
                let c = coin(&key, n);
                c[..2] == [0, 0] && c[2] >> 4 == 0
            })
            .unwrap()
    }

    fn part2(key: Self::Data) -> usize {
        (0..).find(|&n| coin(&key, n)[..3] == [0, 0, 0]).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::not_line_ending, combinator::map};
        map(not_line_ending, String::from)(s)
    }
}

fn coin(key: &str, n: usize) -> [u8; 16] {
    let data = format!("{}{}", key, n);
    let mut hasher = md5::Md5::new();
    hasher.update(data);
    hasher.finalize().into()
}
