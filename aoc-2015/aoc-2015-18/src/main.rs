use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 18;
    const TITLE: &'static str = "Like a GIF For Your Yard";

    type Data = Vec<Vec<bool>>;

    fn part1(lights: Self::Data) -> usize {
        (0..100)
            .fold(lights, |l, _| lights_next(&l))
            .iter()
            .flatten()
            .filter(|&&l| l)
            .count()
    }

    fn part2(lights: Self::Data) -> usize {
        (0..100)
            .fold(lights, |l, _| lights_next_2(&l))
            .iter()
            .flatten()
            .filter(|&&l| l)
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{char, newline},
            combinator::value,
            multi::{many1, separated_list1},
        };

        let row = many1(alt((value(true, char('#')), value(false, char('.')))));
        separated_list1(newline, row)(s)
    }
}

fn light(lights: &[Vec<bool>], x: isize, y: isize) -> bool {
    let height = lights.len() as isize - 1;
    let width = lights[0].len() as isize - 1;
    !(x < 0 || x > width || y < 0 || y > height) && lights[y as usize][x as usize]
}

fn light_2(lights: &[Vec<bool>], x: isize, y: isize) -> bool {
    let height = lights.len() as isize - 1;
    let width = lights[0].len() as isize - 1;
    (x, y) == (0, 0)
        || (x, y) == (0, height)
        || (x, y) == (width, 0)
        || (x, y) == (width, height)
        || (!(x < 0 || x > width || y < 0 || y > height) && lights[y as usize][x as usize])
}

fn light_next(lights: &[Vec<bool>], x: isize, y: isize) -> bool {
    let n = [
        (x - 1, y),
        (x + 1, y),
        (x, y - 1),
        (x, y + 1),
        (x + 1, y + 1),
        (x - 1, y - 1),
        (x + 1, y - 1),
        (x - 1, y + 1),
    ]
    .iter()
    .map(|(x, y)| light(lights, *x, *y) as usize)
    .sum::<usize>();
    let l = light(lights, x, y);
    n == 3 || (l && n == 2)
}

fn light_next_2(lights: &[Vec<bool>], x: isize, y: isize) -> bool {
    if (x, y) == (0, 0)
        || (x, y) == (0, lights.len() as isize - 1)
        || (x, y) == (lights[0].len() as isize - 1, 0)
        || (x, y) == (lights[0].len() as isize - 1, lights.len() as isize - 1)
    {
        return true;
    }
    let n = [
        (x - 1, y),
        (x + 1, y),
        (x, y - 1),
        (x, y + 1),
        (x + 1, y + 1),
        (x - 1, y - 1),
        (x + 1, y - 1),
        (x - 1, y + 1),
    ]
    .iter()
    .map(|(x, y)| light_2(lights, *x, *y) as usize)
    .sum::<usize>();
    let l = light_2(lights, x, y);
    n == 3 || (l && n == 2)
}

fn lights_next(lights: &[Vec<bool>]) -> Vec<Vec<bool>> {
    (0..lights.len())
        .map(|y| {
            (0..lights[0].len())
                .map(move |x| light_next(lights, x as isize, y as isize))
                .collect()
        })
        .collect()
}

fn lights_next_2(lights: &[Vec<bool>]) -> Vec<Vec<bool>> {
    (0..lights.len())
        .map(|y| {
            (0..lights[0].len())
                .map(move |x| light_next_2(lights, x as isize, y as isize))
                .collect()
        })
        .collect()
}
