use aoc::{Aoc, Solution};

const WEAPONS: [(i32, i32, i32); 5] = [(8, 4, 0), (10, 5, 0), (25, 6, 0), (40, 7, 0), (74, 8, 0)];
const ARMOR: [(i32, i32, i32); 5] = [(13, 0, 1), (31, 0, 2), (53, 0, 3), (75, 0, 4), (102, 0, 5)];
const RINGS: [(i32, i32, i32); 6] = [
    (25, 1, 0),
    (50, 2, 0),
    (100, 3, 0),
    (20, 0, 1),
    (40, 0, 2),
    (80, 0, 3),
];

#[derive(Aoc)]
pub struct Sol;

impl Solution<i32, i32> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 21;
    const TITLE: &'static str = "RPG Simulator 20XX";

    type Data = (i32, i32, i32);

    fn part1(boss: Self::Data) -> i32 {
        let mut l = loadouts();
        l.sort_unstable_by_key(|&(g, _, _)| g);
        l.iter()
            .find(|&&(_, d, a)| wins((100, d, a), boss))
            .unwrap()
            .0
    }

    fn part2(boss: Self::Data) -> i32 {
        let mut l = loadouts();
        l.sort_unstable_by_key(|&(g, _, _)| g);
        l.iter()
            .rev()
            .find(|&&(_, d, a)| !wins((100, d, a), boss))
            .unwrap()
            .0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::take_till,
            character::complete::i32,
            sequence::{preceded, tuple},
            IResult,
        };

        fn num(s: &str) -> IResult<&str, i32> {
            preceded(take_till(|c: char| c.is_ascii_digit()), i32)(s)
        }

        tuple((num, num, num))(s)
    }
}

fn loadouts() -> Vec<(i32, i32, i32)> {
    WEAPONS
        .iter()
        .flat_map(|&(c, d, a)| {
            ARMOR
                .iter()
                .map(move |(ca, da, aa)| (c + ca, d + da, a + aa))
                .chain(Some((c, d, a)))
        })
        .flat_map(|(c, d, a)| {
            let one = RINGS
                .iter()
                .map(move |(ca, da, aa)| (c + ca, d + da, a + aa));
            let two = (0..RINGS.len()).flat_map(move |r1| {
                (0..RINGS.len()).filter(move |r2| *r2 != r1).map(move |r2| {
                    (
                        c + RINGS[r1].0 + RINGS[r2].0,
                        d + RINGS[r1].1 + RINGS[r2].1,
                        a + RINGS[r1].2 + RINGS[r2].2,
                    )
                })
            });
            one.chain(two).chain(Some((c, d, a)))
        })
        .collect()
}

fn wins(mut player: (i32, i32, i32), mut boss: (i32, i32, i32)) -> bool {
    while player.0 > 0 {
        boss.0 -= (player.1 - boss.2).max(1);
        if boss.0 <= 0 {
            return true;
        }
        player.0 -= (boss.1 - player.2).max(1);
    }
    false
}
