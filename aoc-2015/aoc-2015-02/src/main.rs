use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<u64, u64> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 2;
    const TITLE: &'static str = "I Was Told There Would Be No Math";

    type Data = Vec<(u64, u64, u64)>;

    fn part1(input: Self::Data) -> u64 {
        input
            .iter()
            .map(|(l, w, h)| (l * w, w * h, h * l))
            .map(|(a, b, c)| [a, b, c].iter().min().unwrap() + 2 * (a + b + c))
            .sum()
    }

    fn part2(input: Self::Data) -> u64 {
        input
            .iter()
            .map(|(l, w, h)| [l + w, w + h, h + l].iter().min().unwrap() * 2 + (l * w * h))
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{char, newline, u64},
            multi::separated_list0,
            separated_triple,
        };

        let nums = separated_triple(u64, char('x'), u64, char('x'), u64);
        separated_list0(newline, nums)(s)
    }
}
