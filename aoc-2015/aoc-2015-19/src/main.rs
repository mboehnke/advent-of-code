use aoc::{Aoc, Solution};
use apply::Also as _;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 19;
    const TITLE: &'static str = "Medicine for Rudolph";

    type Data = (Vec<(String, String)>, String);

    fn part1((repl, mol): Self::Data) -> usize {
        replacements(&mol, &repl).len()
    }

    fn part2((_, mol): Self::Data) -> usize {
        let symbols = mol.chars().filter(char::is_ascii_uppercase).count();
        let rn = mol.match_indices("Rn").count();
        let ar = mol.match_indices("Ar").count();
        let y = mol.match_indices('Y').count();

        symbols - rn - ar - 2 * y - 1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag,
            character::complete::{alpha1, newline},
            multi::many0,
            sequence::separated_pair,
            {combinator::map, multi::separated_list0},
        };

        let repl = separated_pair(
            map(alpha1, String::from),
            tag(" => "),
            map(alpha1, String::from),
        );
        let repls = separated_list0(newline, repl);
        separated_pair(repls, many0(newline), map(alpha1, String::from))(s)
    }
}

fn replacements(mol: &str, repl: &[(String, String)]) -> Vec<String> {
    repl.iter()
        .flat_map(|(ro, rn)| {
            mol.match_indices(ro)
                .map(|(i, _)| {
                    mol.to_string()
                        .also(|m| m.replace_range(i..i + ro.len(), rn))
                })
                .collect_vec()
                .into_iter()
        })
        .unique()
        .collect()
}
