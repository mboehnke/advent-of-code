use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 17;
    const TITLE: &'static str = "No Such Thing as Too Much";

    type Data = Vec<usize>;

    fn part1(containers: Self::Data) -> usize {
        combinations(150, &containers, vec![]).len()
    }

    fn part2(containers: Self::Data) -> usize {
        let c = combinations(150, &containers, vec![]);
        let min_num = c.iter().map(Vec::len).min().unwrap();
        c.into_iter().filter(|x| x.len() == min_num).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize_list(s)
    }
}

fn combinations(eggnog: usize, containers: &[usize], used: Vec<usize>) -> Vec<Vec<usize>> {
    if eggnog == 0 {
        return vec![used];
    }
    if containers.is_empty() {
        return Vec::new();
    }
    let (&c, cs) = containers.split_first().unwrap();
    if c > eggnog {
        combinations(eggnog, cs, used)
    } else {
        let mut u = used.clone();
        u.push(c);
        let mut c = combinations(eggnog - c, cs, u);
        c.append(&mut combinations(eggnog, cs, used));
        c
    }
}
