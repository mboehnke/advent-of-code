use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 24;
    const TITLE: &'static str = "It Hangs in the Balance";

    type Data = Vec<usize>;

    fn part1(packages: Self::Data) -> usize {
        process(packages, 3).unwrap()
    }

    fn part2(packages: Self::Data) -> usize {
        process(packages, 4).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize_list(s)
    }
}

fn process(packages: Vec<usize>, compartments: usize) -> Option<usize> {
    let weight = packages.iter().sum::<usize>() / compartments;
    (1..).find_map(|k| {
        packages
            .iter()
            .combinations(k)
            .filter(|ps| ps.iter().cloned().sum::<usize>() == weight)
            .map(|ps| ps.iter().cloned().product::<usize>())
            .min()
    })
}
