use aoc::{Aoc, Solution};
use assembly::Computer;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2015;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Opening the Turing Lock";

    type Data = Computer;

    fn part1(mut computer: Self::Data) -> isize {
        computer.run();
        computer.get_b()
    }

    fn part2(mut computer: Self::Data) -> isize {
        computer.set_a(1);
        computer.run();
        computer.get_b()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}
