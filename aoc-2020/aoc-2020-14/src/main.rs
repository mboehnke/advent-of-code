use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alphanumeric1, newline, u64},
        combinator::map,
        multi::separated_list0,
        sequence::{preceded, tuple},
    },
    Aoc, Solution,
};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Day14;

#[derive(Clone)]
pub enum Instruction {
    Mask(u64, u64, u64),
    Mem(u64, u64),
}

impl Solution<u64, u64> for Day14 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 14;
    const TITLE: &'static str = "Docking Data";

    type Data = Vec<Instruction>;

    fn part1(instructions: Self::Data) -> u64 {
        instructions
            .into_iter()
            .fold(((0, 0, 0), HashMap::new()), |(mut mask, mut mem), ins| {
                match ins {
                    Instruction::Mask(z, o, x) => mask = (z, o, x),
                    Instruction::Mem(addr, val) => {
                        mem.insert(addr, val & !mask.0 | mask.1);
                    }
                }
                (mask, mem)
            })
            .1
            .values()
            .sum()
    }

    fn part2(instructions: Self::Data) -> u64 {
        instructions
            .into_iter()
            .fold(((0, 0, 0), HashMap::new()), |(mut mask, mut mem), ins| {
                match ins {
                    Instruction::Mask(z, o, x) => mask = (z, o, x),
                    Instruction::Mem(addr, val) => {
                        decode(addr, mask.1, mask.2).into_iter().for_each(|a| {
                            mem.insert(a, val);
                        });
                    }
                }
                (mask, mem)
            })
            .1
            .values()
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let addr = preceded(tag("mem["), u64);
        let val = preceded(tag("] = "), u64);
        let mem = map(tuple((addr, val)), |(a, v)| Instruction::Mem(a, v));
        let mask_raw = map(preceded(tag("mask = "), alphanumeric1), |m: &str| {
            ['0', '1', 'X']
                .iter()
                .map(|c| {
                    m.chars()
                        .map(|d| u64::from(*c == d))
                        .fold(0, |acc, x| acc * 2 + x)
                })
                .collect::<Vec<_>>()
        });
        let mask = map(mask_raw, |m| Instruction::Mask(m[0], m[1], m[2]));
        let instruction = alt((mask, mem));
        separated_list0(newline, instruction)(s)
    }
}

fn decode(addr: u64, o: u64, x: u64) -> Vec<u64> {
    (0..64)
        .filter(|i| (1 << i) & x > 0)
        .fold(vec![addr | o], |acc, i| {
            acc.into_iter()
                .flat_map(|j| vec![j & !(1 << i), j | (1 << i)])
                .collect()
        })
}
