use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, char, space1},
        combinator::{map, opt, value},
        multi::separated_list1,
        sequence::{separated_pair, terminated, tuple},
        usize,
    },
    Aoc, Solution,
};

use hashbrown::{HashMap, HashSet};

#[derive(Aoc)]
pub struct Day07;

#[derive(Clone, Debug)]
pub struct Bag {
    pub name: String,
    pub contents: HashMap<String, usize>,
}

impl Solution<usize, usize> for Day07 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 7;
    const TITLE: &'static str = "Handy Haversacks";

    type Data = Vec<Bag>;

    fn part1(bags: Self::Data) -> usize {
        let mut len = 0;
        let mut res = HashSet::new();
        res.insert("shiny gold".to_string());
        while res.len() > len {
            len = res.len();
            let r = bags
                .iter()
                .filter(|b| b.contents.keys().any(|k| res.contains(k.as_str())))
                .map(|b| b.name.to_string())
                .collect::<Vec<_>>();
            res.extend(r);
        }
        len - 1
    }

    fn part2(bags: Self::Data) -> usize {
        count("shiny gold", &bags) - 1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let bag_name1 = move |s| terminated(alpha1, space1)(s);
        let bag_name2 = move |s| terminated(alpha1, tuple((tag(" bag"), opt(char('s')))))(s);
        let bag_name = move |s| {
            map(tuple((bag_name1, bag_name2)), |(b1, b2)| {
                format!("{} {}", b1, b2)
            })(s)
        };
        let empty = value(Vec::new(), tag("no other bags"));
        let bag_num = map(separated_pair(usize, space1, bag_name), |(i, n)| (n, i));
        let contents = separated_list1(tag(", "), bag_num);
        let bag_contents = alt((empty, contents));
        let name = terminated(bag_name, tag(" contain "));
        let contents = terminated(bag_contents, char('.'));
        let bag = map(tuple((name, contents)), |(name, contents)| Bag {
            name,
            contents: contents.into_iter().collect(),
        });
        separated_list1(char('\n'), bag)(s)
    }
}

fn count(name: &str, bags: &[Bag]) -> usize {
    1 + bags
        .iter()
        .find(|b| b.name == name)
        .unwrap()
        .contents
        .iter()
        .map(|(name, num)| num * count(name, bags))
        .sum::<usize>()
}
