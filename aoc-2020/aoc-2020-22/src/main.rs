use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashSet;
use itertools::Itertools as _;
use std::collections::hash_map::DefaultHasher;
use std::collections::VecDeque;
use std::hash::{Hash, Hasher};

#[derive(Aoc)]
pub struct Day22;

impl Solution<usize, usize> for Day22 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Crab Combat";

    type Data = (VecDeque<usize>, VecDeque<usize>);

    fn part1(decks: Self::Data) -> usize {
        combat(decks).apply(score)
    }

    fn part2(decks: Self::Data) -> usize {
        recursive_combat(decks).apply(score)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.split("\n\n")
                .map(|x| {
                    x.lines()
                        .skip(1)
                        .map(str::parse)
                        .map(Result::unwrap)
                        .collect()
                })
                .collect_tuple()
                .unwrap(),
        ))
    }
}

fn combat(
    (mut deck1, mut deck2): (VecDeque<usize>, VecDeque<usize>),
) -> (VecDeque<usize>, VecDeque<usize>) {
    while !deck1.is_empty() && !deck2.is_empty() {
        let c1 = deck1.pop_front().unwrap();
        let c2 = deck2.pop_front().unwrap();
        if c1 > c2 {
            deck1.push_back(c1);
            deck1.push_back(c2);
        } else {
            deck2.push_back(c2);
            deck2.push_back(c1);
        }
    }
    (deck1, deck2)
}

fn recursive_combat(
    (mut deck1, mut deck2): (VecDeque<usize>, VecDeque<usize>),
) -> (VecDeque<usize>, VecDeque<usize>) {
    let mut previous_states = HashSet::new();
    while !deck1.is_empty() && !deck2.is_empty() {
        if !previous_states.insert(hash(&deck1, &deck2)) {
            return (deck1, VecDeque::new());
        }
        let c1 = deck1.pop_front().unwrap();
        let c2 = deck2.pop_front().unwrap();
        let p1_won = if deck1.len() >= c1 && deck2.len() >= c2 {
            p1_wins(
                deck1.iter().take(c1).cloned().collect(),
                deck2.iter().take(c2).cloned().collect(),
            )
        } else {
            c1 > c2
        };
        if p1_won {
            deck1.push_back(c1);
            deck1.push_back(c2);
        } else {
            deck2.push_back(c2);
            deck2.push_back(c1);
        }
    }
    (deck1, deck2)
}

fn p1_wins(mut deck1: VecDeque<usize>, mut deck2: VecDeque<usize>) -> bool {
    if deck1.iter().max() > deck2.iter().max() {
        return true;
    }
    let mut previous_states = HashSet::new();
    while !deck1.is_empty() && !deck2.is_empty() {
        if !previous_states.insert(hash(&deck1, &deck2)) {
            return true;
        }
        let c1 = deck1.pop_front().unwrap();
        let c2 = deck2.pop_front().unwrap();
        let p1_won = if deck1.len() >= c1 && deck2.len() >= c2 {
            !p1_wins(
                deck1.iter().take(c1).cloned().collect(),
                deck2.iter().take(c2).cloned().collect(),
            )
        } else {
            c1 > c2
        };
        if p1_won {
            deck1.push_back(c1);
            deck1.push_back(c2);
        } else {
            deck2.push_back(c2);
            deck2.push_back(c1);
        }
    }
    deck2.is_empty()
}

fn hash(d1: &VecDeque<usize>, d2: &VecDeque<usize>) -> u64 {
    let mut hasher = DefaultHasher::new();
    (d1, d2).hash(&mut hasher);
    hasher.finish()
}

fn score((deck1, deck2): (VecDeque<usize>, VecDeque<usize>)) -> usize {
    deck1
        .into_iter()
        .chain(deck2)
        .rev()
        .enumerate()
        .map(|(i, c)| (i + 1) * c)
        .sum::<usize>()
}
