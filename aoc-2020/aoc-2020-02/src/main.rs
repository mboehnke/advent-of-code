use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{alpha1, anychar, char, newline},
        combinator::map,
        multi::separated_list1,
        sequence::{terminated, tuple},
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day02;

#[derive(Clone)]
pub struct Password {
    pub min: usize,
    pub max: usize,
    pub char: char,
    pub password: String,
}

impl Solution<usize, usize> for Day02 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Password Philosophy";

    type Data = Vec<Password>;

    fn part1(passwords: Self::Data) -> usize {
        passwords.into_iter().filter(is_valid).count()
    }

    fn part2(passwords: Self::Data) -> usize {
        passwords.into_iter().filter(is_valid_2).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let min = terminated(usize, char('-'));
        let max = terminated(usize, char(' '));
        let char = terminated(anychar, tag(": "));
        let pass = map(alpha1, String::from);
        let password = map(
            tuple((min, max, char, pass)),
            |(min, max, char, password)| Password {
                min,
                max,
                char,
                password,
            },
        );
        separated_list1(newline, password)(s)
    }
}

fn is_valid(p: &Password) -> bool {
    let count = p.password.chars().filter(|&c| c == p.char).count();
    count >= p.min && count <= p.max
}

fn is_valid_2(p: &Password) -> bool {
    let test = |pos: usize| p.password.chars().nth(pos - 1) == Some(p.char);
    test(p.min) != test(p.max)
}
