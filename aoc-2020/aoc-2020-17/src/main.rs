use aoc::{Aoc, Solution};
use game_of_life::GameOfLife;
use itertools::iproduct;

#[derive(Aoc)]
pub struct Day17;

struct Grid3d;
struct Grid4d;

type Position = (i8, i8, i8, i8);

impl Solution<usize, usize> for Day17 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 17;
    const TITLE: &'static str = "Conway Cubes";

    type Data = Vec<Position>;

    fn part1(active: Self::Data) -> usize {
        Grid3d::from_iter(active).nth(6).unwrap().len()
    }

    fn part2(active: Self::Data) -> usize {
        Grid4d::from_iter(active).nth(6).unwrap().len()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.lines()
                .enumerate()
                .flat_map(|(y, l)| {
                    l.chars()
                        .enumerate()
                        .filter(|&(_, c)| c == '#')
                        .map(move |(x, _)| (x as i8, y as i8, 0, 0))
                })
                .collect(),
        ))
    }
}

impl GameOfLife for Grid3d {
    type Pos = Position;

    fn adjacent(&(x, y, z, w): &Self::Pos) -> Vec<Self::Pos> {
        iproduct!(x - 1..=x + 1, y - 1..=y + 1, z - 1..=z + 1, w..=w)
            .filter(move |p| *p != (x, y, z, w))
            .collect()
    }

    fn next_t(tile: bool, adjacent: usize) -> bool {
        adjacent == 3 || tile && adjacent == 2
    }
}

impl GameOfLife for Grid4d {
    type Pos = Position;

    fn adjacent(&(x, y, z, w): &Self::Pos) -> Vec<Self::Pos> {
        iproduct!(x - 1..=x + 1, y - 1..=y + 1, z - 1..=z + 1, w - 1..=w + 1)
            .filter(move |p| *p != (x, y, z, w))
            .collect()
    }

    fn next_t(tile: bool, adjacent: usize) -> bool {
        adjacent == 3 || tile && adjacent == 2
    }
}
