use std::{iter::Sum, ops::Add};

use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::line_ending,
        combinator::value,
        multi::{many1, separated_list1},
    },
    Aoc, Solution,
};
use apply::Apply;
use game_of_life::GameOfLife;
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Day24;

#[derive(Clone, Debug, Hash, PartialEq, Eq, Default)]
struct Position(i32, i32);

#[derive(Clone, Copy, Debug)]
pub enum Dir {
    E,
    NE,
    SE,
    W,
    NW,
    SW,
}

struct Floor;

impl Solution<usize, usize> for Day24 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 24;
    const TITLE: &'static str = "Lobby Layout";

    type Data = Vec<Vec<Dir>>;

    fn part1(paths: Self::Data) -> usize {
        floor(paths).len()
    }

    fn part2(paths: Self::Data) -> usize {
        floor(paths).apply(Floor::from_iter).nth(100).unwrap().len()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let e = value(Dir::E, tag("e"));
        let ne = value(Dir::NE, tag("ne"));
        let se = value(Dir::SE, tag("se"));
        let w = value(Dir::W, tag("w"));
        let nw = value(Dir::NW, tag("nw"));
        let sw = value(Dir::SW, tag("sw"));
        let dir = alt((ne, se, e, nw, sw, w));
        separated_list1(line_ending, many1(dir))(s)
    }
}

fn floor(paths: Vec<Vec<Dir>>) -> HashSet<Position> {
    paths
        .into_iter()
        .map(Position::from_iter)
        .fold(HashSet::new(), |mut floor, pos| {
            if !floor.remove(&pos) {
                floor.insert(pos);
            }
            floor
        })
}

impl From<Dir> for Position {
    fn from(dir: Dir) -> Self {
        match dir {
            Dir::E => Position(2, 0),
            Dir::NE => Position(1, -1),
            Dir::SE => Position(1, 1),
            Dir::W => Position(-2, 0),
            Dir::NW => Position(-1, -1),
            Dir::SW => Position(-1, 1),
        }
    }
}

impl Add for Position {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Sum<Position> for Position {
    fn sum<I: Iterator<Item = Position>>(iter: I) -> Self {
        iter.fold(Position::default(), |acc, x| acc + x)
    }
}

impl FromIterator<Dir> for Position {
    fn from_iter<T: IntoIterator<Item = Dir>>(iter: T) -> Self {
        iter.into_iter().map(Position::from).sum()
    }
}

impl GameOfLife for Floor {
    type Pos = Position;

    fn adjacent(pos: &Self::Pos) -> Vec<Self::Pos> {
        [(-2, 0), (2, 0), (-1, -1), (-1, 1), (1, -1), (1, 1)]
            .iter()
            .map(|&(dx, dy)| Position(pos.0 + dx, pos.1 + dy))
            .collect()
    }

    fn next_t(tile: bool, adjacent: usize) -> bool {
        adjacent == 2 || tile && adjacent == 1
    }
}
