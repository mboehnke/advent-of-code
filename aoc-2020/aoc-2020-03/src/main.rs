use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::map,
        multi::many1,
        multi::separated_list1,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day03;

impl Solution<usize, usize> for Day03 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Toboggan Trajectory";

    type Data = Vec<Vec<u8>>;

    fn part1(input: Self::Data) -> usize {
        trees(3, 1, &input)
    }

    fn part2(input: Self::Data) -> usize {
        [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
            .into_iter()
            .map(|(right, down)| trees(right, down, &input))
            .product()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let valid_u8 = map(alt((char('#'), char('.'))), |c| c as u8);
        separated_list1(newline, many1(valid_u8))(s)
    }
}

fn trees(right: usize, down: usize, input: &[Vec<u8>]) -> usize {
    let len = input[0].len();
    input
        .iter()
        .step_by(down)
        .skip(1)
        .scan(0, |x, line| {
            *x = (*x + right) % len;
            line.get(*x)
        })
        .filter(|&&c| c == b'#')
        .count()
}
