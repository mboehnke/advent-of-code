use aoc::{Aoc, Solution};

const TARGET: i32 = 2020;

#[derive(Aoc)]
pub struct Day01;

impl Solution<i32, i32> for Day01 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Report Repair";

    type Data = Vec<i32>;

    fn part1(values: Self::Data) -> i32 {
        n_sum_product(2, TARGET, &values).unwrap()
    }

    fn part2(values: Self::Data) -> i32 {
        n_sum_product(3, TARGET, &values).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::i32_list(s)
    }
}

fn n_sum_product(n: u32, target: i32, values: &[i32]) -> Option<i32> {
    if n == 1 {
        values.contains(&target).then_some(target)
    } else {
        (0..values.len() - 1).find_map(|i| {
            n_sum_product(n - 1, target - values[i], &values[i + 1..]).map(|p| p * values[i])
        })
    }
}
