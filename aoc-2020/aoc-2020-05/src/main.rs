use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::value,
        multi::{fold_many1, separated_list1},
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day05;

impl Solution<u16, u16> for Day05 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 5;
    const TITLE: &'static str = "Binary Boarding";

    type Data = Vec<u16>;

    fn part1(seats: Self::Data) -> u16 {
        seats.into_iter().max().unwrap()
    }

    fn part2(seats: Self::Data) -> u16 {
        let (min, max, sum) = seats
            .into_iter()
            .fold((u16::MAX, 0, 0), |(min, max, sum), id| {
                (id.min(min), id.max(max), id + sum)
            });
        max * (max + 1) / 2 - min * (min - 1) / 2 - sum
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let bin_true = value(true, alt((char('B'), char('R'))));
        let bin_false = value(false, alt((char('F'), char('L'))));
        let bin_digit = alt((bin_true, bin_false));
        let bin_num = fold_many1(bin_digit, || 0u16, |acc, x| (acc << 1) + x as u16);
        separated_list1(newline, bin_num)(s)
    }
}
