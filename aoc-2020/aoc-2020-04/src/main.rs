use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Day04;

#[derive(Debug, Clone)]
pub struct Passport {
    pub byr: String,
    pub iyr: String,
    pub eyr: String,
    pub hgt: String,
    pub hcl: String,
    pub ecl: String,
    pub pid: String,
    pub cid: Option<String>,
}

impl Solution<usize, usize> for Day04 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 4;
    const TITLE: &'static str = "Passport Processing";

    type Data = Vec<Passport>;

    fn part1(passports: Self::Data) -> usize {
        passports.len()
    }

    fn part2(passports: Self::Data) -> usize {
        passports.into_iter().filter(Passport::is_valid).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.split("\n\n")
                .map(parser::passport)
                .filter_map(Result::ok)
                .map(|r| r.1)
                .collect(),
        ))
    }
}

impl Passport {
    fn is_valid(&self) -> bool {
        let in_limits = |val: &str, min: usize, max: usize| -> bool {
            val.parse()
                .map(|x: usize| x >= min && x <= max)
                .unwrap_or(false)
        };

        in_limits(&self.byr, 1920, 2002)
            && in_limits(&self.iyr, 2010, 2020)
            && in_limits(&self.eyr, 2020, 2030)
            && (self
                .hgt
                .strip_suffix("cm")
                .map(|hgt| in_limits(hgt, 150, 193))
                .unwrap_or(false)
                || self
                    .hgt
                    .strip_suffix("in")
                    .map(|hgt| in_limits(hgt, 59, 76))
                    .unwrap_or(false))
            && self.hcl.len() == 7
            && self.hcl.starts_with('#')
            && self.hcl.chars().skip(1).all(|c| c.is_ascii_hexdigit())
            && ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&self.ecl.as_str())
            && self.pid.len() == 9
            && self.pid.chars().all(char::is_numeric)
    }
}

mod parser {
    use aoc::nom::{
        branch::{alt, permutation},
        bytes::complete::{tag, take_while1},
        character::complete::char,
        combinator::opt,
        IResult,
    };

    use super::Passport;

    pub fn passport(s: &str) -> IResult<&str, Passport> {
        let (s, (byr, iyr, eyr, hgt, hcl, ecl, pid, cid)) = permutation((
            value("byr"),
            value("iyr"),
            value("eyr"),
            value("hgt"),
            value("hcl"),
            value("ecl"),
            value("pid"),
            opt(value("cid")),
        ))(s)?;
        let passport = Passport {
            byr: byr.to_string(),
            iyr: iyr.to_string(),
            eyr: eyr.to_string(),
            hgt: hgt.to_string(),
            hcl: hcl.to_string(),
            ecl: ecl.to_string(),
            pid: pid.to_string(),
            cid: cid.map(str::to_string),
        };
        Ok((s, passport))
    }

    fn value(key: &'static str) -> impl FnMut(&str) -> IResult<&str, &str> {
        move |s: &str| {
            let (s, _) = tag(key)(s)?;
            let (s, _) = char(':')(s)?;
            let (s, val) = take_while1(|c: char| c == '#' || c.is_alphanumeric())(s)?;
            let (s, _) = opt(alt((char(' '), char('\n'))))(s)?;
            Ok((s, val))
        }
    }
}
