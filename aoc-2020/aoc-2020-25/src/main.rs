use aoc::{Aoc, Solution};
use num::BigUint;

#[derive(Aoc)]
pub struct Day25;

struct Transformation(u64, u64);

const MOD: u64 = 20201227;

impl Solution<u64, &'static str> for Day25 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 25;
    const TITLE: &'static str = "Combo Breaker";

    type Data = (u64, u64);

    fn part1((p1, p2): Self::Data) -> u64 {
        let (l, k) = Transformation::from(7).loop_size(p1, p2);
        Transformation::from(k).nth(l).unwrap()
    }

    fn part2(_: Self::Data) -> &'static str {
        ""
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{newline, u64},
            sequence::separated_pair,
        };
        separated_pair(u64, newline, u64)(s)
    }
}

impl Transformation {
    fn loop_size(self, p1: u64, p2: u64) -> (usize, u64) {
        const S: usize = 2000000;
        self.skip_first(S)
            .enumerate()
            .find(|&(_, k)| k == p1 || k == p2)
            .map(|(a, b)| (a + S, if b == p1 { p2 } else { p1 }))
            .unwrap()
    }

    fn skip_first(mut self, n: usize) -> Self {
        self.0 = self.nth(n).unwrap();
        self
    }
}

impl From<u64> for Transformation {
    fn from(n: u64) -> Self {
        Transformation(1, n)
    }
}

impl Iterator for Transformation {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        let out = Some(self.0);
        self.0 = self.0 * self.1 % MOD;
        out
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        let x = BigUint::from(self.1)
            .modpow(&n.into(), &MOD.into())
            .to_u32_digits()[0] as u64;
        Some(x)
    }
}
