use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::{map, value},
        multi::separated_list1,
        sequence::separated_pair,
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day13;

impl Solution<usize, u64> for Day13 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Shuttle Search";

    type Data = (usize, Vec<Option<usize>>);

    fn part1((t, ids): Self::Data) -> usize {
        ids.into_iter()
            .flatten()
            .map(|id| (id, id - t % id))
            .min_by_key(|&(_, d)| d)
            .map(|(id, d)| id * d)
            .unwrap()
    }

    fn part2((_, ids): Self::Data) -> u64 {
        ids.into_iter()
            .enumerate()
            .filter_map(|(i, id)| id.map(|id| (i as u64, id as u64)))
            .fold((0, 1), |(mut sol, step), (i, id)| {
                sol = (sol..).step_by(step).find(|x| (x + i) % id == 0).unwrap();
                (sol, step * id as usize)
            })
            .0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let id_none = value(None, char('x'));
        let id_some = map(usize, Some);
        let id_option = alt((id_some, id_none));
        let id_option_list = separated_list1(char(','), id_option);
        separated_pair(usize, newline, id_option_list)(s)
    }
}
