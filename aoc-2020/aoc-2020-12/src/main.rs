use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::map,
        isize,
        multi::separated_list1,
        sequence::preceded,
    },
    Aoc, Solution,
};
use std::mem::swap;
use Action::*;

#[derive(Aoc)]
pub struct Day12;

#[derive(Clone)]
pub enum Action {
    N(isize),
    S(isize),
    E(isize),
    W(isize),
    L(isize),
    R(isize),
    F(isize),
}

struct Ship {
    x: isize,
    y: isize,
    dir: isize,
    w_x: isize,
    w_y: isize,
}

impl Solution<isize, isize> for Day12 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 12;
    const TITLE: &'static str = "Rain Risk";

    type Data = Vec<Action>;

    fn part1(actions: Self::Data) -> isize {
        actions.into_iter().fold(Ship::new(), Ship::do1).distance()
    }

    fn part2(actions: Self::Data) -> isize {
        actions.into_iter().fold(Ship::new(), Ship::do2).distance()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let n = preceded(char('N'), map(isize, Action::N));
        let e = preceded(char('E'), map(isize, Action::E));
        let sa = preceded(char('S'), map(isize, Action::S));
        let w = preceded(char('W'), map(isize, Action::W));
        let l = preceded(char('L'), map(isize, Action::L));
        let r = preceded(char('R'), map(isize, Action::R));
        let f = preceded(char('F'), map(isize, Action::F));
        let action = alt((n, e, sa, w, l, r, f));
        separated_list1(newline, action)(s)
    }
}

impl Ship {
    fn new() -> Self {
        Ship {
            x: 0,
            y: 0,
            dir: 0,
            w_x: 10,
            w_y: -1,
        }
    }

    fn distance(&self) -> isize {
        self.x.abs() + self.y.abs()
    }

    fn do1(mut self, action: Action) -> Self {
        match action {
            N(v) => self.y -= v,
            S(v) => self.y += v,
            W(v) => self.x -= v,
            E(v) => self.x += v,
            L(v) => self.turn_s(-v),
            R(v) => self.turn_s(v),
            F(v) => match self.dir {
                0 => self.x += v,
                180 => self.x -= v,
                90 => self.y += v,
                270 => self.y -= v,
                _ => panic!(),
            },
        };
        self
    }

    fn do2(mut self, action: Action) -> Self {
        match action {
            N(v) => self.w_y -= v,
            S(v) => self.w_y += v,
            W(v) => self.w_x -= v,
            E(v) => self.w_x += v,
            L(v) => self.turn_w(-v),
            R(v) => self.turn_w(v),
            F(v) => {
                self.x += self.w_x * v;
                self.y += self.w_y * v
            }
        };
        self
    }

    fn turn_s(&mut self, dir: isize) {
        self.dir = (self.dir + dir + 360) % 360;
    }

    fn turn_w(&mut self, dir: isize) {
        match (dir + 360) % 360 {
            0 => {}
            180 => {
                self.w_x *= -1;
                self.w_y *= -1
            }
            90 => {
                self.w_y *= -1;
                swap(&mut self.w_x, &mut self.w_y)
            }
            270 => {
                self.w_x *= -1;
                swap(&mut self.w_x, &mut self.w_y)
            }
            _ => panic!(),
        }
    }
}
