use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline, space0, u64},
        combinator::{map, value},
        multi::{many1, separated_list0},
        sequence::preceded,
    },
    Aoc, Solution,
};
use apply::Apply as _;
use itertools::Itertools as _;
use std::iter::once;
use Token::*;

#[derive(Aoc)]
pub struct Day18;

#[derive(Clone, PartialEq, Eq)]
pub enum Token {
    Num(u64),
    Add,
    Mul,
    OBr,
    CBr,
}

impl Solution<u64, u64> for Day18 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 18;
    const TITLE: &'static str = "Operation Order";

    type Data = Vec<Vec<Token>>;

    fn part1(tokens: Self::Data) -> u64 {
        tokens.into_iter().map(|ts| eval(&ts, false)).sum()
    }

    fn part2(tokens: Self::Data) -> u64 {
        tokens.into_iter().map(|ts| eval(&ts, true)).sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let add = value(Token::Add, char('+'));
        let mul = value(Token::Mul, char('*'));
        let obr = value(Token::OBr, char('('));
        let cbr = value(Token::CBr, char(')'));
        let num = map(u64, Token::Num);
        let token = preceded(space0, alt((add, mul, obr, cbr, num)));
        separated_list0(newline, many1(token))(s)
    }
}

fn eval(ts: &[Token], add_before_mul: bool) -> u64 {
    if let Some((start, _)) = ts.iter().enumerate().find(|(_, t)| **t == OBr) {
        let mut n = 0;
        for (i, t) in ts[start..].iter().enumerate() {
            match t {
                OBr => n += 1,
                CBr => n -= 1,
                _ => {}
            }
            if n == 0 {
                let a = ts.iter().take(if start == 0 { 0 } else { start }).cloned();
                let b = eval(&ts[start + 1..start + i], add_before_mul)
                    .apply(Num)
                    .apply(once);
                let c = ts.iter().skip(start + i + 1).cloned();
                return eval(&a.chain(b).chain(c).collect::<Vec<_>>(), add_before_mul);
            }
        }
        panic!()
    }

    if add_before_mul {
        if let Some((i, _)) = ts.iter().enumerate().find(|(_, t)| **t == Add) {
            let a = ts.iter().take(i - 1).cloned();
            let b = (ts[i - 1].val() + ts[i + 1].val()).apply(Num).apply(once);
            let c = ts.iter().skip(i + 2).cloned();
            return eval(&a.chain(b).chain(c).collect::<Vec<_>>(), add_before_mul);
        }
    }

    ts[1..].iter().tuples().fold(ts[0].val(), |acc, (o, n)| {
        let n = n.val();
        match o {
            Add => acc + n,
            Mul => acc * n,
            _ => panic!(),
        }
    })
}

impl Token {
    fn val(&self) -> u64 {
        if let Num(n) = self {
            *n
        } else {
            panic!()
        }
    }
}
