use std::str::FromStr;

use aoc::{Aoc, Solution};
use apply::Apply;
use hashbrown::HashMap;
use itertools::{iproduct, Itertools};

const SEA_MONSTER: &str = "                  # \n#    ##    ##    ###\n #  #  #  #  #  #   ";

#[derive(Aoc)]
pub struct Day20;

#[derive(Debug, Clone)]
pub struct Tile {
    data: Vec<Vec<bool>>,
    top: Border,
    right: Border,
    bottom: Border,
    left: Border,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Border(u16);

struct Grid(Vec<Vec<Tile>>);

struct Image(Vec<Vec<bool>>);

impl Solution<u64, usize> for Day20 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 20;
    const TITLE: &'static str = "Jurassic Jigsaw";

    type Data = HashMap<u16, Tile>;

    fn part1(tiles: Self::Data) -> u64 {
        find_adjacent(tiles)
            .into_iter()
            .filter(|(_, (_, adj))| adj.len() == 2)
            .map(|(id, _)| id as u64)
            .product()
    }

    fn part2(tiles: Self::Data) -> usize {
        tiles
            .apply(Grid::from)
            .and_then(Image::from)
            .unwrap()
            .habitat_roughness()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.trim()
                .split("\n\n")
                .map(Tile::parse_with_id)
                .map(Result::unwrap)
                .collect(),
        ))
    }
}

impl Grid {
    fn from(tiles: HashMap<u16, Tile>) -> Option<Self> {
        let len = (tiles.len() as f64).sqrt().floor() as usize;
        let mut tiles = find_adjacent(tiles);
        let mut id_grid = vec![vec![0; len]; len];

        for y in 0..len {
            for x in 0..len {
                let num_adjacent =
                    4 - (y == 0 || y == len - 1) as usize - (x == 0 || x == len - 1) as usize;
                let id = *tiles
                    .iter()
                    .filter(|(_, (_, adj))| adj.len() == num_adjacent)
                    .filter(|(id, _)| id_grid.iter().flatten().all(|used_id| used_id != *id))
                    .filter(|(_, (_, adj))| x == 0 || adj.contains(&id_grid[y][x - 1]))
                    .find(|(_, (_, adj))| y == 0 || adj.contains(&id_grid[y - 1][x]))?
                    .0;
                id_grid[y][x] = id;
            }
        }

        id_grid
            .into_iter()
            .map(|row| {
                row.into_iter()
                    .map(|id| tiles.remove(&id).map(|(tile, _)| tile))
                    .collect::<Option<Vec<_>>>()
            })
            .collect::<Option<Vec<_>>>()
            .map(Grid)
    }
}

impl Image {
    fn from(Grid(grid): Grid) -> Option<Image> {
        let len = grid.len();
        let data_len = grid[0][0].data.len();
        let mut img = vec![vec![false; data_len * len]; data_len * len];
        for (y, x) in iproduct!(0..len, 0..len) {
            // flip/rotate tile to align with its neighbors
            let tile = grid[y][x]
                .variants()
                .into_iter()
                .filter(|tile| x == 0 || grid[y][x - 1].has_border(&tile.left))
                .filter(|tile| x + 2 >= len || grid[y][x + 1].has_border(&tile.right))
                .filter(|tile| y == 0 || grid[y - 1][x].has_border(&tile.top))
                .find(|tile| y + 2 >= len || grid[y + 1][x].has_border(&tile.bottom))?;
            // copy tile's data to img
            iproduct!(0..data_len, 0..data_len)
                .filter(|&(y, x)| tile.data[y][x])
                .for_each(|(ty, tx)| img[y * data_len + ty][x * data_len + tx] = true);
        }
        Some(Image(img))
    }

    fn habitat_roughness(self) -> usize {
        let num_monsters = sea_monster_variants()
            .into_iter()
            .map(|sea_monster| {
                let mx = self.0.len() - sea_monster.iter().map(|(x, _)| *x).max().unwrap();
                let my = self.0.len() - sea_monster.iter().map(|(_, y)| *y).max().unwrap();
                iproduct!(0..mx, 0..my)
                    .filter(|&(x, y)| sea_monster.iter().all(|&(dx, dy)| self.0[y + dy][x + dx]))
                    .count()
            })
            .max()
            .unwrap_or_default();
        let monster_size = SEA_MONSTER.chars().filter(|&c| c == '#').count();
        self.0.iter().flatten().filter(|&x| *x).count() - num_monsters * monster_size
    }
}

impl FromStr for Tile {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let raw: Vec<Vec<bool>> = s
            .lines()
            .map(|l| l.chars().map(|c| c == '#').collect())
            .collect();
        let top = raw.get(0).ok_or("parse error")?;
        let bottom = raw.last().ok_or("parse error")?;
        let left: Vec<bool> = raw
            .iter()
            .map(|row| row.first().copied().ok_or("parse error"))
            .try_collect()?;
        let right: Vec<bool> = raw
            .iter()
            .map(|row| row.last().copied().ok_or("parse error"))
            .try_collect()?;
        Ok(Tile {
            top: Border::from(top),
            right: Border::from(right),
            bottom: Border::from(bottom),
            left: Border::from(left),
            data: raw
                .into_iter()
                .skip(1)
                .take(8)
                .map(|row| row.into_iter().skip(1).take(8).collect())
                .collect(),
        })
    }
}

impl Border {
    fn from(i: impl AsRef<[bool]>) -> Self {
        let a = i.as_ref().iter().fold(0, |r, x| (r << 1) + *x as u16);
        let b = i.as_ref().iter().rev().fold(0, |r, x| (r << 1) + *x as u16);
        Border(a.min(b))
    }
}

impl Tile {
    pub fn parse_with_id(s: &str) -> Result<(u16, Tile), Box<dyn std::error::Error>> {
        let (id, tile) = s.split_once('\n').ok_or("parse error")?;
        let id = id
            .chars()
            .filter(|c| c.is_numeric())
            .fold(0, |acc, c| acc * 10 + (c as u8 - b'0') as u16);
        Ok((id, tile.parse()?))
    }

    pub fn adjacent(&self, other: &Tile) -> bool {
        let a = [&self.top, &self.left, &self.right, &self.bottom];
        let b = [&other.top, &other.left, &other.right, &other.bottom];
        a.iter().any(|a| b.contains(a))
    }

    pub fn variants(&self) -> Vec<Self> {
        let rotate = |tile: &Tile| Tile {
            top: tile.left,
            right: tile.top,
            bottom: tile.right,
            left: tile.bottom,
            data: iproduct!(0..8, 0..8)
                .filter(|&(x, y)| tile.data[y][x])
                .fold(vec![vec![false; 8]; 8], |mut data, (x, y)| {
                    data[x][tile.data.len() - 1 - y] = true;
                    data
                }),
        };
        let flip = |tile: &Tile| Tile {
            top: tile.bottom,
            bottom: tile.top,
            left: tile.left,
            right: tile.right,
            data: tile.data.iter().rev().cloned().collect(),
        };
        [rotate, rotate, rotate, flip, rotate, rotate, rotate]
            .iter()
            .fold(vec![self.clone()], |mut variants, f| {
                variants.push(variants.last().map(f).unwrap());
                variants
            })
    }

    fn has_border(&self, other: &Border) -> bool {
        [self.left, self.right, self.top, self.bottom].contains(other)
    }
}

fn sea_monster_variants() -> Vec<Vec<(usize, usize)>> {
    let flip = |m: &Vec<(usize, usize)>| -> Vec<(usize, usize)> {
        let max_y = m.iter().map(|(_, y)| *y).max().unwrap();
        m.iter().map(|&(x, y)| (x, max_y - y)).collect()
    };
    let rotate = |m: &Vec<(usize, usize)>| -> Vec<(usize, usize)> {
        let max_y = m.iter().map(|(_, y)| *y).max().unwrap();
        m.iter().map(|&(x, y)| (max_y - y, x)).collect()
    };
    let m = SEA_MONSTER
        .lines()
        .enumerate()
        .flat_map(|(y, r)| r.chars().enumerate().map(move |(x, c)| (x, y, c)))
        .filter(|(_, _, c)| *c == '#')
        .map(|(x, y, _)| (x, y))
        .collect_vec();
    [rotate, rotate, rotate, flip, rotate, rotate, rotate]
        .iter()
        .fold(vec![m], |mut ms, f| {
            ms.push((f)(ms.last().unwrap()));
            ms
        })
}

fn find_adjacent(tiles: HashMap<u16, Tile>) -> HashMap<u16, (Tile, Vec<u16>)> {
    let mut adjacent = vec![vec![]; tiles.len()];
    let tiles = tiles.into_iter().collect::<Vec<_>>();
    for i in 0..tiles.len() {
        for j in i + 1..tiles.len() {
            if tiles[i].1.adjacent(&tiles[j].1) {
                adjacent[i].push(tiles[j].0);
                adjacent[j].push(tiles[i].0);
            }
        }
    }
    tiles
        .into_iter()
        .zip(adjacent)
        .map(|((id, tile), adj)| (id, (tile, adj)))
        .collect()
}
