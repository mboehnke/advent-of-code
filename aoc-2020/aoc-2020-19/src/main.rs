use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashMap;
use itertools::Itertools as _;
use rayon::prelude::*;
use std::error::Error;
use std::str::FromStr;

#[derive(Aoc)]
pub struct Day19;

#[derive(Clone)]
pub enum Rule {
    Char(char),
    And(Vec<u8>),
    Or(Vec<u8>, Vec<u8>),
}

impl Solution<usize, usize> for Day19 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 19;
    const TITLE: &'static str = "Monster Messages";

    type Data = (HashMap<u8, Rule>, Vec<String>);

    fn part1((rules, messages): Self::Data) -> usize {
        messages
            .par_iter()
            .filter(|message| full_match(message, 0, &rules))
            .count()
    }

    fn part2((mut rules, messages): Self::Data) -> usize {
        rules.insert(8, "42 | 42 8".parse().unwrap());
        rules.insert(11, "42 31 | 42 11 31".parse().unwrap());
        messages
            .par_iter()
            .filter(|message| full_match(message, 0, &rules))
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let (r, m) = s.split("\n\n").tuples().next().unwrap();
        let rules = r
            .lines()
            .map(|l| {
                let (n, r) = l.split(": ").tuples().next().unwrap();
                (n.parse().unwrap(), r.parse().unwrap())
            })
            .collect();
        let messages = m.lines().map(String::from).collect();
        Ok(("", (rules, messages)))
    }
}

fn full_match(message: &str, rule: u8, rules: &HashMap<u8, Rule>) -> bool {
    matches(message, rule, rules).contains(&Some(""))
}

fn matches<'a>(message: &'a str, rule: u8, rules: &HashMap<u8, Rule>) -> Vec<Option<&'a str>> {
    match rules[&rule] {
        Rule::Char(c) if message.starts_with(c) => vec![Some(&message[1..])],
        Rule::Char(_) => vec![None],
        Rule::And(ref rs) if rs.first() == Some(&rule) => vec![None],
        Rule::And(ref rs) => rs.iter().fold(vec![Some(message)], |m, &r| {
            m.into_iter()
                .flat_map(|m| match m {
                    Some(m) if !m.is_empty() => matches(m, r, rules),
                    _ => vec![None],
                })
                .collect()
        }),
        Rule::Or(ref a, ref b) => [a, b]
            .iter()
            .filter(|rs| rs.first() != Some(&rule))
            .flat_map(|rs| {
                rs.iter().fold(vec![Some(message)], |m, &r| {
                    m.into_iter()
                        .flat_map(|m| match m {
                            Some(m) if !m.is_empty() => matches(m, r, rules),
                            _ => vec![None],
                        })
                        .collect()
                })
            })
            .filter(|r| r.is_some())
            .collect::<Vec<Option<&str>>>()
            .apply(|rs| match rs {
                rs if rs.is_empty() => vec![None],
                rs if rs.contains(&Some("")) => vec![Some("")],
                rs => rs,
            }),
    }
}

impl FromStr for Rule {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let nums = |s: &str| {
            s.trim()
                .split(' ')
                .map(str::parse)
                .collect::<Result<Vec<u8>, _>>()
        };
        match s {
            s if s.starts_with('\"') => s.chars().nth(1).ok_or("")?.apply(Rule::Char),
            s if s.contains('|') => {
                let (a, b) = s.split('|').tuples().next().ok_or("")?;
                Rule::Or(nums(a)?, nums(b)?)
            }
            s => s.apply(nums)?.apply(Rule::And),
        }
        .apply(Ok)
    }
}
