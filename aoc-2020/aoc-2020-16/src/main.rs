use std::ops::RangeInclusive;

use aoc::{
    nom::{
        bytes::complete::{tag, take_while1},
        character::complete::{char, newline, u16},
        combinator::map,
        multi::separated_list1,
        sequence::{preceded, separated_pair, tuple},
    },
    Aoc, Solution,
};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Day16;

pub type Ticket = Vec<u16>;
pub type Rules = HashMap<String, Vec<RangeInclusive<u16>>>;

impl Solution<u16, u64> for Day16 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Ticket Translation";

    type Data = (Rules, Ticket, Vec<Ticket>);

    fn part1((rules, _, tickets): Self::Data) -> u16 {
        tickets
            .into_iter()
            .flat_map(|t| error_rate(&t, &rules))
            .sum()
    }

    fn part2((rules, ticket, tickets): Self::Data) -> u64 {
        let vals = tickets
            .into_iter()
            .filter(|t| error_rate(t, &rules).is_none())
            .flat_map(|t| t.into_iter().enumerate())
            .fold(vec![Vec::new(); ticket.len()], |mut vs, (i, v)| {
                vs[i].push(v);
                vs
            });

        let mut possibilities = rules
            .iter()
            .map(|(_, r)| {
                vals.iter()
                    .enumerate()
                    .filter(|(_, vs)| vs.iter().all(|v| r.iter().any(|t| t.contains(v))))
                    .map(|(i, _)| i)
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        (0..ticket.len())
            .fold(vec![""; ticket.len()], |mut fields, _| {
                let (r, i) = possibilities
                    .iter()
                    .enumerate()
                    .find(|(_, is)| is.len() == 1)
                    .map(|(r, is)| (r, is[0]))
                    .unwrap();
                fields[i] = rules.keys().nth(r).unwrap();
                possibilities = possibilities
                    .clone()
                    .into_iter()
                    .map(|p| p.into_iter().filter(|&x| x != i).collect())
                    .collect();
                fields
            })
            .into_iter()
            .enumerate()
            .filter(|(_, name)| name.starts_with("departure"))
            .map(|(i, _)| ticket[i] as u64)
            .product()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let range = move |s| map(separated_pair(u16, char('-'), u16), |(s, e)| s..=e)(s);
        let name = take_while1(|c| c != ':');
        let r1 = preceded(tag(": "), range);
        let r2 = preceded(tag(" or "), range);
        let rule = map(tuple((name, r1, r2)), |(name, r1, r2): (&str, _, _)| {
            (name.to_string(), vec![r1, r2])
        });
        let rules = map(separated_list1(newline, rule), |v| v.into_iter().collect());
        let ticket = move |s| separated_list1(char(','), u16)(s);
        let your_ticket = preceded(tag("\n\nyour ticket:\n"), ticket);
        let nearby_tickets = preceded(
            tag("\n\nnearby tickets:\n"),
            separated_list1(newline, ticket),
        );
        tuple((rules, your_ticket, nearby_tickets))(s)
    }
}

fn error_rate(ticket: &[u16], rules: &Rules) -> Option<u16> {
    let errors = ticket
        .iter()
        .filter(|&n| !rules.values().flatten().any(|r| r.contains(n)))
        .collect::<Vec<_>>();
    (!errors.is_empty()).then(|| errors.into_iter().sum::<u16>())
}
