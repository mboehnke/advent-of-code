use aoc::{
    nom::{
        character::complete::{alpha1, newline},
        combinator::map,
        multi::separated_list1,
        sequence::tuple,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day06;

impl Solution<usize, usize> for Day06 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Custom Customs";

    type Data = Vec<Vec<String>>;

    fn part1(input: Self::Data) -> usize {
        input.into_iter().map(answers_any).sum()
    }

    fn part2(input: Self::Data) -> usize {
        input.into_iter().map(answers_all).sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let group = separated_list1(newline, map(alpha1, String::from));
        separated_list1(tuple((newline, newline)), group)(s)
    }
}

fn answers_any(group: Vec<String>) -> usize {
    answers_n(group, 1)
}

fn answers_all(group: Vec<String>) -> usize {
    let n = group.len();
    answers_n(group, n)
}

fn answers_n(group: Vec<String>, n: usize) -> usize {
    group
        .iter()
        .flat_map(|l| l.chars())
        .map(|c| c as usize - 'a' as usize)
        .fold([0; 26], |mut a, i| {
            a[i] += 1;
            a
        })
        .iter()
        .filter(|&&a| a >= n)
        .count()
}
