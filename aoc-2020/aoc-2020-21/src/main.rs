use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{alpha1, line_ending, space1},
        combinator::map,
        multi::separated_list1,
        sequence::{delimited, tuple},
    },
    Aoc, Solution,
};
use hashbrown::HashMap;
use itertools::Itertools as _;
#[derive(Aoc)]
pub struct Day21;

#[derive(Clone)]
pub struct Food {
    ingredients: Vec<String>,
    allergens: Vec<String>,
}

impl Solution<usize, String> for Day21 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 21;
    const TITLE: &'static str = "Allergen Assessment";

    type Data = Vec<Food>;

    fn part1(foods: Self::Data) -> usize {
        let ingredients = foods
            .iter()
            .flat_map(|f| f.ingredients.clone())
            .collect_vec();
        let unique_ingredients = ingredients.iter().unique().map(String::from).collect_vec();
        let safe_ingredients = foods
            .iter()
            .flat_map(|x| &x.allergens)
            .unique()
            .map(|allergen| unsafe_ingredients(allergen, &foods))
            .fold(unique_ingredients, |i, s_i| {
                i.into_iter().filter(|i| !s_i.contains(i)).collect_vec()
            });
        ingredients
            .iter()
            .filter(|i| safe_ingredients.contains(i))
            .count()
    }

    fn part2(foods: Self::Data) -> String {
        let mut unsafe_ingredients = foods
            .iter()
            .flat_map(|x| &x.allergens)
            .unique()
            .map(|allergen| (allergen, unsafe_ingredients(allergen, &foods)))
            .collect_vec();
        let mut res = Vec::new();
        while !unsafe_ingredients.is_empty() {
            let i = unsafe_ingredients
                .iter()
                .enumerate()
                .find(|(_, (_, is))| is.len() == 1)
                .unwrap()
                .0;
            let (allergen, ingredient) = unsafe_ingredients[i].clone();
            res.push((allergen, ingredient[0].clone()));
            unsafe_ingredients.remove(i);
            unsafe_ingredients = unsafe_ingredients
                .into_iter()
                .map(|(a, is)| {
                    let is = is.into_iter().filter(|i| ingredient[0] != *i).collect_vec();
                    (a, is)
                })
                .collect_vec();
        }
        res.into_iter()
            .sorted_by_key(|&(a, _)| a)
            .map(|(_, i)| i)
            .join(",")
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let ingredient = move |s| map(alpha1, String::from)(s);
        let ingredients = separated_list1(space1, ingredient);
        let allergens = delimited(
            tag(" (contains "),
            separated_list1(tag(", "), ingredient),
            tag(")"),
        );
        let food = map(
            tuple((ingredients, allergens)),
            |(ingredients, allergens)| Food {
                ingredients,
                allergens,
            },
        );
        separated_list1(line_ending, food)(s)
    }
}

fn unsafe_ingredients(allergen: &str, foods: &[Food]) -> Vec<String> {
    let has_allergen = foods
        .iter()
        .filter(|f| f.allergens.contains(&allergen.to_string()))
        .collect_vec();
    let shared_ingredients = has_allergen.iter().fold(HashMap::new(), |mut r, f| {
        f.ingredients.iter().map(String::from).for_each(|i| {
            *r.entry(i).or_insert(0) += 1;
        });
        r
    });
    shared_ingredients
        .iter()
        .filter(|(_, v)| **v == has_allergen.len())
        .map(|(k, _)| k.to_string())
        .collect()
}
