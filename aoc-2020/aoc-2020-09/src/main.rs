use aoc::{
    nom::{character::complete::newline, sequence::separated_pair, u64_list, usize},
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day09;

impl Solution<u64, u64> for Day09 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 9;
    const TITLE: &'static str = "Encoding Error";

    type Data = (usize, Vec<u64>);

    fn part1((preamble, nums): Self::Data) -> u64 {
        first_invalid(&nums, preamble).unwrap()
    }

    fn part2((preamble, nums): Self::Data) -> u64 {
        let num = first_invalid(&nums, preamble).unwrap();
        encryption_weakness(&nums, num).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        separated_pair(usize, newline, u64_list)(s)
    }
}

fn first_invalid(nums: &[u64], preamble: usize) -> Option<u64> {
    nums.windows(preamble + 1)
        .find(|&n| !find_pair(&n[0..preamble], n[preamble]))
        .map(|n| n[preamble])
}

fn find_pair(values: &[u64], sum: u64) -> bool {
    let mut vs = Vec::with_capacity(values.len());
    values.iter().any(|&v| {
        let r = vs.contains(&v);
        vs.push(sum - v);
        r
    })
}

fn encryption_weakness(nums: &[u64], num: u64) -> Option<u64> {
    let mut start = 0;
    let mut end = 0;
    let mut sum = nums[0];
    while end < nums.len() {
        if sum < num {
            end += 1;
            sum += nums[end];
        }
        if sum > num {
            sum -= nums[start];
            start += 1;
        }
        if sum == num {
            let min = *nums[start..end].iter().min()?;
            let max = *nums[start..end].iter().max()?;
            return Some(min + max);
        }
    }
    None
}
