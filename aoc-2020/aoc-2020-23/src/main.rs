use aoc::{Aoc, Solution};
use apply::Also as _;

#[derive(Aoc)]
pub struct Day23;

struct Game {
    cups: Vec<(usize, usize)>,
    pos: usize,
}

impl Solution<u64, u64> for Day23 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Crab Cups";

    type Data = Vec<usize>;

    fn part1(cups: Self::Data) -> u64 {
        Game::from(cups, 0).rounds(100).order()
    }

    fn part2(cups: Self::Data) -> u64 {
        Game::from(cups, 1_000_000).rounds(10_000_000).stars()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.trim()
                .chars()
                .map(|c| c as usize - '0' as usize)
                .collect(),
        ))
    }
}

impl Game {
    fn from(mut c: Vec<usize>, n: usize) -> Self {
        c.extend(c.len() + 1..=n);
        let pos = c[0];
        let cups = c
            .windows(3)
            .fold(vec![(0, 0); c.len() + 1], |mut cups, w| {
                cups[w[1]] = (w[0], w[2]);
                cups
            })
            .also(|cups| {
                cups[c[0]] = (c[c.len() - 1], c[1]);
                cups[c[c.len() - 1]] = (c[c.len() - 2], c[0]);
            });
        Game { cups, pos }
    }

    fn rounds(mut self, n: usize) -> Self {
        (0..n).for_each(|_| self.next());
        self
    }

    fn next(&mut self) {
        let end = self.cups.len() - 1;
        let mut dest = if self.pos == 1 { end } else { self.pos - 1 };
        let c1 = self.cups[self.pos].1;
        let c2 = self.cups[c1].1;
        let c3 = self.cups[c2].1;
        while c1 == dest || c2 == dest || c3 == dest {
            dest = if dest == 1 { end } else { dest - 1 };
        }
        self.cups[self.pos].1 = self.cups[c3].1;
        let p_dest = self.cups[dest].1;
        self.cups[dest].1 = c1;
        self.cups[c1] = (dest, c2);
        self.cups[c2] = (c1, c3);
        self.cups[c3] = (c2, p_dest);
        self.cups[p_dest].0 = c3;
        self.pos = self.cups[self.pos].1;
    }

    fn order(&self) -> u64 {
        (0..self.cups.len() - 2)
            .scan(1, |i, _| {
                *i = self.cups[*i].1;
                Some(*i)
            })
            .fold(0, |acc, x| acc * 10 + x as u64)
    }

    fn stars(&self) -> u64 {
        self.cups[1].1 as u64 * self.cups[self.cups[1].1].1 as u64
    }
}
