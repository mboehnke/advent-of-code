use std::iter::{once, repeat};

use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Day10;

impl Solution<usize, usize> for Day10 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Adapter Array";

    type Data = Vec<usize>;

    fn part1(mut joltages: Self::Data) -> usize {
        joltages.sort_unstable();
        joltages
            .into_iter()
            .scan(0, |p, n| {
                let d = Some(n - *p);
                *p = n;
                d
            })
            .fold(HashMap::new(), |mut ds, j| {
                *ds.entry(j).or_insert(0) += 1;
                ds
            })
            .apply(|d| d[&1] * (d[&3] + 1))
    }

    fn part2(mut joltages: Self::Data) -> usize {
        joltages.sort_unstable();
        let max = *joltages.last().unwrap() + 3;
        joltages.into_iter().chain(once(max)).fold(
            once(1).chain(repeat(0)).take(max + 1).collect::<Vec<_>>(),
            |mut a, j| {
                a[j] = a[j.saturating_sub(3)..j].iter().sum::<usize>();
                a
            },
        )[max]
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize_list(s)
    }
}
