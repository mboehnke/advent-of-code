use aoc::{
    nom::{character::streaming::char, combinator::map, multi::separated_list1, usize},
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day15;

#[derive(Clone, Default)]
pub struct Game {
    starting: Vec<usize>,
    spoken: Vec<usize>,
    turn: usize,
    last: usize,
}

impl Solution<usize, usize> for Day15 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Rambunctious Recitation";

    type Data = Game;

    fn part1(mut game: Self::Data) -> usize {
        game.nth(2020).unwrap()
    }

    fn part2(mut game: Self::Data) -> usize {
        game.nth(30_000_000).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        map(separated_list1(char(','), usize), |starting| Game {
            starting,
            ..Default::default()
        })(s)
    }
}

impl Iterator for Game {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let r = Some(self.last);
        if self.spoken.len() <= self.last {
            self.spoken.resize(self.last + 1, 0)
        }
        self.last = if self.turn < self.starting.len() {
            if self.turn > 0 {
                self.spoken[self.last] = self.turn;
            }
            self.starting[self.turn]
        } else {
            let n = self.turn - self.spoken[self.last];
            self.spoken[self.last] = self.turn;
            Some(n).filter(|&n| n != self.turn).unwrap_or(0)
        };
        self.turn += 1;
        r
    }
}
