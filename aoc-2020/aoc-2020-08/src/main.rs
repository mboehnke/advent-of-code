use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, line_ending},
        combinator::{map, opt},
        isize,
        multi::separated_list1,
        sequence::preceded,
    },
    Aoc, Solution,
};
use apply::Apply as _;
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Day08;
#[derive(Clone, Copy)]
pub enum Instruction {
    Acc(isize),
    Jmp(isize),
    Nop(isize),
}

#[derive(Clone)]
pub struct Program(Vec<Instruction>);

impl Solution<isize, isize> for Day08 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Handheld Halting";

    type Data = Program;

    fn part1(program: Self::Data) -> isize {
        program.run().unwrap_err()
    }

    fn part2(mut program: Self::Data) -> isize {
        program
            .0
            .clone()
            .iter()
            .enumerate()
            .filter_map(|(i, ins)| {
                match *ins {
                    Instruction::Jmp(v) => Instruction::Nop(v).apply(Some),
                    Instruction::Nop(v) if v != 0 => Instruction::Jmp(v).apply(Some),
                    _ => None,
                }
                .map(|ins| (i, ins))
            })
            .map(|(i, ins)| {
                let t = program.0[i];
                program.0[i] = ins;
                let r = program.run();
                program.0[i] = t;
                r
            })
            .find_map(Result::ok)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let value = move |s| preceded(opt(char('+')), isize)(s);
        let nop = map(preceded(tag("nop "), value), Instruction::Nop);
        let jmp = map(preceded(tag("jmp "), value), Instruction::Jmp);
        let acc = map(preceded(tag("acc "), value), Instruction::Acc);
        let instruction = alt((nop, jmp, acc));
        map(separated_list1(line_ending, instruction), Program)(s)
    }
}

impl Program {
    fn run(&self) -> Result<isize, isize> {
        let (mut ip, mut acc, mut executed) = (0, 0, HashSet::new());
        while executed.insert(ip) {
            ip = match self.0[ip] {
                Instruction::Acc(v) => {
                    acc += v;
                    ip + 1
                }
                Instruction::Jmp(v) => (ip as isize + v) as usize,
                Instruction::Nop(_) => ip + 1,
            };
            if ip >= self.0.len() {
                return Ok(acc);
            }
        }
        Err(acc)
    }
}
