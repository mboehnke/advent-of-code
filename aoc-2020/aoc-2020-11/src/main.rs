use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::value,
        multi::{many1, separated_list1},
    },
    Aoc, Solution,
};
use apply::Apply as _;
use itertools::{iproduct, unfold};

#[derive(Aoc)]
pub struct Day11;

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Seat {
    Empty,
    Occupied,
    None,
}

impl Solution<usize, usize> for Day11 {
    const YEAR: u32 = 2020;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Seating System";

    type Data = Vec<Vec<Seat>>;

    fn part1(seats: Self::Data) -> usize {
        process(seats, 4, adjacent).apply(occupied)
    }

    fn part2(seats: Self::Data) -> usize {
        process(seats, 5, in_los).apply(occupied)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let seat_empty = value(Seat::Empty, char('L'));
        let seat_none = value(Seat::None, char('.'));
        let seat = alt((seat_empty, seat_none));
        let row = many1(seat);
        separated_list1(newline, row)(s)
    }
}

fn process<T>(mut seats: Vec<Vec<Seat>>, max_vis: usize, vis_f: T) -> Vec<Vec<Seat>>
where
    T: Fn(&[Vec<Seat>], usize, usize, usize, usize) -> usize,
{
    let max_x = seats[0].len() - 1;
    let max_y = seats.len() - 1;
    let mut change = true;
    let mut o = vec![vec![0; seats[0].len()]; seats.len()];
    while change {
        for (x, y) in iproduct!(0..=max_x, 0..=max_y) {
            o[y][x] = vis_f(seats.as_slice(), x, y, max_x, max_y)
        }
        change = false;
        for (x, y) in iproduct!(0..=max_x, 0..=max_y) {
            seats[y][x] = match seats[y][x] {
                Seat::Empty if o[y][x] == 0 => {
                    change = true;
                    Seat::Occupied
                }
                Seat::Occupied if o[y][x] >= max_vis => {
                    change = true;
                    Seat::Empty
                }
                s => s,
            };
        }
    }
    seats
}

fn adjacent(seats: &[Vec<Seat>], x: usize, y: usize, max_x: usize, max_y: usize) -> usize {
    adjacent::adjacent_d(x, y, max_x, max_y)
        .map(|(x, y)| seats[y][x])
        .filter(Seat::is_occupied)
        .count()
}

fn in_los(seats: &[Vec<Seat>], x: usize, y: usize, max_x: usize, max_y: usize) -> usize {
    #[rustfmt::skip]
    const DIRS: [(isize, isize); 8] = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)];

    let next_seat_in_dir = |(dx, dy): (isize, isize)| {
        unfold((x as isize, y as isize), |(x, y)| {
            *x += dx;
            *y += dy;
            Some((*x, *y))
        })
        .take_while(|&(x, y)| x == x.clamp(0, max_x as isize) && y == y.clamp(0, max_y as isize))
        .map(|(x, y)| seats[y as usize][x as usize])
        .find(Seat::is_some)
    };

    DIRS.into_iter()
        .filter_map(next_seat_in_dir)
        .filter(Seat::is_occupied)
        .count()
}

fn occupied(seats: Vec<Vec<Seat>>) -> usize {
    seats
        .into_iter()
        .flatten()
        .filter(Seat::is_occupied)
        .count()
}

impl Seat {
    fn is_occupied(&self) -> bool {
        *self == Seat::Occupied
    }
    fn is_some(&self) -> bool {
        *self != Seat::None
    }
}
