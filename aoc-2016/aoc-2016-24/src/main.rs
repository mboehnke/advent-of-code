use aoc::{Aoc, Solution};
use hashbrown::{HashMap, HashSet};
use itertools::{iproduct, Itertools as _};
use std::iter::once;
use std::mem::take;

#[derive(Aoc)]
pub struct Sol;

type Pos = (usize, usize);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 24;
    const TITLE: &'static str = "Air Duct Spelunking";

    type Data = (Vec<Vec<bool>>, Pos, Vec<Pos>);

    fn part1(input: Self::Data) -> usize {
        shortest_path(input, false)
    }

    fn part2(input: Self::Data) -> usize {
        shortest_path(input, true)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let grid: Vec<Vec<char>> = s.lines().map(|l| l.chars().collect()).collect();
        let locations = iproduct!(0..grid[0].len(), 0..grid.len())
            .map(|(x, y)| (grid[y][x] as u8 - b'0', x, y))
            .filter(|&(_, x, y)| grid[y][x].is_ascii_digit())
            .collect_vec();
        let start = locations
            .iter()
            .find(|&(n, _, _)| *n == 0)
            .map(|&(_, x, y)| (x, y))
            .unwrap();
        let locations = locations
            .into_iter()
            .filter(|&(n, _, _)| n != 0)
            .map(|(_, x, y)| (x, y))
            .collect();
        let grid = grid
            .into_iter()
            .map(|l| l.into_iter().map(|c| c != '#').collect())
            .collect();
        Ok(("", (grid, start, locations)))
    }
}

fn shortest_path(
    (grid, start, locations): <Sol as Solution<usize, usize>>::Data,
    ret: bool,
) -> usize {
    let paths = locations
        .iter()
        .chain(once(&start))
        .combinations(2)
        .map(|ps| (*ps[0], *ps[1]))
        .map(|(a, b)| ((a, b), path_len(&grid, a, b)))
        .collect::<HashMap<_, _>>();
    let len = locations.len();
    locations
        .into_iter()
        .permutations(len)
        .map(|mut ps| {
            if ret {
                ps.push(start);
            }
            ps
        })
        .map(|locs| {
            locs.iter()
                .scan(start, |s, e| {
                    let len = *paths
                        .get(&(*s, *e))
                        .or_else(|| paths.get(&(*e, *s)))
                        .unwrap();
                    *s = *e;
                    Some(len)
                })
                .sum::<usize>()
        })
        .min()
        .unwrap()
}

fn path_len(grid: &[Vec<bool>], b: Pos, e: Pos) -> usize {
    let mut len = 0;
    let mut visited = HashSet::new();
    let mut prev = vec![b];
    while !visited.contains(&e) {
        for (x, y) in take(&mut prev) {
            if y > 0 && grid[y - 1][x] && !visited.contains(&(x, y - 1)) {
                prev.push((x, y - 1))
            }
            if y < grid.len() - 1 && grid[y + 1][x] && !visited.contains(&(x, y + 1)) {
                prev.push((x, y + 1))
            }
            if x > 0 && grid[y][x - 1] && !visited.contains(&(x - 1, y)) {
                prev.push((x - 1, y))
            }
            if x < grid[0].len() - 1 && grid[y][x + 1] && !visited.contains(&(x + 1, y)) {
                prev.push((x + 1, y))
            }
            for &p in &prev {
                visited.insert(p);
            }
        }
        len += 1;
    }
    len
}
