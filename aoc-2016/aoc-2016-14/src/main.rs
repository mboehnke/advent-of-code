use aoc::{Aoc, Solution};
use md5::Digest;
use std::collections::VecDeque;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 14;
    const TITLE: &'static str = "One-Time Pad";

    type Data = String;

    fn part1(salt: Self::Data) -> usize {
        keys(&salt, 0).last().unwrap().0
    }

    fn part2(salt: Self::Data) -> usize {
        keys(&salt, 2016).last().unwrap().0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::alpha1, combinator::map};

        map(alpha1, String::from)(s)
    }
}

fn keys(salt: &str, stretch: usize) -> Vec<(usize, String)> {
    let hash = |i: usize| -> Vec<char> {
        (0..=stretch)
            .fold(format!("{}{}", salt, i), |h, _| compute_hash(h))
            .chars()
            .collect()
    };
    let mut keys = Vec::new();
    let mut hashes = (0..1000).map(hash).collect::<VecDeque<Vec<char>>>();
    let mut i = 0;
    while keys.len() < 64 {
        hashes.push_back(hash(i + 1000));
        let hash = hashes.pop_front().unwrap();
        if let Some(tc) = (0..hash.len() - 2)
            .find(|i| hash[*i] == hash[*i + 1] && hash[*i] == hash[*i + 2])
            .map(|i| hash[i])
        {
            if hashes.iter().any(|h| {
                (0..hash.len() - 4).any(|i| {
                    h[i] == tc
                        && h[i] == h[i + 1]
                        && h[i] == h[i + 2]
                        && h[i] == h[i + 3]
                        && h[i] == h[i + 4]
                })
            }) {
                keys.push((i, hash.iter().collect()))
            }
        }
        i += 1;
    }
    keys
}

fn compute_hash(data: String) -> String {
    let mut hasher = md5::Md5::new();
    hasher.update(data);
    format!("{:x}", hasher.finalize())
}
