use std::fmt::Write;

use aoc::{Aoc, Solution};
use std::convert::TryFrom;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Dir {
    U,
    D,
    L,
    R,
}

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Bathroom Security";

    type Data = Vec<Vec<Dir>>;

    fn part1(directions: Self::Data) -> String {
        directions
            .into_iter()
            .scan(5, |p, dirs| {
                *p = dirs.into_iter().fold(*p, next);
                Some(*p)
            })
            .map(|n| n.to_string())
            .collect()
    }

    fn part2(directions: Self::Data) -> String {
        directions
            .into_iter()
            .scan(5, |p, dirs| {
                *p = dirs.into_iter().fold(*p, next13);
                Some(*p)
            })
            .fold(String::new(), |mut res, n| {
                write!(res, "{:X}", n).unwrap();
                res
            })
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{char, newline},
            combinator::value,
            multi::{many1, separated_list1},
        };

        let dir_u = value(Dir::U, char('U'));
        let dir_d = value(Dir::D, char('D'));
        let dir_l = value(Dir::L, char('L'));
        let dir_r = value(Dir::R, char('R'));
        let dir = alt((dir_u, dir_d, dir_l, dir_r));
        separated_list1(newline, many1(dir))(s)
    }
}

fn next(n: usize, d: Dir) -> usize {
    match d {
        Dir::U => match n {
            4 => 1,
            5 => 2,
            6 => 3,
            7 => 4,
            8 => 5,
            9 => 6,
            _ => n,
        },
        Dir::D => match n {
            1 => 4,
            2 => 5,
            3 => 6,
            4 => 7,
            5 => 8,
            6 => 9,
            _ => n,
        },
        Dir::L => match n {
            2 => 1,
            3 => 2,
            5 => 4,
            6 => 5,
            8 => 7,
            9 => 8,
            _ => n,
        },
        Dir::R => match n {
            1 => 2,
            2 => 3,
            4 => 5,
            5 => 6,
            7 => 8,
            8 => 9,
            _ => n,
        },
    }
}

fn next13(n: usize, d: Dir) -> usize {
    match d {
        Dir::U => match n {
            3 => 1,
            6 => 2,
            7 => 3,
            8 => 4,
            10 => 6,
            11 => 7,
            12 => 8,
            13 => 11,
            _ => n,
        },
        Dir::D => match n {
            1 => 3,
            2 => 6,
            3 => 7,
            4 => 8,
            6 => 10,
            7 => 11,
            8 => 12,
            11 => 13,
            _ => n,
        },
        Dir::L => match n {
            6 => 5,
            3 => 2,
            7 => 6,
            11 => 10,
            4 => 3,
            8 => 7,
            12 => 11,
            9 => 8,
            _ => n,
        },
        Dir::R => match n {
            5 => 6,
            2 => 3,
            6 => 7,
            10 => 11,
            3 => 4,
            7 => 8,
            11 => 12,
            8 => 9,
            _ => n,
        },
    }
}

impl TryFrom<char> for Dir {
    type Error = String;

    fn try_from(s: char) -> Result<Self, Self::Error> {
        match s {
            'U' => Ok(Dir::U),
            'D' => Ok(Dir::D),
            'L' => Ok(Dir::L),
            'R' => Ok(Dir::R),
            _ => Err(format!("invalid direction: {}", s)),
        }
    }
}
