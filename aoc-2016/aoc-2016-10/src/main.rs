use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Ins {
    Value { val: usize, id: usize },
    Bot { id: usize, low: Dest, high: Dest },
}

#[derive(Debug, Clone, Copy)]
pub enum Dest {
    Output(usize),
    Bot(usize),
}

#[derive(Debug, Clone, Default)]
pub struct Bot {
    val1: Option<usize>,
    val2: Option<usize>,
    destl: Option<Dest>,
    desth: Option<Dest>,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Balance Bots";

    type Data = (Vec<Ins>, usize, usize);

    fn part1((instructions, l, h): Self::Data) -> usize {
        instructions
            .apply(eval)
            .0
            .into_iter()
            .find(|(_, b)| {
                b.val1 == Some(l) && b.val2 == Some(h) || b.val2 == Some(l) && b.val1 == Some(h)
            })
            .unwrap()
            .0
    }

    fn part2((instructions, _, _): Self::Data) -> usize {
        instructions.apply(eval).1.into_iter().product()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::instructions_l_h(s)
    }
}

fn eval(instructions: Vec<Ins>) -> (HashMap<usize, Bot>, Vec<usize>) {
    let mut bots =
        instructions
            .into_iter()
            .fold(HashMap::new(), |mut bots: HashMap<usize, Bot>, i| {
                match i {
                    Ins::Value { val, id } => {
                        let bot = bots.entry(id).or_default();
                        if bot.val1.is_none() {
                            bot.val1 = Some(val)
                        } else {
                            bot.val2 = Some(val)
                        }
                    }
                    Ins::Bot { id, low, high } => {
                        let bot = bots.entry(id).or_default();
                        bot.destl = Some(low);
                        bot.desth = Some(high);
                    }
                };
                bots
            });
    let mut outputs: Vec<usize> = vec![0; 3];
    let mut change = true;
    while change {
        change = false;
        for (_, bot) in bots.clone() {
            if let (Some(v1), Some(v2), Some(dl), Some(dh)) =
                (bot.val1, bot.val2, bot.destl, bot.desth)
            {
                let (vl, vh) = if v1 < v2 { (v1, v2) } else { (v2, v1) };
                for &(d, v) in &[(dl, vl), (dh, vh)] {
                    match d {
                        Dest::Bot(id) => {
                            if let Some(v1) = bots.get(&id).unwrap().val1 {
                                if bots.get(&id).unwrap().val2.is_none() && v1 != v {
                                    bots.entry(id).or_default().val2 = Some(v);
                                    change = true;
                                }
                            } else {
                                bots.entry(id).or_default().val1 = Some(v);
                                change = true;
                            }
                        }
                        Dest::Output(id) if id < 3 => outputs[id] = v,
                        _ => {}
                    }
                }
            }
        }
    }
    (bots, outputs)
}

mod parser {
    use aoc::nom::{
        branch::alt, bytes::complete::tag, character::complete::newline, combinator::map,
        multi::separated_list0, separated_triple, sequence::preceded, usize, IResult,
    };

    use crate::{Dest, Ins};

    pub fn instructions_l_h(s: &str) -> IResult<&str, (Vec<Ins>, usize, usize)> {
        let (s, (l, h, instructions)) =
            separated_triple(usize, newline, usize, newline, instruction_list)(s)?;
        Ok((s, (instructions, l, h)))
    }

    fn instruction_list(s: &str) -> IResult<&str, Vec<Ins>> {
        separated_list0(newline, instruction)(s)
    }

    fn instruction(s: &str) -> IResult<&str, Ins> {
        alt((ins_value, ins_bot))(s)
    }

    fn ins_value(s: &str) -> IResult<&str, Ins> {
        let (s, val) = preceded(tag("value "), usize)(s)?;
        let (s, id) = preceded(tag(" goes to bot "), usize)(s)?;
        Ok((s, Ins::Value { val, id }))
    }

    fn ins_bot(s: &str) -> IResult<&str, Ins> {
        let (s, id) = preceded(tag("bot "), usize)(s)?;
        let (s, low) = preceded(tag(" gives low to "), dest)(s)?;
        let (s, high) = preceded(tag(" and high to "), dest)(s)?;
        Ok((s, Ins::Bot { id, low, high }))
    }

    fn dest(s: &str) -> IResult<&str, Dest> {
        let output = map(preceded(tag("output "), usize), Dest::Output);
        let bot = map(preceded(tag("bot "), usize), Dest::Bot);
        alt((output, bot))(s)
    }
}
