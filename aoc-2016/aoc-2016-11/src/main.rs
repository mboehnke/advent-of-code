use aoc::{Aoc, Solution};
use hashbrown::HashMap;
use itertools::Itertools as _;
use std::hash::Hash;
use std::mem::take;

#[derive(Aoc)]
pub struct Sol;

type Map = Vec<Vec<Item>>;
type Memo = HashMap<Map, usize>;

#[derive(Debug, Clone)]
pub struct State {
    items: Map,
    floor: usize,
    steps: usize,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Item {
    Generator(usize),
    Microchip(usize),
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Radioisotope Thermoelectric Generators";

    type Data = State;

    fn part1(state: Self::Data) -> usize {
        state.move_items().steps
    }

    fn part2(mut state: Self::Data) -> usize {
        state.items[state.floor].append(&mut vec![
            Item::Generator(6),
            Item::Generator(7),
            Item::Microchip(6),
            Item::Microchip(7),
        ]);
        state.move_items().steps
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let (s, items) = parser::map(s)?;
        let floor = items.len() - 1;
        Ok((
            s,
            State {
                items,
                floor,
                steps: 0,
            },
        ))
    }
}

fn move_items(
    floors: Map,
    floor: usize,
    mut previous: Memo,
    mut steps: usize,
) -> Result<(Memo, Map, usize), Memo> {
    if let Some(&s) = previous.get(&floors) {
        if steps < s {
            previous.insert(floors.clone(), steps);
        }
        return Err(previous);
    }
    previous.insert(floors.clone(), steps);
    if is_done(&floors) {
        return Ok((previous, floors, steps));
    }
    if !is_valid(&floors) {
        return Err(previous);
    }
    steps += 1;
    let mut done = Vec::new();
    if floor > 0 {
        for is in floors[floor].iter().combinations(2) {
            let f = floor - 1;
            let mut fs = floors.clone();
            fs[floor].retain(|i| !is.contains(&i));
            for i in is {
                fs[f].push(*i);
                fs[f].sort_unstable();
            }
            let p = take(&mut previous);
            match move_items(fs, f, p, steps) {
                Ok((p, f, s)) => {
                    previous = p;
                    done.push((f, s));
                }
                Err(p) => previous = p,
            };
        }
    }
    if floor < floors.len() - 1 {
        for is in &floors[floor] {
            let f = floor + 1;
            let mut fs = floors.clone();
            fs[floor].retain(|i| i != is);
            fs[f].push(*is);
            let p = take(&mut previous);
            match move_items(fs, f, p, steps) {
                Ok((p, f, s)) => {
                    previous = p;
                    done.push((f, s));
                }
                Err(p) => previous = p,
            };
        }
    }
    done.into_iter()
        .min_by_key(|&(_, n)| n)
        .map(|(fs, n)| (previous.clone(), fs, n))
        .ok_or(previous)
}

fn is_valid(floors: &[Vec<Item>]) -> bool {
    for floor in floors {
        let mut microchips = Vec::new();
        let mut generators = Vec::new();
        for item in floor {
            match *item {
                Item::Microchip(n) => microchips.push(n),
                Item::Generator(n) => generators.push(n),
            }
        }
        for n in microchips {
            if !generators.is_empty() && !generators.contains(&n) {
                return false;
            }
        }
    }
    true
}

fn is_done(floors: &[Vec<Item>]) -> bool {
    floors.iter().skip(1).all(|f| f.is_empty())
}

impl State {
    fn move_items(mut self) -> Self {
        let (_, i, s) = move_items(self.items, self.floor, HashMap::new(), 0).unwrap();
        self.items = i;
        self.steps = s;
        self.floor = 0;
        self
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, char, newline},
        combinator::opt,
        multi::separated_list0,
        sequence::delimited,
        IResult,
    };
    use hashbrown::HashMap;

    use crate::{Item, Map};

    enum RItem<'a> {
        Gen(&'a str),
        Chip(&'a str),
    }

    pub fn map(s: &str) -> IResult<&str, Map> {
        let (s, floors) = floor_list(s)?;

        let ids = floors
            .iter()
            .flatten()
            .map(|i| match *i {
                RItem::Gen(n) => n,
                RItem::Chip(n) => n,
            })
            .fold(HashMap::new(), |mut names, name| {
                if !names.contains_key(name) {
                    let id = names.len();
                    names.insert(name, id);
                }
                names
            });

        let map = floors
            .into_iter()
            .rev()
            .map(|floor| {
                floor.into_iter().fold(Vec::new(), |mut items, item| {
                    let i = match item {
                        RItem::Gen(name) => Item::Generator(*ids.get(name).unwrap()),
                        RItem::Chip(name) => Item::Microchip(*ids.get(name).unwrap()),
                    };
                    items.push(i);
                    items
                })
            })
            .collect();

        Ok((s, map))
    }

    fn floor_list(s: &str) -> IResult<&str, Vec<Vec<RItem>>> {
        separated_list0(newline, floor)(s)
    }

    fn floor(s: &str) -> IResult<&str, Vec<RItem>> {
        let (s, _) = tag("The ")(s)?;
        let (s, _) = alpha1(s)?;
        let (s, _) = tag(" floor contains ")(s)?;
        let (s, items) = alt((floor_empty, ritem_list))(s)?;
        let (s, _) = char('.')(s)?;
        Ok((s, items))
    }

    fn floor_empty(s: &str) -> IResult<&str, Vec<RItem>> {
        let (s, _) = tag("nothing relevant")(s)?;
        Ok((s, Vec::new()))
    }

    fn ritem_list(s: &str) -> IResult<&str, Vec<RItem>> {
        separated_list0(tag(", "), r_item)(s)
    }

    fn r_item(s: &str) -> IResult<&str, RItem> {
        alt((r_item_gen, r_item_chip))(s)
    }

    fn r_item_gen(s: &str) -> IResult<&str, RItem> {
        let (s, _) = opt(tag("and "))(s)?;
        let (s, name) = delimited(tag("a "), alpha1, tag(" generator"))(s)?;
        Ok((s, RItem::Gen(name)))
    }

    fn r_item_chip(s: &str) -> IResult<&str, RItem> {
        let (s, _) = opt(tag("and "))(s)?;
        let (s, name) = delimited(tag("a "), alpha1, tag("-compatible microchip"))(s)?;
        Ok((s, RItem::Chip(name)))
    }
}
