use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Operation {
    SwapP(usize, usize),
    SwapC(char, char),
    RotateL(usize),
    RotateR(usize),
    RotateC(char),
    Reverse(usize, usize),
    Move(usize, usize),
}

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 21;
    const TITLE: &'static str = "Scrambled Letters and Hash";

    type Data = (String, Vec<Operation>);

    fn part1((password, operations): Self::Data) -> String {
        operations.into_iter().fold(password, |p, o| o.scramble(p))
    }

    fn part2((_, operations): Self::Data) -> String {
        const PASSWORD: &str = "fbgdceah";
        operations
            .into_iter()
            .rev()
            .fold(PASSWORD.to_string(), |p, o| o.unscramble(p))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::password_operations(s)
    }
}

impl Operation {
    fn scramble(&self, password: String) -> String {
        let pw = password.chars().collect();
        match *self {
            Operation::SwapP(x, y) => Self::swap_p(pw, x, y),
            Operation::SwapC(x, y) => Self::swap_c(pw, x, y),
            Operation::RotateL(x) => Self::rotate_l(pw, x),
            Operation::RotateR(x) => Self::rotate_r(pw, x),
            Operation::RotateC(x) => Self::rotate_c(pw, x),
            Operation::Reverse(x, y) => Self::reverse(pw, x, y),
            Operation::Move(x, y) => Self::move_p(pw, x, y),
        }
        .into_iter()
        .collect()
    }
    fn unscramble(&self, password: String) -> String {
        let pw = password.chars().collect();
        match *self {
            Operation::SwapP(x, y) => Self::swap_p(pw, x, y),
            Operation::SwapC(x, y) => Self::swap_c(pw, x, y),
            Operation::RotateL(x) => Self::rotate_r(pw, x),
            Operation::RotateR(x) => Self::rotate_l(pw, x),
            Operation::RotateC(x) => Self::un_rotate_c(pw, x),
            Operation::Reverse(x, y) => Self::reverse(pw, x, y),
            Operation::Move(x, y) => Self::move_p(pw, y, x),
        }
        .into_iter()
        .collect()
    }
    fn swap_p(mut pw: Vec<char>, x: usize, y: usize) -> Vec<char> {
        pw.swap(x, y);
        pw
    }
    fn swap_c(mut pw: Vec<char>, x: char, y: char) -> Vec<char> {
        let x = pw.iter().enumerate().find(|(_, c)| **c == x).unwrap().0;
        let y = pw.iter().enumerate().find(|(_, c)| **c == y).unwrap().0;
        pw.swap(x, y);
        pw
    }
    fn rotate_l(pw: Vec<char>, x: usize) -> Vec<char> {
        pw[x..].iter().chain(pw[0..x].iter()).copied().collect()
    }
    fn rotate_r(pw: Vec<char>, x: usize) -> Vec<char> {
        let x = pw.len() - x;
        Self::rotate_l(pw, x)
    }
    fn rotate_c(pw: Vec<char>, x: char) -> Vec<char> {
        let i = pw.iter().enumerate().find(|(_, c)| **c == x).unwrap().0;
        let x = (1 + i + usize::from(i >= 4)) % pw.len();
        Self::rotate_r(pw, x)
    }
    fn un_rotate_c(pw: Vec<char>, x: char) -> Vec<char> {
        (0..pw.len())
            .map(|x| Self::rotate_l(pw.clone(), x))
            .find(|p| Self::rotate_c(p.clone(), x) == pw)
            .unwrap()
    }
    fn reverse(mut pw: Vec<char>, mut x: usize, mut y: usize) -> Vec<char> {
        while x < y {
            pw.swap(x, y);
            x += 1;
            y -= 1;
        }
        pw
    }
    fn move_p(mut pw: Vec<char>, x: usize, y: usize) -> Vec<char> {
        let l = pw[x];
        pw.remove(x);
        pw.insert(y, l);
        pw
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, anychar, char, newline},
        combinator::{map, opt},
        multi::separated_list0,
        sequence::{delimited, preceded, separated_pair, terminated},
        usize, IResult,
    };

    use crate::Operation;

    pub fn password_operations(s: &str) -> IResult<&str, (String, Vec<Operation>)> {
        let password = map(alpha1, String::from);
        separated_pair(password, newline, op_list)(s)
    }

    fn op_list(s: &str) -> IResult<&str, Vec<Operation>> {
        separated_list0(newline, op)(s)
    }

    fn op(s: &str) -> IResult<&str, Operation> {
        alt((
            op_reverse,
            op_move,
            op_swap_p,
            op_swap_c,
            op_rotate_l,
            op_rotate_r,
            op_rotate_c,
        ))(s)
    }

    fn op_reverse(s: &str) -> IResult<&str, Operation> {
        let (s, p1) = preceded(tag("reverse positions "), usize)(s)?;
        let (s, p2) = preceded(tag(" through "), usize)(s)?;
        Ok((s, Operation::Reverse(p1, p2)))
    }

    fn op_move(s: &str) -> IResult<&str, Operation> {
        let (s, p1) = preceded(tag("move position "), usize)(s)?;
        let (s, p2) = preceded(tag(" to position "), usize)(s)?;
        Ok((s, Operation::Move(p1, p2)))
    }

    fn op_swap_p(s: &str) -> IResult<&str, Operation> {
        let (s, p1) = preceded(tag("swap position "), usize)(s)?;
        let (s, p2) = preceded(tag(" with position "), usize)(s)?;
        Ok((s, Operation::SwapP(p1, p2)))
    }

    fn op_swap_c(s: &str) -> IResult<&str, Operation> {
        let (s, c1) = preceded(tag("swap letter "), anychar)(s)?;
        let (s, c2) = preceded(tag(" with letter "), anychar)(s)?;
        Ok((s, Operation::SwapC(c1, c2)))
    }

    fn op_rotate_l(s: &str) -> IResult<&str, Operation> {
        let prec = tag("rotate left ");
        let succ = tag(" step");
        let op = map(delimited(prec, usize, succ), Operation::RotateL);
        terminated(op, opt(char('s')))(s)
    }

    fn op_rotate_r(s: &str) -> IResult<&str, Operation> {
        let prec = tag("rotate right ");
        let succ = tag(" step");
        let op = map(delimited(prec, usize, succ), Operation::RotateR);
        terminated(op, opt(char('s')))(s)
    }

    fn op_rotate_c(s: &str) -> IResult<&str, Operation> {
        let prec = tag("rotate based on position of letter ");
        map(preceded(prec, anychar), Operation::RotateC)(s)
    }
}
