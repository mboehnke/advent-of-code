use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 20;
    const TITLE: &'static str = "Firewall Rules";

    type Data = (usize, usize, Vec<(usize, usize)>);

    fn part1((_, _, blacklist): Self::Data) -> usize {
        flatten(blacklist)[0].1 + 1
    }

    fn part2((min, max, blacklist): Self::Data) -> usize {
        let blocked: usize = flatten(blacklist)
            .into_iter()
            .map(|(min, max)| max - min + 1)
            .sum();
        max - min + 1 - blocked
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{char, newline},
            multi::separated_list0,
            separated_triple,
            sequence::separated_pair,
            usize,
        };

        let numbers = separated_pair(usize, char('-'), usize);
        let numbers_list = separated_list0(newline, numbers);
        separated_triple(usize, newline, usize, newline, numbers_list)(s)
    }
}

fn flatten(mut blacklist: Vec<(usize, usize)>) -> Vec<(usize, usize)> {
    blacklist.sort_unstable_by_key(|&(min, _)| min);
    blacklist
        .iter()
        .skip(1)
        .fold(vec![blacklist[0]], |mut fb, b| {
            let m = fb.last().unwrap().1;
            if m + 1 >= b.0 {
                fb.last_mut().unwrap().1 = b.1.max(m);
            } else {
                fb.push(*b)
            }
            fb
        })
}
