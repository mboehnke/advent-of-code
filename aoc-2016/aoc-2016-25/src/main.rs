use std::iter::repeat;

use aoc::{Aoc, Solution};
use assembly::{Computer, State};

#[derive(Aoc)]
pub struct Sol;

pub struct Clock(Computer);

impl Solution<isize, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 25;
    const TITLE: &'static str = "Clock Signal";

    type Data = Computer;

    fn part1(computer: Self::Data) -> isize {
        let e = repeat(vec![0, 1]).flatten().take(1000).collect::<Vec<_>>();
        (0..)
            .find(|&num| {
                Clock::from(&computer, num)
                    .zip(e.iter())
                    .all(|(a, b)| a == *b)
            })
            .unwrap()
    }

    fn part2(_: Self::Data) -> String {
        String::new()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}

impl Clock {
    fn from(computer: &Computer, num: isize) -> Self {
        let mut computer = computer.clone();
        computer.set_a(num);
        Clock(computer)
    }
}

impl Iterator for Clock {
    type Item = isize;

    fn next(&mut self) -> Option<Self::Item> {
        match self.0.run() {
            State::Output(x) => Some(x),
            _ => None,
        }
    }
}
