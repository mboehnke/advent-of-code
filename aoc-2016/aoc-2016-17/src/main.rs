use aoc::{Aoc, Solution};
use apply::Apply as _;
use md5::Digest as _;
use std::mem::take;

#[derive(Aoc)]
pub struct Sol;

impl Solution<String, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 17;
    const TITLE: &'static str = "Two Steps Forward";

    type Data = String;

    fn part1(passcode: Self::Data) -> String {
        let mut paths = vec![String::new()];
        loop {
            for p in take(&mut paths).into_iter().flat_map(|path| {
                open_doors(&passcode, &path)
                    .into_iter()
                    .map(move |dir| format!("{}{}", path, dir))
            }) {
                match valid_position(&p) {
                    None => {}
                    Some((3, 3)) => return p,
                    _ => paths.push(p),
                }
            }
        }
    }

    fn part2(passcode: Self::Data) -> usize {
        let mut paths = vec![String::new()];
        let mut valid_paths = Vec::new();
        while !paths.is_empty() {
            for p in take(&mut paths).into_iter().flat_map(|path| {
                open_doors(&passcode, &path)
                    .into_iter()
                    .map(move |dir| format!("{}{}", path, dir))
            }) {
                match valid_position(&p) {
                    None => {}
                    Some((3, 3)) => valid_paths.push(p.len()),
                    _ => paths.push(p),
                }
            }
        }
        valid_paths.into_iter().max().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::alpha1, combinator::map};

        map(alpha1, String::from)(s)
    }
}

fn valid_position(path: &str) -> Option<(usize, usize)> {
    let (mut x, mut y) = (0, 0);
    for c in path.chars() {
        match c {
            'L' if x == 0 => return None,
            'R' if x >= 3 => return None,
            'U' if y == 0 => return None,
            'D' if y >= 3 => return None,
            'L' => x -= 1,
            'R' => x += 1,
            'U' => y -= 1,
            'D' => y += 1,
            _ => panic!("invalid direction"),
        }
    }
    Some((x, y))
}

fn open_doors(passcode: &str, path: &str) -> Vec<char> {
    format!("{}{}", passcode, path)
        .apply(compute)
        .chars()
        .zip(vec!['U', 'D', 'L', 'R'])
        .filter(|&(x, _)| matches!(x, 'b' | 'c' | 'd' | 'e' | 'f'))
        .map(|(_, x)| x)
        .collect()
}

fn compute(data: String) -> String {
    let mut hasher = md5::Md5::new();
    hasher.update(data);
    format!("{:x}", hasher.finalize())
}
