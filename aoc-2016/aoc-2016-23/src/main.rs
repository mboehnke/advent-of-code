use aoc::{nom::combinator::map, Aoc, Solution};
use assembly::Computer;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Safe Cracking";

    type Data = Computer;

    fn part1(mut computer: Self::Data) -> isize {
        computer.run();
        computer.get_a()
    }

    fn part2(mut computer: Self::Data) -> isize {
        computer.set_a(12);
        computer.run();
        computer.get_a()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::newline, isize, parse, sequence::separated_pair};

        map(
            separated_pair(isize, newline, parse),
            |(v, mut c): (isize, Computer)| {
                c.set_a(v);
                c
            },
        )(s)
    }
}
