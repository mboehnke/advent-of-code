use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<i32, u32> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 19;
    const TITLE: &'static str = "An Elephant Named Joseph";

    type Data = u32;

    fn part1(num: Self::Data) -> i32 {
        format!("{:b}", num)
            .chars()
            .skip(1)
            .chain(Some('1'))
            .fold(0, |acc, x| (acc << 1) + i32::from(x == '1'))
    }

    fn part2(num: Self::Data) -> u32 {
        let mut v: Vec<u32> = (1..=num).collect();
        while v.len() > 1 {
            let prev = std::mem::take(&mut v);
            let (f, a, i) = ((prev.len() + 2) / 3, prev.len() / 2, 2 - prev.len() % 2);
            v.extend_from_slice(&prev[f..a]);
            v.extend((a + i..prev.len()).step_by(3).map(|i| prev[i]));
            v.extend_from_slice(&prev[..f]);
        }
        v[0]
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::character::complete::u32(s)
    }
}
