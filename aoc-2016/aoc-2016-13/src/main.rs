use std::usize;

use aoc::{Aoc, Solution};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 13;
    const TITLE: &'static str = "A Maze of Twisty Little Cubicles";

    type Data = (usize, usize, usize);

    fn part1((xd, yd, num): Self::Data) -> usize {
        let mut map = HashMap::new();
        map.insert((1, 1), true);
        let mut steps = 0;
        let mut coords = vec![(1, 1)];
        loop {
            let mut coordsn = Vec::new();
            for (x, y) in coords.into_iter().collect::<Vec<_>>() {
                if (x, y) == (xd, yd) {
                    return steps;
                }
                for (nx, ny) in [(x + 1, y), (x, y + 1)]
                    .iter()
                    .cloned()
                    .chain(if x > 0 { Some((x - 1, y)) } else { None })
                    .chain(if y > 0 { Some((x, y - 1)) } else { None })
                {
                    if !map.contains_key(&(nx, ny)) {
                        let o = open(nx, ny, num);
                        if o {
                            coordsn.push((nx, ny));
                        }
                        map.insert((nx, ny), o);
                    }
                }
            }
            steps += 1;
            coords = coordsn;
        }
    }

    fn part2((_, _, num): Self::Data) -> usize {
        let mut map = HashMap::new();
        map.insert((1, 1), true);
        let mut coords = vec![(1, 1)];
        for _ in 0..50 {
            let mut coordsn = Vec::new();
            for (x, y) in coords.into_iter().collect::<Vec<_>>() {
                for (nx, ny) in [(x + 1, y), (x, y + 1)]
                    .iter()
                    .cloned()
                    .chain(if x > 0 { Some((x - 1, y)) } else { None })
                    .chain(if y > 0 { Some((x, y - 1)) } else { None })
                {
                    if !map.contains_key(&(nx, ny)) {
                        let o = open(nx, ny, num);
                        if o {
                            coordsn.push((nx, ny));
                        }
                        map.insert((nx, ny), o);
                    }
                }
            }
            coords = coordsn;
        }
        map.into_iter().filter(|&(_, o)| o).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::newline, separated_triple, usize};

        separated_triple(usize, newline, usize, newline, usize)(s)
    }
}

fn open(x: usize, y: usize, num: usize) -> bool {
    let mut n = x * x + 3 * x + 2 * x * y + y + y * y + num;
    let mut o = true;
    while n > 0 {
        if n % 2 != 0 {
            o = !o;
        }
        n >>= 1;
    }
    o
}
