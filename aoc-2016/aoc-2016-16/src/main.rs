use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone)]
pub struct DragonCurve(Vec<bool>);

#[derive(Clone)]
pub struct Checksum(Vec<bool>);

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Dragon Checksum";

    type Data = (usize, Vec<bool>);

    fn part1((size, data): Self::Data) -> String {
        DragonCurve(data)
            .find(|s| s.len() >= size)
            .map(|d| checksum(&d[0..size]))
            .unwrap()
    }

    fn part2((_, data): Self::Data) -> String {
        Self::part1((35651584, data))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{char, newline},
            combinator::value,
            multi::many1,
            sequence::separated_pair,
            usize,
        };

        let bin_digit = alt((value(true, char('1')), value(false, char('0'))));
        separated_pair(usize, newline, many1(bin_digit))(s)
    }
}

impl Iterator for DragonCurve {
    type Item = Vec<bool>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0 = self
            .0
            .iter()
            .copied()
            .chain(Some(false))
            .chain(self.0.iter().rev().map(|&c| !c))
            .collect();
        Some(self.0.clone())
    }
}

fn checksum(s: &[bool]) -> String {
    let mut csize = 1;
    while s.len() % (csize * 2) == 0 {
        csize *= 2;
    }
    s.chunks(csize)
        .map(|c| c.iter().fold(true, |acc, &x| acc != x))
        .map(|b| if b { '1' } else { '0' })
        .collect()
}
