use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Copy)]
pub enum Dir {
    L(isize),
    R(isize),
}
#[derive(Debug, Clone, Copy)]
pub enum Direction {
    U,
    D,
    L,
    R,
}

#[derive(Debug, Clone)]
pub struct Path {
    pos: (isize, isize),
    dir: Direction,
    directions: Vec<Dir>,
    i: usize,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 1;
    const TITLE: &'static str = "No Time for a Taxicab";

    type Data = Path;

    fn part1(path: Self::Data) -> usize {
        path.flatten().last().unwrap().apply(distance)
    }

    fn part2(path: Self::Data) -> usize {
        let mut visited = HashSet::new();
        path.flatten()
            .find(|&pos| !visited.insert(pos))
            .unwrap()
            .apply(distance)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::path(s)
    }
}

fn distance((x, y): (isize, isize)) -> usize {
    (x.abs() + y.abs()) as usize
}

impl Iterator for Path {
    type Item = Vec<(isize, isize)>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.i >= self.directions.len() {
            return None;
        }
        let (x, y, d) = match (self.dir, self.directions[self.i]) {
            (Direction::U, Dir::L(n)) => (self.pos.0 - n, self.pos.1, Direction::L),
            (Direction::U, Dir::R(n)) => (self.pos.0 + n, self.pos.1, Direction::R),
            (Direction::D, Dir::L(n)) => (self.pos.0 + n, self.pos.1, Direction::R),
            (Direction::D, Dir::R(n)) => (self.pos.0 - n, self.pos.1, Direction::L),
            (Direction::R, Dir::L(n)) => (self.pos.0, self.pos.1 - n, Direction::U),
            (Direction::R, Dir::R(n)) => (self.pos.0, self.pos.1 + n, Direction::D),
            (Direction::L, Dir::L(n)) => (self.pos.0, self.pos.1 + n, Direction::D),
            (Direction::L, Dir::R(n)) => (self.pos.0, self.pos.1 - n, Direction::U),
        };
        let p = match d {
            Direction::U => (1..=self.pos.1 - y)
                .map(|dy| (self.pos.0, self.pos.1 - dy))
                .collect(),
            Direction::D => (1..=y - self.pos.1)
                .map(|dy| (self.pos.0, self.pos.1 + dy))
                .collect(),
            Direction::L => (1..=self.pos.0 - x)
                .map(|dx| (self.pos.0 - dx, self.pos.1))
                .collect(),
            Direction::R => (1..=x - self.pos.0)
                .map(|dx| (self.pos.0 + dx, self.pos.1))
                .collect(),
        };
        self.dir = d;
        self.pos = (x, y);
        self.i += 1;
        Some(p)
    }
}

mod parser {
    use aoc::nom::{
        branch::alt, bytes::complete::tag, character::complete::char, combinator::map, isize,
        multi::separated_list0, sequence::preceded, IResult,
    };

    use crate::{Dir, Direction, Path};

    pub fn path(s: &str) -> IResult<&str, Path> {
        map(dir_list, |directions| Path {
            directions,
            dir: Direction::U,
            pos: (0, 0),
            i: 0,
        })(s)
    }

    fn dir_list(s: &str) -> IResult<&str, Vec<Dir>> {
        separated_list0(tag(", "), dir)(s)
    }

    fn dir(s: &str) -> IResult<&str, Dir> {
        let left = map(preceded(char('L'), isize), Dir::L);
        let right = map(preceded(char('R'), isize), Dir::R);
        alt((left, right))(s)
    }
}
