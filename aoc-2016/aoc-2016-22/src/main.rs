use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone, PartialEq, Eq)]
pub struct Node {
    x: usize,
    y: usize,
    size: usize,
    used: usize,
    free: usize,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Grid Computing";

    type Data = Vec<Node>;

    fn part1(nodes: Self::Data) -> usize {
        nodes
            .iter()
            .filter(|&n| n.used > 0)
            .flat_map(|n1| nodes.iter().map(move |n2| (n1, n2)))
            .filter(|&(n1, n2)| n1 != n2)
            .filter(|&(n1, n2)| n1.used <= n2.free)
            .count()
    }

    fn part2(nodes: Self::Data) -> usize {
        let mx = nodes.iter().map(|n| n.x).max().unwrap();
        let w = nodes
            .iter()
            .filter(|n| n.used > 100)
            .map(|n| n.x)
            .min()
            .unwrap();
        let e = nodes
            .iter()
            .find(|n| n.used == 0)
            .map(|n| (n.x, n.y))
            .unwrap();
        e.0 - w + 1 + e.1 + mx - w + (mx - 1) * 5 + 1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::node_list(s)
    }
}

mod parser {
    use aoc::nom::{
        bytes::complete::{tag, take_till1},
        character::complete::{char, newline, space1},
        multi::separated_list0,
        sequence::{delimited, preceded},
        usize, IResult,
    };

    use crate::Node;

    pub fn node_list(s: &str) -> IResult<&str, Vec<Node>> {
        let (s, _) = take_till1(|c| c == '\n')(s)?;
        let (s, _) = newline(s)?;
        let (s, _) = take_till1(|c| c == '\n')(s)?;
        let (s, _) = newline(s)?;
        separated_list0(newline, node)(s)
    }

    fn node(s: &str) -> IResult<&str, Node> {
        let (s, x) = preceded(tag("/dev/grid/node-x"), usize)(s)?;
        let (s, y) = preceded(tag("-y"), usize)(s)?;
        let (s, size) = delimited(space1, usize, char('T'))(s)?;
        let (s, used) = delimited(space1, usize, char('T'))(s)?;
        let (s, free) = delimited(space1, usize, char('T'))(s)?;
        let (s, _) = take_till1(|c| c == '\n')(s)?;
        let node = Node {
            x,
            y,
            size,
            used,
            free,
        };
        Ok((s, node))
    }
}
