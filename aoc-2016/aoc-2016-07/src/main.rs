use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Default)]
pub struct IP(Vec<String>, Vec<String>);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 7;
    const TITLE: &'static str = "Internet Protocol Version 7";

    type Data = Vec<IP>;

    fn part1(input: Self::Data) -> usize {
        input.into_iter().filter(IP::tls).count()
    }

    fn part2(input: Self::Data) -> usize {
        input.into_iter().filter(IP::ssl).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::ip_list(s)
    }
}

impl IP {
    fn tls(&self) -> bool {
        self.0.iter().any(abba) && !self.1.iter().any(abba)
    }

    fn ssl(&self) -> bool {
        let aba = self
            .1
            .iter()
            .flat_map(|s| {
                let v = s.chars().collect::<Vec<_>>();
                v.windows(3)
                    .filter(|s| s[0] != s[1] && s[0] == s[2])
                    .map(|c| [c[1], c[0], c[1]])
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        self.0
            .iter()
            .map(|v| v.chars().collect::<Vec<char>>())
            .any(|v| v.windows(3).any(|s| aba.contains(&[s[0], s[1], s[2]])))
    }
}

fn abba<T: AsRef<str>>(s: T) -> bool {
    s.as_ref()
        .chars()
        .collect::<Vec<_>>()
        .windows(4)
        .any(|s| s[0] != s[1] && s[0] == s[3] && s[1] == s[2])
}

mod parser {
    use aoc::nom::{
        branch::alt,
        character::complete::{alpha1, char, newline},
        combinator::map,
        multi::{many0, separated_list0},
        sequence::delimited,
        IResult,
    };

    use crate::IP;

    enum Seq<'a> {
        S(&'a str),
        H(&'a str),
    }

    pub fn ip_list(s: &str) -> IResult<&str, Vec<IP>> {
        separated_list0(newline, ip)(s)
    }

    fn ip(s: &str) -> IResult<&str, IP> {
        let (s, parts) = parts(s)?;
        let ip = parts
            .into_iter()
            .fold(IP::default(), |IP(mut s, mut h), part| {
                match part {
                    Seq::S(p) => s.push(p.to_string()),
                    Seq::H(p) => h.push(p.to_string()),
                }
                IP(s, h)
            });
        Ok((s, ip))
    }

    fn parts(s: &str) -> IResult<&str, Vec<Seq>> {
        let seq = map(alpha1, Seq::S);
        let h_seq = map(delimited(char('['), alpha1, char(']')), Seq::H);
        many0(alt((seq, h_seq)))(s)
    }
}
