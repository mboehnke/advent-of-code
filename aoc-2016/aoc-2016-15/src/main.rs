use aoc::{Aoc, Solution};
use num::integer::lcm;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Timing is Everything";

    type Data = Vec<(usize, usize)>;

    fn part1(discs: Self::Data) -> usize {
        time(discs)
    }

    fn part2(mut discs: Self::Data) -> usize {
        discs.push((11, 0));
        time(discs)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::disc_list(s)
    }
}

fn time(discs: Vec<(usize, usize)>) -> usize {
    discs
        .into_iter()
        .enumerate()
        .fold((0, 1), |(offset, interval), (i, (n, s))| {
            (offset..)
                .step_by(interval)
                .find(|&t| (t + i + 1 + s) % n == 0)
                .map(|t| (t, lcm(interval, n)))
                .unwrap()
        })
        .0
}

mod parser {
    use aoc::nom::{
        character::complete::{anychar, char, digit1, newline},
        combinator::peek,
        multi::{many_till, separated_list1},
        sequence::{preceded, terminated, tuple},
        usize, IResult,
    };

    pub fn disc_list(s: &str) -> IResult<&str, Vec<(usize, usize)>> {
        separated_list1(newline, disc)(s)
    }

    fn disc(s: &str) -> IResult<&str, (usize, usize)> {
        terminated(tuple((second_usize_found, second_usize_found)), char('.'))(s)
    }

    fn second_usize_found(s: &str) -> IResult<&str, usize> {
        preceded(tuple((non_digit0, digit1, non_digit0)), usize)(s)
    }

    fn non_digit0(s: &str) -> IResult<&str, Vec<char>> {
        let (s, (t, _)) = many_till(anychar, peek(digit1))(s)?;
        Ok((s, t))
    }
}
