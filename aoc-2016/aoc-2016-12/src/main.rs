use aoc::{Aoc, Solution};
use assembly::Computer;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 12;
    const TITLE: &'static str = "Leonardo's Monorail";

    type Data = Computer;

    fn part1(mut computer: Self::Data) -> isize {
        computer.run();
        computer.get_a()
    }

    fn part2(mut computer: Self::Data) -> isize {
        computer.set_c(1);
        computer.run();
        computer.get_a()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}
