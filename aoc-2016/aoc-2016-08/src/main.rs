use aoc::{Aoc, Solution};
use apply::Apply as _;
use itertools::{iproduct, Itertools as _};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Screen {
    instructions: Vec<Instruction>,
    screen: Vec<Vec<bool>>,
}

#[derive(Debug, Clone)]
pub enum Instruction {
    Rect(usize, usize),
    RRow(usize, usize),
    RCol(usize, usize),
}

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Two-Factor Authentication";

    type Data = Screen;

    fn part1(screen: Self::Data) -> usize {
        screen.render().voltage()
    }

    fn part2(screen: Self::Data) -> String {
        screen.render().display().apply(ocr::parse_5x6).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let (s, instructions) = parser::instruction_list(s)?;
        Ok((
            s,
            Screen {
                instructions,
                screen: vec![vec![false; 50]; 6],
            },
        ))
    }
}

impl Screen {
    fn voltage(self) -> usize {
        self.screen.into_iter().flatten().filter(|&x| x).count()
    }
    fn render(mut self) -> Self {
        for ins in self.instructions.clone() {
            match ins {
                Instruction::Rect(x, y) => self.rect(x, y),
                Instruction::RRow(r, d) => self.rrow(r, d),
                Instruction::RCol(c, d) => self.rcol(c, d),
            }
        }
        self
    }
    fn display(self) -> Vec<String> {
        self.screen
            .into_iter()
            .map(|r| r.into_iter().map(|b| if b { '█' } else { ' ' }).collect())
            .collect()
    }
    fn rect(&mut self, x: usize, y: usize) {
        iproduct!(0..x, 0..y).for_each(|(x, y)| self.screen[y][x] = true);
    }
    fn rrow(&mut self, r: usize, d: usize) {
        let (b, e) = self.screen[r].split_at(50 - d);
        self.screen[r] = e.iter().chain(b.iter()).copied().collect_vec();
    }
    fn rcol(&mut self, c: usize, d: usize) {
        let col = (0..6).map(|r| self.screen[r][c]).collect_vec();
        let (b, e) = col.split_at(6 - d);
        e.iter()
            .chain(b.iter())
            .copied()
            .enumerate()
            .for_each(|(y, p)| self.screen[y][c] = p);
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline},
        multi::separated_list0,
        sequence::preceded,
        usize, IResult,
    };

    use crate::Instruction;

    pub fn instruction_list(s: &str) -> IResult<&str, Vec<Instruction>> {
        separated_list0(newline, instruction)(s)
    }

    fn instruction(s: &str) -> IResult<&str, Instruction> {
        alt((rrow, rcol, rect))(s)
    }

    fn rrow(s: &str) -> IResult<&str, Instruction> {
        let (s, x) = preceded(tag("rotate row y="), usize)(s)?;
        let (s, y) = preceded(tag(" by "), usize)(s)?;
        Ok((s, Instruction::RRow(x, y)))
    }

    fn rcol(s: &str) -> IResult<&str, Instruction> {
        let (s, x) = preceded(tag("rotate column x="), usize)(s)?;
        let (s, y) = preceded(tag(" by "), usize)(s)?;
        Ok((s, Instruction::RCol(x, y)))
    }

    fn rect(s: &str) -> IResult<&str, Instruction> {
        let (s, x) = preceded(tag("rect "), usize)(s)?;
        let (s, y) = preceded(char('x'), usize)(s)?;
        Ok((s, Instruction::Rect(x, y)))
    }
}
