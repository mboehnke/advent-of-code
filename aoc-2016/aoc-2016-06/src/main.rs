use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Signals and Noise";

    type Data = Vec<String>;

    fn part1(signals: Self::Data) -> String {
        decode(signals, false)
    }

    fn part2(signals: Self::Data) -> String {
        decode(signals, true)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::string_list(s)
    }
}

fn decode(signals: Vec<String>, modified: bool) -> String {
    let len = signals[0].len();
    signals
        .into_iter()
        .fold(vec![String::new(); len], |mut columns, signal| {
            signal
                .chars()
                .enumerate()
                .for_each(|(i, c)| columns[i].push(c));
            columns
        })
        .into_iter()
        .map(|col| {
            col.chars().fold(vec![0; 26], |mut chars, char| {
                chars[(char as u8 - b'a') as usize] += 1;
                chars
            })
        })
        .map(|chars| {
            chars
                .into_iter()
                .enumerate()
                .filter(|&(_, n)| n > 0)
                .apply(|i| {
                    if modified {
                        i.min_by_key(|&(_, n)| n)
                    } else {
                        i.max_by_key(|&(_, n)| n)
                    }
                })
                .unwrap()
        })
        .map(|(i, _)| (i as u8 + b'a') as char)
        .collect()
}
