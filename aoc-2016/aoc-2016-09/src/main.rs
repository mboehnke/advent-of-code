use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 9;
    const TITLE: &'static str = "Explosives in Cyberspace";

    type Data = Vec<char>;

    fn part1(data: Self::Data) -> usize {
        u_length(&data, false)
    }

    fn part2(data: Self::Data) -> usize {
        u_length(&data, true)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.trim().chars().collect()))
    }
}

fn u_length(data: &[char], version2: bool) -> usize {
    let mut i = 0;
    let mut len = 0;
    while i < data.len() {
        if data[i] != '(' {
            len += 1;
            i += 1;
            continue;
        }
        let marker = (i + 1..)
            .map(|i| data[i])
            .take_while(|&c| c != ')')
            .collect::<String>();
        let (l, t) = marker
            .split('x')
            .map(str::parse::<usize>)
            .map(Result::unwrap)
            .collect_tuple()
            .unwrap();
        if version2 {
            let j = i + marker.len() + 2;
            len += u_length(&data[j..j + l], version2) * t
        } else {
            len += l * t;
        }
        i += marker.len() + 2 + l
    }
    len
}
