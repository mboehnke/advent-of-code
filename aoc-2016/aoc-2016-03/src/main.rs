use std::usize;

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Squares With Three Sides";

    type Data = Vec<(usize, usize, usize)>;

    fn part1(input: Self::Data) -> usize {
        input.into_iter().filter(possible).count()
    }

    fn part2(input: Self::Data) -> usize {
        input
            .chunks(3)
            .flat_map(|c| {
                vec![
                    (c[0].0, c[1].0, c[2].0),
                    (c[0].1, c[1].1, c[2].1),
                    (c[0].2, c[1].2, c[2].2),
                ]
            })
            .filter(possible)
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::numbers_list(s)
    }
}

fn possible(&(a, b, c): &(usize, usize, usize)) -> bool {
    a + b > c && a + c > b && b + c > a
}

mod parser {
    use aoc::nom::{
        character::complete::{newline, space0, u64},
        combinator::map_res,
        multi::separated_list0,
        sequence::{preceded, tuple},
        IResult,
    };

    pub fn numbers_list(s: &str) -> IResult<&str, Vec<(usize, usize, usize)>> {
        separated_list0(newline, tuple((usize, usize, usize)))(s)
    }

    fn usize(s: &str) -> IResult<&str, usize> {
        preceded(space0, map_res(u64, usize::try_from))(s)
    }
}
