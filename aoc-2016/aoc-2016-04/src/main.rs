use aoc::{Aoc, Solution};
use apply::{Also as _, Apply as _};
use hashbrown::HashMap;
use itertools::Itertools as _;
use std::iter::FromIterator as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Room {
    name: String,
    id: usize,
    checksum: String,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 4;
    const TITLE: &'static str = "Security Through Obscurity";

    type Data = Vec<Room>;

    fn part1(rooms: Self::Data) -> usize {
        rooms
            .into_iter()
            .filter(Room::is_real)
            .map(|r| r.id)
            .sum::<usize>()
    }

    fn part2(rooms: Self::Data) -> usize {
        const ROOM: &str = "northpole object storage";
        rooms
            .into_iter()
            .filter(Room::is_real)
            .map(Room::decrypt)
            .find(|r| r.name == ROOM)
            .unwrap()
            .id
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::room_list(s)
    }
}

impl Room {
    fn is_real(&self) -> bool {
        self.name
            .chars()
            .filter(|c| c.is_ascii_alphabetic())
            .fold(HashMap::new(), |mut letters: HashMap<char, usize>, c| {
                *letters.entry(c).or_default() += 1;
                letters
            })
            .apply(|l| {
                l.into_iter()
                    .fold(HashMap::new(), |mut l: HashMap<usize, String>, (c, n)| {
                        l.entry(n).or_default().push(c);
                        l
                    })
            })
            .apply(Vec::from_iter)
            .also(|v| v.sort_unstable_by_key(|&(n, _)| n))
            .into_iter()
            .rev()
            .map(|(n, s)| (n, s.chars().sorted().collect::<String>()))
            .fold(String::new(), |mut s, (_, c)| {
                s.push_str(&c);
                s
            })
            .chars()
            .take(self.checksum.len())
            .collect::<String>()
            == self.checksum
    }

    fn decrypt(mut self) -> Self {
        self.name = self
            .name
            .chars()
            .map(|c| match c {
                '-' => ' ',
                c => ((((c as u8 - b'a') as usize + self.id) % 26) as u8 + b'a') as char,
            })
            .collect();
        self
    }
}

mod parser {
    use aoc::nom::{
        character::complete::{alpha1, anychar, char, digit1, newline},
        combinator::{map, peek},
        multi::{many_till, separated_list1},
        sequence::delimited,
        usize, IResult,
    };

    use crate::Room;

    pub fn room_list(s: &str) -> IResult<&str, Vec<Room>> {
        separated_list1(newline, room)(s)
    }

    fn room(s: &str) -> IResult<&str, Room> {
        let (s, (mut name, _)) = many_till(anychar, peek(digit1))(s)?;
        let (s, id) = usize(s)?;
        let (s, checksum) = delimited(char('['), map(alpha1, String::from), char(']'))(s)?;
        name.pop();
        let room = Room {
            name: name.into_iter().collect(),
            id,
            checksum,
        };
        Ok((s, room))
    }
}
