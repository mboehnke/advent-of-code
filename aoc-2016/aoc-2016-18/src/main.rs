use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone)]
pub struct Rows(Vec<bool>);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 18;
    const TITLE: &'static str = "Like a Rogue";

    type Data = (usize, Rows);

    fn part1((n, rows): Self::Data) -> usize {
        rows.take(n).sum()
    }

    fn part2((_, rows): Self::Data) -> usize {
        Self::part1((400000, rows))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{char, newline},
            combinator::{map, value},
            multi::many1,
            sequence::separated_pair,
            usize,
        };

        let bin_digit = alt((value(true, char('.')), value(false, char('^'))));
        let rows = map(many1(bin_digit), Rows);
        separated_pair(usize, newline, rows)(s)
    }
}

impl Iterator for Rows {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let r = self.0.iter().filter(|&&b| b).count();
        self.0 = (0..self.0.len())
            .map(|i| {
                if i == 0 {
                    self.0[i + 1]
                } else if i >= self.0.len() - 1 {
                    self.0[i - 1]
                } else {
                    self.0[i - 1] == self.0[i + 1]
                }
            })
            .collect();
        Some(r)
    }
}
