use aoc::{Aoc, Solution};
use apply::Apply as _;
use itertools::FoldWhile::{Continue, Done};
use itertools::Itertools as _;
use md5::Digest as _;
use std::char::from_digit;
use std::collections::HashMap;

#[derive(Aoc)]
pub struct Sol;

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2016;
    const DAY: u32 = 5;
    const TITLE: &'static str = "How About a Nice Game of Chess?";

    type Data = String;

    fn part1(id: Self::Data) -> String {
        pw_chars(id)
            .take(8)
            .map(|(c, _)| from_digit(c as u32, 16).unwrap())
            .collect()
    }

    fn part2(id: Self::Data) -> String {
        pw_chars(id)
            .fold_while(HashMap::new(), |mut password, (n, c)| {
                if n < 8 && !password.contains_key(&n) {
                    password.insert(n, from_digit(c as u32, 16).unwrap());
                }
                let l = password.len();
                password.apply(if l == 8 { Done } else { Continue })
            })
            .into_inner()
            .apply(|password| {
                (0..8)
                    .map(|i| password.get(&i))
                    .map(Option::unwrap)
                    .collect()
            })
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::alpha1, combinator::map};

        map(alpha1, String::from)(s)
    }
}

fn pw_chars(id: String) -> impl Iterator<Item = (u8, u8)> {
    (0..)
        .map(move |n| format!("{}{}", id, n))
        .map(compute)
        .filter(|s| s[..2] == [0, 0] && s[2] < 16)
        .map(|s| (s[2] % 16, s[3] >> 4))
}

fn compute(data: String) -> [u8; 16] {
    let mut hasher = md5::Md5::new();
    hasher.update(data);
    hasher.finalize().into()
}
