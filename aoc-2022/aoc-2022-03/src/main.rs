use aoc::{nom::string_list, Aoc, Solution};
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Day03;

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Item(char);

impl Solution<u32, u32> for Day03 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Rucksack Reorganization";

    type Data = Vec<Vec<char>>;

    fn part1(rucksacks: Self::Data) -> u32 {
        rucksacks
            .into_iter()
            .flat_map(|rucksack| {
                let (a, b) = rucksack.split_at(rucksack.len() / 2);
                intersection(a, b).into_iter().map(priority)
            })
            .sum()
    }

    fn part2(rucksacks: Self::Data) -> u32 {
        rucksacks
            .chunks(3)
            .flat_map(|bs| {
                intersection(&intersection(&bs[0], &bs[1]), &bs[2])
                    .first()
                    .cloned()
                    .map(priority)
            })
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        string_list(s).map(|(s, rucksacks)| {
            (
                s,
                rucksacks.into_iter().map(|b| b.chars().collect()).collect(),
            )
        })
    }
}

fn priority(c: char) -> u32 {
    match c {
        c if c.is_ascii_lowercase() => (c as u8 - b'a' + 1) as u32,
        c if c.is_ascii_uppercase() => (c as u8 - b'A' + 27) as u32,
        _ => 0,
    }
}

fn intersection(a: &[char], b: &[char]) -> Vec<char> {
    let a = HashSet::<&char>::from_iter(a);
    let b = HashSet::<&char>::from_iter(b);
    a.intersection(&b).cloned().cloned().collect()
}
