use std::collections::HashSet;

use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{newline, u32},
        combinator::map,
        multi::separated_list1,
        sequence::preceded,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day;

#[derive(Debug, Clone)]
pub enum Motion {
    Up(u32),
    Down(u32),
    Left(u32),
    Right(u32),
}

impl Solution<usize, usize> for Day {
    const YEAR: u32 = 2022;
    const DAY: u32 = 99;
    const TITLE: &'static str = "";

    type Data = Vec<Motion>;

    fn part1(motions: Self::Data) -> usize {
        let mut rope = Rope::new(2);
        for motion in motions {
            rope.move_head(&motion);
        }
        rope.positions.len()
    }

    fn part2(motions: Self::Data) -> usize {
        let mut rope = Rope::new(10);
        for motion in motions {
            rope.move_head(&motion);
        }
        rope.positions.len()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let up = preceded(tag("U "), map(u32, Motion::Up));
        let down = preceded(tag("D "), map(u32, Motion::Down));
        let left = preceded(tag("L "), map(u32, Motion::Left));
        let right = preceded(tag("R "), map(u32, Motion::Right));
        let dir = alt((up, down, left, right));
        separated_list1(newline, dir)(s)
    }
}

#[derive(Debug, Clone, Default)]
struct Rope {
    rope: Vec<(i32, i32)>,
    positions: HashSet<(i32, i32)>,
}

impl Rope {
    fn new(len: usize) -> Self {
        let mut positions = HashSet::new();
        positions.insert((0, 0));
        Self {
            rope: vec![(0, 0); len],
            positions,
        }
    }

    fn move_head(&mut self, motion: &Motion) {
        match *motion {
            Motion::Up(d) => {
                for _ in 0..d {
                    self.rope[0].1 -= 1;
                    self.move_tail()
                }
            }
            Motion::Down(d) => {
                for _ in 0..d {
                    self.rope[0].1 += 1;
                    self.move_tail()
                }
            }
            Motion::Left(d) => {
                for _ in 0..d {
                    self.rope[0].0 -= 1;
                    self.move_tail()
                }
            }
            Motion::Right(d) => {
                for _ in 0..d {
                    self.rope[0].0 += 1;
                    self.move_tail()
                }
            }
        }
    }

    fn new_tail_pos((hx, hy): (i32, i32), (tx, ty): (i32, i32)) -> (i32, i32) {
        if (tx + 2, ty) == (hx, hy) {
            return (tx + 1, ty);
        }
        if (tx - 2, ty) == (hx, hy) {
            return (tx - 1, ty);
        }
        if (tx, ty + 2) == (hx, hy) {
            return (tx, ty + 1);
        }
        if (tx, ty - 2) == (hx, hy) {
            return (tx, ty - 1);
        }

        if (tx + 2, ty + 1) == (hx, hy)
            || (tx + 2, ty + 2) == (hx, hy)
            || (tx + 1, ty + 2) == (hx, hy)
        {
            return (tx + 1, ty + 1);
        }
        if (tx - 2, ty + 1) == (hx, hy)
            || (tx - 2, ty + 2) == (hx, hy)
            || (tx - 1, ty + 2) == (hx, hy)
        {
            return (tx - 1, ty + 1);
        }
        if (tx - 2, ty - 1) == (hx, hy)
            || (tx - 2, ty - 2) == (hx, hy)
            || (tx - 1, ty - 2) == (hx, hy)
        {
            return (tx - 1, ty - 1);
        }
        if (tx + 2, ty - 1) == (hx, hy)
            || (tx + 2, ty - 2) == (hx, hy)
            || (tx + 1, ty - 2) == (hx, hy)
        {
            return (tx + 1, ty - 1);
        }

        (tx, ty)
    }

    fn move_tail(&mut self) {
        for i in 0..self.rope.len() - 1 {
            self.rope[i + 1] = Self::new_tail_pos(self.rope[i], self.rope[i + 1]);
        }
        self.positions.insert(*self.rope.last().unwrap());
    }
}
