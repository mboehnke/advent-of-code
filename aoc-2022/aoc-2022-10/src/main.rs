use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{i32, newline},
        combinator::{map, value},
        multi::separated_list1,
        sequence::preceded,
    },
    Aoc, Solution,
};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Day10;

pub struct Cpu {
    program: Vec<Instruction>,
    register: i32,
    pause: bool,
    value: i32,
    ip: usize,
}

#[derive(Debug, Clone, Copy)]
pub enum Instruction {
    Noop,
    Addx(i32),
}

impl Solution<i32, String> for Day10 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Cathode-Ray Tube";

    type Data = Vec<Instruction>;

    fn part1(program: Self::Data) -> i32 {
        Cpu::new(program)
            .enumerate()
            .skip(19)
            .step_by(40)
            .map(|(cycle, value)| (cycle as i32 + 1) * value)
            .sum()
    }

    fn part2(program: Self::Data) -> String {
        const WIDTH: usize = 40;
        const HEIGHT: usize = 6;
        Cpu::new(program)
            .enumerate()
            .map(|(cycle, value)| {
                let col = (cycle % WIDTH) as i32;
                col == value || col == value - 1 || col == value + 1
            })
            .map(|lit| if lit { '#' } else { ' ' })
            .zip((0..HEIGHT).flat_map(|y| (0..WIDTH).map(move |x| (x, y))))
            .fold(vec![vec![' '; WIDTH]; HEIGHT], |mut image, (c, (x, y))| {
                image[y][x] = c;
                image
            })
            .into_iter()
            .map(|row| row.into_iter().collect::<String>())
            .apply(ocr::parse_5x6)
            .unwrap_or_default()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let noop = value(Instruction::Noop, tag("noop"));
        let addx = map(preceded(tag("addx "), i32), Instruction::Addx);
        let ins = alt((noop, addx));
        separated_list1(newline, ins)(s)
    }
}

impl Cpu {
    fn new(program: Vec<Instruction>) -> Self {
        Self {
            program,
            register: 1,
            pause: false,
            value: 0,
            ip: 0,
        }
    }
}

impl Iterator for Cpu {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pause {
            self.pause = false
        } else {
            if self.ip >= self.program.len() {
                return None;
            }
            self.register += self.value;
            self.value = 0;
            if let Instruction::Addx(v) = self.program[self.ip] {
                self.value = v;
                self.pause = true
            };
            self.ip += 1
        }

        Some(self.register)
    }
}
