use std::{cell::RefCell, iter::once, rc::Rc};

use aoc::{
    nom::{
        branch::alt,
        bytes::complete::{tag, take_until1},
        character::complete::{newline, space1, u32},
        combinator::{map, value},
        multi::separated_list1,
        sequence::{preceded, separated_pair},
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day07;

#[derive(Debug, Clone)]
pub enum Output {
    Cd(String),
    Ls,
    Dir(String),
    File(String, u32),
}

#[derive(Debug, Clone, Default)]
pub struct Fs {
    files: Vec<(String, u32)>,
    parent: Option<Rc<RefCell<Fs>>>,
    directories: Vec<(String, Rc<RefCell<Fs>>)>,
}

impl Solution<u32, u32> for Day07 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 7;
    const TITLE: &'static str = "No Space Left On Device";

    type Data = Vec<Output>;

    fn part1(output: Self::Data) -> u32 {
        let fs = Fs::from(output);
        dirs(&fs)
            .iter()
            .map(total_size)
            .filter(|&s| s <= 100000)
            .sum()
    }

    fn part2(output: Self::Data) -> u32 {
        const TOTAL_DISK_SPACE: u32 = 70000000;
        const NEEDED_DISK_SPACE: u32 = 30000000;

        let fs = Fs::from(output);
        let mut dir_sizes = dirs(&fs).iter().map(total_size).collect::<Vec<_>>();
        dir_sizes.sort_unstable();
        let free = TOTAL_DISK_SPACE - dir_sizes.pop().unwrap_or_default();
        let needed = NEEDED_DISK_SPACE - free;
        dir_sizes
            .into_iter()
            .find(|&s| s >= needed)
            .unwrap_or_default()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let cd = map(preceded(tag("$ cd "), take_until1("\n")), |s: &str| {
            Output::Cd(s.into())
        });
        let ls = value(Output::Ls, tag("$ ls"));
        let dir = map(preceded(tag("dir "), take_until1("\n")), |s: &str| {
            Output::Dir(s.into())
        });
        let file = map(
            separated_pair(u32, space1, take_until1("\n")),
            |(size, name): (u32, &str)| Output::File(name.into(), size),
        );
        let line = alt((cd, ls, dir, file));
        separated_list1(newline, line)(s)
    }
}

fn cd(fs: &Rc<RefCell<Fs>>, dir: &str) -> Option<Rc<RefCell<Fs>>> {
    if dir == ".." {
        fs.borrow().parent.clone()
    } else if dir == "/" {
        if let Some(ref f) = fs.borrow().parent {
            cd(f, dir)
        } else {
            Some(fs.clone())
        }
    } else {
        fs.borrow()
            .directories
            .iter()
            .find(|(d, _)| d == dir)
            .map(|(_, f)| f.clone())
    }
}

fn add_dir(fs: &Rc<RefCell<Fs>>, dir: String) {
    let mut f = fs.borrow_mut();
    if !f.directories.iter().any(|(d, _)| &dir == d) {
        f.directories.push((
            dir,
            Rc::new(RefCell::new(Fs {
                parent: Some(fs.clone()),
                ..Default::default()
            })),
        ))
    }
}

fn add_file(fs: &Rc<RefCell<Fs>>, name: String, size: u32) {
    let mut f = fs.borrow_mut();
    if !f.files.iter().any(|(n, _)| n == &name) {
        f.files.push((name, size))
    }
}

fn dirs(fs: &Rc<RefCell<Fs>>) -> Vec<Rc<RefCell<Fs>>> {
    fs.borrow()
        .directories
        .iter()
        .flat_map(|(_, f)| dirs(f))
        .chain(once(fs.clone()))
        .collect()
}

fn total_size(fs: &Rc<RefCell<Fs>>) -> u32 {
    let files = fs.borrow().files.iter().map(|(_, s)| s).sum::<u32>();
    let dirs = fs
        .borrow()
        .directories
        .iter()
        .map(|(_, f)| total_size(f))
        .sum::<u32>();
    files + dirs
}

impl Fs {
    fn from(output: Vec<Output>) -> Rc<RefCell<Self>> {
        let fs = Rc::new(RefCell::new(Fs::default()));
        let mut cwd = cd(&fs, "/").unwrap();
        for line in output {
            match line {
                Output::Cd(dir) => {
                    if let Some(f) = cd(&cwd, &dir) {
                        cwd = f
                    }
                }
                Output::Ls => {}
                Output::Dir(dir) => add_dir(&cwd, dir),
                Output::File(name, size) => add_file(&cwd, name, size),
            }
        }
        fs
    }
}
