use aoc::{
    nom::{
        character::complete::{char, newline, u8},
        multi::separated_list1,
        sequence::separated_pair,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day04;

impl Solution<usize, usize> for Day04 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 4;
    const TITLE: &'static str = "Camp Cleanup";

    type Data = Vec<((u8, u8), (u8, u8))>;

    fn part1(assignments: Self::Data) -> usize {
        assignments
            .into_iter()
            .filter(|((a1, a2), (b1, b2))| (a1 <= b1 && a2 >= b2) || (b1 <= a1 && b2 >= a2))
            .count()
    }

    fn part2(assignments: Self::Data) -> usize {
        assignments
            .into_iter()
            .filter(|((a1, a2), (b1, b2))| {
                (a1..=a2).contains(&b1)
                    || (a1..=a2).contains(&b2)
                    || (b1..=b2).contains(&a1)
                    || (b1..=b2).contains(&a2)
            })
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let pair = separated_pair(
            separated_pair(u8, char('-'), u8),
            char(','),
            separated_pair(u8, char('-'), u8),
        );
        separated_list1(newline, pair)(s)
    }
}
