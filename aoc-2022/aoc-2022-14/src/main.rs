use std::mem::swap;

use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{char, newline},
        multi::separated_list0,
        sequence::separated_pair,
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day;

struct Map(Vec<Vec<bool>>);

impl Solution<usize, usize> for Day {
    const YEAR: u32 = 2022;
    const DAY: u32 = 99;
    const TITLE: &'static str = "";

    type Data = Vec<Vec<(usize, usize)>>;

    fn part1(input: Self::Data) -> usize {
        let mut map = Map::from(input);

        (0..).find(|_| !map.add_sand_1(500, 0)).unwrap_or_default()
    }

    fn part2(input: Self::Data) -> usize {
        let mut map = Map::from(input);
        map.0.push(vec![true; map.0[0].len()]);

        (0..).find(|_| !map.add_sand_2(500, 0)).unwrap_or_default()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let coords = separated_pair(usize, char(','), usize);
        let line = separated_list0(tag(" -> "), coords);
        separated_list0(newline, line)(s)
    }
}

fn line((mut x1, mut y1): (usize, usize), (mut x2, mut y2): (usize, usize)) -> Vec<(usize, usize)> {
    if x1 > x2 {
        swap(&mut x1, &mut x2)
    }
    if y1 > y2 {
        swap(&mut y1, &mut y2)
    }
    if x1 == x2 {
        return (y1..=y2).map(|y| (x1, y)).collect();
    }
    if y1 == y2 {
        return (x1..=x2).map(|x| (x, y1)).collect();
    }
    unimplemented!("no diagonal lines")
}

impl From<Vec<Vec<(usize, usize)>>> for Map {
    fn from(lines: Vec<Vec<(usize, usize)>>) -> Self {
        let max_x = lines
            .iter()
            .flatten()
            .map(|(x, _)| x)
            .max()
            .copied()
            .unwrap_or_default();
        let max_y = lines
            .iter()
            .flatten()
            .map(|(_, y)| y)
            .max()
            .copied()
            .unwrap_or_default();
        lines.iter().flat_map(|line| line.windows(2)).fold(
            Map(vec![vec![false; max_x * 2 + 2]; max_y + 2]),
            |mut map, coords| {
                for (x, y) in line(coords[0], coords[1]) {
                    map.0[y][x] = true;
                }
                map
            },
        )
    }
}

impl Map {
    fn add_sand_1(&mut self, x: usize, y: usize) -> bool {
        if y + 1 >= self.0.len() {
            return false;
        }
        if !self.0[y + 1][x] {
            return self.add_sand_1(x, y + 1);
        }
        if !self.0[y + 1][x - 1] {
            return self.add_sand_1(x - 1, y + 1);
        }
        if !self.0[y + 1][x + 1] {
            return self.add_sand_1(x + 1, y + 1);
        }
        self.0[y][x] = true;
        true
    }

    fn add_sand_2(&mut self, x: usize, y: usize) -> bool {
        if self.0[y][x] {
            return false;
        }
        if !self.0[y + 1][x] {
            return self.add_sand_2(x, y + 1);
        }
        if !self.0[y + 1][x - 1] {
            return self.add_sand_2(x - 1, y + 1);
        }
        if !self.0[y + 1][x + 1] {
            return self.add_sand_2(x + 1, y + 1);
        }
        self.0[y][x] = true;
        true
    }
}
