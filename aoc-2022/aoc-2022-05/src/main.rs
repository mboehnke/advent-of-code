use aoc::{
    nom::{
        branch::alt,
        bytes::complete::{tag, take_until1},
        character::complete::{anychar, char, newline},
        combinator::{map, value},
        multi::separated_list1,
        separated_triple,
        sequence::{delimited, preceded, separated_pair},
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day05;

#[derive(Debug, Clone)]
pub struct Move {
    num: usize,
    from: usize,
    to: usize,
}

impl Solution<String, String> for Day05 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 5;
    const TITLE: &'static str = "Supply Stacks";

    type Data = (Vec<Vec<char>>, Vec<Move>);

    fn part1((mut stacks, moves): Self::Data) -> String {
        for Move { num, from, to } in moves {
            for _ in 0..num {
                if let Some(c) = stacks[from - 1].pop() {
                    stacks[to - 1].push(c)
                }
            }
        }
        stacks
            .into_iter()
            .filter_map(|mut stack| stack.pop())
            .collect()
    }

    fn part2((mut stacks, moves): Self::Data) -> String {
        for Move { num, from, to } in moves {
            (0..num)
                .filter_map(|_| stacks[from - 1].pop())
                .collect::<Vec<_>>()
                .into_iter()
                .rev()
                .for_each(|c| stacks[to - 1].push(c))
        }
        stacks
            .into_iter()
            .filter_map(|mut stack| stack.pop())
            .collect()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let empty = value(None, tag("   "));
        let crates = delimited(char('['), map(anychar, Some), char(']'));
        let row = separated_list1(char(' '), alt((empty, crates)));
        let transpose = |m: Vec<Vec<Option<char>>>| {
            let mut res = vec![Vec::new(); m[0].len()];
            (0..m[0].len()).for_each(|x| {
                for y in (0..m.len()).rev() {
                    if let Some(c) = m[y][x] {
                        res[x].push(c)
                    }
                }
            });
            res
        };
        let stacks = map(separated_list1(newline, row), transpose);
        let moves = preceded(
            tag("move "),
            separated_triple(usize, tag(" from "), usize, tag(" to "), usize),
        );
        let to_move = |(num, from, to)| Move { num, from, to };
        let moves = separated_list1(newline, map(moves, to_move));

        separated_pair(stacks, take_until1("move"), moves)(s)
    }
}
