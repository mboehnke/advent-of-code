use std::collections::HashSet;

use aoc::{nom::i8_matrix, Aoc, Solution};

#[derive(Aoc)]
pub struct Day08;

#[derive(Debug, Clone)]
pub struct Map(Vec<Vec<i8>>);

impl Solution<usize, usize> for Day08 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Treetop Tree House";

    type Data = Map;

    fn part1(map: Self::Data) -> usize {
        map.rows()
            .flat_map(|line| map.visible(line))
            .chain(map.rows_r().flat_map(|line| map.visible(line)))
            .chain(map.cols().flat_map(|line| map.visible(line)))
            .chain(map.cols_r().flat_map(|line| map.visible(line)))
            .collect::<HashSet<_>>()
            .len()
    }

    fn part2(map: Self::Data) -> usize {
        let mut scores = vec![vec![1usize; map.0[0].len()]; map.0[0].len()];

        for (r, c, s) in map.rows().flat_map(|line| map.score(line)) {
            scores[r][c] *= s;
        }
        for (r, c, s) in map.rows_r().flat_map(|line| map.score(line)) {
            scores[r][c] *= s;
        }
        for (r, c, s) in map.cols().flat_map(|line| map.score(line)) {
            scores[r][c] *= s;
        }
        for (r, c, s) in map.cols_r().flat_map(|line| map.score(line)) {
            scores[r][c] *= s;
        }

        scores.into_iter().flatten().max().unwrap_or_default()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        i8_matrix(s).map(|(r, m)| (r, Map(m)))
    }
}

impl Map {
    fn rows(&self) -> impl Iterator<Item = impl Iterator<Item = (usize, usize)>> + '_ {
        (0..self.0.len()).map(|r| (0..self.0[0].len()).map(move |c| (r, c)))
    }
    fn rows_r(&self) -> impl Iterator<Item = impl Iterator<Item = (usize, usize)>> + '_ {
        Box::new((0..self.0.len()).map(|r| (0..self.0[0].len()).rev().map(move |c| (r, c))))
    }
    fn cols(&self) -> impl Iterator<Item = impl Iterator<Item = (usize, usize)>> + '_ {
        (0..self.0[0].len()).map(|c| (0..self.0.len()).map(move |r| (r, c)))
    }
    fn cols_r(&self) -> impl Iterator<Item = impl Iterator<Item = (usize, usize)>> + '_ {
        (0..self.0[0].len()).map(|c| (0..self.0.len()).rev().map(move |r| (r, c)))
    }

    fn visible<'a>(
        &'a self,
        line: impl Iterator<Item = (usize, usize)> + 'a,
    ) -> impl Iterator<Item = (usize, usize)> + 'a {
        line.scan(-1, |height, (r, c)| {
            let tree = self.0[r][c];
            let res = (tree > *height).then_some((r, c));
            if tree > *height {
                *height = tree
            }
            Some(res)
        })
        .flatten()
    }

    fn score<'a>(
        &'a self,
        mut line: impl Iterator<Item = (usize, usize)> + 'a,
    ) -> impl Iterator<Item = (usize, usize, usize)> + 'a {
        let (r, c) = line.next().unwrap();
        let mut res = vec![(r, c, 0)];

        let mut scores = [0; 10];
        for (r, c) in line {
            res.push((r, c, scores[self.0[r][c] as usize] + 1));
            scores
                .iter_mut()
                .enumerate()
                .skip(1)
                .for_each(|(i, score)| {
                    if (i as i8) > self.0[r][c] {
                        *score += 1
                    } else {
                        *score = 0
                    }
                })
        }

        res.into_iter()
    }
}
