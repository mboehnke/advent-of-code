use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::value,
        multi::separated_list1,
        sequence::separated_pair,
    },
    Aoc, Solution,
};

use Outcome::*;
use Shape::*;

#[derive(Aoc)]
pub struct Day02;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Shape {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Outcome {
    Win,
    Draw,
    Loss,
}

impl Solution<u32, u32> for Day02 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Rock Paper Scissors";

    type Data = Vec<(Shape, Shape)>;

    fn part1(guide: Self::Data) -> u32 {
        guide.into_iter().map(|(opp, you)| you.score(&opp)).sum()
    }

    fn part2(guide: Self::Data) -> u32 {
        guide
            .into_iter()
            .map(|(opp, you)| (opp, opp.has_outcome(&Outcome::from(you))))
            .map(|(opp, you)| you.score(&opp))
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let shape = |s| {
            alt((
                value(Rock, alt((char('A'), char('X')))),
                value(Paper, alt((char('B'), char('Y')))),
                value(Scissors, alt((char('C'), char('Z')))),
            ))(s)
        };
        separated_list1(newline, separated_pair(shape, char(' '), shape))(s)
    }
}

impl Shape {
    fn value(&self) -> u32 {
        match self {
            Rock => 1,
            Paper => 2,
            Scissors => 3,
        }
    }

    fn score(&self, other: &Self) -> u32 {
        self.value()
            + if self == other {
                3
            } else if (self == &Rock && other == &Scissors)
                || (self == &Paper && other == &Rock)
                || (self == &Scissors && other == &Paper)
            {
                6
            } else {
                0
            }
    }

    fn has_outcome(&self, outcome: &Outcome) -> Shape {
        match outcome {
            Win => match self {
                Rock => Paper,
                Paper => Scissors,
                Scissors => Rock,
            },
            Loss => match self {
                Rock => Scissors,
                Paper => Rock,
                Scissors => Paper,
            },
            Draw => *self,
        }
    }
}

impl From<Shape> for Outcome {
    fn from(s: Shape) -> Self {
        match s {
            Rock => Self::Loss,
            Paper => Self::Draw,
            Scissors => Self::Win,
        }
    }
}
