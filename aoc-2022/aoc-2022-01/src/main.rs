use aoc::{
    nom::{character::complete::newline, multi::separated_list1, u32_list, IResult},
    Aoc, Solution,
};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Day01;

impl Solution<u32, u32> for Day01 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Calorie Counting";

    type Data = Vec<Vec<u32>>;

    fn part1(elves: Self::Data) -> u32 {
        elves
            .into_iter()
            .map(|items| items.into_iter().sum())
            .max()
            .unwrap_or_default()
    }

    fn part2(elves: Self::Data) -> u32 {
        elves
            .into_iter()
            .map(|items| items.into_iter().sum())
            .sorted_by(|a: &u32, b: &u32| b.cmp(a))
            .take(3)
            .sum()
    }

    fn parser(s: &str) -> IResult<&str, Self::Data> {
        separated_list1(newline, u32_list)(s)
    }
}
