use std::mem::take;

use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline, u64},
        combinator::{map, value},
        multi::{separated_list0, separated_list1},
        sequence::{delimited, preceded, tuple},
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day11;

#[derive(Debug, Clone)]
pub struct Monkey {
    items: Vec<u64>,
    operation: Operation,
    test_mod: u64,
    test_true: usize,
    test_false: usize,
    inspections: u64,
}

#[derive(Debug, Clone)]
enum Operation {
    Add(u64),
    Mul(u64),
    Sqr,
}

impl Solution<u64, u64> for Day11 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Monkey in the Middle";

    type Data = Vec<Monkey>;

    fn part1(monkeys: Self::Data) -> u64 {
        monkey_business(monkeys, 3, 20)
    }

    fn part2(monkeys: Self::Data) -> u64 {
        monkey_business(monkeys, 1, 10000)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let number = delimited(tag("Monkey "), usize, char(':'));
        let items = preceded(tag("\n  Starting items: "), separated_list0(tag(", "), u64));
        let op_add = preceded(tag("\n  Operation: new = old + "), map(u64, Operation::Add));
        let op_mul = preceded(tag("\n  Operation: new = old * "), map(u64, Operation::Mul));
        let op_sqr = value(Operation::Sqr, tag("\n  Operation: new = old * old"));
        let operation = alt((op_add, op_mul, op_sqr));
        let test_mod = preceded(tag("\n  Test: divisible by "), u64);
        let test_true = preceded(tag("\n    If true: throw to monkey "), usize);
        let test_false = preceded(tag("\n    If false: throw to monkey "), usize);
        let monkey = map(
            tuple((number, items, operation, test_mod, test_true, test_false)),
            |(_, items, operation, test_mod, test_true, test_false)| Monkey {
                items,
                operation,
                test_mod,
                test_true,
                test_false,
                inspections: 0,
            },
        );
        separated_list1(tuple((newline, newline)), monkey)(s)
    }
}

fn monkey_business(mut monkeys: Vec<Monkey>, worry_div: u64, rounds: u64) -> u64 {
    let mods = monkeys.iter().map(|m| m.test_mod).product::<u64>();
    for _ in 0..rounds {
        let num = monkeys.len();
        for i in 0..num {
            for item in take(&mut monkeys[i].items) {
                monkeys[i].inspections += 1;
                let item = match monkeys[i].operation {
                    Operation::Add(x) => item + x,
                    Operation::Mul(x) => item * x,
                    Operation::Sqr => item * item,
                } / worry_div
                    % mods;
                let j = if item % monkeys[i].test_mod == 0 {
                    monkeys[i].test_true
                } else {
                    monkeys[i].test_false
                };
                monkeys[j].items.push(item);
            }
        }
    }

    let mut inspections = monkeys
        .into_iter()
        .map(|m| m.inspections)
        .collect::<Vec<_>>();
    inspections.sort_unstable();
    inspections.pop().unwrap_or_default() * inspections.pop().unwrap_or_default()
}
