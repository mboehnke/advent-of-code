use std::ops::Range;

use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::newline,
        combinator::map,
        isize,
        multi::separated_list0,
        sequence::{preceded, tuple},
    },
    Aoc, Solution,
};
use apply::Apply as _;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Day;

impl Solution<usize, isize> for Day {
    const YEAR: u32 = 2022;
    const DAY: u32 = 99;
    const TITLE: &'static str = "";

    type Data = Vec<((isize, isize), (isize, isize))>;

    fn part1(input: Self::Data) -> usize {
        // this one is different for example and actual data
        let row = if input.len() < 20 { 10 } else { 2000000 };

        let sensors_and_beacons = input
            .iter()
            .flat_map(|(s, b)| [s, b])
            .filter(|&&(_x, y)| y == row)
            .unique()
            .count();
        input
            .into_iter()
            .map(BeaconRange::from)
            .flat_map(|r| r.coverage(row))
            .collect::<Vec<_>>()
            .apply(flatten_ranges)
            .iter()
            .map(Range::len)
            .sum::<usize>()
            - sensors_and_beacons
    }

    fn part2(input: Self::Data) -> isize {
        // this one is different for example and actual data
        let max: isize = if input.len() < 20 { 20 } else { 4000000 };

        let ranges = input.into_iter().map(BeaconRange::from).collect::<Vec<_>>();

        for y in 0..=max {
            if let Some(r) = ranges
                .iter()
                .flat_map(|r| r.coverage(y))
                .collect::<Vec<_>>()
                .apply(flatten_ranges)
                .into_iter()
                .find(|r| r.end > 0 && r.end <= max)
            {
                return r.end * 4000000 + y;
            }
        }
        0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let line = tuple((
            preceded(tag("Sensor at x="), isize),
            preceded(tag(", y="), isize),
            preceded(tag(": closest beacon is at x="), isize),
            preceded(tag(", y="), isize),
        ));
        let coords = map(line, |(x1, y1, x2, y2)| ((x1, y1), (x2, y2)));
        separated_list0(newline, coords)(s)
    }
}

#[derive(Debug)]
struct BeaconRange {
    x: isize,
    y: isize,
    dist: isize,
}

impl From<((isize, isize), (isize, isize))> for BeaconRange {
    fn from(((sx, sy), (bx, by)): ((isize, isize), (isize, isize))) -> Self {
        Self {
            x: sx,
            y: sy,
            dist: bx.abs_diff(sx) as isize + by.abs_diff(sy) as isize,
        }
    }
}

impl BeaconRange {
    fn coverage(&self, y: isize) -> Option<Range<isize>> {
        let dy = (self.y - y).abs();
        (self.dist - dy >= 0).then_some(self.x - self.dist + dy..self.x + self.dist - dy + 1)
    }
}

fn flatten_ranges(mut ranges: Vec<Range<isize>>) -> Vec<Range<isize>> {
    ranges.sort_unstable_by_key(|r| r.start);

    let r = ranges
        .into_iter()
        .fold(Vec::<Range<isize>>::new(), |mut rs, r| {
            if rs.is_empty() {
                rs.push(r)
            } else {
                let rp = rs.last_mut().unwrap();
                if rp.end >= r.start {
                    rp.end = rp.end.max(r.end)
                } else {
                    rs.push(r)
                }
            }
            rs
        });
    r
}
