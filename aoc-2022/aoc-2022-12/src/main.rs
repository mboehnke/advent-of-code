use aoc::{nom::combinator::map, Aoc, Solution};
use pathfinding::prelude::dijkstra;

#[derive(Aoc)]
pub struct Day12;

#[derive(Debug, Clone)]
pub struct Map {
    map: Vec<Vec<i8>>,
    pub start: (usize, usize),
    destination: (usize, usize),
}

impl Solution<usize, usize> for Day12 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 12;
    const TITLE: &'static str = "Hill Climbing Algorithm";

    type Data = Map;

    fn part1(map: Self::Data) -> usize {
        map.shortest_path_len().unwrap()
    }

    fn part2(mut map: Self::Data) -> usize {
        let possible_starts = map
            .map
            .iter()
            .enumerate()
            .flat_map(|(y, row)| {
                row.iter()
                    .enumerate()
                    .filter_map(move |(x, c)| (*c == 0).then_some((x, y)))
            })
            .collect::<Vec<_>>();

        possible_starts
            .into_iter()
            .flat_map(|start| {
                map.start = start;
                map.shortest_path_len()
            })
            .min()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let map_pos = |map: Vec<String>| {
            let mut map: Vec<Vec<char>> = map.into_iter().map(|r| r.chars().collect()).collect();

            let (sx, sy) = map
                .iter()
                .enumerate()
                .flat_map(|(y, row)| row.iter().enumerate().map(move |(x, c)| (x, y, c)))
                .find(|(_, _, c)| **c == 'S')
                .map(|(x, y, _)| (x, y))
                .unwrap_or_default();
            let (ex, ey) = map
                .iter()
                .enumerate()
                .flat_map(|(y, row)| row.iter().enumerate().map(move |(x, c)| (x, y, c)))
                .find(|(_, _, c)| **c == 'E')
                .map(|(x, y, _)| (x, y))
                .unwrap_or_default();
            map[sy][sx] = 'a';
            map[ey][ex] = 'z';

            let map = map
                .into_iter()
                .map(|row| row.into_iter().map(|c| (c as u8 - b'a') as i8).collect())
                .collect();
            Map {
                map,
                start: (sx, sy),
                destination: (ex, ey),
            }
        };
        map(aoc::nom::string_list, map_pos)(s)
    }
}

impl Map {
    fn successors(&self, (x, y): (usize, usize)) -> impl Iterator<Item = ((usize, usize), usize)> {
        let max_x = self.map[0].len() - 1;
        let max_y = self.map.len() - 1;
        let left = (x > 0 && (self.map[y][x] - self.map[y][x - 1]) <= 1).then_some(((x - 1, y), 1));
        let right =
            (x < max_x && (self.map[y][x] - self.map[y][x + 1]) <= 1).then_some(((x + 1, y), 1));
        let up = (y > 0 && (self.map[y][x] - self.map[y - 1][x]) <= 1).then_some(((x, y - 1), 1));
        let down =
            (y < max_y && (self.map[y][x] - self.map[y + 1][x]) <= 1).then_some(((x, y + 1), 1));
        left.into_iter().chain(right).chain(up).chain(down)
    }

    fn shortest_path_len(&self) -> Option<usize> {
        dijkstra(
            &self.destination,
            |&pos| self.successors(pos),
            |&pos| pos == self.start,
        )
        .map(|(_, len)| len)
    }
}
