use std::cmp::Ordering;

use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline, u8},
        combinator::map,
        multi::{separated_list0, separated_list1},
        sequence::{delimited, separated_pair, tuple},
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day13;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Value {
    List(Vec<Value>),
    Integer(u8),
}

impl Solution<usize, usize> for Day13 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Distress Signal";

    type Data = Vec<(Value, Value)>;

    fn part1(packets: Self::Data) -> usize {
        packets
            .into_iter()
            .enumerate()
            .filter(|(_, (p1, p2))| p1 <= p2)
            .map(|(i, _)| i + 1)
            .sum()
    }

    fn part2(packets: Self::Data) -> usize {
        let div1: Value = Value::List(vec![Value::List(vec![Value::Integer(2)])]);
        let div2: Value = Value::List(vec![Value::List(vec![Value::Integer(6)])]);

        let mut packets = packets
            .iter()
            .flat_map(|(a, b)| [a, b])
            .chain([&div1, &div2])
            .collect::<Vec<_>>();

        packets.sort_unstable();

        packets
            .iter()
            .enumerate()
            .filter(|(_, v)| v == &&&div1 || v == &&&div2)
            .map(|(i, _)| i + 1)
            .product()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        fn integer(s: &str) -> aoc::nom::IResult<&str, Value> {
            map(u8, Value::Integer)(s)
        }

        fn value(s: &str) -> aoc::nom::IResult<&str, Value> {
            alt((list, integer))(s)
        }

        fn list(s: &str) -> aoc::nom::IResult<&str, Value> {
            let inner = separated_list0(char(','), value);
            map(delimited(char('['), inner, char(']')), Value::List)(s)
        }

        let pair = separated_pair(value, newline, value);

        separated_list1(tuple((newline, newline)), pair)(s)
    }
}

impl Ord for Value {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Value::Integer(a), Value::Integer(b)) => a.cmp(b),
            (a @ Value::List(_), b @ Value::Integer(_)) => a.cmp(&Value::List(vec![b.clone()])),
            (a @ Value::Integer(_), b @ Value::List(_)) => Value::List(vec![a.clone()]).cmp(b),
            (Value::List(a), Value::List(b)) if a == b => Ordering::Equal,
            (Value::List(a), Value::List(b)) => {
                for (a, b) in a.iter().zip(b) {
                    if a < b {
                        return Ordering::Less;
                    }
                    if a > b {
                        return Ordering::Greater;
                    }
                }
                a.len().cmp(&b.len())
            }
        }
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
