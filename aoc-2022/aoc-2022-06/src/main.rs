use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Day06;

impl Solution<usize, usize> for Day06 {
    const YEAR: u32 = 2022;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Tuning Trouble";

    type Data = Vec<char>;

    fn part1(buffer: Self::Data) -> usize {
        first_unique(buffer, 4).unwrap_or_default()
    }

    fn part2(buffer: Self::Data) -> usize {
        first_unique(buffer, 14).unwrap_or_default()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.trim().chars().collect()))
    }
}

fn first_unique(buffer: Vec<char>, len: usize) -> Option<usize> {
    buffer
        .windows(len)
        .enumerate()
        .find(|&(_, cs)| unique(cs))
        .map(|(i, _)| i + len)
}

fn unique(cs: &[char]) -> bool {
    (0..cs.len() - 1).all(|i| !(cs[i + 1..].contains(&cs[i])))
}
