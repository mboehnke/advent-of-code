#!/usr/bin/fish
set name (printf "aoc-2023-%02d" $argv)
set url "https://adventofcode.com/2023/day/$argv/input"
set session (read < ../session)

cp -r ../template $name

sed -i "s/##name##/$name/" "$name/Cargo.toml"

curl -b session=$session -o "$name/data/input" $url
