use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Day01;

impl Solution<usize, usize> for Day01 {
    const YEAR: u32 = 2023;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Trebuchet?!";

    type Data = Vec<String>;

    fn part1(input: Self::Data) -> usize {
        input
            .into_iter()
            .map(|line| {
                let mut digits = line
                    .chars()
                    .filter(char::is_ascii_digit)
                    .map(|c| c as u8 - b'0');
                let first = digits.next().unwrap_or_default();
                let last = digits.last().unwrap_or(first);
                (first * 10 + last) as usize
            })
            .sum()
    }

    fn part2(input: Self::Data) -> usize {
        input
            .into_iter()
            .map(parse_nums)
            .collect::<Self::Data>()
            .apply(Self::part1)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::string_list(s)
    }
}

fn parse_nums(line: String) -> String {
    let mut line = line.as_str();
    let mut res = String::new();
    'w: while !line.is_empty() {
        for (i, num) in [
            "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
        ]
        .into_iter()
        .enumerate()
        {
            if line.starts_with(num) {
                let (_, l) = line.split_at(num.len() - 1);
                line = l;
                res.push((i as u8 + b'1') as char);
                continue 'w;
            }
        }
        let (c, l) = line.split_at(1);
        line = l;
        res.push_str(c);
    }
    res
}
