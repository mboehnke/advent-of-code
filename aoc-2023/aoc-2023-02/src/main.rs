use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline, space1},
        combinator::map,
        multi::separated_list0,
        sequence::{delimited, preceded},
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day02;

#[derive(Debug, Clone)]
pub enum Cubes {
    Red(usize),
    Green(usize),
    Blue(usize),
}

impl Solution<usize, usize> for Day02 {
    const YEAR: u32 = 2023;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Cube Conundrum";

    type Data = Vec<Vec<Vec<Cubes>>>;

    fn part1(input: Self::Data) -> usize {
        const RED: usize = 12;
        const GREEN: usize = 13;
        const BLUE: usize = 14;

        input
            .into_iter()
            .enumerate()
            .filter(|(_, game)| {
                game.iter().all(|cubes| {
                    let (r, g, b) = total_cubes(cubes);
                    r <= RED && g <= GREEN && b <= BLUE
                })
            })
            .map(|(i, _)| i + 1)
            .sum()
    }

    fn part2(input: Self::Data) -> usize {
        input
            .into_iter()
            .map(|game| {
                let (r, g, b) = game.iter().fold((0, 0, 0), |(r1, g1, b1), cubes| {
                    let (r2, g2, b2) = total_cubes(cubes);
                    (r1.max(r2), g1.max(g2), b1.max(b2))
                });
                r * g * b
            })
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let id = delimited(tag("Game "), usize, char(':'));
        let red = map(delimited(space1, usize, tag(" red")), Cubes::Red);
        let green = map(delimited(space1, usize, tag(" green")), Cubes::Green);
        let blue = map(delimited(space1, usize, tag(" blue")), Cubes::Blue);
        let cubes = alt((red, green, blue));
        let round = separated_list0(char(','), cubes);
        let game = preceded(id, separated_list0(char(';'), round));
        separated_list0(newline, game)(s)
    }
}

fn total_cubes(cubes: &[Cubes]) -> (usize, usize, usize) {
    cubes.iter().fold((0, 0, 0), |(r, g, b), c| match c {
        Cubes::Red(n) => (r + n, g, b),
        Cubes::Green(n) => (r, g + n, b),
        Cubes::Blue(n) => (r, g, b + n),
    })
}
