use aoc::{Aoc, Solution};
use intcode::Computer;
use itertools::Itertools as _;
use std::collections::VecDeque;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 7;
    const TITLE: &'static str = "Amplification Circuit";

    type Data = Computer;

    fn part1(computer: Self::Data) -> isize {
        (0..5)
            .permutations(5)
            .map(|settings| {
                settings
                    .into_iter()
                    .map(|s| computer.clone().input(Some(s)).run())
                    .fold(0, |i, t| *t.input(Some(i)).run().output().last().unwrap())
            })
            .max()
            .unwrap()
    }

    fn part2(computer: Self::Data) -> isize {
        (5..10)
            .permutations(5)
            .map(|settings| {
                let mut amps = settings
                    .iter()
                    .map(|s| computer.clone().input(Some(*s)))
                    .collect::<VecDeque<Computer>>();
                let mut s = 0;
                while let Some(mut amp) = amps.pop_front() {
                    amp = amp.input(Some(s)).run();
                    if let Some(o) = amp.output().last() {
                        s = *o;
                        amps.push_back(amp);
                    }
                }
                s
            })
            .max()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(512)))
    }
}
