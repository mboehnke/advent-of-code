use std::fmt::Write;

use aoc::{Aoc, Solution};
use intcode::Computer;
use itertools::{iproduct, Itertools as _};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 17;
    const TITLE: &'static str = "Set and Forget";

    type Data = Computer;

    fn part1(computer: Self::Data) -> usize {
        let img = image(computer);
        iproduct!(1..img[0].len() - 1, 1..img.len() - 1)
            .filter(|&(x, y)| {
                [(x, y), (x, y - 1), (x, y + 1), (x - 1, y), (x + 1, y)]
                    .iter()
                    .all(|&(x, y)| img[y][x] == '#')
            })
            .map(|(x, y)| x * y)
            .sum()
    }

    fn part2(computer: Self::Data) -> isize {
        let img = image(computer.clone());
        let path = path(&img);
        let mut inputs = vec![
            "L,12,R,4,R,4,L,6,",
            "L,12,R,4,R,4,R,12,",
            "L,10,L,6,R,4,",
            "n",
        ];
        let main_routine = path
            .replace(inputs[0], "A,")
            .replace(inputs[1], "B,")
            .replace(inputs[2], "C,");
        inputs.insert(0, &main_routine);
        let cinput = inputs
            .iter()
            .map(|s| {
                let mut i = s.split(',').filter(|v| v != &"").join(",");
                i.push('\n');
                i
            })
            .collect::<String>();

        *computer
            .poke(0, 2)
            .input_str(&cinput)
            .run()
            .output()
            .last()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(4096)))
    }
}

fn image(computer: Computer) -> Vec<Vec<char>> {
    computer
        .run()
        .output()
        .iter()
        .map(|o| *o as u8 as char)
        .collect::<String>()
        .lines()
        .filter(|l| !l.is_empty())
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>()
}

fn path(map: &[Vec<char>]) -> String {
    let (mut x, mut y) = (0..map[0].len())
        .flat_map(|y| (0..map.len()).map(move |x| (x, y)))
        .find(|(x, y)| map[*y][*x] == '^')
        .unwrap();
    let mut out = "".to_string();
    let mut dir = '^';
    loop {
        //choose direction
        let l_dir = match dir {
            '^' => '<',
            'v' => '>',
            '<' => 'v',
            _ => '^',
        };
        dir = if dir_ok(l_dir, x, y, map) {
            out.push_str("L,");
            l_dir
        } else {
            out.push_str("R,");
            match dir {
                '^' => '>',
                'v' => '<',
                '<' => '^',
                _ => 'v',
            }
        };
        // walk forward
        let mut steps = 0;
        while dir_ok(dir, x, y, map) {
            steps += 1;
            match dir {
                '^' => y -= 1,
                'v' => y += 1,
                '<' => x -= 1,
                _ => x += 1,
            };
        }
        write!(out, "{},", steps).unwrap();
        // stop if at end
        if ['^', 'v', '<', '>']
            .iter()
            .filter(|d| dir_ok(**d, x, y, map))
            .count()
            <= 1
        {
            break;
        }
    }
    out
}

fn dir_ok(dir: char, x: usize, y: usize, map: &[Vec<char>]) -> bool {
    match dir {
        '^' if y < 1 => false,
        'v' if y >= map.len() - 1 => false,
        '<' if x < 1 => false,
        '>' if x >= map[0].len() - 1 => false,
        '^' => map[y - 1][x] == '#',
        'v' => map[y + 1][x] == '#',
        '<' => map[y][x - 1] == '#',
        _ => map[y][x + 1] == '#',
    }
}
