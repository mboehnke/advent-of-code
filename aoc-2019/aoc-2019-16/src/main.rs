use std::iter::repeat;

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Flawed Frequency Transmission";

    type Data = Vec<i64>;

    fn part1(signal: Self::Data) -> String {
        (0..100)
            .fold(signal, |i, _| phase(i))
            .iter()
            .take(8)
            .map(|d| (d + 48) as u8 as char)
            .collect()
    }

    fn part2(mut signal: Self::Data) -> String {
        signal = repeat(&signal)
            .take(10000)
            .flatten()
            .copied()
            .skip(
                signal[0..7]
                    .iter()
                    .map(|i| (i + 48) as u8 as char)
                    .collect::<String>()
                    .parse::<usize>()
                    .unwrap(),
            )
            .collect();

        for _ in 0..100 {
            (0..signal.len() - 1)
                .rev()
                .for_each(|i| signal[i] = (signal[i] + signal[i + 1]) % 10);
        }

        signal[0..8]
            .iter()
            .map(|d| (d + 48) as u8 as char)
            .collect()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.trim().chars().map(|c| c as u8 as i64 - 48).collect()))
    }
}

fn phase(input: Vec<i64>) -> Vec<i64> {
    let len = input.len();
    (0..len)
        .map(|i| {
            let (mut digit, mut sum) = (i, 0);
            while digit < len {
                sum += input.iter().skip(digit).take(i + 1).sum::<i64>();
                digit += 2 * (i + 1);
                sum -= input.iter().skip(digit).take(i + 1).sum::<i64>();
                digit += 2 * (i + 1);
            }
            sum.abs() % 10
        })
        .collect()
}
