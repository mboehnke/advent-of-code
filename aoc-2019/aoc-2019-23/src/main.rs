use aoc::{Aoc, Solution};
use intcode::Computer;
use itertools::Itertools as _;
use std::mem::swap;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Category Six";

    type Data = Computer;

    fn part1(mut computer: Self::Data) -> isize {
        let mut computer2 = computer.clone();
        let mut computers = (0..50)
            .map(|i| computer.clone().input(Some(i)))
            .collect::<Vec<Computer>>();

        for i in (0..50).cycle() {
            swap(&mut computers[i], &mut computer);

            computer = computer.input(Some(-1)).run();
            let outs = computer.output();
            for (&a, &x, &y) in outs.iter().tuples() {
                match a {
                    255 => return y,
                    a => {
                        swap(&mut computers[a as usize], &mut computer2);
                        computer2 = computer2.input(vec![x, y]);
                        swap(&mut computers[a as usize], &mut computer2);
                    }
                }
            }
            swap(&mut computers[i], &mut computer);
        }
        panic!()
    }

    fn part2(mut computer: Self::Data) -> isize {
        let mut computer2 = computer.clone();
        let mut computers = (0..50)
            .map(|i| computer.clone().input(Some(i)))
            .collect::<Vec<_>>();

        let mut prev = None;
        let mut nat = (0, 0);
        let mut idle = 0;
        for i in (0..50).cycle() {
            swap(&mut computers[i], &mut computer);

            computer = computer.input(Some(-1)).run();
            let outs = computer.output();
            if outs.len() > 2 {
                for (&a, &x, &y) in outs.iter().tuples() {
                    match a {
                        255 => nat = (x, y),
                        _ => {
                            swap(&mut computers[a as usize], &mut computer2);
                            computer2 = computer2.input(vec![x, y]);
                            swap(&mut computers[a as usize], &mut computer2);
                            idle = 0
                        }
                    }
                }
            } else {
                idle += 1;
            }
            swap(&mut computers[i], &mut computer);
            if idle == 50 {
                if prev == Some(nat) {
                    return nat.1;
                }
                prev = Some(nat);
                swap(&mut computers[0], &mut computer);
                computer = computer.input(vec![nat.0, nat.1]);
                swap(&mut computers[0], &mut computer);
                idle = 0;
            }
        }
        panic!()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(4096)))
    }
}
