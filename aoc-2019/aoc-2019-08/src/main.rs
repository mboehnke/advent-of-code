use aoc::{Aoc, Solution};
use apply::Apply as _;
use itertools::Itertools;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Space Image Format";

    type Data = String;

    fn part1(input: Self::Data) -> usize {
        const SIZE: usize = 25 * 6;

        let layer = (0..(input.len() / SIZE))
            .map(|i| input[(i * SIZE)..((i + 1) * SIZE)].to_string())
            .min_by_key(|l| l.chars().filter(|c| c == &'0').count())
            .unwrap();

        layer.chars().filter(|c| c == &'1').count() * layer.chars().filter(|c| c == &'2').count()
    }

    fn part2(input: Self::Data) -> String {
        const WIDTH: usize = 25;
        const SIZE: usize = 25 * 6;

        let overlay = |top: &str, bottom: &str| {
            (0..top.len())
                .map(|i| (top.chars().nth(i).unwrap(), bottom.chars().nth(i).unwrap()))
                .map(|(t, b)| if t == '2' { b } else { t })
                .collect()
        };

        input
            .chars()
            .chunks(SIZE)
            .into_iter()
            .map(|l| l.collect::<String>())
            .fold(String::new(), |o, l| {
                if o.is_empty() {
                    l
                } else {
                    overlay(&o, &l)
                }
            })
            .chars()
            .map(|c| match c {
                '0' => ' ',
                _ => '█',
            })
            .chunks(WIDTH)
            .into_iter()
            .map(|l| l.collect::<String>())
            .collect_vec()
            .apply(ocr::parse_5x6)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::digit1, combinator::map};

        map(digit1, String::from)(s)
    }
}
