use aoc::{Aoc, Solution};
use itertools::unfold;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 1;
    const TITLE: &'static str = "The Tyranny of the Rocket Equation";

    type Data = Vec<isize>;

    fn part1(input: Self::Data) -> isize {
        input.into_iter().map(fuel).sum()
    }

    fn part2(input: Self::Data) -> isize {
        input.into_iter().map(t_fuel).sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::isize_list(s)
    }
}

fn fuel(m: isize) -> isize {
    m / 3 - 2
}

fn t_fuel(m: isize) -> isize {
    unfold(m, move |f| {
        *f = fuel(*f);
        Some(*f).filter(|x| *x >= 0)
    })
    .sum()
}
