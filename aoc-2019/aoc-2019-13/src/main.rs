use aoc::{Aoc, Solution};
use intcode::Computer;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Care Package";

    type Data = Computer;

    fn part1(computer: Self::Data) -> usize {
        computer
            .run()
            .output()
            .into_iter()
            .skip(2)
            .step_by(3)
            .filter(|&z| z == 2)
            .count()
    }

    fn part2(mut computer: Self::Data) -> isize {
        computer = computer.poke(0, 2).run();
        let (mut paddle, mut ball, mut score) = (0, 0, 0);
        loop {
            for (&x, _, &z) in computer.output().iter().tuples() {
                match z {
                    3 => paddle = x,
                    4 => ball = x,
                    s if x == -1 => score = s,
                    _ => {}
                }
            }
            if computer.is_halted() {
                return score;
            }
            if computer.is_awaiting_input() {
                computer = computer.input(vec![ball - paddle]).run()
            }
        }
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(3072)))
    }
}
