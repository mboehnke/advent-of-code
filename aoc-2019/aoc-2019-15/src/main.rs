use aoc::{Aoc, Solution};
use hashbrown::HashMap;
use intcode::Computer;
use std::collections::VecDeque;
#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Oxygen System";

    type Data = Computer;

    fn part1(computer: Self::Data) -> isize {
        let (map, oxygen) = get_map_oxygen(computer);
        *fill_distance(&map, (0, 0)).get(&oxygen).unwrap()
    }

    fn part2(computer: Self::Data) -> isize {
        let (map, oxygen) = get_map_oxygen(computer);
        *fill_distance(&map, oxygen).values().max().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(4096)))
    }
}

fn dp(p: &(isize, isize), d: isize) -> (isize, isize) {
    match d {
        1 => (p.0, p.1 - 1),
        2 => (p.0, p.1 + 1),
        3 => (p.0 - 1, p.1),
        _ => (p.0 + 1, p.1),
    }
}

fn opp(d: isize) -> isize {
    match d {
        1 => 2,
        2 => 1,
        3 => 4,
        _ => 3,
    }
}

fn get_map_oxygen(mut computer: Computer) -> (HashMap<(isize, isize), isize>, (isize, isize)) {
    let mut trace = vec![];
    let mut map = {
        let mut m = HashMap::new();
        m.insert((0, 0), 1);
        m
    };
    let (mut oxygen, mut pos) = ((0, 0), (0, 0));

    while let Some(dir) = (1..5)
        .find(|d| map.get(&dp(&pos, *d)).is_none())
        .or_else(|| trace.pop())
    {
        let next_pos = dp(&pos, dir);
        computer = computer.input(vec![dir]).run();
        let res = *computer.output().last().unwrap();
        let new = map.insert(next_pos, res).is_none();
        if res > 0 {
            if res > 1 {
                oxygen = next_pos;
            }
            if new {
                trace.push(opp(dir))
            };
            pos = next_pos;
        };
    }

    (map, oxygen)
}

fn fill_distance(
    map: &HashMap<(isize, isize), isize>,
    origin: (isize, isize),
) -> HashMap<(isize, isize), isize> {
    let mut dists = {
        let mut m = HashMap::new();
        m.insert(origin, 0);
        m
    };
    let mut work = VecDeque::from(vec![origin]);

    while let Some(p0) = work.pop_front() {
        let pts = (1..5)
            .map(|d| dp(&p0, d))
            .filter(|p1| dists.get(p1).is_none() && *map.get(p1).unwrap_or(&0) > 0)
            .collect::<Vec<_>>();
        let d1 = 1 + dists.get(&p0).unwrap();
        for p in pts {
            dists.insert(p, d1);
            work.push_back(p);
        }
    }

    dists
}
