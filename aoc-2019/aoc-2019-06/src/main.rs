use aoc::{Aoc, Solution};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Universal Orbit Map";

    type Data = HashMap<String, String>;

    fn part1(orbits: Self::Data) -> usize {
        orbits.iter().map(|(a, _)| total_orbits(a, &orbits)).sum()
    }

    fn part2(orbits: Self::Data) -> usize {
        let you = orbit_list(vec!["YOU".to_string()], &orbits);
        let santa = orbit_list(vec!["SAN".to_string()], &orbits);

        let (yt, first_common_orbit) = you
            .iter()
            .enumerate()
            .find(|(_, o)| santa.contains(o))
            .unwrap();

        let (st, _) = santa
            .iter()
            .enumerate()
            .find(|(_, o)| o == &first_common_orbit)
            .unwrap();

        yt + st - 2
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{alphanumeric1, char, newline},
            combinator::map,
            multi::separated_list1,
            sequence::separated_pair,
        };

        let orbit = map(
            separated_pair(alphanumeric1, char(')'), alphanumeric1),
            |(a, b): (&str, &str)| (b.to_string(), a.to_string()),
        );
        let orbit_list = separated_list1(newline, orbit);
        map(orbit_list, HashMap::from_iter)(s)
    }
}

fn orbit_list(mut list: Vec<String>, map: &HashMap<String, String>) -> Vec<String> {
    match list.last().unwrap().clone() {
        x if x == "COM" => list,
        x => {
            list.push(map[&x].clone());
            orbit_list(list, map)
        }
    }
}

fn total_orbits(b: &str, map: &HashMap<String, String>) -> usize {
    orbit_list(vec![b.to_string()], map).len() - 1
}
