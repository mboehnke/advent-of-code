use aoc::{Aoc, Solution};
use apply::Apply as _;
use intcode::Computer;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 25;
    const TITLE: &'static str = "Cryostasis";

    type Data = Computer;

    fn part1(mut droid: Self::Data) -> usize {
        let rooms = map(&droid, Vec::new());

        droid = droid
            .input_str(&gather_items_and_go_to_puzzle(&rooms))
            .run();
        droid.output_str();

        let dir_cmd = rooms
            .iter()
            .find(|r| r.name == "Security Checkpoint")
            .unwrap()
            .directions
            .iter()
            .find(|d| d.name() == "Pressure-Sensitive Floor")
            .unwrap()
            .cmd();

        safe_items(&rooms)
            .iter()
            .powerset()
            .map(|list| {
                list.iter()
                    .map(|item| item.drop())
                    .chain(std::iter::once(dir_cmd.clone()))
                    .collect::<Vec<_>>()
                    .join("")
            })
            .map(|cmds| droid.clone().input_str(&cmds))
            .map(|c| c.run().output_str())
            .find(|o| !o.contains("Alert!"))
            .unwrap()
            .trim()
            .lines()
            .last()
            .unwrap()
            .chars()
            .filter(|c| c.is_numeric())
            .fold(0, |acc, c| acc * 10 + (c as u8 - b'0') as usize)
    }

    fn part2(_: Self::Data) -> String {
        String::new()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(6144)))
    }
}

#[derive(Clone, Debug)]
pub struct Room {
    pub name: String,
    pub description: String,
    pub directions: Vec<Direction>,
    pub items: Vec<Item>,
}

#[derive(Clone, Debug)]
pub enum Item {
    Safe(String),
    Dangerous(String),
}

#[derive(Clone, Debug)]
pub enum Direction {
    North(String),
    East(String),
    South(String),
    West(String),
}

impl Item {
    pub fn name(&self) -> String {
        match self {
            Item::Safe(name) => name,
            Item::Dangerous(name) => name,
        }
        .to_string()
    }

    pub fn take(&self) -> String {
        format!("take {}\n", self.name())
    }

    pub fn drop(&self) -> String {
        format!("drop {}\n", self.name())
    }

    pub fn is_safe(&self) -> bool {
        matches!(self, Item::Safe(_))
    }
}

impl Direction {
    pub fn cmd(&self) -> String {
        match self {
            Direction::North(_) => "north\n",
            Direction::East(_) => "east\n",
            Direction::South(_) => "south\n",
            Direction::West(_) => "west\n",
        }
        .to_string()
    }

    pub fn name(&self) -> String {
        match self {
            Direction::North(name) => name,
            Direction::East(name) => name,
            Direction::South(name) => name,
            Direction::West(name) => name,
        }
        .to_string()
    }
}

impl Room {
    pub fn from_droid(droid: &Computer) -> Self {
        let mut droid = droid.clone().run();
        let mut room = droid.output_str().as_str().apply(Room::from);
        room.directions = room
            .directions
            .iter()
            .map(|d| {
                let mut c = droid.clone().input_str(&d.cmd()).run();
                let name = c.output_str().as_str().apply(Room::from).name;
                match d {
                    Direction::North(_) => Direction::North(name),
                    Direction::East(_) => Direction::East(name),
                    Direction::South(_) => Direction::South(name),
                    Direction::West(_) => Direction::West(name),
                }
            })
            .collect();
        room.items = room
            .items
            .iter()
            .map(|i| {
                let mut c = droid.clone();
                let name = i.name();
                if name == "infinite loop" || name == "giant electromagnet" {
                    Item::Dangerous(name)
                } else {
                    c = c.input_str(&i.take()).run();
                    let o = c.output_str();
                    if o.trim().lines().last().unwrap() == "Command?" {
                        Item::Safe(name)
                    } else {
                        Item::Dangerous(name)
                    }
                }
            })
            .collect();
        room
    }
}

fn map(droid: &Computer, mut rooms: Vec<Room>) -> Vec<Room> {
    let room = Room::from_droid(droid);
    if rooms.iter().any(|r| r.name == room.name) {
        return rooms;
    }
    rooms.push(room.clone());
    for dir in room.directions {
        if !rooms.iter().any(|r| r.name == dir.name()) {
            let mut d = droid.clone().run();
            d.output();
            rooms = map(&d.input_str(&dir.cmd()), rooms);
        }
    }
    rooms
}

fn safe_items(rooms: &[Room]) -> Vec<Item> {
    rooms
        .iter()
        .flat_map(|r| r.items.iter())
        .filter(|item| item.is_safe())
        .cloned()
        .collect()
}

fn gather_items_and_go_to_puzzle(rooms: &[Room]) -> String {
    let mut prev_room = rooms[0].name.clone();
    let mut cmds = "".to_string();

    for room in rooms {
        for item in room.items.iter().filter(|i| i.is_safe()) {
            cmds.push_str(&path(&prev_room, &room.name, rooms));
            cmds.push_str(&item.take());
            prev_room = room.name.clone();
        }
    }
    cmds.push_str(&path(&prev_room, "Security Checkpoint", rooms));
    cmds
}

fn path(from: &str, to: &str, rooms: &[Room]) -> String {
    if from == to {
        return "".to_string();
    }

    let p_to = |dest: &str| {
        rooms
            .iter()
            .flat_map(|r| {
                r.directions
                    .iter()
                    .filter(|d| d.name() == dest)
                    .map(move |d| (r.name.clone(), d.cmd()))
            })
            .collect::<Vec<_>>()
    };

    let mut paths: Vec<(String, String)> = p_to(to);

    loop {
        if let Some((_, cmds)) = paths.iter().find(|p| p.0 == from) {
            return cmds.to_string();
        }

        let mut p = vec![];
        for path in paths {
            for t in p_to(&path.0) {
                p.push((t.0, [t.1, path.1.clone()].join("")))
            }
        }
        paths = p;
    }
}

impl From<&str> for Room {
    fn from(s: &str) -> Self {
        let parts = s.trim().split("\n\n").collect::<Vec<_>>();
        let mut directions = vec![];
        let mut items: Vec<Item> = vec![];
        let name = parts[0]
            .lines()
            .next()
            .unwrap()
            .replace("==", "")
            .trim()
            .to_string();
        let description = parts[0].lines().nth(1).unwrap().to_string();
        for part in parts {
            match part.lines().next() {
                Some("Doors here lead:") => {
                    directions = part
                        .lines()
                        .skip(1)
                        .map(|l| match l {
                            "- north" => Direction::North("".to_string()),
                            "- east" => Direction::East("".to_string()),
                            "- south" => Direction::South("".to_string()),
                            "- west" => Direction::West("".to_string()),
                            _ => panic!("could not parse room:\n{}", s),
                        })
                        .collect::<Vec<_>>()
                }
                Some("Items here:") => {
                    items = part
                        .lines()
                        .skip(1)
                        .map(|l| Item::Dangerous(l.chars().skip(2).collect::<String>()))
                        .collect::<Vec<_>>()
                }
                _ => {}
            }
        }
        Room {
            name,
            description,
            directions,
            items,
        }
    }
}
