use aoc::{Aoc, Solution};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Sol;

type Rule = HashMap<String, i64>;

impl Solution<i64, i64> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 14;
    const TITLE: &'static str = "Space Stoichiometry";

    type Data = HashMap<String, (i64, Rule)>;

    fn part1(rules: Self::Data) -> i64 {
        produce(&rules, 1)
    }

    fn part2(rules: Self::Data) -> i64 {
        let target = 1_000_000_000_000i64;
        let (mut good, mut bad) = (target / produce(&rules, 1), None);

        while bad.map_or(true, |bad| good < bad - 1) {
            let mid = bad.map_or_else(|| good * 2, |bad| (good + bad) / 2);
            match produce(&rules, mid) {
                p if p < target => good = mid,
                p if p == target => return mid,
                _ => bad = Some(mid),
            }
        }

        good
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::lines_map(s)
    }
}

fn produce(rules: &HashMap<String, (i64, Rule)>, quantity: i64) -> i64 {
    let mut mats = HashMap::new();
    mats.insert("FUEL".to_string(), quantity);
    while let Some((key, n)) = mats
        .iter()
        .filter_map(|(key, n)| match key {
            key if key != "ORE" && *n > 0 => Some((key.clone(), *n)),
            _ => None,
        })
        .next()
    {
        let (m, srcs) = &rules[&key];
        let x = (n + *m - 1) / *m;
        mats.insert(key, n - *m * x);
        for (k, v) in srcs.iter() {
            let y = mats.entry(k.to_string()).or_default();
            *y += *v * x;
        }
    }
    mats.get("ORE").cloned().unwrap_or_default()
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{alpha1, i64, newline, space1},
        combinator::map,
        multi::separated_list1,
        sequence::separated_pair,
        IResult,
    };
    use hashbrown::HashMap;

    use crate::Rule;

    pub fn lines_map(s: &str) -> IResult<&str, HashMap<String, (i64, Rule)>> {
        map(separated_list1(newline, line), HashMap::from_iter)(s)
    }

    fn line(s: &str) -> IResult<&str, (String, (i64, Rule))> {
        let (s, (inputs, output)) = separated_pair(inputs, tag(" => "), name_num)(s)?;
        Ok((s, (output.0.to_string(), (output.1, inputs))))
    }

    fn inputs(s: &str) -> IResult<&str, HashMap<String, i64>> {
        map(separated_list1(tag(", "), name_num), HashMap::from_iter)(s)
    }

    fn name_num(s: &str) -> IResult<&str, (String, i64)> {
        let (s, (num, name)) = separated_pair(i64, space1, alpha1)(s)?;
        Ok((s, (name.to_string(), num)))
    }
}
