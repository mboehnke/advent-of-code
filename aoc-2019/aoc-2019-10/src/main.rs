use aoc::{Aoc, Solution};
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Monitoring Station";

    type Data = Vec<Vec<char>>;

    fn part1(map: Self::Data) -> usize {
        best_location(&map).2
    }

    fn part2(map: Self::Data) -> usize {
        let (x, y, _) = best_location(&map);
        let polar_coords = |p: &(usize, usize)| {
            let c_c = coord_transforms::prelude::Vector2::new(
                (p.1 as i32 - y as i32) as f64,
                (p.0 as i32 - x as i32) as f64,
            );
            let pc = coord_transforms::d2::cartesian2polar(&c_c);
            (
                (pc[1] * 10000.0).round() as isize,
                (pc[0] * 10000.0).round() as isize,
            )
        };
        let asteroids = (0..map[0].len())
            .flat_map(|x| (0..map.len()).map(move |y| (x, y)))
            .filter(|(x, y)| map[*y][*x] == '#')
            .filter(|c| *c != (x, y))
            .map(|a| {
                let p = polar_coords(&a);
                (a.0, a.1, p.0, p.1)
            })
            .collect::<Vec<(usize, usize, isize, isize)>>();
        let mut angles = asteroids
            .iter()
            .map(|d| d.2)
            .unique()
            .collect::<Vec<isize>>();
        angles.sort_unstable();
        angles.reverse();
        let (x, y, _, _) = asteroids
            .iter()
            .filter(|d| d.2 == angles[199])
            .min_by_key(|x| x.3)
            .unwrap();
        x * 100 + y
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            character::complete::{char, newline},
            multi::{many1, separated_list1},
        };

        let row = many1(alt((char('.'), char('#'))));
        separated_list1(newline, row)(s)
    }
}

fn best_location(map: &[Vec<char>]) -> (usize, usize, usize) {
    let angle = |p1: &(usize, usize), p2: &(usize, usize)| {
        ((p2.1 as i32 - p1.1 as i32) as f64).atan2((p2.0 as i32 - p1.0 as i32) as f64)
    };

    let asteroids = (0..map[0].len())
        .flat_map(|x| (0..map.len()).map(move |y| (x, y)))
        .filter(|(x, y)| map[*y][*x] == '#')
        .collect::<Vec<(usize, usize)>>();
    let visible_asteroids = |p| {
        asteroids
            .iter()
            .filter(|a| a != &p)
            .map(|a| (angle(p, a) * 1000.0) as isize)
            .unique()
            .count()
    };
    asteroids
        .iter()
        .map(|p| (p.0, p.1, visible_asteroids(p)))
        .max_by_key(|x| x.2)
        .unwrap()
}
