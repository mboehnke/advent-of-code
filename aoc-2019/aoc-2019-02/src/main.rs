use aoc::{Aoc, Solution};
use intcode::Computer;
use itertools::iproduct;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Program Alarm";

    type Data = Computer;

    fn part1(computer: Self::Data) -> isize {
        computer.poke(1, 12).poke(2, 2).run().peek(0)
    }

    fn part2(computer: Self::Data) -> isize {
        iproduct!(0..=99, 0..=99)
            .find(|&(a, b)| computer.clone().poke(1, a).poke(2, b).run().peek(0) == 19690720)
            .map(|(a, b)| a * 100 + b)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}
