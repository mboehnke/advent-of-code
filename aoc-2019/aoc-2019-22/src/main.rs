use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Copy)]
pub enum Shuffle {
    NewStack,
    Increment(i128),
    Cut(i128),
}

impl Solution<i128, i128> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Slam Shuffle";

    type Data = Vec<Shuffle>;

    fn part1(shuffles: Self::Data) -> i128 {
        const CARD: i128 = 2019;
        const NUM_CARDS: i128 = 10007;
        shuffles.iter().fold(CARD, |c, s| match s {
            Shuffle::NewStack => NUM_CARDS - c - 1,
            Shuffle::Cut(s) => (c - s) % NUM_CARDS,
            Shuffle::Increment(s) => (c * s) % NUM_CARDS,
        })
    }

    fn part2(shuffles: Self::Data) -> i128 {
        const D: i128 = 119_315_717_514_047;
        const N: i128 = 101_741_582_076_661;
        const TGT: i128 = 2020;
        let (mut a, mut b) = (1, 0);

        for op in shuffles.into_iter().rev() {
            match op {
                Shuffle::Cut(n) => {
                    b += if n < 0 { n + D } else { n };
                }
                Shuffle::Increment(n) => {
                    let inv = modinv(n, D);
                    a = a * inv % D;
                    b = b * inv % D;
                }
                Shuffle::NewStack => {
                    b = -(b + 1);
                    a = -a;
                }
            }
            a %= D;
            b %= D;
            if a < 0 {
                a += D;
            }
            if b < 0 {
                b += D;
            }
        }
        let i1 = modp(a, N, D) * TGT % D;
        let i2 = (modp(a, N, D) + D - 1) % D;
        let i3 = b * i2 % D;
        let i4 = modp(a - 1, D - 2, D);
        (i1 + i3 * i4) % D
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.lines().map(Shuffle::from).collect()))
    }
}

impl From<&str> for Shuffle {
    fn from(s: &str) -> Self {
        match s {
            s if s.contains("new") => Shuffle::NewStack,
            s if s.contains("cut") => Shuffle::Cut(s[4..].parse().unwrap()),
            s if s.contains("increment") => Shuffle::Increment(s[20..].parse().unwrap()),
            _ => panic!("invalid data"),
        }
    }
}

fn modinv(mut a: i128, mut base: i128) -> i128 {
    if base == 1 {
        return 0;
    }
    let orig = base;
    let (mut x, mut y) = (1, 0);
    while a > 1 {
        let q = a / base;
        let tmp = base;
        base = a % base;
        a = tmp;
        let tmp = y;
        y = x - q * y;
        x = tmp;
    }
    if x < 0 {
        x + orig
    } else {
        x
    }
}

fn modp(b: i128, exp: i128, base: i128) -> i128 {
    let mut x = 1;
    let mut p = b % base;
    for i in 0..128 {
        if 1 & (exp >> i) == 1 {
            x = x * p % base;
        }
        p = p * p % base;
    }
    x
}
