use aoc::{Aoc, Solution};
use apply::Also as _;
use hashbrown::HashSet;
#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 24;
    const TITLE: &'static str = "Planet of Discord";

    type Data = String;

    fn part1(input: Self::Data) -> usize {
        let mut tiles = input
            .lines()
            .map(|l| l.chars().collect::<Vec<_>>())
            .collect::<Vec<Vec<char>>>();
        let height = tiles.len();
        let mut layouts = vec![];
        loop {
            layouts.push(
                tiles
                    .iter()
                    .map(|l| l.iter().collect::<String>())
                    .collect::<Vec<_>>()
                    .join(""),
            );
            if repeated(&layouts) {
                break;
            }
            let mut buf = vec![vec!['.'; 5]; 5];
            for x in 0..tiles[0].len() {
                for y in 0..height {
                    if tiles[y][x] == '#' {
                        buf[y][x] = if adjacent(x, y, &tiles) == 1 {
                            '#'
                        } else {
                            '.'
                        };
                    } else {
                        let a = adjacent(x, y, &tiles);
                        buf[y][x] = if a == 1 || a == 2 { '#' } else { '.' };
                    }
                }
            }
            tiles = buf;
        }

        biodiversity(layouts.last().unwrap())
    }

    fn part2(input: Self::Data) -> usize {
        Grid::from(&input)
            .also(|g| (0..200).for_each(|_| g.tick()))
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.to_string()))
    }
}

fn biodiversity(layout: &str) -> usize {
    layout
        .chars()
        .enumerate()
        .filter(|&(_, v)| v == '#')
        .map(|(i, _)| 1 << i as u32)
        .sum()
}

fn repeated(layouts: &[String]) -> bool {
    layouts
        .iter()
        .filter(|l| l == &layouts.last().unwrap())
        .count()
        > 1
}

fn adjacent(x: usize, y: usize, tiles: &[Vec<char>]) -> usize {
    adjacent::adjacent(x, y, tiles[0].len() - 1, tiles.len() - 1)
        .filter(|&(x, y)| tiles[y][x] == '#')
        .count()
}

type Cell = (isize, isize, isize);

struct Grid {
    rows: isize,
    cols: isize,
    bugs: HashSet<Cell>,
}

const DIRS: [(isize, isize); 4] = [(-1, 0), (1, 0), (0, 1), (0, -1)];

impl Grid {
    fn adj_cells(&self, cell: &Cell) -> Vec<Cell> {
        let (r, c, l) = cell;

        let mut adj = Vec::new();
        for (dr, dc) in &DIRS {
            let (nr, nc, nl) = (r + dr, c + dc, *l);

            if nc == self.cols / 2 && nr == self.rows / 2 {
                if *dr == 0 {
                    for nr in 0..self.rows {
                        adj.push(match dc {
                            1 => (nr, 0, nl - 1),
                            _ => (nr, self.cols - 1, nl - 1),
                        });
                    }
                } else {
                    for nc in 0..self.cols {
                        adj.push(match dr {
                            1 => (0, nc, nl - 1),
                            _ => (self.rows - 1, nc, nl - 1),
                        });
                    }
                }
            } else if nr < 0 || nc < 0 || nr >= self.rows || nc >= self.cols {
                adj.push((self.rows / 2 + dr, self.cols / 2 + dc, l + 1));
            } else {
                adj.push((nr, nc, nl));
            }
        }
        adj
    }

    fn adj_bugs_count(&self, cell: &Cell) -> usize {
        self.adj_cells(cell)
            .into_iter()
            .filter(|cell| self.bugs.contains(cell))
            .count()
    }

    fn tick(&mut self) {
        let mut bugs = HashSet::new();
        for bug in &self.bugs {
            if self.adj_bugs_count(bug) == 1 {
                bugs.insert(*bug);
            }

            for infested in self.adj_cells(bug) {
                if !&self.bugs.contains(&infested) {
                    let count = self.adj_bugs_count(&infested);
                    if count == 1 || count == 2 {
                        bugs.insert(infested);
                    }
                }
            }
        }

        self.bugs = bugs;
    }

    fn count(&self) -> usize {
        self.bugs.len()
    }
}

impl From<&String> for Grid {
    fn from(s: &String) -> Self {
        let (mut bugs, mut rows, mut cols) = (HashSet::new(), 0isize, 0isize);

        for (r, row) in s.lines().enumerate() {
            for (c, cell) in row.chars().enumerate() {
                cols = cols.max(c as isize + 1);
                rows = rows.max(r as isize + 1);
                if cell == '#' {
                    bugs.insert((r as isize, c as isize, 0));
                }
            }
        }

        Grid { rows, cols, bugs }
    }
}
