use aoc::{Aoc, Solution};
use intcode::Computer;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 21;
    const TITLE: &'static str = "Springdroid Adventure";

    type Data = Computer;

    fn part1(computer: Self::Data) -> isize {
        const CODE: &str = "NOT C T\nAND D T\nNOT A J\nOR T J\nWALK\n";
        *computer.input_str(CODE).run().output().last().unwrap()
    }

    fn part2(computer: Self::Data) -> isize {
        const CODE: &str =
            "OR A J\nAND B J\nAND C J\nNOT J J\nAND D J\nOR E T\nOR H T\nAND T J\nRUN\n";
        *computer.input_str(CODE).run().output().last().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(3072)))
    }
}
