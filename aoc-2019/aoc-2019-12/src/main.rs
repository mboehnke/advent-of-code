use aoc::{Aoc, Solution};
use apply::{Also as _, Apply as _};
use itertools::Itertools as _;
use num::integer::lcm;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, u64> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 12;
    const TITLE: &'static str = "The N-Body Problem";

    type Data = Vec<Vec<isize>>;

    fn part1(mut moons: Self::Data) -> isize {
        for _ in 0..1000 {
            for i in 0..3 {
                for m in (0..4).combinations(2) {
                    match moons[m[0]][i] {
                        x if x > moons[m[1]][i] => {
                            moons[m[1]][i + 3] += 1;
                            moons[m[0]][i + 3] -= 1;
                        }
                        x if x < moons[m[1]][i] => {
                            moons[m[0]][i + 3] += 1;
                            moons[m[1]][i + 3] -= 1;
                        }
                        _ => {}
                    }
                }
                (0..4).for_each(|m| moons[m][i] += moons[m][i + 3]);
            }
        }
        moons
            .iter()
            .map(|m| {
                (m[0].abs() + m[1].abs() + m[2].abs()) * (m[3].abs() + m[4].abs() + m[5].abs())
            })
            .sum()
    }

    fn part2(mut moons: Self::Data) -> u64 {
        let mut cycles: Vec<u64> = vec![];
        let initial_state = moons.clone();
        for i in 0..3 {
            for c in 1..u64::max_value() {
                for m in (0..4).combinations(2) {
                    match moons[m[0]][i] {
                        x if x > moons[m[1]][i] => {
                            moons[m[1]][i + 3] += 1;
                            moons[m[0]][i + 3] -= 1;
                        }
                        x if x < moons[m[1]][i] => {
                            moons[m[0]][i + 3] += 1;
                            moons[m[1]][i + 3] -= 1;
                        }
                        _ => {}
                    }
                }
                (0..4).for_each(|m| moons[m][i] += moons[m][i + 3]);
                if moons == initial_state {
                    cycles.push(c);
                    break;
                }
            }
        }
        lcm(cycles[0], cycles[1]).apply(|m| lcm(cycles[2], m))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag,
            character::complete::{char, newline, one_of},
            combinator::map,
            isize,
            multi::separated_list1,
            sequence::{delimited, preceded, tuple},
        };

        let number = preceded(tuple((one_of("xyz"), char('='))), isize);
        let number_list = delimited(char('<'), separated_list1(tag(", "), number), char('>'));
        let resized_number_list = map(number_list, |nums| nums.also(|n| n.resize(6, 0)));
        separated_list1(newline, resized_number_list)(s)
    }
}
