use aoc::{Aoc, Solution};
use intcode::Computer;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 9;
    const TITLE: &'static str = "Sensor Boost";

    type Data = Computer;

    fn part1(c: Self::Data) -> isize {
        *c.input(Some(1)).run().output().last().unwrap()
    }

    fn part2(c: Self::Data) -> isize {
        *c.input(Some(2)).run().output().last().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((s, s.parse::<Computer>().unwrap().mem_size(2048)))
    }
}
