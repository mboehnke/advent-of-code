use aoc::{Aoc, Solution};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Crossed Wires";

    type Data = Vec<HashMap<(isize, isize), isize>>;

    fn part1(wires: Self::Data) -> isize {
        wires[0]
            .keys()
            .filter(|&p| wires[1].contains_key(p))
            .map(|(x, y)| x.abs() + y.abs())
            .min()
            .unwrap()
    }

    fn part2(wires: Self::Data) -> isize {
        wires[0]
            .iter()
            .filter(|(&k, _)| wires[1].contains_key(&k))
            .map(|(k, v)| v + wires[1].get(k).unwrap())
            .min()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{anychar, char, newline},
            combinator::map,
            isize,
            multi::separated_list1,
            sequence::tuple,
        };

        let cmd = tuple((anychar, isize));
        let points = map(separated_list1(char(','), cmd), points);
        separated_list1(newline, points)(s)
    }
}

fn points(cmds: Vec<(char, isize)>) -> HashMap<(isize, isize), isize> {
    let (mut x, mut y, mut s, mut p) = (0, 0, 0, HashMap::new());
    for (dir, dis) in cmds {
        for _ in 0..dis {
            s += 1;
            match dir {
                'R' => x += 1,
                'L' => x -= 1,
                'U' => y += 1,
                _ => y -= 1,
            };
            p.entry((x, y)).or_insert(s);
        }
    }
    p
}
