use aoc::{Aoc, Solution};
use hashbrown::{HashMap, HashSet};
use std::collections::VecDeque;

#[derive(Aoc)]
pub struct Sol;

type Pos = (i64, i64);

impl Solution<i64, i64> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 20;
    const TITLE: &'static str = "Donut Maze";

    type Data = MazeGraph;

    fn part1(maze: Self::Data) -> i64 {
        maze.shortest_path_length("AA", "ZZ", false)
    }

    fn part2(maze: Self::Data) -> i64 {
        maze.shortest_path_length("AA", "ZZ", true)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut raw_labels: HashMap<String, Vec<Label>> = HashMap::new();
        let mut incomplete_labels: HashMap<Pos, char> = HashMap::new();
        let mut grid: Vec<Vec<char>> = vec![];

        for (row, line) in s.lines().enumerate() {
            let mut grid_row = vec![];
            for (col, c) in line.chars().enumerate() {
                let signed_coords = (col as i64, row as i64);
                match c {
                    'A'..='Z' => {
                        if let Some((label, pos)) =
                            attempt_label_completion(&incomplete_labels, c, signed_coords)
                        {
                            let entry = raw_labels.entry(label).or_insert_with(Vec::new);
                            entry.push(pos);
                        } else {
                            incomplete_labels.insert(signed_coords, c);
                        }
                        grid_row.push(' ');
                    }
                    _ => grid_row.push(c),
                }
            }
            grid.push(grid_row);
        }

        let mut nodes: HashSet<Pos> = HashSet::new();
        let labels = raw_labels
            .iter()
            .map(|(s, raws)| {
                (
                    s.clone(),
                    raws.iter()
                        .map(|raw| {
                            let node = match raw {
                                Label::Vertical { x, y1, y2 } => {
                                    if *y1 == 0 {
                                        (*x, y2 + 1)
                                    } else if grid[(y1 - 1) as usize][*x as usize] == '.' {
                                        (*x, y1 - 1)
                                    } else {
                                        (*x, y2 + 1)
                                    }
                                }
                                Label::Horizontal { y, x1, x2 } => {
                                    if *x1 == 0 {
                                        (x2 + 1, *y)
                                    } else if grid[(*y) as usize][(x1 - 1) as usize] == '.' {
                                        (x1 - 1, *y)
                                    } else {
                                        (x2 + 1, *y)
                                    }
                                }
                            };
                            nodes.insert(node);
                            node
                        })
                        .collect::<Vec<Pos>>(),
                )
            })
            .collect::<HashMap<String, Vec<Pos>>>();

        let mut distances: HashMap<Pos, HashMap<Pos, i64>> = HashMap::new();
        for node in &nodes {
            let mut node_distances: HashMap<Pos, i64> = HashMap::new();
            let mut visited: HashSet<Pos> = HashSet::new();
            let mut to_visit: VecDeque<(Pos, i64)> = VecDeque::new();
            visited.insert(*node);
            to_visit.push_front((*node, 0));
            while !to_visit.is_empty() {
                let (curr_pos, curr_dist) = to_visit.pop_back().unwrap();
                if nodes.contains(&curr_pos) && curr_pos != *node {
                    node_distances.insert(curr_pos, curr_dist);
                }
                for next in get_next(curr_pos) {
                    if !visited.contains(&next) && grid[next.1 as usize][next.0 as usize] == '.' {
                        visited.insert(next);
                        to_visit.push_front((next, curr_dist + 1));
                    }
                }
            }
            distances.insert(*node, node_distances);
        }

        let mut internal_nodes: HashMap<Pos, Pos> = HashMap::new();
        let mut external_nodes: HashMap<Pos, Pos> = HashMap::new();
        for (_, nodes) in &labels {
            if nodes.len() == 2 {
                let (node0, node1) = (nodes[0], nodes[1]);
                let (internal, external) = if node0.0 == 2
                    || node0.1 == 2
                    || node0.0 + 3 == grid[0].len() as i64
                    || node0.1 + 3 == grid.len() as i64
                {
                    (node1, node0)
                } else {
                    (node0, node1)
                };
                internal_nodes.insert(internal, external);
                external_nodes.insert(external, internal);
            }
        }

        Ok((
            "",
            MazeGraph {
                distances,
                internal_nodes,
                external_nodes,
                labels,
            },
        ))
    }
}

enum Label {
    Vertical { x: i64, y1: i64, y2: i64 },
    Horizontal { y: i64, x1: i64, x2: i64 },
}

fn attempt_label_completion(
    incomplete: &HashMap<Pos, char>,
    letter: char,
    pos: Pos,
) -> Option<(String, Label)> {
    if let Some(c) = incomplete.get(&(pos.0, pos.1 - 1)) {
        Some((
            vec![*c, letter].into_iter().collect(),
            Label::Vertical {
                x: pos.0,
                y1: (pos.1 - 1),
                y2: pos.1,
            },
        ))
    } else if let Some(c) = incomplete.get(&(pos.0, pos.1 + 1)) {
        Some((
            vec![letter, *c].into_iter().collect(),
            Label::Vertical {
                x: pos.0,
                y1: pos.1,
                y2: (pos.1 + 1),
            },
        ))
    } else if let Some(c) = incomplete.get(&(pos.0 - 1, pos.1)) {
        Some((
            vec![*c, letter].into_iter().collect(),
            Label::Horizontal {
                y: pos.1,
                x1: (pos.0 - 1),
                x2: pos.0,
            },
        ))
    } else {
        incomplete.get(&(pos.0 + 1, pos.1)).map(|c| {
            (
                vec![letter, *c].into_iter().collect(),
                Label::Horizontal {
                    y: pos.1,
                    x1: pos.0,
                    x2: (pos.0 + 1),
                },
            )
        })
    }
}

fn get_next(pos: Pos) -> Vec<Pos> {
    vec![
        (pos.0 + 1, pos.1),
        (pos.0 - 1, pos.1),
        (pos.0, pos.1 + 1),
        (pos.0, pos.1 - 1),
    ]
}

fn open_with_min_f(open: &HashMap<(Pos, i64), Pos>) -> ((Pos, i64), Pos) {
    let (mut min_f, mut best) = (std::i64::MAX, (((0, 0), 0), (0, 0)));
    for (node, (g, h)) in open {
        if g + h < min_f {
            min_f = g + h;
            best = (*node, (*g, *h));
        }
    }
    best
}

#[derive(Clone)]
pub struct MazeGraph {
    distances: HashMap<Pos, HashMap<Pos, i64>>,
    internal_nodes: HashMap<Pos, Pos>,
    external_nodes: HashMap<Pos, Pos>,
    labels: HashMap<String, Vec<Pos>>,
}

impl MazeGraph {
    fn shortest_path_length(&self, start: &str, end: &str, recursive: bool) -> i64 {
        let start_point = (self.labels[start][0], 0);
        let end_point = (self.labels[end][0], 0);
        let mut open: HashMap<(Pos, i64), Pos> = HashMap::new();
        let mut closed: HashMap<(Pos, i64), Pos> = HashMap::new();
        open.insert(start_point, (0, 0));

        while !open.is_empty() {
            let ((q_pos, q_layer), (q_g, q_h)) = open_with_min_f(&open);
            open.remove(&(q_pos, q_layer));
            for (neighbor_pos, dist_to_neighbor) in &self.distances[&q_pos] {
                if (*neighbor_pos, q_layer) == end_point {
                    return q_g + dist_to_neighbor;
                }

                let (pos, mut layer, dist) = if recursive
                    && q_layer == 0
                    && self.external_nodes.contains_key(neighbor_pos)
                {
                    (*neighbor_pos, q_layer, *dist_to_neighbor)
                } else if let Some(dest) = self.external_nodes.get(neighbor_pos) {
                    (*dest, q_layer - 1, *dist_to_neighbor + 1)
                } else if let Some(dest) = self.internal_nodes.get(neighbor_pos) {
                    (*dest, q_layer + 1, *dist_to_neighbor + 1)
                } else {
                    (*neighbor_pos, q_layer, *dist_to_neighbor)
                };
                if !recursive {
                    layer = 0;
                }

                let (g, h) = (q_g + dist, q_layer);

                if let Some((open_g, open_h)) = open.get(&(pos, layer)) {
                    if open_g + open_h < g + h {
                        continue;
                    }
                }
                if let Some((closed_g, closed_h)) = closed.get(&(pos, layer)) {
                    if closed_g + closed_h < g + h {
                        continue;
                    }
                }

                open.insert((pos, layer), (g, h));
                closed.insert((q_pos, q_layer), (q_g, q_h));
            }
        }
        panic!()
    }
}
