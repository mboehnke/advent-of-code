use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 4;
    const TITLE: &'static str = "Secure Container";

    type Data = (usize, usize);

    fn part1((min, max): Self::Data) -> usize {
        (min..=max)
            .map(digits)
            .filter(|d| increases(d))
            .filter(|d| double_digit(d))
            .count()
    }

    fn part2((min, max): Self::Data) -> usize {
        (min..=max)
            .map(digits)
            .filter(|d| increases(d))
            .filter(|d| double_digit_2(d))
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::char, sequence::separated_pair, usize};

        separated_pair(usize, char('-'), usize)(s)
    }
}

fn double_digit(d: &[usize]) -> bool {
    (0..5).any(|i| d[i] == d[i + 1])
}

fn double_digit_2(d: &[usize]) -> bool {
    (0..5).any(|i| {
        d[i] == d[i + 1]
            && ((i == 0 && d[i] != d[i + 2])
                || (i == 4 && d[i] != d[i - 1])
                || (i > 0 && i < 4 && (d[i]) != d[i - 1] && d[i] != d[i + 2]))
    })
}

fn increases(d: &[usize]) -> bool {
    (0..5).all(|i| d[i] >= d[i + 1])
}

fn digits(n: usize) -> Vec<usize> {
    (0..6).map(|i| n / 10usize.pow(i) % 10).collect()
}
