use aoc::{Aoc, Solution};
use apply::Apply as _;
use hashbrown::HashMap;
use intcode::Computer;
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Robot {
    x_pos: isize,
    y_pos: isize,
    direction: char,
    panels: HashMap<(isize, isize), bool>,
    computer: Computer,
}

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2019;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Space Police";

    type Data = Robot;

    fn part1(robot: Self::Data) -> usize {
        robot.run().painted_panels()
    }

    fn part2(mut robot: Self::Data) -> String {
        robot.paint_panel(1);
        robot.run().registration().apply(ocr::parse_5x6).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}

impl Robot {
    fn run(mut self) -> Robot {
        while !self.computer.is_halted() {
            self.computer = self.computer.run();
            let o = self.computer.output();
            if o.len() > 1 {
                self.paint_panel(o[0]);
                self.turn_and_move(o[1]);
            }
            if self.computer.is_awaiting_input() {
                let color = self.current_panel() as isize;
                self.computer = self.computer.input(vec![color])
            }
        }
        self
    }

    fn turn_and_move(&mut self, dir: isize) {
        self.direction = match self.direction {
            'U' => ['L', 'R'][(dir > 0) as usize],
            'R' => ['U', 'D'][(dir > 0) as usize],
            'D' => ['R', 'L'][(dir > 0) as usize],
            _ => ['D', 'U'][(dir > 0) as usize],
        };
        match self.direction {
            'U' => self.y_pos -= 1,
            'D' => self.y_pos += 1,
            'L' => self.x_pos -= 1,
            _ => self.x_pos += 1,
        }
    }

    fn current_panel(&self) -> bool {
        self.panels.get(&(self.x_pos, self.y_pos)) == Some(&true)
    }

    fn paint_panel(&mut self, color: isize) {
        self.panels.insert((self.x_pos, self.y_pos), color == 1);
    }

    fn registration(&self) -> Vec<String> {
        let min_x = *self.panels.keys().map(|(x, _)| x).min().unwrap() + 1;
        let max_x = *self.panels.keys().map(|(x, _)| x).max().unwrap() - 2;
        let min_y = *self.panels.keys().map(|(_, y)| y).min().unwrap();
        let max_y = *self.panels.keys().map(|(_, y)| y).max().unwrap();
        (min_y..=max_y)
            .map(|y| {
                (min_x..=max_x)
                    .map(move |x| match self.panels.get(&(x, y)) {
                        Some(true) => '█',
                        _ => ' ',
                    })
                    .collect()
            })
            .collect()
    }

    fn painted_panels(&self) -> usize {
        self.panels.len()
    }
}

impl FromStr for Robot {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Robot {
            x_pos: 0,
            y_pos: 0,
            direction: 'U',
            panels: HashMap::new(),
            computer: s.parse::<Computer>()?.mem_size(2048),
        })
    }
}
