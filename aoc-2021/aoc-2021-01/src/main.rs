use aoc::{nom::u32_list, Aoc, Solution};

#[derive(Aoc)]
pub struct Day01;

impl Solution<usize, usize> for Day01 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Sonar Sweep";

    type Data = Vec<u32>;

    fn part1(report: Self::Data) -> usize {
        report.windows(2).filter(|w| w[0] < w[1]).count()
    }

    fn part2(report: Self::Data) -> usize {
        report.windows(4).filter(|w| w[0] < w[3]).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        u32_list(s)
    }
}
