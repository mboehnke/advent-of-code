use aoc::{
    nom::{
        bytes::complete::tag,
        isize,
        sequence::{preceded, separated_pair},
    },
    Aoc, Solution,
};
use itertools::iproduct;

#[derive(Aoc)]
pub struct Day17;

type Target = ((isize, isize), (isize, isize));

#[derive(Debug, Clone, Default)]
pub struct Probe {
    position: (isize, isize),
    velocity: (isize, isize),
    max_y: isize,
}
impl Solution<isize, usize> for Day17 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 17;
    const TITLE: &'static str = "Trick Shot";

    type Data = Target;

    fn part1(((left, right), (bottom, top)): Self::Data) -> isize {
        iproduct!(1..=-bottom, 1..=right)
            .flat_map(|(y, x)| Probe::new(x, y).hits(((left, right), (bottom, top))))
            .max()
            .unwrap()
    }

    fn part2(((left, right), (bottom, top)): Self::Data) -> usize {
        iproduct!(bottom..=-bottom, 1..=right)
            .flat_map(|(y, x)| Probe::new(x, y).hits(((left, right), (bottom, top))))
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let range = |s| separated_pair(isize, tag(".."), isize)(s);
        let ranges = separated_pair(range, tag(", y="), range);
        preceded(tag("target area: x="), ranges)(s)
    }
}

impl Probe {
    fn new(x: isize, y: isize) -> Self {
        Self {
            velocity: (x, y),
            ..Self::default()
        }
    }

    fn hits(&mut self, ((left, right), (bottom, top)): Target) -> Option<isize> {
        self.take_while(|&(x, y)| x <= right && y >= bottom)
            .any(|(x, y)| x >= left && y <= top)
            .then_some(self.max_y)
    }
}

impl Iterator for Probe {
    type Item = (isize, isize);

    fn next(&mut self) -> Option<Self::Item> {
        self.position.0 += self.velocity.0;
        self.position.1 += self.velocity.1;
        self.max_y = self.max_y.max(self.position.1);
        self.velocity.0 = (self.velocity.0 - 1).max(0);
        self.velocity.1 -= 1;
        Some(self.position)
    }
}
