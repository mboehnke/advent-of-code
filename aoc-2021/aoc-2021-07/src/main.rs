use std::convert::identity;

use aoc::{
    nom::{
        character::complete::{char, i32},
        multi::separated_list1,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day07;

impl Solution<i32, i32> for Day07 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 7;
    const TITLE: &'static str = "The Treachery of Whales";

    type Data = Vec<i32>;

    fn part1(crabs: Self::Data) -> i32 {
        let fuel = identity;
        min_fuel(crabs, fuel)
    }

    fn part2(crabs: Self::Data) -> i32 {
        let fuel = |dist| dist * (dist + 1) / 2;
        min_fuel(crabs, fuel)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        separated_list1(char(','), i32)(s)
    }
}

fn min_fuel(crabs: Vec<i32>, fuel: impl Fn(i32) -> i32) -> i32 {
    let mut min = i32::MAX;
    for pos in 0..=*crabs.iter().max().unwrap() {
        let f = crabs.iter().map(|p| fuel((pos - p).abs())).sum();
        if f > min {
            break;
        }
        min = f
    }
    min
}
