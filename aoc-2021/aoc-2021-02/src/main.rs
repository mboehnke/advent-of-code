use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{i32, newline},
        combinator::map,
        multi::separated_list1,
        sequence::preceded,
    },
    Aoc, Solution,
};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Day02;

impl Solution<i32, i32> for Day02 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Dive!";

    type Data = Vec<(i32, i32)>;

    fn part1(course: Self::Data) -> i32 {
        course
            .into_iter()
            .fold((0, 0), |(h_pos, depth), (x, y)| (h_pos + x, depth + y))
            .apply(|(h_pos, depth)| h_pos * depth)
    }

    fn part2(course: Self::Data) -> i32 {
        course
            .into_iter()
            .scan(0, |aim, (x, y)| {
                *aim += y;
                Some((x, x * *aim))
            })
            .collect::<Vec<_>>()
            .apply(Self::part1)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let up = map(preceded(tag("up "), i32), |n| (0, -n));
        let down = map(preceded(tag("down "), i32), |n| (0, n));
        let forward = map(preceded(tag("forward "), i32), |n| (n, 0));
        let dir = alt((up, down, forward));
        separated_list1(newline, dir)(s)
    }
}
