use std::{
    cmp::{max, min, Reverse},
    collections::BinaryHeap,
};

use aoc::{Aoc, Solution};
use hashbrown::HashMap;
use itertools::{iproduct, FoldWhile, Itertools};

#[derive(Aoc)]
pub struct Day;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct State(Vec<Vec<u8>>);

type Moves = Vec<(u8, usize, usize, usize, usize)>;

impl Solution<usize, usize> for Day {
    const YEAR: u32 = 2021;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Amphipod";

    type Data = State;

    fn part1(state: Self::Data) -> usize {
        let mut costs = HashMap::new();
        let mut heap = BinaryHeap::new();
        heap.push((Reverse(0), state));
        loop {
            let state = heap.pop().unwrap().1;
            let cost = *costs.entry(state.clone()).or_insert(0);
            if state.is_done() {
                return cost;
            }
            for (a, i, x, j, y) in state.moves() {
                let mut state = state.clone();
                state.0[i][x] = 0;
                state.0[j][y] = a;
                let w = (1..a).fold(1, |w, _| 10 * w);
                let cost2 = cost + w * (i.max(j) - i.min(j) + x + y);
                if costs.get(&state).filter(|&&c| c <= cost2).is_none() {
                    costs.insert(state.clone(), cost2);
                    heap.push((Reverse(cost2), state));
                }
            }
        }
    }

    fn part2(mut state: Self::Data) -> usize {
        iproduct!(1..=4, 2..=3)
            .zip([4, 4, 3, 2, 2, 1, 1, 3])
            .for_each(|((i, j), n)| state.0[2 * i].insert(j, n));
        Self::part1(state)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let inner = s
            .chars()
            .filter(|&c| "ABCD".contains(c))
            .enumerate()
            .map(|(i, c)| ((i % 4) * 2 + 2, c as u8 - b'A' + 1))
            .fold(vec![vec![0]; 11], |mut state, (i, n)| {
                state[i].push(n);
                state
            });
        Ok(("", State(inner)))
    }
}

impl State {
    fn is_done(&self) -> bool {
        self.0
            .iter()
            .enumerate()
            .all(|(x, stack)| stack.iter().all(|&a| a == 0 || x == 2 * a as usize))
    }

    fn moves(&self) -> Moves {
        self.0
            .iter()
            .enumerate()
            .filter(|&(i, src)| {
                i == 0
                    || i >= self.0.len() - 1
                    || i % 2 > 0
                    || src.iter().any(|&a| !(a == 0 || i == 2 * a as usize))
            })
            .filter_map(|(i, src)| {
                src.iter()
                    .enumerate()
                    .find(|&(_, a)| (*a != 0))
                    .map(|(x, a)| (i, x, *a))
            })
            .flat_map(|(i, x, a)| {
                self.0
                    .iter()
                    .enumerate()
                    .filter(move |&(j, _)| i != j)
                    .filter(move |&(j, _)| !((min(i, j) + 1..max(i, j)).any(|k| self.0[k][0] != 0)))
                    .filter_map(|(j, dst)| {
                        dst.iter()
                            .enumerate()
                            .rev()
                            .find(|(_, b)| **b == 0)
                            .map(|(y, _)| (j, y, dst))
                    })
                    .map(move |(j, y, dst)| (i, j, x, y, a, dst))
            })
            .fold_while(Vec::new(), |mut moves, (i, j, x, y, a, dst)| {
                if j == 2 * a as usize
                    && y > 0
                    && dst.iter().all(|&b| b == 0 || j == 2 * b as usize)
                {
                    FoldWhile::Done(vec![(a, i, x, j, y)])
                } else {
                    if x > 0 && (j == 0 || j == self.0.len() - 1 || j % 2 != 0) {
                        moves.push((a, i, x, j, y));
                    }
                    FoldWhile::Continue(moves)
                }
            })
            .into_inner()
    }
}
