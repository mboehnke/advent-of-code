use std::{collections::HashMap, iter::once, mem::swap, ops::Sub};

use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{char, digit1, multispace1, newline},
        combinator::map,
        isize,
        multi::separated_list1,
        separated_triple,
        sequence::{preceded, tuple},
    },
    Aoc, Solution,
};
use itertools::{iproduct, Itertools as _};

#[derive(Aoc)]
pub struct Day;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Beacon(isize, isize, isize);

#[derive(Debug, Clone)]
pub struct Scanner(Vec<Vec<Beacon>>, Vec<Beacon>);

impl Solution<usize, usize> for Day {
    const YEAR: u32 = 2021;
    const DAY: u32 = 19;
    const TITLE: &'static str = "Beacon Scanner";

    type Data = Vec<Scanner>;

    fn part1(scanners: Self::Data) -> usize {
        Scanner::flatten(scanners).beacons()
    }

    fn part2(scanners: Self::Data) -> usize {
        Scanner::flatten(scanners).max_dist().unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let beacon_r = separated_triple(isize, char(','), isize, char(','), isize);
        let beacon = map(beacon_r, |(a, b, c)| Beacon(a, b, c));
        let scanner_h = tuple((tag("--- scanner "), digit1, tag(" ---\n")));
        let scanner_r = preceded(scanner_h, separated_list1(newline, beacon));
        let scanner = map(map(scanner_r, |bs| vec![bs]), |bs| {
            Scanner(bs, vec![Beacon(0, 0, 0)])
        });
        separated_list1(multispace1, scanner)(s)
    }
}

impl Scanner {
    fn join(&self, other: &mut Self) -> Option<Self> {
        other.rotations().iter().find_map(|o| self.overlap(o))
    }

    fn rotations(&mut self) -> &Vec<Vec<Beacon>> {
        if self.0.len() == 1 {
            let rs = iproduct!(0..4, 0..4, 0..4)
                .map(|(rx, ry, rz)| self.rotate(rx, ry, rz))
                .collect();
            self.0 = rs;
        }
        &self.0
    }

    fn rotate(&self, rx: usize, ry: usize, rz: usize) -> Vec<Beacon> {
        let mut beacons = self.0[0].clone();
        for _ in 0..rx {
            for Beacon(_, x, y) in beacons.iter_mut() {
                swap(x, y);
                *x *= -1;
            }
        }
        for _ in 0..ry {
            for Beacon(x, _, y) in beacons.iter_mut() {
                swap(x, y);
                *x *= -1;
            }
        }
        for _ in 0..rz {
            for Beacon(x, y, _) in beacons.iter_mut() {
                swap(x, y);
                *x *= -1;
            }
        }
        beacons
    }

    fn overlap(&self, other: &[Beacon]) -> Option<Self> {
        let dist = self.0[0]
            .iter()
            .flat_map(|b1| other.iter().map(|b2| b2.sub(b1)))
            .fold(HashMap::<Beacon, usize>::new(), |mut beacons, b| {
                *beacons.entry(b).or_default() += 1;
                beacons
            })
            .into_iter()
            .find(|&(_, n)| n >= 12)
            .map(|(b, _)| b)?;
        let beacons = self.0[0]
            .iter()
            .copied()
            .chain(other.iter().map(|b| b.sub(&dist)))
            .unique()
            .collect();
        let scanners = self.1.iter().copied().chain(once(dist)).collect();
        Some(Scanner(vec![beacons], scanners))
    }

    fn beacons(&self) -> usize {
        self.0[0].len()
    }

    fn flatten(mut scanners: Vec<Self>) -> Self {
        let mut scanner = scanners.pop().unwrap();
        while !scanners.is_empty() {
            for i in 0..scanners.len() {
                if let Some(s) = scanner.join(&mut scanners[i]) {
                    scanner = s;
                    scanners.remove(i);
                    break;
                }
            }
        }
        scanner
    }

    fn max_dist(&self) -> Option<usize> {
        self.1
            .iter()
            .combinations(2)
            .map(|bs| bs[0] - bs[1])
            .map(Beacon::dist)
            .max()
    }
}

impl Sub for &Beacon {
    type Output = Beacon;

    fn sub(self, rhs: Self) -> Self::Output {
        Beacon(self.0 - rhs.0, self.1 - rhs.1, self.2 - rhs.2)
    }
}

impl Beacon {
    fn dist(self) -> usize {
        self.0.unsigned_abs() + self.1.unsigned_abs() + self.2.unsigned_abs()
    }
}
