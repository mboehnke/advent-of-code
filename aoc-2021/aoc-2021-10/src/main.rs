use aoc::{
    nom::{
        character::complete::{newline, one_of},
        combinator::map,
        multi::{many1, separated_list1},
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day10;

impl Solution<usize, usize> for Day10 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 10;
    const TITLE: &'static str = "Syntax Scoring";

    type Data = Vec<String>;

    fn part1(input: Self::Data) -> usize {
        input
            .into_iter()
            .map(simplified)
            .flat_map(Result::err)
            .map(value)
            .sum()
    }

    fn part2(input: Self::Data) -> usize {
        let mut scores = input
            .into_iter()
            .map(simplified)
            .flat_map(Result::ok)
            .map(|s| s.into_iter().rev().map(value).fold(0, |acc, x| acc * 5 + x))
            .collect::<Vec<_>>();
        scores.sort_unstable();
        scores[scores.len() / 2]
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let chars = many1(one_of("[]{}<>()"));
        let line = map(chars, String::from_iter);
        separated_list1(newline, line)(s)
    }
}

fn simplified(line: String) -> Result<Vec<char>, char> {
    let is_valid = |a, b| {
        a == '(' && b == ')' || a == '[' && b == ']' || a == '{' && b == '}' || a == '<' && b == '>'
    };
    let mut stack = Vec::new();
    for c in line.chars() {
        if "([{<".contains(c) {
            stack.push(c)
        } else if stack.pop().filter(|&p| is_valid(p, c)).is_none() {
            return Err(c);
        }
    }
    Ok(stack)
}

fn value(c: char) -> usize {
    match c {
        '(' => 1,
        '[' => 2,
        '{' => 3,
        '<' => 4,
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => panic!("invalid character"),
    }
}
