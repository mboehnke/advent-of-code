use std::collections::HashMap;

use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{newline, one_of, space1},
        combinator::map,
        multi::{many1, separated_list1},
        sequence::separated_pair,
    },
    Aoc, Solution,
};
use apply::Apply as _;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Day08;

#[derive(Debug, Clone)]
pub struct Display(Vec<Vec<char>>, Vec<Vec<char>>);

impl Solution<usize, usize> for Day08 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Seven Segment Search";

    type Data = Vec<Display>;

    fn part1(displays: Self::Data) -> usize {
        displays.iter().map(Display::contains_1478).sum()
    }

    fn part2(displays: Self::Data) -> usize {
        displays
            .iter()
            .map(Display::decode)
            .map(Option::unwrap)
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let segments = |s| separated_list1(space1, many1(one_of("abcdefg")))(s);
        let entry = separated_pair(segments, tag(" | "), segments);
        let display = map(entry, |(a, b)| Display(a, b));
        separated_list1(newline, display)(s)
    }
}

impl Display {
    fn contains_1478(&self) -> usize {
        self.1
            .iter()
            .map(Vec::len)
            .filter(|l| [2, 3, 4, 7].contains(l))
            .count()
    }

    fn get_mapping(&self) -> Option<HashMap<char, char>> {
        let is_valid = |map: &HashMap<char, char>| {
            self.0.iter().all(|segs| {
                segs.iter()
                    .map(|c| map.get(c))
                    .collect::<Option<String>>()
                    .and_then(Self::decode_segments)
                    .is_some()
            })
        };
        "abcdefg"
            .chars()
            .permutations(7)
            .map(|cs| {
                cs.into_iter()
                    .zip("abcdefg".chars())
                    .collect::<HashMap<_, _>>()
            })
            .find(is_valid)
    }

    fn decode(&self) -> Option<usize> {
        let map = self.get_mapping()?;
        self.1
            .iter()
            .map(|segs| segs.iter().flat_map(|s| map.get(s)).collect::<String>())
            .map(Display::decode_segments)
            .collect::<Option<Vec<_>>>()?
            .into_iter()
            .fold(0, |acc, x| acc * 10 + x)
            .apply(Some)
    }

    fn decode_segments(segs: String) -> Option<usize> {
        let mut segs = segs.chars().collect::<Vec<_>>();
        segs.sort_unstable();
        let segs = segs.into_iter().collect::<String>();
        match segs.as_str() {
            "abcefg" => Some(0),
            "cf" => Some(1),
            "acdeg" => Some(2),
            "acdfg" => Some(3),
            "bcdf" => Some(4),
            "abdfg" => Some(5),
            "abdefg" => Some(6),
            "acf" => Some(7),
            "abcdefg" => Some(8),
            "abcdfg" => Some(9),
            _ => None,
        }
    }
}
