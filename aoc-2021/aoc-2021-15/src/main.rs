use aoc::{
    nom::{combinator::map, usize_matrix},
    Aoc, Solution,
};
use pathfinding::prelude::{dijkstra, Matrix};

#[derive(Aoc)]
pub struct Day15;

impl Solution<usize, usize> for Day15 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Chiton";

    type Data = Matrix<usize>;

    fn part1(map: Self::Data) -> usize {
        dijkstra(
            &(0, 0),
            |&p| map.neighbours(p, false).map(|p| (p, map[p])),
            |&(r, c)| r + 1 == map.rows && c + 1 == map.columns,
        )
        .map(|(_, cost)| cost)
        .unwrap()
    }

    fn part2(map: Self::Data) -> usize {
        let (width, height) = (map.columns, map.rows);
        let val = |x, y| (map[(y % height, x % width)] + (y / height + x / width) - 1) % 9 + 1;
        let expanded_map = (0..height * 5)
            .map(|y| (0..width * 5).map(move |x| val(x, y)))
            .collect();
        Self::part1(expanded_map)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        map(usize_matrix, Matrix::from_iter)(s)
    }
}
