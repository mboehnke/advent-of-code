use aoc::{nom::character::complete::hex_digit1, Aoc, Solution};

#[derive(Aoc)]
pub struct Day;

#[derive(Debug, Clone)]
pub struct Packet {
    version: u64,
    data: PType,
}

#[derive(Debug, Clone)]
pub enum PType {
    Value(u64),
    Sum(Vec<Packet>),
    Product(Vec<Packet>),
    Min(Vec<Packet>),
    Max(Vec<Packet>),
    Greater(Vec<Packet>),
    Less(Vec<Packet>),
    Equal(Vec<Packet>),
}

impl Solution<u64, u64> for Day {
    const YEAR: u32 = 2021;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Packet Decoder";

    type Data = Packet;

    fn part1(packet: Self::Data) -> u64 {
        packet.version_sum()
    }

    fn part2(packet: Self::Data) -> u64 {
        packet.eval()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let s = hex_digit1(s)?
            .1
            .bytes()
            .flat_map(|b| match b {
                b'0' => b"0000",
                b'1' => b"0001",
                b'2' => b"0010",
                b'3' => b"0011",
                b'4' => b"0100",
                b'5' => b"0101",
                b'6' => b"0110",
                b'7' => b"0111",
                b'8' => b"1000",
                b'9' => b"1001",
                b'A' | b'a' => b"1010",
                b'B' | b'b' => b"1011",
                b'C' | b'c' => b"1100",
                b'D' | b'd' => b"1101",
                b'E' | b'e' => b"1110",
                b'F' | b'f' => b"1111",
                _ => unreachable!(),
            })
            .copied()
            .collect::<Vec<u8>>();
        Ok(("", parser::packet(&s).unwrap().1))
    }
}

impl Packet {
    fn version_sum(&self) -> u64 {
        match &self.data {
            PType::Value(_) => return self.version,
            PType::Sum(ps) => ps,
            PType::Product(ps) => ps,
            PType::Min(ps) => ps,
            PType::Max(ps) => ps,
            PType::Greater(ps) => ps,
            PType::Less(ps) => ps,
            PType::Equal(ps) => ps,
        }
        .iter()
        .map(Packet::version_sum)
        .sum::<u64>()
            + self.version
    }

    fn eval(&self) -> u64 {
        match &self.data {
            PType::Value(v) => *v,
            PType::Sum(ps) => ps.iter().map(Packet::eval).sum(),
            PType::Product(ps) => ps.iter().map(Packet::eval).product(),
            PType::Min(ps) => ps.iter().map(Packet::eval).min().unwrap(),
            PType::Max(ps) => ps.iter().map(Packet::eval).max().unwrap(),
            PType::Greater(ps) => (ps[0].eval() > ps[1].eval()) as u64,
            PType::Less(ps) => (ps[0].eval() < ps[1].eval()) as u64,
            PType::Equal(ps) => (ps[0].eval() == ps[1].eval()) as u64,
        }
    }
}

mod parser {
    use std::iter::once;

    use aoc::nom::{
        branch::alt,
        bytes::complete::{tag, take},
        character::complete::char,
        combinator::map,
        multi::{length_count, length_value, many0, many1},
        sequence::{preceded, tuple},
        IResult,
    };

    use crate::{PType, Packet};

    pub fn packet(s: &[u8]) -> IResult<&[u8], Packet> {
        let parts = tuple((number(3), data));
        map(parts, |(version, data)| Packet { version, data })(s)
    }

    fn data(s: &[u8]) -> IResult<&[u8], PType> {
        let sum = preceded(tag(b"000"), map(sub_packets, PType::Sum));
        let product = preceded(tag(b"001"), map(sub_packets, PType::Product));
        let min = preceded(tag(b"010"), map(sub_packets, PType::Min));
        let max = preceded(tag(b"011"), map(sub_packets, PType::Max));
        let value = preceded(tag(b"100"), map(value, PType::Value));
        let greater = preceded(tag(b"101"), map(sub_packets, PType::Greater));
        let less = preceded(tag(b"110"), map(sub_packets, PType::Less));
        let equal = preceded(tag(b"111"), map(sub_packets, PType::Equal));
        alt((sum, product, min, max, value, greater, less, equal))(s)
    }

    fn sub_packets(s: &[u8]) -> IResult<&[u8], Vec<Packet>> {
        let sub_packets_size = length_value(preceded(char('0'), number(15)), many1(packet));
        let sub_packets_num = length_count(preceded(char('1'), number(11)), packet);
        alt((sub_packets_size, sub_packets_num))(s)
    }

    fn value(s: &[u8]) -> IResult<&[u8], u64> {
        let n_a = preceded(char('1'), number(4));
        let n_b = preceded(char('0'), number(4));
        let to_u64 = |(gs, g): (Vec<u64>, u64)| {
            gs.into_iter()
                .chain(once(g))
                .fold(0, |acc, n| (acc << 4) + n)
        };
        map(tuple((many0(n_a), n_b)), to_u64)(s)
    }

    fn number(digits: usize) -> impl Fn(&[u8]) -> IResult<&[u8], u64> {
        move |s: &[u8]| {
            let to_u64 = |ds: &[u8]| ds.iter().fold(0, |acc, d| (acc << 1) + (d - b'0') as u64);
            map(take(digits), to_u64)(s)
        }
    }
}
