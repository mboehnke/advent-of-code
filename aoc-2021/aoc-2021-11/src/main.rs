use adjacent::adjacent_d;
use aoc::{nom::u8_matrix, Aoc, Solution};
use hashbrown::HashSet;
use itertools::iproduct;

#[derive(Aoc)]
pub struct Day11;

#[derive(Clone, Debug)]
pub struct Flashes(Vec<Vec<u8>>);

impl Solution<usize, usize> for Day11 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Dumbo Octopus";

    type Data = Vec<Vec<u8>>;

    fn part1(octopuses: Self::Data) -> usize {
        Flashes(octopuses).take(100).sum()
    }

    fn part2(octopuses: Self::Data) -> usize {
        Flashes(octopuses)
            .enumerate()
            .find(|&(_, f)| f == 100)
            .map(|(i, _)| i + 1)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        u8_matrix(s)
    }
}

impl Iterator for Flashes {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let mx = self.0[0].len() - 1;
        let my = self.0.len() - 1;
        let mut flashes = HashSet::new();
        let mut stack = iproduct!(0..=mx, 0..=my).collect::<Vec<_>>();
        while let Some((x, y)) = stack.pop() {
            self.0[y][x] += 1;
            if self.0[y][x] > 9 && flashes.insert((x, y)) {
                stack.extend(adjacent_d(x, y, mx, my))
            }
        }
        let num_flashes = flashes.len();
        flashes.into_iter().for_each(|(x, y)| self.0[y][x] = 0);
        Some(num_flashes)
    }
}
