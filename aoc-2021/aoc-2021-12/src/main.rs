use std::iter::once;

use aoc::{
    nom::{
        character::{
            complete::{alpha1, newline},
            streaming::char,
        },
        combinator::map,
        multi::separated_list1,
        sequence::separated_pair,
    },
    Aoc, Solution,
};
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Day12;

impl Solution<usize, usize> for Day12 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 12;
    const TITLE: &'static str = "Passage Pathing";

    type Data = HashMap<String, Vec<String>>;

    fn part1(map: Self::Data) -> usize {
        paths(&map, Vec::new(), true)
    }

    fn part2(map: Self::Data) -> usize {
        paths(&map, Vec::new(), false)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let raw = separated_list1(newline, separated_pair(alpha1, char('-'), alpha1));
        map(raw, |r: Vec<(&str, &str)>| {
            r.into_iter()
                .flat_map(|(a, b)| once((a, b)).chain(once((b, a))))
                .fold(HashMap::<String, Vec<String>>::new(), |mut map, (a, b)| {
                    map.entry(a.to_string()).or_default().push(b.to_string());
                    map
                })
        })(s)
    }
}

fn paths(map: &HashMap<String, Vec<String>>, path: Vec<&str>, s_c_t: bool) -> usize {
    let curr = *path.last().unwrap_or(&"start");
    if curr == "end" {
        1
    } else {
        map.get(curr)
            .into_iter()
            .flatten()
            .map(|n| {
                let t = n.chars().next().unwrap().is_lowercase() && path.contains(&n.as_str());
                (n, t)
            })
            .filter(|&(next, t)| next != "start" && !(s_c_t && t))
            .map(|(next, t)| {
                let mut path = path.clone();
                path.push(next);
                paths(map, path, s_c_t || t)
            })
            .sum()
    }
}
