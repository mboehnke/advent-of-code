use aoc::Solution as _;
use aoc_2021_25::{aoc_data::INPUT, Day25};
use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn parser_benchmark(c: &mut Criterion) {
    let routine = || Day25::parser(black_box(INPUT));
    c.bench_function("parser", |b| b.iter(routine));
}

fn part1_benchmark(c: &mut Criterion) {
    let parsed = Day25::parser(INPUT).unwrap().1;
    let routine = || Day25::part1(black_box(parsed.clone()));
    c.bench_function("part 1", |b| b.iter(routine));
}

fn part2_benchmark(c: &mut Criterion) {
    let parsed = Day25::parser(INPUT).unwrap().1;
    let routine = || Day25::part2(black_box(parsed.clone()));
    c.bench_function("part 2", |b| b.iter(routine));
}

criterion_group!(benches, parser_benchmark, part1_benchmark, part2_benchmark);
criterion_main!(benches);
