use aoc::{
    nom::{
        character::complete::{newline, one_of},
        combinator::map,
        multi::{many1, separated_list1},
    },
    Aoc, Solution,
};
use hashbrown::HashSet;

#[derive(Aoc)]
pub struct Day25;

#[derive(Debug, Clone)]
pub struct Swarms {
    width: usize,
    length: usize,
    east: HashSet<(usize, usize)>,
    south: HashSet<(usize, usize)>,
}

impl Solution<usize, &str> for Day25 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 25;
    const TITLE: &'static str = "Sea Cucumber";

    type Data = Swarms;

    fn part1(swarms: Self::Data) -> usize {
        swarms.enumerate().find(|&(_, m)| !m).unwrap().0 + 1
    }

    fn part2(_: Self::Data) -> &'static str {
        ""
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        map(separated_list1(newline, many1(one_of("v>."))), Swarms::from)(s)
    }
}

impl From<Vec<Vec<char>>> for Swarms {
    fn from(map: Vec<Vec<char>>) -> Self {
        let width = map[0].len();
        let length = map.len();
        let (east, south) = map
            .into_iter()
            .enumerate()
            .flat_map(|(y, row)| row.into_iter().enumerate().map(move |(x, c)| (x, y, c)))
            .fold(
                (HashSet::new(), HashSet::new()),
                |(mut east, mut south), (x, y, c)| {
                    match c {
                        'v' => south.insert((x, y)),
                        '>' => east.insert((x, y)),
                        _ => true,
                    };
                    (east, south)
                },
            );
        Self {
            width,
            length,
            east,
            south,
        }
    }
}

impl Iterator for Swarms {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        let mut movement = false;

        self.east = self
            .east
            .iter()
            .map(|&(x, y)| {
                let next_pos = ((x + 1) % self.width, y);
                if self.east.contains(&next_pos) || self.south.contains(&next_pos) {
                    (x, y)
                } else {
                    movement = true;
                    next_pos
                }
            })
            .collect();

        self.south = self
            .south
            .iter()
            .map(|&(x, y)| {
                let next_pos = (x, (y + 1) % self.length);
                if self.east.contains(&next_pos) || self.south.contains(&next_pos) {
                    (x, y)
                } else {
                    movement = true;
                    next_pos
                }
            })
            .collect();

        Some(movement)
    }
}
