use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, newline},
        combinator::value,
        multi::{many1, separated_list1},
    },
    Aoc, Solution,
};
use gazebo::prelude::*;
use itertools::{FoldWhile::*, Itertools};

#[derive(Aoc)]
pub struct Day03;

impl Solution<usize, usize> for Day03 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 3;
    const TITLE: &'static str = "Binary Diagnostic";

    type Data = Vec<Vec<bool>>;

    fn part1(report: Self::Data) -> usize {
        to_usize(most_common(&report)) * to_usize(least_common(&report))
    }

    fn part2(report: Self::Data) -> usize {
        rating(report.clone(), most_common) * rating(report, least_common)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let bin_digit = alt((value(true, char('1')), value(false, char('0'))));
        let bin_number = many1(bin_digit);
        separated_list1(newline, bin_number)(s)
    }
}

fn most_common(report: &[Vec<bool>]) -> Vec<bool> {
    occurences(report).map(|n| (n * 2 >= report.len()))
}

fn least_common(report: &[Vec<bool>]) -> Vec<bool> {
    occurences(report).map(|n| (n * 2 < report.len()))
}

fn occurences(report: &[Vec<bool>]) -> Vec<usize> {
    report.iter().fold(vec![0; 12], |acc, c| {
        acc.into_iter()
            .zip(c)
            .map(|(a, &b)| a + b as usize)
            .collect()
    })
}

fn rating(report: Vec<Vec<bool>>, cmp: impl Fn(&[Vec<bool>]) -> Vec<bool>) -> usize {
    (0..report[0].len())
        .fold_while(report, |mut report, i| {
            if report.len() < 2 {
                Done(report)
            } else {
                let c = cmp(&report);
                report.retain(|r| r[i] == c[i]);
                Continue(report)
            }
        })
        .into_inner()
        .first()
        .map(to_usize)
        .expect("no valid rating found")
}

fn to_usize(bin: impl AsRef<[bool]>) -> usize {
    bin.as_ref()
        .iter()
        .fold(0, |acc, &x| (acc << 1) + x as usize)
}
