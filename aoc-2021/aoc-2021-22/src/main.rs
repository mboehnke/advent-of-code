use std::ops::Sub;

use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline},
        combinator::map,
        isize,
        multi::separated_list1,
        separated_triple,
        sequence::{preceded, separated_pair, tuple},
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day;

#[derive(Clone, Debug)]
pub struct Step(Cuboid, bool);

#[derive(Clone, Debug)]
pub struct Cuboid {
    x: (isize, isize),
    y: (isize, isize),
    z: (isize, isize),
}

#[derive(Clone, Debug, Default)]
struct Reactor(Vec<Cuboid>);

impl Solution<usize, usize> for Day {
    const YEAR: u32 = 2021;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Reactor Reboot";

    type Data = Vec<Step>;

    fn part1(steps: Self::Data) -> usize {
        let steps = steps.into_iter().flat_map(Step::init).collect();
        Self::part2(steps)
    }

    fn part2(steps: Self::Data) -> usize {
        steps
            .into_iter()
            .fold(Reactor::default(), Reactor::step)
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let range = |c| {
            let r = separated_pair(isize, tag(".."), isize);
            preceded(tuple((char(c), char('='))), r)
        };
        let cuboid = move |s| {
            let ranges = separated_triple(range('x'), char(','), range('y'), char(','), range('z'));
            map(ranges, |(x, y, z)| Cuboid { x, y, z })(s)
        };
        let step_on = map(preceded(tag("on "), cuboid), |c| Step(c, true));
        let step_off = map(preceded(tag("off "), cuboid), |c| Step(c, false));
        let step = alt((step_on, step_off));
        separated_list1(newline, step)(s)
    }
}

impl Reactor {
    fn step(mut self, step: Step) -> Self {
        self.0 = self.0.iter().flat_map(|c| c - &step.0).collect();
        if step.1 {
            self.0.push(step.0);
        }
        self
    }

    fn count(self) -> usize {
        self.0.into_iter().map(Cuboid::volume).sum()
    }
}

impl Step {
    fn init(self) -> Option<Self> {
        const RANGE: (isize, isize) = (-50, 50);
        self.0.clamp(RANGE, RANGE, RANGE).map(|c| (Step(c, self.1)))
    }
}

impl Cuboid {
    fn clamp(&self, xs: (isize, isize), ys: (isize, isize), zs: (isize, isize)) -> Option<Self> {
        let x = Some((self.x.0.max(xs.0), self.x.1.min(xs.1))).filter(|(a, b)| b >= a)?;
        let y = Some((self.y.0.max(ys.0), self.y.1.min(ys.1))).filter(|(a, b)| b >= a)?;
        let z = Some((self.z.0.max(zs.0), self.z.1.min(zs.1))).filter(|(a, b)| b >= a)?;
        Some(Cuboid { x, y, z })
    }

    fn volume(Cuboid { x, y, z }: Self) -> usize {
        let len = |(a, b)| (b - a + 1) as usize;
        len(x) * len(y) * len(z)
    }
}

impl Sub for &Cuboid {
    type Output = Vec<Cuboid>;

    fn sub(self, rhs: Self) -> Self::Output {
        let a = self.clamp((self.x.0.min(rhs.x.0), rhs.x.0 - 1), self.y, self.z);
        let b = self.clamp((rhs.x.1 + 1, self.x.1.max(rhs.x.1)), self.y, self.z);
        let rest = match self.clamp(rhs.x, self.y, self.z) {
            Some(r) => r,
            None => return a.into_iter().chain(b).collect(),
        };
        let c = rest.clamp(rest.x, (rest.y.0.min(rhs.y.0), rhs.y.0 - 1), rest.z);
        let d = rest.clamp(rest.x, (rhs.y.1 + 1, self.y.1.max(rhs.y.1)), rest.z);
        let rest = match rest.clamp(rest.x, rhs.y, rest.z) {
            Some(r) => r,
            None => return a.into_iter().chain(b).chain(c).chain(d).collect(),
        };
        let e = rest.clamp(rest.x, rest.y, (rest.z.0.min(rhs.z.0), rhs.z.0 - 1));
        let f = rest.clamp(rest.x, rest.y, (rhs.z.1 + 1, self.z.1.max(rhs.z.1)));
        [a, b, c, d, e, f].into_iter().flatten().collect()
    }
}
