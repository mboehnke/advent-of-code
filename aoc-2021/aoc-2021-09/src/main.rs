use std::collections::VecDeque;

use adjacent::adjacent;
use aoc::{nom::u8_matrix, Aoc, Solution};
use itertools::{iproduct, Itertools};

#[derive(Aoc)]
pub struct Day09;

impl Solution<usize, usize> for Day09 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 9;
    const TITLE: &'static str = "Smoke Basin";

    type Data = Vec<Vec<u8>>;

    fn part1(map: Self::Data) -> usize {
        let mx = map[0].len() - 1;
        let my = map.len() - 1;
        iproduct!(0..=mx, 0..=my)
            .filter(|&(x, y)| adjacent(x, y, mx, my).all(|(ax, ay)| map[y][x] < map[ay][ax]))
            .map(|(x, y)| map[y][x] as usize + 1)
            .sum()
    }

    fn part2(mut map: Self::Data) -> usize {
        iproduct!(0..map[0].len(), 0..map.len())
            .flat_map(|(x, y)| fill(&mut map, x, y))
            .sorted_unstable()
            .rev()
            .take(3)
            .product()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        u8_matrix(s)
    }
}

fn fill(map: &mut [Vec<u8>], x: usize, y: usize) -> Option<usize> {
    let mx = map[0].len() - 1;
    let my = map.len() - 1;
    let mut num_filled = 0;
    let mut queue: VecDeque<(usize, usize)> = [(x, y)].into();
    while let Some((x, y)) = queue.pop_front() {
        if map[y][x] != 9 {
            map[y][x] = 9;
            num_filled += 1;
            adjacent(x, y, mx, my).for_each(|c| queue.push_back(c));
        }
    }
    (num_filled > 0).then_some(num_filled)
}
