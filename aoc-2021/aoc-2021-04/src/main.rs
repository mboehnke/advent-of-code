use aoc::{
    nom::{
        character::complete::{char, newline, space0, space1},
        combinator::map,
        multi::separated_list1,
        sequence::{preceded, separated_pair, tuple},
        usize,
    },
    Aoc, Solution,
};
use apply::Apply as _;
use hashbrown::HashMap;
use itertools::iproduct;

#[derive(Aoc)]
pub struct Day04;

#[derive(Debug, Clone)]
pub struct Board(Vec<Vec<usize>>);

impl Solution<usize, usize> for Day04 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 4;
    const TITLE: &'static str = "Giant Squid";

    type Data = (Vec<usize>, Vec<Board>);

    fn part1((numbers, boards): Self::Data) -> usize {
        boards
            .into_iter()
            .flat_map(|board| board.evaluate(&numbers))
            .min_by_key(|&(turn, _)| turn)
            .map(|(_, score)| score)
            .unwrap()
    }

    fn part2((numbers, boards): Self::Data) -> usize {
        boards
            .into_iter()
            .flat_map(|board| board.evaluate(&numbers))
            .max_by_key(|&(turn, _)| turn)
            .map(|(_, score)| score)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let number_list = separated_list1(char(','), usize);
        let row = preceded(space0, separated_list1(space1, usize));
        let board = map(separated_list1(newline, row), Board);
        let board_list = separated_list1(tuple((newline, newline)), board);
        separated_pair(number_list, tuple((newline, newline)), board_list)(s)
    }
}

impl Board {
    fn evaluate(&self, numbers: &[usize]) -> Option<(usize, usize)> {
        let m = self.0.len();
        let n = self.0[0].len();
        let board = iproduct!(0..n, 0..m)
            .map(|(i, j)| (self.0[j][i], (i, j)))
            .collect::<HashMap<_, _>>();

        numbers
            .iter()
            .enumerate()
            .flat_map(|(turn, &number)| board.get(&number).map(|&pos| (turn, number, pos)))
            .scan(
                (vec![vec![false; n]; m], 0),
                |(marks, marked_sum), (turn, number, (i, j))| {
                    *marked_sum += number;
                    marks[j][i] = true;
                    ((0..n).any(|i| marks.iter().all(|row| row[i]))
                        || marks.iter().any(|row| row.iter().all(|&x| x)))
                    .then(|| {
                        let unmarked_sum = board.keys().sum::<usize>() - *marked_sum;
                        (turn, unmarked_sum * number)
                    })
                    .apply(Some)
                },
            )
            .flatten()
            .next()
    }
}
