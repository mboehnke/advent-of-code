use std::{iter::Sum, ops::Add};

use aoc::{
    nom::{
        character::complete::{newline, one_of},
        combinator::map,
        multi::{many1, separated_list1},
    },
    Aoc, Solution,
};
use itertools::Itertools;

#[derive(Aoc)]
pub struct Day18;

#[derive(Clone, Debug, Default)]
pub struct Number(Vec<(u8, u8)>);

impl Solution<u64, u64> for Day18 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 18;
    const TITLE: &'static str = "Snailfish";

    type Data = Vec<Number>;

    fn part1(numbers: Self::Data) -> u64 {
        numbers.into_iter().sum::<Number>().magnitude()
    }

    fn part2(numbers: Self::Data) -> u64 {
        numbers
            .into_iter()
            .tuple_combinations()
            .map(|(a, b)| (&a + &b).magnitude().max((b + a).magnitude()))
            .max()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let from_chars = |chars| {
            let mut digits = Vec::new();
            let mut depth = 0;
            for c in chars {
                match c {
                    '[' => depth += 1,
                    ']' => depth -= 1,
                    ',' => {}
                    c => digits.push((depth, c as u8 - b'0')),
                }
            }
            Number(digits)
        };
        let number = map(many1(one_of("[],0123456789")), from_chars);
        separated_list1(newline, number)(s)
    }
}

impl Number {
    fn explode(&mut self) -> bool {
        if let Some(i) = (0..self.0.len() - 1).find(|&i| self.0[i].0 == 5) {
            let (l, r) = (self.0[i].1, self.0[i + 1].1);
            self.0[i] = (4, 0);
            self.0.remove(i + 1);
            if let Some(n) = (i > 0).then(|| &mut self.0[i - 1]) {
                n.1 += l
            }
            if let Some(n) = self.0.get_mut(i + 1) {
                n.1 += r
            };
            true
        } else {
            false
        }
    }

    fn split(&mut self) -> bool {
        if let Some(i) = (0..self.0.len()).find(|&i| self.0[i].1 >= 10) {
            let (d, n) = self.0[i];
            let a = n / 2;
            let b = n - a;
            self.0[i] = (d + 1, a);
            self.0.insert(i + 1, (d + 1, b));
            true
        } else {
            false
        }
    }

    fn reduced(mut self) -> Self {
        while self.explode() || self.split() {}
        self
    }

    fn magnitude(self) -> u64 {
        fn magnitude_(i: &mut usize, depth: u8, num: &Vec<(u8, u8)>) -> u64 {
            let l = if num[*i].0 == depth {
                *i += 1;
                num[*i - 1].1 as u64
            } else {
                magnitude_(i, depth + 1, num)
            };
            let r = if num[*i].0 == depth {
                *i += 1;
                num[*i - 1].1 as u64
            } else {
                magnitude_(i, depth + 1, num)
            };
            3 * l + 2 * r
        }

        magnitude_(&mut 0, 1, &self.0)
    }
}

impl Add for Number {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        &self + &rhs
    }
}

impl Add for &Number {
    type Output = Number;

    fn add(self, rhs: Self) -> Self::Output {
        let n = self
            .0
            .iter()
            .chain(&rhs.0)
            .map(|&(d, n)| (d + 1, n))
            .collect();
        Number(n).reduced()
    }
}

impl Sum for Number {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.reduce(|a, b| a + b).unwrap_or_default()
    }
}
