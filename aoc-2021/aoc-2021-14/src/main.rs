use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{alpha1, anychar, multispace1, newline, space0},
        combinator::map,
        multi::separated_list1,
        separated_triple,
        sequence::separated_pair,
    },
    Aoc, Solution,
};
use counter::Counter;
use hashbrown::HashMap;

#[derive(Aoc)]
pub struct Day14;

#[derive(Clone, Debug)]
pub struct Polymer {
    formula: Counter<(char, char), u64>,
    rules: HashMap<(char, char), char>,
    elements: Counter<char, u64>,
}

impl Solution<u64, u64> for Day14 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 14;
    const TITLE: &'static str = "Extended Polymerization";

    type Data = Polymer;

    fn part1(mut polymer: Self::Data) -> u64 {
        polymer.nth(9).unwrap()
    }

    fn part2(mut polymer: Self::Data) -> u64 {
        polymer.nth(39).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let template = map(alpha1, |s: &str| s.chars().collect());
        let rule = separated_triple(anychar, space0, anychar, tag(" -> "), anychar);
        let rules = separated_list1(newline, rule);
        let raw = separated_pair(template, multispace1, rules);
        map(raw, |(template, rules)| Polymer::new(template, rules))(s)
    }
}

impl Polymer {
    fn new(template: Vec<char>, rules: Vec<(char, char, char)>) -> Self {
        Self {
            formula: template.windows(2).map(|w| (w[0], w[1])).collect(),
            rules: rules.into_iter().map(|(a, b, c)| ((a, b), c)).collect(),
            elements: Counter::from_iter(template),
        }
    }
}

impl Iterator for Polymer {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        for (&(a, b), &n) in self.formula.clone().iter() {
            if let Some(&i) = self.rules.get(&(a, b)) {
                *self.formula.entry((a, b)).or_default() -= n;
                *self.formula.entry((a, i)).or_default() += n;
                *self.formula.entry((i, b)).or_default() += n;
                *self.elements.entry(i).or_default() += n;
            }
        }
        Some(self.elements.values().max()? - self.elements.values().min()?)
    }
}
