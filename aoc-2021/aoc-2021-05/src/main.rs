use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{char, newline, u32},
        combinator::map,
        multi::separated_list1,
        sequence::separated_pair,
    },
    Aoc, Solution,
};
use geo::{
    line_intersection::{line_intersection, LineIntersection},
    Coord,
};
use itertools::Itertools;

#[derive(Aoc)]
pub struct Day05;

#[derive(Debug, Clone)]
pub struct Line((u32, u32), (u32, u32));

impl Solution<usize, usize> for Day05 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 5;
    const TITLE: &'static str = "Hydrothermal Venture";

    type Data = Vec<Line>;

    fn part1(mut lines: Self::Data) -> usize {
        lines.retain(Line::is_h_v);
        Self::part2(lines)
    }

    fn part2(lines: Self::Data) -> usize {
        lines
            .iter()
            .combinations(2)
            .flat_map(|lines| lines[0].intersection(lines[1]))
            .flat_map(Line::points)
            .unique()
            .count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let coords = |s| separated_pair(u32, char(','), u32)(s);
        let coord_tuple = separated_pair(coords, tag(" -> "), coords);
        let line = map(coord_tuple, |(a, b)| Line(a, b));
        separated_list1(newline, line)(s)
    }
}

impl Line {
    fn points(self) -> impl Iterator<Item = (u32, u32)> {
        let range = |a: u32, b: u32| -> Vec<u32> {
            if a <= b {
                (a..=b).collect()
            } else {
                (b..=a).rev().collect()
            }
        };
        let xs = range(self.0 .0, self.1 .0);
        let ys = range(self.0 .1, self.1 .1);
        let len = xs.len().max(ys.len());
        xs.into_iter().cycle().zip(ys.into_iter().cycle()).take(len)
    }

    fn is_h_v(&self) -> bool {
        self.0 .1 == self.1 .1 || self.0 .0 == self.1 .0
    }

    fn intersection(&self, other: &Line) -> Option<Line> {
        let to_coord = |(x, y)| Coord {
            x: x as f64,
            y: y as f64,
        };
        let from_coord = |Coord::<f64> { x, y }| {
            ((x.fract() - 0.5).abs() > 0.4).then(|| (x.round() as u32, y.round() as u32))
        };

        let a = geo::Line::new(to_coord(self.0), to_coord(self.1));
        let b = geo::Line::new(to_coord(other.0), to_coord(other.1));
        let line = match line_intersection(a, b)? {
            LineIntersection::SinglePoint { intersection, .. } => {
                Line(from_coord(intersection)?, from_coord(intersection)?)
            }
            LineIntersection::Collinear {
                intersection: geo::Line { start, end },
            } => Line(from_coord(start)?, from_coord(end)?),
        };
        Some(line)
    }
}
