use std::mem::swap;

use apply::Apply;
use hashbrown::HashMap;

use aoc::{
    nom::{
        bytes::complete::tag,
        character::complete::{digit1, newline},
        sequence::{preceded, separated_pair, tuple},
        usize,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day21;

#[derive(Debug, Clone, Default)]
pub struct Game1 {
    pos: (usize, usize),
    score: (usize, usize),
    die: usize,
}

impl Solution<usize, u64> for Day21 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 21;
    const TITLE: &'static str = "Dirac Dice";

    type Data = (usize, usize);

    fn part1((p1, p2): Self::Data) -> usize {
        Game1::new(p1, p2)
            .enumerate()
            .find(|&(_, (s1, s2))| s1.max(s2) >= 1000)
            .map(|(t, (s1, s2))| s1.min(s2) * (t + 1) * 3)
            .unwrap()
    }

    fn part2((p1, p2): Self::Data) -> u64 {
        game2_wins(&mut HashMap::new(), p1 as u8, p2 as u8, 0, 0).apply(|(w1, w2)| w1.max(w2))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let pos = |s| {
            preceded(
                tuple((tag("Player "), digit1, tag(" starting position: "))),
                usize,
            )(s)
        };
        separated_pair(pos, newline, pos)(s)
    }
}

impl Game1 {
    fn new(p1: usize, p2: usize) -> Self {
        Self {
            pos: (p1, p2),
            ..Self::default()
        }
    }
}

impl Iterator for Game1 {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        let r1 = self.die % 100 + 1;
        let r2 = r1 % 100 + 1;
        self.die = r2 % 100 + 1;
        let rolls = self.die + r1 + r2;
        self.pos.0 = (self.pos.0 + rolls) % 10;
        self.score.0 += if self.pos.0 == 0 { 10 } else { self.pos.0 };
        swap(&mut self.pos.0, &mut self.pos.1);
        swap(&mut self.score.0, &mut self.score.1);
        Some(self.score)
    }
}

fn game2_wins(memo: &mut HashMap<u32, (u64, u64)>, p1: u8, p2: u8, s1: u8, s2: u8) -> (u64, u64) {
    if s1 >= 21 {
        return (1, 0);
    }
    if s2 >= 21 {
        return (0, 1);
    }
    let state = u32::from_le_bytes([p1, p2, s1, s2]);
    if let Some(&wins) = memo.get(&state) {
        return wins;
    }

    let wins = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)]
        .into_iter()
        .map(|(score, num)| {
            let pos = (p1 + score - 1) % 10 + 1;
            (pos, s1 + pos, num)
        })
        .fold((0, 0), |(wins1, wins2), (pos1, score1, num)| {
            let (w2, w1) = game2_wins(memo, p2, pos1, s2, score1);
            (wins1 + w1 * num, wins2 + w2 * num)
        });

    memo.insert(state, wins);

    wins
}
