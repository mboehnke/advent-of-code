use std::collections::VecDeque;

use aoc::{
    nom::{
        character::complete::{char, u8},
        multi::separated_list1,
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day06;

#[derive(Debug, Clone)]
pub struct School(VecDeque<u64>, u64);

impl Solution<u64, u64> for Day06 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Lanternfish";

    type Data = Vec<u8>;

    fn part1(school: Self::Data) -> u64 {
        School::from_iter(school).nth(79).unwrap()
    }

    fn part2(school: Self::Data) -> u64 {
        School::from_iter(school).nth(255).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        separated_list1(char(','), u8)(s)
    }
}

impl FromIterator<u8> for School {
    fn from_iter<T: IntoIterator<Item = u8>>(iter: T) -> Self {
        let fish = iter
            .into_iter()
            .fold(VecDeque::from([0; 9]), |mut fish, f| {
                fish[f as usize] += 1;
                fish
            });
        let num = fish.iter().sum::<u64>();
        School(fish, num)
    }
}

impl Iterator for School {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        let new = self.0.pop_front().unwrap();
        self.0[6] += new;
        self.0.push_back(new);
        self.1 += new;
        Some(self.1)
    }
}
