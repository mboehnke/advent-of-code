use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        combinator::map_opt,
        find, isize,
        multi::many_m_n,
        sequence::{preceded, tuple},
    },
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Day24;

#[derive(Debug, Clone)]
pub struct Monad(Vec<(usize, usize, isize)>);

impl Solution<u64, u64> for Day24 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 24;
    const TITLE: &'static str = "Arithmetic Logic Unit";

    type Data = Monad;

    fn part1(monad: Self::Data) -> u64 {
        monad.max()
    }

    fn part2(monad: Self::Data) -> u64 {
        monad.min()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let c1 = preceded(alt((tag("z 1\nadd x "), tag("z 26\nadd x "))), isize);
        let c2 = preceded(tag("add y w\nadd y "), isize);
        map_opt(many_m_n(14, 14, tuple((find(c1), find(c2)))), Monad::new)(s)
    }
}

impl Monad {
    fn new(cs: Vec<(isize, isize)>) -> Option<Self> {
        let mut stack = Vec::new();
        let mut rels = Vec::new();
        for i in 0..14 {
            if cs[i].0 >= 0 {
                stack.push(i);
            } else {
                let j = stack.pop()?;
                rels.push((i, j, cs[i].0 + cs[j].1));
            }
        }
        Some(Self(rels))
    }

    fn max(&self) -> u64 {
        self.0
            .iter()
            .fold([9; 14], |mut digits, &(i, j, k)| {
                if k >= 0 {
                    digits[j] -= k as u64;
                } else {
                    digits[i] -= (-k) as u64;
                }
                digits
            })
            .into_iter()
            .reduce(|acc, x| acc * 10 + x)
            .unwrap()
    }

    fn min(&self) -> u64 {
        self.0
            .iter()
            .fold([1; 14], |mut digits, &(i, j, k)| {
                if k >= 0 {
                    digits[i] += k as u64;
                } else {
                    digits[j] += (-k) as u64;
                }
                digits
            })
            .into_iter()
            .reduce(|acc, x| acc * 10 + x)
            .unwrap()
    }
}
