use aoc::{
    nom::{
        branch::alt,
        character::complete::{char, multispace1, newline},
        combinator::{map, value},
        multi::{many1, many_m_n, separated_list1},
        sequence::separated_pair,
    },
    Aoc, Solution,
};
use apply::Apply as _;
use hashbrown::HashSet;
use itertools::iproduct;

#[derive(Aoc)]
pub struct Day20;

#[derive(Debug, Clone)]
pub struct Image {
    algo: Vec<bool>,
    image: HashSet<(isize, isize)>,
    min_x: isize,
    max_x: isize,
    min_y: isize,
    max_y: isize,
    border: bool,
}

impl Solution<usize, usize> for Day20 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 20;
    const TITLE: &'static str = "Trench Map";

    type Data = Image;

    fn part1(mut image: Self::Data) -> usize {
        image.nth(1).unwrap()
    }

    fn part2(mut image: Self::Data) -> usize {
        image.nth(49).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let bit = |s| alt((value(true, char('#')), value(false, char('.'))))(s);
        let algo = many_m_n(512, 512, bit);
        let image = separated_list1(newline, many1(bit));
        let parts = separated_pair(algo, multispace1, image);
        map(parts, |(algo, image)| {
            let max_x = image[0].len() as isize - 1;
            let max_y = image.len() as isize - 1;
            let image = image
                .into_iter()
                .enumerate()
                .flat_map(|(y, row)| {
                    row.into_iter()
                        .enumerate()
                        .map(move |(x, c)| (x as isize, y as isize, c))
                })
                .filter(|&(_, _, c)| c)
                .map(|(x, y, _)| (x, y))
                .collect();
            Image {
                algo,
                min_x: 0,
                min_y: 0,
                max_x,
                max_y,
                image,
                border: false,
            }
        })(s)
    }
}

impl Iterator for Image {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        self.min_x -= 1;
        self.max_x += 1;
        self.min_y -= 1;
        self.max_y += 1;
        let next_p = |&(x, y): &(isize, isize)| {
            [
                (x - 1, y - 1),
                (x, y - 1),
                (x + 1, y - 1),
                (x - 1, y),
                (x, y),
                (x + 1, y),
                (x - 1, y + 1),
                (x, y + 1),
                (x + 1, y + 1),
            ]
            .into_iter()
            .map(|(x, y)| {
                if x <= self.min_x || y <= self.min_y || x >= self.max_x || y >= self.max_y {
                    self.border
                } else {
                    self.image.contains(&(x, y))
                }
            })
            .fold(0, |acc, x| (acc << 1) + x as usize)
            .apply(|i| self.algo[i])
        };
        let next = iproduct!(self.min_x..=self.max_x, self.min_y..=self.max_y)
            .filter(next_p)
            .collect::<HashSet<_>>();
        let len = next.len();
        self.image = next;
        self.border = if self.border {
            self.algo[511]
        } else {
            self.algo[0]
        };
        Some(len)
    }
}
