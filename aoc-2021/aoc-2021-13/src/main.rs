use aoc::{
    nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, multispace1, newline},
        combinator::map,
        multi::separated_list1,
        sequence::{preceded, separated_pair},
        usize,
    },
    Aoc, Solution,
};
use apply::Apply as _;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Day13;

#[derive(Clone, Copy, Debug)]
pub enum Fold {
    X(usize),
    Y(usize),
}

impl Solution<usize, String> for Day13 {
    const YEAR: u32 = 2021;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Transparent Origami";

    type Data = (Vec<(usize, usize)>, Vec<Fold>);

    fn part1((dots, mut folds): Self::Data) -> usize {
        folds.truncate(1);
        fold(dots, folds).len()
    }

    fn part2((dots, folds): Self::Data) -> String {
        fold(dots, folds)
            .apply(display)
            .apply(ocr::parse_5x6)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let dot = separated_pair(usize, char(','), usize);
        let dots = separated_list1(newline, dot);
        let fold_x = preceded(tag("fold along x="), map(usize, Fold::X));
        let fold_y = preceded(tag("fold along y="), map(usize, Fold::Y));
        let fold = alt((fold_x, fold_y));
        let folds = separated_list1(newline, fold);
        separated_pair(dots, multispace1, folds)(s)
    }
}

fn fold(dots: Vec<(usize, usize)>, folds: Vec<Fold>) -> Vec<(usize, usize)> {
    dots.into_iter()
        .map(|(mut x, mut y)| {
            for fold in &folds {
                match *fold {
                    Fold::X(f) if x > f => x = 2 * f - x,
                    Fold::Y(f) if y > f => y = 2 * f - y,
                    _ => {}
                }
            }
            (x, y)
        })
        .unique()
        .collect()
}

fn display(dots: Vec<(usize, usize)>) -> Vec<String> {
    let (max_x, max_y) = dots.iter().fold((0, 0), |(max_x, max_y), &(x, y)| {
        (max_x.max(x), max_y.max(y))
    });
    dots.into_iter()
        .fold(
            vec![vec![' '; max_x + 2]; max_y + 1],
            |mut matrix, (x, y)| {
                matrix[y][x] = '█';
                matrix
            },
        )
        .into_iter()
        .map(String::from_iter)
        .collect()
}
