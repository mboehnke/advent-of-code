use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Day;

impl Solution<isize, isize> for Day {
    const YEAR: u32 = 2023;
    const DAY: u32 = 99;
    const TITLE: &'static str = "";

    type Data = Vec<isize>;

    fn part1(input: Self::Data) -> isize {
        0 as isize
    }

    fn part2(input: Self::Data) -> isize {
        0 as isize
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::isize_list(s)
    }
}
