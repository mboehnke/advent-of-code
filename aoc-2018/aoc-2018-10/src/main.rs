use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Lights(Vec<(isize, isize, isize, isize)>);

impl Solution<String, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 10;
    const TITLE: &'static str = "The Stars Align";

    type Data = Lights;

    fn part1(mut lights: Self::Data) -> String {
        lights.find_map(Lights::message).unwrap()
    }

    fn part2(lights: Self::Data) -> usize {
        lights
            .enumerate()
            .find_map(|(t, l)| l.message().map(|m| (t + 1, m)))
            .unwrap()
            .0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag,
            character::complete::{char, newline, space0},
            combinator::map,
            isize,
            multi::separated_list1,
            sequence::{delimited, preceded, tuple},
        };

        let light = tuple((
            preceded(tuple((tag("position=<"), space0)), isize),
            preceded(tuple((char(','), space0)), isize),
            preceded(tuple((tag("> velocity=<"), space0)), isize),
            delimited(tuple((char(','), space0)), isize, char('>')),
        ));
        map(separated_list1(newline, light), Lights)(s)
    }
}

impl Lights {
    fn message(self) -> Option<String> {
        let minx = *self.0.iter().map(|(x, _, _, _)| x).min().unwrap();
        let maxx = *self.0.iter().map(|(x, _, _, _)| x).max().unwrap() + 2;
        let miny = *self.0.iter().map(|(_, y, _, _)| y).min().unwrap();
        let maxy = *self.0.iter().map(|(_, y, _, _)| y).max().unwrap();
        if maxy - miny > 10 {
            return None;
        }
        let x = self
            .0
            .iter()
            .fold(
                vec![vec![' '; (maxx - minx) as usize + 1]; (maxy - miny) as usize + 1],
                |mut message, (dx, dy, _, _)| {
                    message[(dy - miny) as usize][(dx - minx) as usize] = '█';
                    message
                },
            )
            .into_iter()
            .map(|v| v.into_iter().collect::<String>())
            .collect::<Vec<_>>();
        ocr::parse_8x10(x)
    }
}

impl Iterator for Lights {
    type Item = Lights;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.iter_mut().for_each(|l| {
            l.0 += l.2;
            l.1 += l.3
        });
        Some(self.clone())
    }
}
