use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Mine {
    map: Vec<Vec<char>>,
    carts: Vec<Cart>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Cart(usize, usize, Move, usize);

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Move {
    U,
    D,
    L,
    R,
    C,
}

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 13;
    const TITLE: &'static str = "Mine Cart Madness";

    type Data = Mine;

    fn part1(mut mine: Self::Data) -> String {
        mine.next_crash().apply(|(x, y)| format!("{},{}", x, y))
    }

    fn part2(mut mine: Self::Data) -> String {
        while mine.carts().len() > 1 {
            mine.next_crash();
        }
        mine.carts()[0].apply(|(x, y)| format!("{},{}", x, y))
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut map: Vec<Vec<char>> = s.lines().map(|l| l.chars().collect()).collect();
        let mut carts = Vec::new();
        for y in 0..map.len() {
            for x in 0..map[0].len() {
                match map[y][x] {
                    '^' => {
                        carts.push(Cart(x, y, Move::U, 0));
                        map[y][x] = '|'
                    }
                    'v' => {
                        carts.push(Cart(x, y, Move::D, 0));
                        map[y][x] = '|'
                    }
                    '<' => {
                        carts.push(Cart(x, y, Move::L, 0));
                        map[y][x] = '-'
                    }
                    '>' => {
                        carts.push(Cart(x, y, Move::R, 0));
                        map[y][x] = '-'
                    }
                    _ => {}
                }
            }
        }
        Ok(("", Mine { map, carts }))
    }
}

impl Mine {
    fn next_crash(&mut self) -> (usize, usize) {
        let mut crash = None;
        while crash.is_none() {
            self.carts.sort_unstable();
            for i in 0..self.carts.len() {
                match self.carts[i].2 {
                    Move::U => self.carts[i].1 -= 1,
                    Move::D => self.carts[i].1 += 1,
                    Move::L => self.carts[i].0 -= 1,
                    Move::R => self.carts[i].0 += 1,
                    Move::C => continue,
                }
                match (self.map[self.carts[i].1][self.carts[i].0], &self.carts[i].2) {
                    ('/', Move::U) => self.carts[i].2 = Move::R,
                    ('\\', Move::U) => self.carts[i].2 = Move::L,
                    ('/', Move::D) => self.carts[i].2 = Move::L,
                    ('\\', Move::D) => self.carts[i].2 = Move::R,
                    ('/', Move::L) => self.carts[i].2 = Move::D,
                    ('\\', Move::L) => self.carts[i].2 = Move::U,
                    ('/', Move::R) => self.carts[i].2 = Move::U,
                    ('\\', Move::R) => self.carts[i].2 = Move::D,
                    ('+', m) => {
                        match m {
                            Move::U => match self.carts[i].3 {
                                0 => self.carts[i].2 = Move::L,
                                1 => self.carts[i].2 = Move::U,
                                2 => self.carts[i].2 = Move::R,
                                _ => panic!(),
                            },
                            Move::D => match self.carts[i].3 {
                                0 => self.carts[i].2 = Move::R,
                                1 => self.carts[i].2 = Move::D,
                                2 => self.carts[i].2 = Move::L,
                                _ => panic!(),
                            },
                            Move::L => match self.carts[i].3 {
                                0 => self.carts[i].2 = Move::D,
                                1 => self.carts[i].2 = Move::L,
                                2 => self.carts[i].2 = Move::U,
                                _ => panic!(),
                            },
                            Move::R => match self.carts[i].3 {
                                0 => self.carts[i].2 = Move::U,
                                1 => self.carts[i].2 = Move::R,
                                2 => self.carts[i].2 = Move::D,
                                _ => panic!(),
                            },
                            _ => {}
                        }
                        self.carts[i].3 = (self.carts[i].3 + 1) % 3;
                    }
                    _ => {}
                }
                for j in 0..self.carts.len() {
                    if j != i
                        && self.carts[i].0 == self.carts[j].0
                        && self.carts[i].1 == self.carts[j].1
                        && self.carts[i].2 != Move::C
                        && self.carts[j].2 != Move::C
                    {
                        self.carts[i].2 = Move::C;
                        self.carts[j].2 = Move::C;
                        crash = Some((self.carts[i].0, self.carts[i].1));
                    }
                }
            }
        }
        crash.unwrap()
    }

    fn carts(&self) -> Vec<(usize, usize)> {
        self.carts
            .iter()
            .filter(|Cart(_, _, m, _)| *m != Move::C)
            .map(|&Cart(x, y, _, _)| (x, y))
            .collect()
    }
}

impl PartialOrd for Cart {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Cart {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        (self.1, self.0).cmp(&(other.1, other.0))
    }
}
