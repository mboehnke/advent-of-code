use aoc::{nom::bytes::complete::take, Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Recipes {
    recipes: Vec<u8>,
    elves: (usize, usize),
    i: usize,
}

impl Solution<String, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 14;
    const TITLE: &'static str = "Chocolate Charts";

    type Data = Vec<u8>;

    fn part1(n: Self::Data) -> String {
        let n = n.into_iter().fold(0, |acc, x| acc * 10 + x as usize);
        Recipes::new()
            .skip(n)
            .take(10)
            .map(|b| (b + b'0') as char)
            .collect()
    }

    fn part2(n: Self::Data) -> usize {
        let sequence = n.iter().fold(0, |acc, &x| acc * 10 + x as usize);
        let x = 10usize.pow(n.len() as u32 - 1);
        let mut recipes = Recipes::new();
        let window = (0..n.len() - 1)
            .flat_map(|_| recipes.next())
            .fold(0, |acc, x| acc * 10 + x as usize);
        recipes
            .enumerate()
            .scan(window, |window, (i, n)| {
                *window = (*window % x) * 10 + n as usize;
                Some((i, *window))
            })
            .find(|&(_, window)| sequence == window)
            .unwrap()
            .0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::u8, combinator::map_parser, multi::many1};

        let digit = map_parser(take(1usize), u8);
        many1(digit)(s)
    }
}

impl Recipes {
    fn new() -> Self {
        Recipes {
            recipes: vec![3, 7],
            elves: (0, 1),
            i: 0,
        }
    }
}

impl Iterator for Recipes {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        self.i += 1;
        if self.i > self.recipes.len() {
            let score0 = self.recipes[self.elves.0 % self.recipes.len()];
            let score1 = self.recipes[self.elves.1 % self.recipes.len()];
            let new = score0 + score1;
            if new >= 10 {
                self.recipes.push(new / 10);
                self.recipes.push(new % 10);
            } else {
                self.recipes.push(new);
            }
            self.elves.0 = (self.elves.0 + score0 as usize + 1) % self.recipes.len();
            self.elves.1 = (self.elves.1 + score1 as usize + 1) % self.recipes.len();
        }
        Some(self.recipes[self.i - 1])
    }
}
