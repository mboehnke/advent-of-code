use std::fmt::Write;

use aoc::{Aoc, Solution};
use hashbrown::HashMap;
use wrist_mounted_device::{Computer, Instruction};

#[derive(Aoc)]
pub struct Sol;

type Sample = (Vec<usize>, Vec<usize>, Vec<usize>);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 16;
    const TITLE: &'static str = "Chronal Classification";

    type Data = (Vec<Sample>, Vec<Vec<usize>>);

    fn part1((samples, _): Self::Data) -> usize {
        samples
            .into_iter()
            .map(find_op)
            .filter(|os| os.len() >= 3)
            .count()
    }

    fn part2((samples, program): Self::Data) -> usize {
        let mut possibilities = samples.into_iter().fold(
            HashMap::new(),
            |mut ops: HashMap<usize, Vec<String>>, sample| {
                let opcode = sample.1[0];
                let mut ins = find_op(sample);
                if let Some(prev_ins) = ops.get(&opcode) {
                    ins.retain(|i| prev_ins.contains(i));
                }
                ops.insert(opcode, ins);
                ops
            },
        );
        let mut ops: HashMap<usize, String> = HashMap::new();
        while !possibilities.is_empty() {
            let (opcode, op) = possibilities
                .iter()
                .find(|(_, ops)| ops.len() == 1)
                .map(|(a, b)| (*a, b[0].clone()))
                .unwrap();
            possibilities.remove(&opcode);
            for (_, os) in possibilities.iter_mut() {
                *os = os
                    .iter_mut()
                    .filter(|o| **o != op)
                    .map(|o| o.to_string())
                    .collect()
            }
            ops.insert(opcode, op);
        }
        let mut device = program
            .into_iter()
            .fold(String::new(), |mut res, ns| {
                writeln!(
                    res,
                    "{} {} {} {}",
                    ops.get(&ns[0]).unwrap(),
                    ns[1],
                    ns[2],
                    ns[3]
                )
                .unwrap();
                res
            })
            .parse::<Computer>()
            .unwrap();
        device.run();
        device.registers[0]
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut i = s.split("\n\n\n\n");
        let samples = i
            .next()
            .unwrap()
            .split("\n\n")
            .map(|sample| {
                let mut si = sample.lines().map(|l| {
                    l.chars()
                        .filter(|c| c.is_ascii_digit() || c.is_ascii_whitespace())
                        .collect::<String>()
                        .trim()
                        .split_ascii_whitespace()
                        .map(str::parse)
                        .map(Result::unwrap)
                        .collect::<Vec<usize>>()
                });
                (si.next().unwrap(), si.next().unwrap(), si.next().unwrap())
            })
            .collect();
        let program = i
            .next()
            .unwrap()
            .lines()
            .map(|l| {
                l.split_ascii_whitespace()
                    .map(str::parse)
                    .map(Result::unwrap)
                    .collect()
            })
            .collect();
        Ok(("", (samples, program)))
    }
}

fn find_op(sample: Sample) -> Vec<String> {
    let mut registers = sample.0.clone();
    registers.extend([0, 0].iter());
    let (a, b, c) = (sample.1[1], sample.1[2], sample.1[3]);
    [
        Instruction::ADDR(a, b, c),
        Instruction::ADDI(a, b, c),
        Instruction::MULR(a, b, c),
        Instruction::MULI(a, b, c),
        Instruction::BANR(a, b, c),
        Instruction::BANI(a, b, c),
        Instruction::BORR(a, b, c),
        Instruction::BORI(a, b, c),
        Instruction::SETR(a, b, c),
        Instruction::SETI(a, b, c),
        Instruction::GTIR(a, b, c),
        Instruction::GTRI(a, b, c),
        Instruction::GTRR(a, b, c),
        Instruction::EQIR(a, b, c),
        Instruction::EQRI(a, b, c),
        Instruction::EQRR(a, b, c),
    ]
    .iter()
    .map(|ins| {
        let mut device = Computer {
            registers: registers.clone(),
            instructions: vec![ins.clone()],
            ip: 5,
            icounter: 0,
        };
        device.run();
        (ins, device.registers)
    })
    .filter(|(_, rs)| rs[0..4] == sample.2)
    .map(|(ins, _)| {
        match ins {
            Instruction::ADDR(_, _, _) => "addr",
            Instruction::ADDI(_, _, _) => "addi",
            Instruction::MULR(_, _, _) => "mulr",
            Instruction::MULI(_, _, _) => "muli",
            Instruction::BANR(_, _, _) => "banr",
            Instruction::BANI(_, _, _) => "bani",
            Instruction::BORR(_, _, _) => "borr",
            Instruction::BORI(_, _, _) => "bori",
            Instruction::SETR(_, _, _) => "setr",
            Instruction::SETI(_, _, _) => "seti",
            Instruction::GTIR(_, _, _) => "gtir",
            Instruction::GTRI(_, _, _) => "gtri",
            Instruction::GTRR(_, _, _) => "gtrr",
            Instruction::EQIR(_, _, _) => "eqir",
            Instruction::EQRI(_, _, _) => "eqri",
            Instruction::EQRR(_, _, _) => "eqrr",
        }
        .to_string()
    })
    .collect()
}
