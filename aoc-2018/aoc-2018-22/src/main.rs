use aoc::{Aoc, Solution};
use pathfinding::prelude::{astar, Matrix};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 22;
    const TITLE: &'static str = "Mode Maze";

    type Data = (usize, usize, usize);

    fn part1((depth, tx, ty): Self::Data) -> usize {
        erosion_map(tx, ty, depth, tx, ty).values().sum()
    }

    fn part2((depth, tx, ty): Self::Data) -> usize {
        const NEITHER: usize = 1;
        const TORCH: usize = 2;
        const GEAR: usize = 4;
        const ALLOWED: [usize; 3] = [TORCH + GEAR, NEITHER + GEAR, NEITHER + TORCH];

        let map = erosion_map(tx * 7, ty * 7, depth, tx, ty);
        astar(
            &((0, 0), TORCH),
            |&((x, y), eq)| {
                map.neighbours((x, y), false)
                    .filter(|&(nx, ny)| ALLOWED[map[(nx, ny)]] & eq == eq)
                    .map(|(nx, ny)| (((nx, ny), eq), 1))
                    .chain(std::iter::once((((x, y), ALLOWED[map[(x, y)]] - eq), 7)))
                    .collect::<Vec<_>>()
            },
            |&((x, y), _)| x.abs_diff(tx) + y.abs_diff(ty),
            |&((x, y), eq)| x == tx && y == ty && eq == TORCH,
        )
        .unwrap()
        .1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut si = s.lines();
        let d = si
            .next()
            .unwrap()
            .trim_start_matches("depth: ")
            .parse()
            .unwrap();
        let mut si = si.next().unwrap().trim_start_matches("target: ").split(',');
        let x = si.next().unwrap().parse().unwrap();
        let y = si.next().unwrap().parse().unwrap();
        Ok(("", (d, x, y)))
    }
}

fn erosion_map(maxx: usize, maxy: usize, depth: usize, tx: usize, ty: usize) -> Matrix<usize> {
    let mut map = Matrix::new(maxx + 1, maxy + 1, 0);
    for x in 0..=maxx {
        for y in 0..=maxy {
            let index = if (x == 0 && y == 0) || (x == tx && y == ty) {
                0
            } else if y == 0 {
                x * 16807
            } else if x == 0 {
                y * 48271
            } else {
                map[(x - 1, y)] * map[(x, y - 1)]
            };
            map[(x, y)] = (index + depth) % 20183;
        }
    }
    map.as_mut().iter_mut().for_each(|n| *n %= 3);
    map
}
