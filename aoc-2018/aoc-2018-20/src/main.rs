use std::collections::{BTreeMap, BTreeSet, HashMap};

use aoc::{Aoc, Solution};
use pathfinding::prelude::dijkstra_all;

type Point = (i32, i32);

#[derive(Aoc)]
pub struct Sol;

impl Solution<i32, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 20;
    const TITLE: &'static str = "A Regular Map";

    type Data = HashMap<Point, (Point, i32)>;

    fn part1(cells: Self::Data) -> i32 {
        cells.values().map(|(_, c)| *c).max().unwrap()
    }

    fn part2(cells: Self::Data) -> usize {
        cells.values().filter(|&(_, c)| *c >= 1000).count()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        let mut map = BTreeMap::new();
        explore(&mut map, (0, 0), s.as_bytes(), &mut 1);
        Ok((
            "",
            dijkstra_all(&(0, 0), |pos| {
                map.get(pos)
                    .into_iter()
                    .flat_map(|neighbours| neighbours.iter().map(|n| (*n, 1)))
            }),
        ))
    }
}

fn explore(
    map: &mut BTreeMap<Point, BTreeSet<Point>>,
    mut pos: Point,
    input: &[u8],
    index: &mut usize,
) {
    loop {
        match input[*index] {
            b'|' | b')' | b'$' => break,
            b'(' => {
                while input[*index] != b')' {
                    *index += 1;
                    explore(map, pos, input, index)
                }
            }
            dir => {
                let dir = usize::from((dir ^ (dir >> 2)) & 3); // ENWS => 0..=3
                let newpos = (pos.0 + [1, 0, -1, 0][dir], pos.1 + [0, -1, 0, 1][dir]);
                map.entry(pos).or_default().insert(newpos);
                pos = newpos;
            }
        }
        *index += 1;
    }
}
