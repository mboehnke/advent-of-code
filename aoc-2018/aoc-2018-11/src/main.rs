use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<String, String> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 11;
    const TITLE: &'static str = "Chronal Charge";

    type Data = usize;

    fn part1(sn: Self::Data) -> String {
        let grid = grid(sn);
        let (x, y, _) = max_power(&grid, 3);
        format!("{},{}", x + 1, y + 1)
    }

    fn part2(sn: Self::Data) -> String {
        let grid = grid(sn);
        let (x, y, _, size) = (1..300)
            .map(|size| max_power(&grid, size).apply(|(x, y, p)| (x, y, p, size)))
            .max_by_key(|&(_, _, p, _)| p)
            .unwrap();
        format!("{},{},{}", x + 1, y + 1, size)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::usize(s)
    }
}

fn max_power(grid: &[Vec<isize>], size: usize) -> (usize, usize, isize) {
    (0..300 - size + 1)
        .flat_map(|y| {
            (0..300 - size + 1).map(move |x| {
                let p = (y..y + size)
                    .flat_map(|y1| (x..x + size).map(move |x1| grid[y1][x1]))
                    .sum::<isize>();
                (x, y, p)
            })
        })
        .max_by_key(|&(_, _, p)| p)
        .unwrap()
}

fn grid(sn: usize) -> Vec<Vec<isize>> {
    (0..300)
        .map(|y| {
            (0..300)
                .map(|x| (((((x + 11) * (y + 1) + sn as isize) * (x + 11)) / 100) % 10) - 5)
                .collect()
        })
        .collect()
}
