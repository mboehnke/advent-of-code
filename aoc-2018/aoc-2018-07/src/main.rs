use aoc::{nom::separated_triple, Aoc, Solution};
use apply::Also;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<String, isize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 7;
    const TITLE: &'static str = "The Sum of Its Parts";

    type Data = (Vec<(char, char)>, usize, u8);

    fn part1((mut instructions, _, _): Self::Data) -> String {
        let mut steps = instructions
            .iter()
            .flat_map(|(a, b)| vec![a, b])
            .copied()
            .unique()
            .collect_vec();
        let mut order = String::new();
        while !steps.is_empty() {
            let next_step: char = **steps
                .iter()
                .filter(|&s| instructions.iter().all(|(_, st)| *st != *s))
                .collect_vec()
                .also(|ns| ns.sort_unstable())
                .first()
                .unwrap();
            order.push(next_step);
            steps.retain(|s| *s != next_step);
            instructions = instructions
                .into_iter()
                .filter(|&(a, _)| a != next_step)
                .collect();
        }
        order
    }

    fn part2((mut instructions, workers, duration): Self::Data) -> isize {
        let mut steps = instructions
            .iter()
            .flat_map(|(a, b)| vec![a, b])
            .copied()
            .unique()
            .collect_vec();
        let mut workers = vec![(0, None); workers];
        let mut time = 0;
        while !steps.is_empty() {
            let completed_steps =
                workers
                    .iter_mut()
                    .filter(|w| w.1.is_some())
                    .fold(Vec::new(), |mut c, w| {
                        w.0 -= 1;
                        if w.0 == 0 {
                            c.push(w.1.unwrap());
                            w.1 = None;
                        }
                        c
                    });
            steps.retain(|s| !completed_steps.contains(s));
            instructions.retain(|(s, _)| !completed_steps.contains(s));
            let available_workers = workers
                .iter()
                .enumerate()
                .filter(|(_, (_, s))| s.is_none())
                .map(|(i, _)| i)
                .collect_vec();
            if !available_workers.is_empty() {
                let mut available_steps = steps
                    .iter()
                    .filter(|&s| instructions.iter().all(|(_, st)| *st != *s))
                    .filter(|&s| workers.iter().all(|sw| sw.1 != Some(*s)))
                    .copied()
                    .collect_vec();
                available_steps.sort_unstable();
                available_steps.reverse();
                for i in available_workers {
                    if let Some(step) = available_steps.pop() {
                        workers[i].0 = step as u8 - b'A' + 1 + duration;
                        workers[i].1 = Some(step);
                    }
                }
            }
            time += 1;
        }
        time - 1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag,
            character::complete::{anychar, newline, u8},
            multi::separated_list1,
            sequence::{delimited, separated_pair},
            usize,
        };

        let instruction = delimited(
            tag("Step "),
            separated_pair(anychar, tag(" must be finished before step "), anychar),
            tag(" can begin."),
        );
        let instruction_list = separated_list1(newline, instruction);

        let (s, (workers, duration, instructions)) =
            separated_triple(usize, newline, u8, newline, instruction_list)(s)?;
        Ok((s, (instructions, workers, duration)))
    }
}
