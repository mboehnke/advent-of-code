use aoc::{Aoc, Solution};
use hashbrown::{HashMap, HashSet};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 6;
    const TITLE: &'static str = "Chronal Coordinates";

    type Data = Vec<(usize, usize)>;

    fn part1(coordinates: Self::Data) -> usize {
        let grid = grid(&coordinates);
        *grid
            .into_iter()
            .flatten()
            .flatten()
            .fold(HashMap::new(), |mut counts: HashMap<usize, usize>, area| {
                *counts.entry(area).or_default() += 1;
                counts
            })
            .values()
            .max()
            .unwrap()
    }

    fn part2(coordinates: Self::Data) -> usize {
        let x = coordinates.iter().map(|&(x, _)| x).max().unwrap() as isize / 2;
        let y = coordinates.iter().map(|&(_, y)| y).max().unwrap() as isize / 2;
        let mut n = 0;
        let mut region = HashSet::new();
        let mut l = 0;
        loop {
            let new = (x - n..=x + n)
                .map(|x| (x, y + n))
                .chain((x - n..=x + n).map(|x| (x, y - n)))
                .chain((y - n..=y + n).map(|y| (x + n, y)))
                .chain((y - n..=y + n).map(|y| (x - n, y)))
                .filter(|&(x, y)| total_distance(x, y, &coordinates) < 10000);
            region.extend(new);
            if l == region.len() {
                break;
            }
            l = region.len();
            n += 1;
        }
        region.len()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            bytes::complete::tag, character::complete::newline, multi::separated_list1,
            sequence::separated_pair, usize,
        };

        let coords = separated_pair(usize, tag(", "), usize);
        separated_list1(newline, coords)(s)
    }
}

fn grid(coordinates: &[(usize, usize)]) -> Vec<Vec<Option<usize>>> {
    let xmax = coordinates.iter().map(|&(x, _)| x).max().unwrap();
    let ymax = coordinates.iter().map(|&(_, y)| y).max().unwrap();
    let mut grid = (0..ymax + 2)
        .map(|y| {
            (0..xmax + 2)
                .map(|x| closest(x, y, coordinates))
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    let border = (0..grid[0].len())
        .map(|x| (x, 0))
        .chain((0..grid[0].len()).map(|x| (x, grid.len() - 1)))
        .chain((1..grid.len() - 1).map(|y| (0, y)))
        .chain((1..grid.len() - 1).map(|y| (grid[0].len() - 1, y)))
        .collect::<Vec<_>>();
    for (x, y) in border {
        if grid[y][x].is_some() {
            grid = remove_area(x, y, grid);
        }
    }
    grid
}

fn remove_area(x: usize, y: usize, mut grid: Vec<Vec<Option<usize>>>) -> Vec<Vec<Option<usize>>> {
    if let Some(n) = grid[y][x] {
        for y in 0..grid.len() {
            for x in 0..grid[0].len() {
                if grid[y][x] == Some(n) {
                    grid[y][x] = None
                }
            }
        }
    }
    grid
}

fn closest(x: usize, y: usize, coordinates: &[(usize, usize)]) -> Option<usize> {
    let distances = coordinates.iter().fold(Vec::new(), |mut ds, &(nx, ny)| {
        let dx = (x as isize - (nx + 1) as isize).unsigned_abs();
        let dy = (y as isize - (ny + 1) as isize).unsigned_abs();
        ds.push(dx + dy);
        ds
    });
    let min_d = *distances.iter().min().unwrap();
    if distances.iter().filter(|&d| *d == min_d).count() > 1 {
        None
    } else {
        distances
            .iter()
            .enumerate()
            .find(|&(_, d)| *d == min_d)
            .map(|(i, _)| i)
    }
}

fn total_distance(x: isize, y: isize, coordinates: &[(usize, usize)]) -> usize {
    coordinates
        .iter()
        .map(|&(xn, yn)| (xn as isize - x).unsigned_abs() + (yn as isize - y).unsigned_abs())
        .sum::<usize>()
}
