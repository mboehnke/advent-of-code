use aoc::{Aoc, Solution};
use apply::Apply as _;
use itertools::Itertools as _;

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 2;
    const TITLE: &'static str = "Inventory Management System";

    type Data = Vec<String>;

    fn part1(ids: Self::Data) -> usize {
        (2..=3)
            .map(|n| ids.iter().filter(|id| n_of_any_letter(id, n)).count())
            .product()
    }

    fn part2(ids: Self::Data) -> String {
        ids.iter().permutations(2).find_map(compare).unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::string_list(s)
    }
}

fn n_of_any_letter(id: &str, n: u8) -> bool {
    id.chars()
        .fold([0; 26], |mut chars, c| {
            chars[(c as u8 - b'a') as usize] += 1;
            chars
        })
        .iter()
        .any(|&x| x == n)
}

fn compare(ids: Vec<&String>) -> Option<String> {
    ids[0]
        .chars()
        .zip(ids[1].chars())
        .filter(|&(a, b)| a == b)
        .map(|(a, _)| a)
        .collect::<String>()
        .apply(Some)
        .filter(|id| ids[0].len() == id.len() + 1)
}
