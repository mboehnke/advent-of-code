use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Pots {
    pots: Vec<i64>,
    rules: Vec<u8>,
}

impl Solution<i64, i64> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 12;
    const TITLE: &'static str = "Subterranean Sustainability";

    type Data = Pots;

    fn part1(mut pots: Self::Data) -> i64 {
        pots.gen(20)
    }

    fn part2(mut pots: Self::Data) -> i64 {
        pots.gen(50_000_000_000)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::pots(s)
    }
}

impl Iterator for Pots {
    type Item = Vec<i64>;

    fn next(&mut self) -> Option<Self::Item> {
        let min = self.pots.iter().min().unwrap() - 3;
        let max = self.pots.iter().max().unwrap() + 3;
        let previous = std::mem::take(&mut self.pots);
        for i in min..=max {
            let x = (i..i + 5)
                .map(|i| previous.binary_search(&i).is_ok())
                .map(u8::from)
                .fold(0, |acc, x| (acc << 1) + x);
            if self.rules.binary_search(&x).is_ok() {
                self.pots.push(i + 2);
            }
        }
        Some(previous)
    }
}

impl Pots {
    fn gen(&mut self, n: usize) -> i64 {
        let mut p_state = self.next().unwrap();
        for gen in 1..=n {
            let n_state = self.next().unwrap();
            let p_sum: i64 = p_state.iter().sum();
            let n_sum: i64 = n_state.iter().sum();
            if gen > 20
                && p_sum - (p_state[0] * p_state.len() as i64)
                    == n_sum - (n_state[0] * n_state.len() as i64)
            {
                return n_sum + (n_sum - p_sum) * (n - gen) as i64;
            }
            p_state = n_state;
        }
        p_state.iter().sum()
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline},
        combinator::map,
        multi::{many1, separated_list1},
        sequence::{preceded, separated_pair, tuple},
    };

    use crate::Pots;

    pub fn pots(s: &str) -> aoc::nom::IResult<&str, Pots> {
        let pots_rules = separated_pair(pots_p, tuple((newline, newline)), rules);
        map(pots_rules, |(pots, rules)| Pots { pots, rules })(s)
    }

    fn rules(s: &str) -> aoc::nom::IResult<&str, Vec<u8>> {
        let rule = separated_pair(bin_num, tag(" => "), bin_digit);
        let rule_list = separated_list1(newline, rule);
        map(rule_list, |r| {
            let mut rules: Vec<u8> = r
                .into_iter()
                .flat_map(|(a, b)| {
                    let a = a
                        .into_iter()
                        .map(|c| u8::from(c == '#'))
                        .fold(0, |acc, x| (acc << 1) + x);
                    Some(a).filter(|_| b == '#')
                })
                .collect();
            rules.sort_unstable();
            rules
        })(s)
    }

    fn pots_p(s: &str) -> aoc::nom::IResult<&str, Vec<i64>> {
        let state = preceded(tag("initial state: "), bin_num);
        map(state, |p| {
            p.into_iter()
                .enumerate()
                .flat_map(|(i, c)| if c == '#' { Some(i as i64) } else { None })
                .collect()
        })(s)
    }

    fn bin_num(s: &str) -> aoc::nom::IResult<&str, Vec<char>> {
        many1(bin_digit)(s)
    }

    fn bin_digit(s: &str) -> aoc::nom::IResult<&str, char> {
        alt((char('#'), char('.')))(s)
    }
}
