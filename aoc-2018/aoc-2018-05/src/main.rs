use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 5;
    const TITLE: &'static str = "Alchemical Reduction";

    type Data = Vec<u8>;

    fn part1(polymer: Self::Data) -> usize {
        react(polymer).len()
    }

    fn part2(polymer: Self::Data) -> usize {
        let polymer = react(polymer);
        (b'A'..=b'Z')
            .map(|b| (b, b | 0b00100000))
            .map(|(a, b)| {
                polymer
                    .iter()
                    .copied()
                    .filter(|&c| c != a && c != b)
                    .collect::<Vec<u8>>()
            })
            .map(react)
            .map(|s| s.len())
            .min()
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{character::complete::alpha1, combinator::map};

        map(map(alpha1, str::as_bytes), Vec::from)(s)
    }
}

fn react(mut polymer: Vec<u8>) -> Vec<u8> {
    let mut n = 0;
    for i in 0..polymer.len() {
        polymer[n] = polymer[i];
        n += 1;
        while n > 1 && do_react(polymer[n - 2], polymer[n - 1]) {
            n -= 2;
        }
    }
    polymer.truncate(n);
    polymer
}

fn do_react(a: u8, b: u8) -> bool {
    (a & 0b11011111) == (b & 0b11011111) && (a & 0b00100000) != (b & 0b00100000)
}
