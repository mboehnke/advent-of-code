use std::str::FromStr;

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone, PartialEq, Eq)]
pub struct FixedPoint(i8, i8, i8, i8);

impl Solution<usize, String> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 25;
    const TITLE: &'static str = "Four-Dimensional Adventure";

    type Data = Vec<FixedPoint>;

    fn part1(mut fixed_points: Self::Data) -> usize {
        let mut constellations: Vec<Vec<FixedPoint>> = Vec::new();
        while let Some(point) = fixed_points.pop() {
            constellations.push(vec![point]);
            let mut added = true;
            while added {
                added = false;
                for constellation in &mut constellations {
                    let add: Vec<FixedPoint> = fixed_points
                        .iter()
                        .filter(|&p| p.in_constellation(constellation))
                        .cloned()
                        .collect();
                    if !add.is_empty() {
                        fixed_points.retain(|p| !add.contains(p));
                        for p in add {
                            constellation.push(p);
                        }
                        added = true;
                        break;
                    }
                }
            }
        }
        constellations.len()
    }

    fn part2(_: Self::Data) -> String {
        String::new()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", s.lines().map(str::parse).map(Result::unwrap).collect()))
    }
}

impl FromStr for FixedPoint {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut cs = s.split(',');
        let ca = cs.next().ok_or("invalid input")?.parse()?;
        let cb = cs.next().ok_or("invalid input")?.parse()?;
        let cc = cs.next().ok_or("invalid input")?.parse()?;
        let cd = cs.next().ok_or("invalid input")?.parse()?;
        Ok(FixedPoint(ca, cb, cc, cd))
    }
}

impl FixedPoint {
    fn distance(&self, other: &Self) -> i8 {
        (self.0 - other.0).abs()
            + (self.1 - other.1).abs()
            + (self.2 - other.2).abs()
            + (self.3 - other.3).abs()
    }

    fn in_constellation(&self, constellation: &[FixedPoint]) -> bool {
        constellation.iter().any(|point| point.distance(self) <= 3)
    }
}
