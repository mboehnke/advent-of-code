use std::{ops::RangeInclusive, usize};

use aoc::{Aoc, Solution};
use hashbrown::{HashMap, HashSet};

use Tile::*;

const SPRING: Pos = (500, 0);

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone, Debug)]
pub enum Vein {
    Horizontal(usize, RangeInclusive<usize>),
    Vertical(usize, RangeInclusive<usize>),
}

struct Map {
    tiles: HashMap<Pos, Tile>,
    top: usize,
    bottom: usize,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Tile {
    Clay,
    Water,
    FlowingWater,
}

type Pos = (usize, usize);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 17;
    const TITLE: &'static str = "Reservoir Research";

    type Data = Vec<Vein>;

    fn part1(veins: Self::Data) -> usize {
        Map::from(veins).fill(SPRING).total_water()
    }

    fn part2(veins: Self::Data) -> usize {
        Map::from(veins).fill(SPRING).water_at_rest()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::vein_list(s)
    }
}

impl Map {
    fn from(veins: Vec<Vein>) -> Self {
        let clay_tiles = veins
            .into_iter()
            .flat_map(Vein::tiles)
            .collect::<HashSet<_>>();
        let (min_y, max_y) = clay_tiles
            .iter()
            .fold((usize::MAX, 0), |(min_y, max_y), &(_, y)| {
                (min_y.min(y), max_y.max(y))
            });
        Map {
            tiles: clay_tiles.into_iter().map(|c| (c, Clay)).collect(),
            bottom: max_y,
            top: min_y,
        }
    }

    fn fill(mut self, (x, y): Pos) -> Self {
        if y < self.top {
            // out of bounds
            let y = self.top;
            self.fill((x, y))
        } else if y > self.bottom
            || (self.tiles.get(&(x, y)) == Some(&FlowingWater)
                && self.tiles.get(&(x, y + 1)) == Some(&FlowingWater))
            || matches!(self.tiles.get(&(x, y)), Some(&Clay | &Water))
        {
            // out of bounds OR we've been here before OR we hit standing water or clay
            self
        } else if !matches!(self.tiles.get(&(x, y + 1)), Some(&Clay | &Water)) {
            // next row is neither clay nor standing water
            self.tiles.insert((x, y), FlowingWater);
            self.fill((x, y + 1))
        } else {
            // we can't go down anymore so we fill the row and go from there
            let (tiles, next) = self.fill_row((x, y));
            self.tiles.extend(tiles);
            if next.is_empty() {
                // clay on both sides -> go up
                self.fill((x, y - 1))
            } else {
                for pos in next {
                    self = self.fill(pos);
                }
                self
            }
        }
    }

    fn fill_row(&self, (x, y): Pos) -> (Vec<(Pos, Tile)>, Vec<Pos>) {
        let mut row = Vec::new();
        let mut next = Vec::new();

        for x1 in (0..x).rev() {
            if matches!(self.tiles.get(&(x1, y)), Some(&Clay)) {
                break;
            }
            row.push((x1, y));
            if matches!(self.tiles.get(&(x1, y + 1)), None | Some(&FlowingWater)) {
                next.push((x1, y));
                break;
            }
        }
        for x1 in x.. {
            if matches!(self.tiles.get(&(x1, y)), Some(&Clay)) {
                break;
            }
            row.push((x1, y));
            if matches!(self.tiles.get(&(x1, y + 1)), None | Some(&FlowingWater)) {
                next.push((x1, y));
                break;
            }
        }

        let tile = if next.is_empty() { Water } else { FlowingWater };
        let tiles = row.into_iter().map(|pos| (pos, tile)).collect();
        (tiles, next)
    }

    fn total_water(&self) -> usize {
        self.tiles
            .iter()
            .filter(|&(_, tile)| tile == &Water || tile == &FlowingWater)
            .count()
    }

    fn water_at_rest(&self) -> usize {
        self.tiles
            .iter()
            .filter(|&(_, tile)| tile == &Water)
            .count()
    }
}

impl Vein {
    fn tiles(self) -> Vec<Pos> {
        match self {
            Vein::Horizontal(y, xs) => xs.map(|x| (x, y)).collect(),
            Vein::Vertical(x, ys) => ys.map(|y| (x, y)).collect(),
        }
    }
}

mod parser {
    use std::ops::RangeInclusive;

    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline, u64},
        combinator::map_res,
        multi::separated_list0,
        sequence::{separated_pair, tuple},
        IResult,
    };

    use crate::Vein;

    pub fn vein_list(s: &str) -> IResult<&str, Vec<Vein>> {
        separated_list0(newline, vein)(s)
    }

    fn vein(s: &str) -> IResult<&str, Vein> {
        let h_vein = vein_generic('y', 'x', Vein::Horizontal);
        let v_vein = vein_generic('x', 'y', Vein::Vertical);
        alt((h_vein, v_vein))(s)
    }

    fn vein_generic(
        a: char,
        b: char,
        constructor: impl Fn(usize, RangeInclusive<usize>) -> Vein,
    ) -> impl FnMut(&str) -> IResult<&str, Vein> {
        move |s: &str| {
            let (s, _) = tuple((char(a), char('=')))(s)?;
            let (s, a) = usize(s)?;
            let (s, _) = tuple((tag(", "), char(b), char('=')))(s)?;
            let (s, (b1, b2)) = separated_pair(usize, tag(".."), usize)(s)?;
            Ok((s, constructor(a, b1..=b2)))
        }
    }

    fn usize(s: &str) -> IResult<&str, usize> {
        map_res(u64, usize::try_from)(s)
    }
}
