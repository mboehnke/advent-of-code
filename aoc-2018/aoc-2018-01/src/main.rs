use hashbrown::HashSet;

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

impl Solution<isize, isize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 1;
    const TITLE: &'static str = "Chronal Calibration";

    type Data = Vec<isize>;

    fn part1(changes: Self::Data) -> isize {
        changes.into_iter().sum()
    }

    fn part2(changes: Self::Data) -> isize {
        let mut p = HashSet::new();
        p.insert(0);
        changes
            .into_iter()
            .cycle()
            .scan(0, |f, c| {
                *f += c;
                Some(*f)
            })
            .find(|&f| !p.insert(f))
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::isize_list(s)
    }
}
