use std::collections::BTreeMap;

use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Pos(i64, i64, i64);

impl Solution<usize, i64> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 23;
    const TITLE: &'static str = "Experimental Emergency Teleportation";

    type Data = Vec<(Pos, i64)>;

    fn part1(bots: Self::Data) -> usize {
        let (pos, range) = &bots.iter().max_by_key(|(_, range)| range).unwrap();
        bots.iter()
            .filter(|(pos2, _)| pos.distance(pos2) <= *range)
            .count()
    }

    fn part2(bots: Self::Data) -> i64 {
        let run = bots
            .into_iter()
            .fold(BTreeMap::new(), |mut dist, (pos, range)| {
                let d = pos.0 + pos.1 + pos.2;
                *dist.entry(d - range).or_insert(0) += 1;
                *dist.entry(d + range + 1).or_insert(0) -= 1;
                dist
            })
            .into_iter()
            .scan(0, |s, (d, x)| {
                *s += x;
                Some((d, *s))
            })
            .collect::<Vec<_>>();
        let max = run.iter().map(|&(_, n)| n).max().unwrap();
        let intervals = run
            .iter()
            .zip(run.iter().skip(1))
            .filter(|(&(_, n), _)| n == max)
            .map(|(&(a, _), &(b, _))| (a, b - 1))
            .collect::<Vec<_>>();
        if intervals.iter().any(|&(a, b)| a <= 0 && b >= 0) {
            0
        } else {
            intervals
                .iter()
                .map(|&(a, b)| if b < 0 { -b } else { a })
                .min()
                .unwrap()
        }
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.lines()
                .map(|line| {
                    let l = line
                        .chars()
                        .filter(|&c| c == ',' || c == '-' || c.is_ascii_digit())
                        .collect::<String>();
                    let nums: Vec<i64> = l.split(',').map(str::parse).map(Result::unwrap).collect();
                    (Pos(nums[0], nums[1], nums[2]), nums[3])
                })
                .collect(),
        ))
    }
}

impl Pos {
    fn distance(&self, pos2: &Pos) -> i64 {
        (self.0 - pos2.0).abs() + (self.1 - pos2.1).abs() + (self.2 - pos2.2).abs()
    }
}
