use aoc::{Aoc, Solution};
use hashbrown::HashSet;
use wrist_mounted_device::{Computer, Instruction};

#[derive(Aoc)]
pub struct Sol;

#[derive(Clone)]
pub struct InvalidValues(Computer);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 21;
    const TITLE: &'static str = "Chronal Conversion";

    type Data = InvalidValues;

    fn part1(mut vals: Self::Data) -> usize {
        vals.next().unwrap()
    }

    fn part2(mut vals: Self::Data) -> usize {
        let mut set = HashSet::new();
        let mut last = 0;
        loop {
            let val = vals.next().unwrap();
            if set.insert(val) {
                last = val
            } else {
                break;
            }
        }
        last
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok(("", InvalidValues(s.parse().unwrap())))
    }
}

impl Iterator for InvalidValues {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        while self.0.registers[self.0.ip] != 28 {
            self.0.step();
        }
        let val = if let Instruction::EQRR(r, _, _) = self.0.instructions[28] {
            Some(self.0.registers[r])
        } else {
            None
        };
        self.0.step();
        val
    }
}
