use aoc::{Aoc, Solution};

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub struct Node {
    children: Vec<Node>,
    metadata: Vec<u8>,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 8;
    const TITLE: &'static str = "Memory Maneuver";

    type Data = Node;

    fn part1(root: Self::Data) -> usize {
        root.m_sum()
    }

    fn part2(root: Self::Data) -> usize {
        root.value()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            character::complete::{space1, u8},
            combinator::map,
            multi::separated_list1,
        };

        map(separated_list1(space1, u8), |vs| Node::from(&vs).0)(s)
    }
}

impl Node {
    fn from(data: &[u8]) -> (Node, usize) {
        let num_children = data[0] as usize;
        let len_metadata = data[1] as usize;
        let mut len_children = 0;
        let mut children = Vec::new();
        for _ in 0..num_children {
            let (n, l) = Self::from(&data[2 + len_children..]);
            len_children += l;
            children.push(n);
        }
        let metadata = data[2 + len_children..2 + len_children + len_metadata].to_vec();
        (Node { children, metadata }, len_children + len_metadata + 2)
    }
    fn m_sum(&self) -> usize {
        self.children
            .iter()
            .map(Self::m_sum)
            .chain(self.metadata.iter().map(|&x| x as usize))
            .sum::<usize>()
    }
    fn value(&self) -> usize {
        if self.children.is_empty() {
            self.metadata.iter().map(|&x| x as usize).sum::<usize>()
        } else {
            self.metadata
                .iter()
                .filter_map(|&i| self.children.get(i as usize - 1))
                .map(Self::value)
                .sum::<usize>()
        }
    }
}
