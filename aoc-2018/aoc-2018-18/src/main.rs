use std::{convert::TryFrom, mem::swap};

use aoc::{Aoc, Solution};
use apply::Apply as _;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Acre {
    Open,
    Wooded,
    Lumberyard,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Area(Vec<Vec<Acre>>);

#[derive(Debug, Clone)]
pub struct AreaIterator(Area);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 18;
    const TITLE: &'static str = "Settlers of The North Pole";

    type Data = Area;

    fn part1(area: Self::Data) -> usize {
        area.into_iter().nth(10).unwrap().resource_value()
    }

    fn part2(area: Self::Data) -> usize {
        area.into_iter()
            .nth(1_000_000_000)
            .unwrap()
            .resource_value()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.lines()
                .map(|l| l.chars().map(Acre::try_from).map(Result::unwrap).collect())
                .collect::<Vec<Vec<Acre>>>()
                .apply(Area),
        ))
    }
}

impl TryFrom<char> for Acre {
    type Error = &'static str;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Acre::Open,
            '|' => Acre::Wooded,
            '#' => Acre::Lumberyard,
            _ => return Err("invalid input"),
        }
        .apply(Ok)
    }
}

impl IntoIterator for Area {
    type Item = Area;

    type IntoIter = AreaIterator;

    fn into_iter(self) -> Self::IntoIter {
        AreaIterator(self)
    }
}

impl Area {
    pub fn resource_value(&self) -> usize {
        let (w, l) = self.0.iter().flatten().fold((0, 0), |(w, l), a| match a {
            Acre::Open => (w, l),
            Acre::Wooded => (w + 1, l),
            Acre::Lumberyard => (w, l + 1),
        });
        w * l
    }
}

impl Iterator for AreaIterator {
    type Item = Area;

    fn next(&mut self) -> Option<Self::Item> {
        let current = &mut self.0 .0;
        let height = current.len();
        let width = current[0].len();
        let adjacent = |x: usize, y: usize| {
            adjacent::adjacent_d(x, y, width - 1, height - 1)
                .map(|(x, y)| current[y][x])
                .fold((0, 0), |(w, l), a| match a {
                    Acre::Open => (w, l),
                    Acre::Wooded => (w + 1, l),
                    Acre::Lumberyard => (w, l + 1),
                })
        };
        let mut next = vec![vec![Acre::Open; width]; height];
        for x in 0..width {
            for y in 0..height {
                let (w, l) = adjacent(x, y);
                next[y][x] = match current[y][x] {
                    Acre::Open if w >= 3 => Acre::Wooded,
                    Acre::Wooded if l >= 3 => Acre::Lumberyard,
                    Acre::Lumberyard if l >= 1 && w >= 1 => Acre::Lumberyard,
                    Acre::Lumberyard => Acre::Open,
                    a => a,
                }
            }
        }
        swap(current, &mut next);
        Some(Area(next))
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        let mut areas: Vec<Area> = Vec::new();
        for _ in 0..=n {
            let area = self.next().unwrap();
            if let Some(cycle_start) = areas
                .iter()
                .enumerate()
                .find(|(_, a)| area == **a)
                .map(|(i, _)| i)
            {
                let cycle_len = areas.len() - cycle_start;
                let offset = (1_000_000_000 - cycle_start) % cycle_len;
                return Some(areas[cycle_start + offset].clone());
            }
            areas.push(area);
        }
        areas.last().cloned()
    }
}

impl std::fmt::Display for Acre {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            Acre::Open => '.',
            Acre::Wooded => '|',
            Acre::Lumberyard => '#',
        };
        write!(f, "{}", c)
    }
}

impl std::fmt::Display for Area {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let lines = self
            .0
            .iter()
            .map(|l| l.iter().map(|c| c.to_string()).collect::<String>())
            .collect::<Vec<_>>()
            .join("\n");
        write!(f, "{}", lines)
    }
}
