use std::{cmp::Reverse, collections::HashSet};

use aoc::{Aoc, Solution};
use apply::Also;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
struct Fight(Vec<Group>);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Group {
    army: Army,
    units: u32,
    hit_points: u32,
    attack_damage: u32,
    attack_type: AttackType,
    initiative: u32,
    weaknesses: Vec<AttackType>,
    immunities: Vec<AttackType>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AttackType {
    Fire,
    Radiation,
    Slashing,
    Bludgeoning,
    Cold,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Army {
    ImmuneSystem,
    Infection,
}

impl Solution<u32, u32> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 24;
    const TITLE: &'static str = "Immune System Simulator 20XX";

    type Data = Vec<Group>;

    fn part1(groups: Self::Data) -> u32 {
        let (immune, infection) = Fight(groups).last().unwrap();
        immune.max(infection)
    }

    fn part2(groups: Self::Data) -> u32 {
        let boosted_groups = |boost| {
            groups
                .iter()
                .cloned()
                .map(|mut g| {
                    if g.army == Army::ImmuneSystem {
                        g.attack_damage += boost;
                    };
                    g
                })
                .collect::<Vec<_>>()
        };

        (0..)
            .map(boosted_groups)
            .map(Fight)
            .flat_map(Iterator::last)
            .find(|&(_, infection)| infection == 0)
            .map(|(immune, _)| immune)
            .unwrap()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::groups(s)
    }
}

impl Group {
    fn effective_power(&self) -> u32 {
        self.units * self.attack_damage
    }

    fn calc_damage(&self, dmg: u32, at: AttackType) -> u32 {
        if self.immunities.contains(&at) {
            0
        } else if self.weaknesses.contains(&at) {
            dmg * 2
        } else {
            dmg
        }
    }

    fn take_damage(&mut self, dmg: u32, at: AttackType) -> u32 {
        let damage = self.calc_damage(dmg, at);
        let kills = (damage / self.hit_points).min(self.units);
        self.units -= kills;
        kills
    }
}

impl Fight {
    fn units(&self) -> (u32, u32) {
        self.0.iter().fold((0, 0), |(immune, infection), group| {
            match (group.army, group.units) {
                (Army::ImmuneSystem, u) => (immune + u, infection),
                (Army::Infection, u) => (immune, infection + u),
            }
        })
    }
}

impl Iterator for Fight {
    type Item = (u32, u32);

    fn next(&mut self) -> Option<Self::Item> {
        (0..self.0.len())
            .filter(|&a| self.0[a].units > 0)
            .collect::<Vec<_>>()
            .also(|order| {
                order.sort_by_key(|&a| {
                    let power = self.0[a].effective_power();
                    let initiative = self.0[a].initiative;
                    (Reverse(power), Reverse(initiative))
                })
            })
            .into_iter()
            .fold(
                (HashSet::new(), Vec::new()),
                |(mut selected, mut attacks), a| {
                    let targets = (0..self.0.len())
                        .filter(|&d| self.0[d].units > 0)
                        .filter(|&d| self.0[d].army != self.0[a].army)
                        .filter(|d| !selected.contains(d))
                        .map(|d| {
                            let dmg = self.0[a].effective_power();
                            let at = self.0[a].attack_type;
                            (d, self.0[d].calc_damage(dmg, at))
                        })
                        .filter(|&(_, dmg)| dmg > 0)
                        .collect::<Vec<(usize, u32)>>()
                        .also(|targets| {
                            targets.sort_by_key(|&(d, damage)| {
                                let power = self.0[d].effective_power();
                                let initiative = self.0[d].initiative;
                                (Reverse(damage), Reverse(power), Reverse(initiative))
                            })
                        });
                    if let Some(&(d, _)) = targets.first() {
                        selected.insert(d);
                        attacks.push((a, d));
                    }
                    (selected, attacks)
                },
            )
            .1
            .also(|attacks| attacks.sort_by_key(|&(a, _)| Reverse(self.0[a].initiative)))
            .into_iter()
            .fold(false, |any_kills, (a, d)| {
                let dmg = self.0[a].effective_power();
                let at = self.0[a].attack_type;
                let kills = self.0[d].take_damage(dmg, at);
                any_kills || kills > 0
            })
            .then(|| self.units())
    }
}

mod parser {
    use aoc::nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{char, newline, space1, u32},
        combinator::{map, opt, value},
        multi::{many0, separated_list1},
        sequence::{delimited, preceded, separated_pair, terminated, tuple},
        IResult,
    };

    use crate::{Army, AttackType, Group};

    pub fn groups(s: &str) -> IResult<&str, Vec<Group>> {
        let (s, (mut g1, mut g2)) = separated_pair(group_list, many0(newline), group_list)(s)?;
        g1.append(&mut g2);
        Ok((s, g1))
    }

    fn group_list(s: &str) -> IResult<&str, Vec<Group>> {
        let (s, army) = army(s)?;
        separated_list1(newline, group(army))(s)
    }

    fn army(s: &str) -> IResult<&str, Army> {
        let imm = value(Army::ImmuneSystem, tag("Immune System:\n"));
        let inf = value(Army::Infection, tag("Infection:\n"));
        alt((imm, inf))(s)
    }

    fn group(army: Army) -> impl FnMut(&str) -> IResult<&str, Group> {
        move |s: &str| {
            let (s, units) = u32(s)?;
            let (s, hit_points) = preceded(tag(" units each with "), u32)(s)?;
            let (s, (weaknesses, immunities)) = preceded(
                tag(" hit points "),
                map(opt(weaknesses_and_immunities), Option::unwrap_or_default),
            )(s)?;
            let (s, attack_damage) = preceded(tag("with an attack that does "), u32)(s)?;
            let (s, attack_type) = preceded(space1, attack_type)(s)?;
            let (s, initiative) = preceded(tag(" damage at initiative "), u32)(s)?;
            let group = Group {
                army,
                units,
                hit_points,
                attack_damage,
                attack_type,
                initiative,
                weaknesses,
                immunities,
            };
            Ok((s, group))
        }
    }
    fn weaknesses_and_immunities(s: &str) -> IResult<&str, (Vec<AttackType>, Vec<AttackType>)> {
        let (s, (i1, w, i2)) = delimited(
            char('('),
            tuple((
                terminated(opt(immunities), opt(tag("; "))),
                terminated(opt(weaknesses), opt(tag("; "))),
                opt(immunities),
            )),
            tag(") "),
        )(s)?;
        let i = i1.or(i2);
        Ok((s, (w.unwrap_or_default(), i.unwrap_or_default())))
    }
    fn weaknesses(s: &str) -> IResult<&str, Vec<AttackType>> {
        preceded(tag("weak to "), separated_list1(tag(", "), attack_type))(s)
    }
    fn immunities(s: &str) -> IResult<&str, Vec<AttackType>> {
        preceded(tag("immune to "), separated_list1(tag(", "), attack_type))(s)
    }
    fn attack_type(s: &str) -> IResult<&str, AttackType> {
        let cold = value(AttackType::Cold, tag("cold"));
        let fire = value(AttackType::Fire, tag("fire"));
        let radiation = value(AttackType::Radiation, tag("radiation"));
        let slashing = value(AttackType::Slashing, tag("slashing"));
        let bludgeoning = value(AttackType::Bludgeoning, tag("bludgeoning"));
        alt((cold, fire, radiation, slashing, bludgeoning))(s)
    }
}
