use aoc::{Aoc, Solution};
use apply::Apply as _;
use chrono::{NaiveDateTime, Timelike};
use hashbrown::HashMap;
use std::str::FromStr;

#[derive(Aoc)]
pub struct Sol;

#[derive(Debug, Clone)]
pub enum Record {
    BeginsShift(usize),
    FallsAsleep,
    WakesUp,
}

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 4;
    const TITLE: &'static str = "Repose Record";

    type Data = Vec<(NaiveDateTime, Record)>;

    fn part1(records: Self::Data) -> usize {
        best_minute(records, |m| m.iter().sum())
    }

    fn part2(records: Self::Data) -> usize {
        best_minute(records, |m| *m.iter().max().unwrap())
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        use aoc::nom::{
            branch::alt,
            bytes::complete::{tag, take},
            character::complete::newline,
            combinator::{map, map_res, value},
            multi::separated_list1,
            sequence::{delimited, tuple},
            usize,
        };

        let r_wakes = value(Record::WakesUp, tag("wakes up"));
        let r_sleeps = value(Record::FallsAsleep, tag("falls asleep"));
        let r_begin = map(
            delimited(tag("Guard #"), usize, tag(" begins shift")),
            Record::BeginsShift,
        );
        let record = alt((r_begin, r_sleeps, r_wakes));
        let time = map_res(take(19usize), |s| {
            NaiveDateTime::parse_from_str(s, "[%Y-%m-%d %H:%M] ")
        });
        separated_list1(newline, tuple((time, record)))(s)
    }
}

fn best_minute<F>(records: Vec<(NaiveDateTime, Record)>, f: F) -> usize
where
    F: Fn(&Vec<usize>) -> usize,
{
    sleep_times(records)
        .into_iter()
        .map(|(guard, times)| (guard, sleep_minutes(times)))
        .max_by_key(|(_, minutes)| f(minutes))
        .map(|(guard, minutes)| guard * mminute(&minutes))
        .unwrap()
}

fn sleep_times(
    mut records: Vec<(NaiveDateTime, Record)>,
) -> HashMap<usize, Vec<(NaiveDateTime, NaiveDateTime)>> {
    records.sort_unstable_by_key(|&(d, _)| d);

    let mut guard = None;
    let mut fallen_asleep = None;
    let mut times = HashMap::new();

    for (time, record) in records {
        match record {
            Record::BeginsShift(g) => {
                if let Some(gp) = guard {
                    if let Some(a) = fallen_asleep {
                        times.entry(gp).or_insert_with(Vec::new).push((a, time));
                    }
                }
                fallen_asleep = None;
                guard = Some(g);
            }
            Record::FallsAsleep => {
                fallen_asleep = Some(time);
            }
            Record::WakesUp => {
                if let Some(a) = fallen_asleep {
                    if let Some(g) = guard {
                        times.entry(g).or_insert_with(Vec::new).push((a, time));
                        fallen_asleep = None;
                    }
                }
            }
        }
    }
    times
}

fn sleep_minutes(times: Vec<(NaiveDateTime, NaiveDateTime)>) -> Vec<usize> {
    times
        .into_iter()
        .map(|(a, b)| (a.minute() as usize, b.minute() as usize))
        .fold(vec![0; 60], |mut minutes, (a, b)| {
            (a..b).for_each(|m| minutes[m] += 1);
            minutes
        })
}

fn mminute(minutes: &[usize]) -> usize {
    minutes
        .iter()
        .enumerate()
        .max_by_key(|&(_, n)| *n)
        .unwrap()
        .0
}

impl FromStr for Record {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with('f') {
            Record::FallsAsleep
        } else if s.starts_with('w') {
            Record::WakesUp
        } else {
            s.split_ascii_whitespace().nth(1).ok_or("parse error")?[1..]
                .parse::<usize>()?
                .apply(Record::BeginsShift)
        }
        .apply(Ok)
    }
}
