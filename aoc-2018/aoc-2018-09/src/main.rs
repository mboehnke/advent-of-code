use aoc::{
    nom::{bytes::complete::tag, sequence::separated_pair, usize},
    Aoc, Solution,
};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 9;
    const TITLE: &'static str = "Marble Mania";

    type Data = (usize, usize);

    fn part1((players, marbles): Self::Data) -> usize {
        highscore(players, marbles)
    }

    fn part2((players, marbles): Self::Data) -> usize {
        highscore(players, marbles * 100)
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        separated_pair(usize, tag(" players; last marble is worth "), usize)(s)
    }
}

fn highscore(players: usize, marbles: usize) -> usize {
    let mut circle = Vec::with_capacity(marbles - marbles / 23 + 1);
    circle.push((0, 0, 0));
    let mut i = 0;
    let mut scores = vec![0; players];
    for (player, marble) in (0..players).cycle().zip(1..=marbles) {
        if marble % 23 == 0 {
            scores[player] += marble;
            for _ in 0..7 {
                i = circle[i].1;
            }
            let (m, l, r) = circle[i];
            scores[player] += m;
            circle[l].2 = r;
            circle[r].1 = l;
            i = r;
        } else {
            let l = circle[i].2;
            let r = circle[l].2;
            i = circle.len();
            circle[l].2 = i;
            circle[r].1 = i;
            circle.push((marble, l, r));
        }
    }
    scores.into_iter().max().unwrap()
}
