use aoc::{Aoc, Solution};
use itertools::iproduct;

#[derive(Aoc)]
pub struct Sol;

type Claim = (usize, usize, usize, usize);

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 3;
    const TITLE: &'static str = "No Matter How You Slice It";

    type Data = Vec<Claim>;

    fn part1(claims: Self::Data) -> usize {
        let width = claims.iter().map(|&(_, _, x, _)| x).max().unwrap() + 1;
        let height = claims.iter().map(|&(_, _, _, y)| y).max().unwrap() + 1;
        claims
            .into_iter()
            .flat_map(|(minx, miny, maxx, maxy)| iproduct!(minx..=maxx, miny..=maxy))
            .fold(vec![vec![0; width]; height], |mut fabric, (x, y)| {
                fabric[y][x] += 1;
                fabric
            })
            .into_iter()
            .flat_map(|r| r.into_iter())
            .filter(|&n| n >= 2)
            .count()
    }

    fn part2(claims: Self::Data) -> usize {
        claims
            .iter()
            .enumerate()
            .find(|&(id, claim)| {
                !claims
                    .iter()
                    .enumerate()
                    .filter(|&(i, _)| i != id)
                    .any(|(_, claim2)| overlaps(claim, claim2))
            })
            .unwrap()
            .0
            + 1
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        parser::claim_list(s)
    }
}

fn overlaps(c1: &Claim, c2: &Claim) -> bool {
    !(c1.0 > c2.2 || c1.2 < c2.0 || c1.1 > c2.3 || c1.3 < c2.1)
}

mod parser {
    use aoc::nom::{
        bytes::complete::tag,
        character::complete::{char, digit1, newline},
        multi::separated_list0,
        sequence::{preceded, tuple},
        usize,
    };

    pub fn claim_list(s: &str) -> aoc::nom::IResult<&str, Vec<(usize, usize, usize, usize)>> {
        separated_list0(newline, claim)(s)
    }

    fn claim(s: &str) -> aoc::nom::IResult<&str, (usize, usize, usize, usize)> {
        let (s, x1) = preceded(tuple((char('#'), digit1, tag(" @ "))), usize)(s)?;
        let (s, y1) = preceded(char(','), usize)(s)?;
        let (s, x2) = preceded(tag(": "), usize)(s)?;
        let (s, y2) = preceded(char('x'), usize)(s)?;
        Ok((s, (x1, y1, x1 + x2 - 1, y1 + y2 - 1)))
    }
}
