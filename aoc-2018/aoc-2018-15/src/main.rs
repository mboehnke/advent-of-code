use std::collections::HashSet;

use aoc::{Aoc, Solution};
use itertools::{iproduct, Itertools};

#[derive(Aoc)]
pub struct Sol;

const ELF_HP: u32 = 200;
const GOBLIN_HP: u32 = 200;
const ELF_DMG: u32 = 3;
const GOBLIN_DMG: u32 = 3;

#[derive(Debug, Clone)]
pub struct Cave {
    map: Vec<Vec<Square>>,
    goblin_hp: u32,
    elf_hp: u32,
    goblin_dmg: u32,
    elf_dmg: u32,
    elves_killed: bool,
    fight_ongoing: bool,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Square {
    Empty,
    Wall,
    Goblin(u32),
    Elf(u32),
}

impl Solution<u32, u32> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 15;
    const TITLE: &'static str = "Beverage Bandits";

    type Data = Vec<Vec<Square>>;

    fn part1(map: Self::Data) -> u32 {
        Cave::from(map).outcome().0
    }

    fn part2(map: Self::Data) -> u32 {
        let cave = Cave::from(map);
        (4..=200)
            .map(|elf_dmg| cave.with_elf_dmg(elf_dmg).outcome())
            .find(|&(_, elves_killed)| !elves_killed)
            .unwrap()
            .0
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        Ok((
            "",
            s.lines()
                .map(|l| l.chars().map(Square::from).collect())
                .collect(),
        ))
    }
}

impl From<Vec<Vec<Square>>> for Cave {
    fn from(map: Vec<Vec<Square>>) -> Self {
        let (goblin_hp, elf_hp) =
            map.iter()
                .flatten()
                .fold((0, 0), |(g, e), square| match square {
                    Square::Goblin(hp) => (g + hp, e),
                    Square::Elf(hp) => (g, e + hp),
                    _ => (g, e),
                });
        Cave {
            map,
            goblin_hp,
            elf_hp,
            goblin_dmg: GOBLIN_DMG,
            elf_dmg: ELF_DMG,
            elves_killed: false,
            fight_ongoing: true,
        }
    }
}

impl Cave {
    fn with_elf_dmg(&self, elf_dmg: u32) -> Self {
        Cave {
            elf_dmg,
            ..self.clone()
        }
    }

    fn outcome(&mut self) -> (u32, bool) {
        let (i, hp) = self.enumerate().last().unwrap();
        (hp * i as u32, self.elves_killed)
    }

    fn units(&self) -> Vec<(usize, usize)> {
        iproduct!(0..self.map.len(), 0..self.map[0].len())
            .filter(|&(y, x)| self.map[y][x].is_unit())
            .map(|(y, x)| (x, y))
            .collect()
    }

    fn adjacent(&self, x: usize, y: usize) -> impl Iterator<Item = (usize, usize)> {
        adjacent::adjacent(x, y, self.map[0].len() - 1, self.map.len() - 1)
    }

    fn take_turn(&mut self, x: usize, y: usize) -> Option<(usize, usize)> {
        let (x, y) = self
            .take_move(x, y)
            .map(|(nx, ny)| {
                let a = self.map[y][x].clone();
                let b = self.map[ny][nx].clone();
                self.map[y][x] = b;
                self.map[ny][nx] = a;
                (nx, ny)
            })
            .unwrap_or((x, y));

        self.take_attack(x, y)
    }

    fn take_attack(&mut self, x: usize, y: usize) -> Option<(usize, usize)> {
        let iself = self.map[y][x].is_elf();
        let (target_x, target_y) = self
            .adjacent(x, y)
            .filter(|&(x, y)| {
                (iself && self.map[y][x].is_goblin()) || (!iself && self.map[y][x].is_elf())
            })
            .min_by_key(|&(x, y)| (self.map[y][x].hp(), y, x))?;

        let dmg = if iself { self.elf_dmg } else { self.goblin_dmg };
        let (dmg_taken, killed) = self.map[target_y][target_x].take_damage(dmg);
        if iself {
            self.goblin_hp -= dmg_taken
        } else {
            self.elf_hp -= dmg_taken;
            self.elves_killed = self.elves_killed || killed;
        }
        killed.then(|| {
            self.map[target_y][target_x] = Square::Empty;
            (target_x, target_y)
        })
    }

    fn take_move(&mut self, x: usize, y: usize) -> Option<(usize, usize)> {
        let is_elf = self.map[y][x].is_elf();
        let is_enemy = |x: usize, y: usize| {
            (is_elf && self.map[y][x].is_goblin()) || (!is_elf && self.map[y][x].is_elf())
        };

        if self.adjacent(x, y).any(|(x, y)| is_enemy(x, y)) {
            return None;
        }

        let valid_moves = self
            .adjacent(x, y)
            .filter(|&(x, y)| self.map[y][x].is_empty())
            .collect::<HashSet<_>>();

        if valid_moves.is_empty() {
            return None;
        }

        let valid_targets = iproduct!(0..self.map.len(), 0..self.map[0].len())
            .filter(|&(y, x)| is_enemy(x, y))
            .flat_map(|(y, x)| self.adjacent(x, y))
            .unique()
            .filter(|&(x, y)| self.map[y][x].is_empty())
            .collect();

        let (target_x, target_y) = self.closest(x, y, valid_targets)?;
        self.closest(target_x, target_y, valid_moves)
    }

    fn closest(
        &self,
        start_x: usize,
        start_y: usize,
        targets: HashSet<(usize, usize)>,
    ) -> Option<(usize, usize)> {
        let mut visited = HashSet::new();
        let mut boundary = vec![(start_x, start_y)];
        while !boundary.is_empty() {
            if let Some(&closest) = boundary
                .iter()
                .filter(|&pos| targets.contains(pos))
                .min_by_key(|&(x, y)| (y, x))
            {
                return Some(closest);
            }
            boundary = boundary
                .iter()
                .flat_map(|&(x, y)| self.adjacent(x, y))
                .unique()
                .filter(|&(x, y)| self.map[y][x].is_empty() && visited.insert((x, y)))
                .collect();
        }
        None
    }
}

impl Square {
    fn is_elf(&self) -> bool {
        matches!(self, Square::Elf(_))
    }

    fn is_goblin(&self) -> bool {
        matches!(self, Square::Goblin(_))
    }

    fn is_unit(&self) -> bool {
        matches!(self, Square::Elf(_) | Square::Goblin(_))
    }

    fn is_empty(&self) -> bool {
        matches!(self, Square::Empty)
    }

    fn hp(&self) -> u32 {
        match *self {
            Square::Goblin(hp) => hp,
            Square::Elf(hp) => hp,
            _ => 0,
        }
    }

    fn take_damage(&mut self, dmg: u32) -> (u32, bool) {
        let hp = match self {
            Square::Goblin(ref mut hp) => hp,
            Square::Elf(ref mut hp) => hp,
            _ => return (0, false),
        };
        let actual_dmg = dmg.min(*hp);
        *hp -= actual_dmg;
        (actual_dmg, *hp == 0)
    }
}

impl Iterator for Cave {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        self.fight_ongoing.then(|| {
            let mut killed = Vec::new();
            for (x, y) in self.units() {
                if !killed.contains(&(x, y)) {
                    self.fight_ongoing = self.goblin_hp > 0 && self.elf_hp > 0;
                    if let Some((xk, yk)) = self.take_turn(x, y) {
                        killed.push((xk, yk));
                    }
                }
            }
            self.goblin_hp + self.elf_hp
        })
    }
}

impl From<char> for Square {
    fn from(s: char) -> Self {
        match s {
            '#' => Square::Wall,
            '.' => Square::Empty,
            'E' => Square::Elf(ELF_HP),
            'G' => Square::Goblin(GOBLIN_HP),
            _ => panic!("parse error"),
        }
    }
}
