use aoc::{Aoc, Solution};
use wrist_mounted_device::{Computer, Instruction};

#[derive(Aoc)]
pub struct Sol;

impl Solution<usize, usize> for Sol {
    const YEAR: u32 = 2018;
    const DAY: u32 = 19;
    const TITLE: &'static str = "Go With The Flow";

    type Data = Computer;

    fn part1(mut device: Self::Data) -> usize {
        device.run();
        device.registers[0]
    }

    fn part2(mut device: Self::Data) -> usize {
        *device.instructions.last_mut().unwrap() = Instruction::SETI(999, 0, 3);
        device.registers[0] = 1;
        device.run();
        (1..=device.registers[1])
            .filter(|n| device.registers[1] % n == 0)
            .sum()
    }

    fn parser(s: &str) -> aoc::nom::IResult<&str, Self::Data> {
        aoc::nom::parse(s)
    }
}
